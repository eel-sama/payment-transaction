package com.smartstart.paymentclover;

import com.smartstart.paymentbase.Base;

public class Clover extends Base {
    @Override
    public void PreAuthorize() {

    }

    @Override
    public void PostAuthorize() {

    }

    @Override
    public void Void() {

    }

    @Override
    public void Refund() {

    }

    @Override
    public void CancelRequest() {

    }
}
