package com.smartstart.paymentbroadpos;

import com.smartstart.paymentbase.Base;

public class BroadPOS extends Base {
    @Override
    public void PreAuthorize() {

    }

    @Override
    public void PostAuthorize() {

    }

    @Override
    public void Void() {

    }

    @Override
    public void Refund() {

    }

    @Override
    public void CancelRequest() {

    }
}
