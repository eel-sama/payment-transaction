package com.enrollandpay.serviceinterface;

import org.json.JSONObject;

import java.util.Locale;
import java.util.Map;

public class Helper {
    public static String GetLanguageText(Map<String, String> languageTexts){
        String returnVal = "";
        String language = GetCurrentSelectedLanguage();
        if (languageTexts.containsKey(language)){
            returnVal = languageTexts.get(language);
        }
        else if (languageTexts.containsKey(GlobalVariables.LANGUAGE_DEFAULT)){
            returnVal = languageTexts.get(GlobalVariables.LANGUAGE_DEFAULT);
        }
        else if (languageTexts.containsKey(GlobalVariables.LANGUAGE_DEFAULT2)){
            returnVal = languageTexts.get(GlobalVariables.LANGUAGE_DEFAULT2);
        }
        else {
            for (Map.Entry<String,String> entry : languageTexts.entrySet()) {
                if (entry.getKey().indexOf("$") > -1) {
                    returnVal = entry.getValue();
                    break;
                }
            }
        }
        return returnVal;
    }
    public static String GetLanguageText(String languageTexts){
        String returnVal = languageTexts;
        String language = GetCurrentSelectedLanguage();
        try {
            JSONObject jsonObject = new JSONObject(languageTexts);
            if (jsonObject.has(language)) {
                returnVal = jsonObject.optString(language);
            }
            else if (jsonObject.has(GlobalVariables.LANGUAGE_DEFAULT)){
                returnVal = jsonObject.optString(GlobalVariables.LANGUAGE_DEFAULT);
            }
            else if (jsonObject.has(GlobalVariables.LANGUAGE_DEFAULT2)){
                returnVal = jsonObject.optString(GlobalVariables.LANGUAGE_DEFAULT2);
            }
            else if (jsonObject.has(GlobalVariables.LANGUAGE_US)){
                returnVal = jsonObject.optString(GlobalVariables.LANGUAGE_US);
            }
            else if (jsonObject.has(GlobalVariables.LANGUAGE_SPANISH)){
                returnVal = jsonObject.optString(GlobalVariables.LANGUAGE_SPANISH);
            }
        }
        catch (Exception e){

        }
        return returnVal;
    }
    public static String GetCurrentSelectedLanguage() {
        String language= Locale.getDefault().toString().replace("_","-");
        if (language != null) {
            switch (language.toLowerCase()){
                case "es-us":
                case "es-es":
                    language = "es-MX";
                    break;
            }
        }
        return language;
    }
    public static String GetCardTypeName(Integer cardType){
        String cardName = null;
        if (cardType == null){return null;}
        switch (cardType){
            case 1:
                return "AMERICAN EXPRESS";
            case 2:
                return "VISA";
            case 3://Types.CardTypes.Mastercard
                return "MASTERCARD";
            case 4://Types.CardTypes.Discover:
                return "DISCOVER";
            case 5://Types.CardTypes.Electron:
                return "ELECTRON";
            case 6:
                return "MAESTRO";
            case 7:
                return "DANKORT";
            case 8:
                return "INTERPAYMENT";
            case 9:
                return "UNIONPAY";
            case 10:
                return "DINERS";
            case 11:
                return "JCB";
            case 12:
                return "Gift";
        }
        return cardName;
    }
}