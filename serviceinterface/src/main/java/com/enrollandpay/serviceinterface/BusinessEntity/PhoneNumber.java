package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class PhoneNumber extends BaseBusinessEntity {
	private Integer PhoneNumberId; 
	public Integer getPhoneNumberId() { return this.PhoneNumberId; }
	public void setPhoneNumberId(Integer PhoneNumberId) { this.PhoneNumberId = PhoneNumberId; }
	private String Number; 
	public String getNumber() { return this.Number; }
	public void setNumber(String Number) { this.Number = Number; }
	private String CNAMId; 
	public String getCNAMId() { return this.CNAMId; }
	public void setCNAMId(String CNAMId) { this.CNAMId = CNAMId; }
	private Integer PhoneNumberProviderType; 
	public Integer getPhoneNumberProviderType() { return this.PhoneNumberProviderType; }
	public void setPhoneNumberProviderType(Integer PhoneNumberProviderType) { this.PhoneNumberProviderType = PhoneNumberProviderType; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Location> Locations; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram> LoyaltyPrograms; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Merchant> Merchants; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Coalition> Coalitions; 
    public static PhoneNumber Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                PhoneNumber entity = new PhoneNumber();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.PhoneNumberId = Entities.ParseIntegerValue(obj, "PhoneNumberId");
          entity.Number = Entities.ParseStringValue(obj, "Number");
          entity.CNAMId = Entities.ParseStringValue(obj, "CNAMId");
          entity.PhoneNumberProviderType = Entities.ParseIntegerValue(obj, "PhoneNumberProviderType");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          JSONArray jsonLocations = Entities.ParseJSONArrayValue(obj, "Locations");
                if (jsonLocations != null){
                    for (int index = 0; index < jsonLocations.length();index++){
                        JSONObject lstItem = jsonLocations.getJSONObject(index);
                        entity.Locations.add(com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonLoyaltyPrograms = Entities.ParseJSONArrayValue(obj, "LoyaltyPrograms");
                if (jsonLoyaltyPrograms != null){
                    for (int index = 0; index < jsonLoyaltyPrograms.length();index++){
                        JSONObject lstItem = jsonLoyaltyPrograms.getJSONObject(index);
                        entity.LoyaltyPrograms.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMerchants = Entities.ParseJSONArrayValue(obj, "Merchants");
                if (jsonMerchants != null){
                    for (int index = 0; index < jsonMerchants.length();index++){
                        JSONObject lstItem = jsonMerchants.getJSONObject(index);
                        entity.Merchants.add(com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonCoalitions = Entities.ParseJSONArrayValue(obj, "Coalitions");
                if (jsonCoalitions != null){
                    for (int index = 0; index < jsonCoalitions.length();index++){
                        JSONObject lstItem = jsonCoalitions.getJSONObject(index);
                        entity.Coalitions.add(com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (PhoneNumber)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public PhoneNumber() {
		Locations = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Location>();
			LoyaltyPrograms = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram>();
			Merchants = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Merchant>();
			Coalitions = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Coalition>();
			}
}
