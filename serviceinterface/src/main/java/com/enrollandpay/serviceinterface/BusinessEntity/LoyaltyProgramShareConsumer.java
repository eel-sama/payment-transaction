package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class LoyaltyProgramShareConsumer extends BaseBusinessEntity {
	private Integer LoyaltyProgramShareConsumerId; 
	public Integer getLoyaltyProgramShareConsumerId() { return this.LoyaltyProgramShareConsumerId; }
	public void setLoyaltyProgramShareConsumerId(Integer LoyaltyProgramShareConsumerId) { this.LoyaltyProgramShareConsumerId = LoyaltyProgramShareConsumerId; }
	private String LoyaltyProgramShareId; 
	public String getLoyaltyProgramShareId() { return this.LoyaltyProgramShareId; }
	public void setLoyaltyProgramShareId(String LoyaltyProgramShareId) { this.LoyaltyProgramShareId = LoyaltyProgramShareId; }
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    private com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShare LoyaltyProgramShare;
	public LoyaltyProgramShare getLoyaltyProgramShare() { return this.LoyaltyProgramShare; }
	public void setLoyaltyProgramShare(LoyaltyProgramShare LoyaltyProgramShare) { this.LoyaltyProgramShare = LoyaltyProgramShare; }
    public static LoyaltyProgramShareConsumer Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                LoyaltyProgramShareConsumer entity = new LoyaltyProgramShareConsumer();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.LoyaltyProgramShareConsumerId = Entities.ParseIntegerValue(obj, "LoyaltyProgramShareConsumerId");
          entity.LoyaltyProgramShareId = Entities.ParseStringValue(obj, "LoyaltyProgramShareId");
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
          if (obj.has("LoyaltyProgramShare")){  entity.LoyaltyProgramShare = com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShare.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LoyaltyProgramShare"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (LoyaltyProgramShareConsumer)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public LoyaltyProgramShareConsumer() {
		}
}
