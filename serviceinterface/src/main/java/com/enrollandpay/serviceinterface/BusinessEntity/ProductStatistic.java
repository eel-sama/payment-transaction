package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class ProductStatistic extends BaseBusinessEntity {
	private Integer ProductStatisticId; 
	public Integer getProductStatisticId() { return this.ProductStatisticId; }
	public void setProductStatisticId(Integer ProductStatisticId) { this.ProductStatisticId = ProductStatisticId; }
	private Integer ProductId; 
	public Integer getProductId() { return this.ProductId; }
	public void setProductId(Integer ProductId) { this.ProductId = ProductId; }
	private Date SaleDate; 
	public Date getSaleDate() { return this.SaleDate; }
	public void setSaleDate(Date SaleDate) { this.SaleDate = SaleDate; }
	private Double Quantity; 
	public Double getQuantity() { return this.Quantity; }
	public void setQuantity(Double Quantity) { this.Quantity = Quantity; }
	private Double LoyaltyQuantity; 
	public Double getLoyaltyQuantity() { return this.LoyaltyQuantity; }
	public void setLoyaltyQuantity(Double LoyaltyQuantity) { this.LoyaltyQuantity = LoyaltyQuantity; }
	private Integer OrderCount; 
	public Integer getOrderCount() { return this.OrderCount; }
	public void setOrderCount(Integer OrderCount) { this.OrderCount = OrderCount; }
	private Integer LoyaltyOrderCount; 
	public Integer getLoyaltyOrderCount() { return this.LoyaltyOrderCount; }
	public void setLoyaltyOrderCount(Integer LoyaltyOrderCount) { this.LoyaltyOrderCount = LoyaltyOrderCount; }
	private Date UpdateDateTime; 
	public Date getUpdateDateTime() { return this.UpdateDateTime; }
	public void setUpdateDateTime(Date UpdateDateTime) { this.UpdateDateTime = UpdateDateTime; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Double Amount; 
	public Double getAmount() { return this.Amount; }
	public void setAmount(Double Amount) { this.Amount = Amount; }
	private Double LoyaltyAmount; 
	public Double getLoyaltyAmount() { return this.LoyaltyAmount; }
	public void setLoyaltyAmount(Double LoyaltyAmount) { this.LoyaltyAmount = LoyaltyAmount; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Product Product;
	public Product getProduct() { return this.Product; }
	public void setProduct(Product Product) { this.Product = Product; }
    public static ProductStatistic Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                ProductStatistic entity = new ProductStatistic();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.ProductStatisticId = Entities.ParseIntegerValue(obj, "ProductStatisticId");
          entity.ProductId = Entities.ParseIntegerValue(obj, "ProductId");
          entity.SaleDate = Entities.ParseDateValue(obj, "SaleDate");
          entity.Quantity = Entities.ParseDoubleValue(obj, "Quantity");
          entity.LoyaltyQuantity = Entities.ParseDoubleValue(obj, "LoyaltyQuantity");
          entity.OrderCount = Entities.ParseIntegerValue(obj, "OrderCount");
          entity.LoyaltyOrderCount = Entities.ParseIntegerValue(obj, "LoyaltyOrderCount");
          entity.UpdateDateTime = Entities.ParseDateValue(obj, "UpdateDateTime");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.Amount = Entities.ParseDoubleValue(obj, "Amount");
          entity.LoyaltyAmount = Entities.ParseDoubleValue(obj, "LoyaltyAmount");
          if (obj.has("Product")){  entity.Product = com.enrollandpay.serviceinterface.BusinessEntity.Product.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Product"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (ProductStatistic)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public ProductStatistic() {
		}
}
