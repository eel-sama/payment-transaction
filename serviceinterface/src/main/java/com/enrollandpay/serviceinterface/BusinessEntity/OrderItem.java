package com.enrollandpay.serviceinterface.BusinessEntity;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class OrderItem implements Parcelable {
	private Integer OrderItemId; 
	public Integer getOrderItemId() { return this.OrderItemId; }
	public void setOrderItemId(Integer OrderItemId) { this.OrderItemId = OrderItemId; }
	private Integer OrderId; 
	public Integer getOrderId() { return this.OrderId; }
	public void setOrderId(Integer OrderId) { this.OrderId = OrderId; }
	private Integer ProductId; 
	public Integer getProductId() { return this.ProductId; }
	public void setProductId(Integer ProductId) { this.ProductId = ProductId; }
	private String Name; 
	public String getName() { return this.Name; }
	public void setName(String Name) { this.Name = Name; }
	private Double AmountTax; 
	public Double getAmountTax() { return this.AmountTax; }
	public void setAmountTax(Double AmountTax) { this.AmountTax = AmountTax; }
	private String PosIdent; 
	public String getPosIdent() { return this.PosIdent; }
	public void setPosIdent(String PosIdent) { this.PosIdent = PosIdent; }
	private String ProductIdent;

	public String getProductIdent() {		return ProductIdent;	}
	public void setProductIdent(String productIdent) {		ProductIdent = productIdent;	}

	private Double Quantity;
	public Double getQuantity() { return this.Quantity; }
	public void setQuantity(Double Quantity) { this.Quantity = Quantity; }
	private String Details; 
	public String getDetails() { return this.Details; }
	public void setDetails(String Details) { this.Details = Details; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Double AmountTotal; 
	public Double getAmountTotal() { return this.AmountTotal; }
	public void setAmountTotal(Double AmountTotal) { this.AmountTotal = AmountTotal; }
	private Double AmountReturned; 
	public Double getAmountReturned() { return this.AmountReturned; }
	public void setAmountReturned(Double AmountReturned) { this.AmountReturned = AmountReturned; }
	private Double QuantityReturned; 
	public Double getQuantityReturned() { return this.QuantityReturned; }
	public void setQuantityReturned(Double QuantityReturned) { this.QuantityReturned = QuantityReturned; }
	private Integer TaxId; 
	public Integer getTaxId() { return this.TaxId; }
	public void setTaxId(Integer TaxId) { this.TaxId = TaxId; }
	private Integer AddOnToOrderItemId; 
	public Integer getAddOnToOrderItemId() { return this.AddOnToOrderItemId; }
	public void setAddOnToOrderItemId(Integer AddOnToOrderItemId) { this.AddOnToOrderItemId = AddOnToOrderItemId; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Order Order;
	public Order getOrder() { return this.Order; }
	public void setOrder(Order Order) { this.Order = Order; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Product Product;
	public Product getProduct() { return this.Product; }
	public void setProduct(Product Product) { this.Product = Product; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Tax Tax;
	public Tax getTax() { return this.Tax; }
	public void setTax(Tax Tax) { this.Tax = Tax; }
    private com.enrollandpay.serviceinterface.BusinessEntity.OrderItem AddOnToOrderItem;
	public OrderItem getAddOnToOrderItem() { return this.AddOnToOrderItem; }
	public void setAddOnToOrderItem(OrderItem AddOnToOrderItem) { this.AddOnToOrderItem = AddOnToOrderItem; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderItem> AddOns; 
    public static OrderItem Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                OrderItem entity = new OrderItem();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.OrderItemId = Entities.ParseIntegerValue(obj, "OrderItemId");
          entity.OrderId = Entities.ParseIntegerValue(obj, "OrderId");
          entity.ProductId = Entities.ParseIntegerValue(obj, "ProductId");
          entity.Name = Entities.ParseStringValue(obj, "Name");
          entity.AmountTax = Entities.ParseDoubleValue(obj, "AmountTax");
          entity.PosIdent = Entities.ParseStringValue(obj, "PosIdent");
          entity.ProductIdent = Entities.ParseStringValue(obj, "ProductIdent");
          entity.Quantity = Entities.ParseDoubleValue(obj, "Quantity");
          entity.Details = Entities.ParseStringValue(obj, "Details");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.AmountTotal = Entities.ParseDoubleValue(obj, "AmountTotal");
          entity.AmountReturned = Entities.ParseDoubleValue(obj, "AmountReturned");
          entity.QuantityReturned = Entities.ParseDoubleValue(obj, "QuantityReturned");
          entity.TaxId = Entities.ParseIntegerValue(obj, "TaxId");
          entity.AddOnToOrderItemId = Entities.ParseIntegerValue(obj, "AddOnToOrderItemId");
          if (obj.has("Order")){  entity.Order = com.enrollandpay.serviceinterface.BusinessEntity.Order.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Order"));}
          if (obj.has("Product")){  entity.Product = com.enrollandpay.serviceinterface.BusinessEntity.Product.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Product"));}
          if (obj.has("Tax")){  entity.Tax = com.enrollandpay.serviceinterface.BusinessEntity.Tax.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Tax"));}
          if (obj.has("AddOnToOrderItem")){  entity.AddOnToOrderItem = com.enrollandpay.serviceinterface.BusinessEntity.OrderItem.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"AddOnToOrderItem"));}
          JSONArray jsonAddOns = Entities.ParseJSONArrayValue(obj, "AddOns");
                if (jsonAddOns != null){
                    for (int index = 0; index < jsonAddOns.length();index++){
                        JSONObject lstItem = jsonAddOns.getJSONObject(index);
                        entity.AddOns.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderItem.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (OrderItem)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public OrderItem() {
		AddOns = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderItem>();
			}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeValue(this.OrderItemId);
		dest.writeValue(this.OrderId);
		dest.writeValue(this.ProductId);
		dest.writeString(this.Name);
		dest.writeValue(this.AmountTax);
		dest.writeString(this.PosIdent);
		dest.writeString(this.ProductIdent);
		dest.writeValue(this.Quantity);
		dest.writeString(this.Details);
		dest.writeValue(this.StatusType);
		dest.writeString(this.TS);
		dest.writeValue(this.AmountTotal);
		dest.writeValue(this.AmountReturned);
		dest.writeValue(this.QuantityReturned);
		dest.writeValue(this.TaxId);
		dest.writeValue(this.AddOnToOrderItemId);
		dest.writeParcelable(this.Order, flags);
		dest.writeSerializable(this.Product);
		dest.writeSerializable(this.Tax);
		dest.writeParcelable(this.AddOnToOrderItem, flags);
		dest.writeTypedList(this.AddOns);
	}

	public void readFromParcel(Parcel source) {
		this.OrderItemId = (Integer) source.readValue(Integer.class.getClassLoader());
		this.OrderId = (Integer) source.readValue(Integer.class.getClassLoader());
		this.ProductId = (Integer) source.readValue(Integer.class.getClassLoader());
		this.Name = source.readString();
		this.AmountTax = (Double) source.readValue(Double.class.getClassLoader());
		this.PosIdent = source.readString();
		this.ProductIdent = source.readString();
		this.Quantity = (Double) source.readValue(Double.class.getClassLoader());
		this.Details = source.readString();
		this.StatusType = (Integer) source.readValue(Integer.class.getClassLoader());
		this.TS = source.readString();
		this.AmountTotal = (Double) source.readValue(Double.class.getClassLoader());
		this.AmountReturned = (Double) source.readValue(Double.class.getClassLoader());
		this.QuantityReturned = (Double) source.readValue(Double.class.getClassLoader());
		this.TaxId = (Integer) source.readValue(Integer.class.getClassLoader());
		this.AddOnToOrderItemId = (Integer) source.readValue(Integer.class.getClassLoader());
		this.Order = source.readParcelable(com.enrollandpay.serviceinterface.BusinessEntity.Order.class.getClassLoader());
		this.Product = (com.enrollandpay.serviceinterface.BusinessEntity.Product) source.readSerializable();
		this.Tax = (com.enrollandpay.serviceinterface.BusinessEntity.Tax) source.readSerializable();
		this.AddOnToOrderItem = source.readParcelable(OrderItem.class.getClassLoader());
		this.AddOns = source.createTypedArrayList(OrderItem.CREATOR);
	}

	protected OrderItem(Parcel in) {
		this.OrderItemId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.OrderId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.ProductId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.Name = in.readString();
		this.AmountTax = (Double) in.readValue(Double.class.getClassLoader());
		this.PosIdent = in.readString();
		this.ProductIdent = in.readString();
		this.Quantity = (Double) in.readValue(Double.class.getClassLoader());
		this.Details = in.readString();
		this.StatusType = (Integer) in.readValue(Integer.class.getClassLoader());
		this.TS = in.readString();
		this.AmountTotal = (Double) in.readValue(Double.class.getClassLoader());
		this.AmountReturned = (Double) in.readValue(Double.class.getClassLoader());
		this.QuantityReturned = (Double) in.readValue(Double.class.getClassLoader());
		this.TaxId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.AddOnToOrderItemId = (Integer) in.readValue(Integer.class.getClassLoader());
		this.Order = in.readParcelable(com.enrollandpay.serviceinterface.BusinessEntity.Order.class.getClassLoader());
		this.Product = (com.enrollandpay.serviceinterface.BusinessEntity.Product) in.readSerializable();
		this.Tax = (com.enrollandpay.serviceinterface.BusinessEntity.Tax) in.readSerializable();
		this.AddOnToOrderItem = in.readParcelable(OrderItem.class.getClassLoader());
		this.AddOns = in.createTypedArrayList(OrderItem.CREATOR);
	}

	public static final Creator<OrderItem> CREATOR = new Creator<OrderItem>() {
		@Override
		public OrderItem createFromParcel(Parcel source) {
			return new OrderItem(source);
		}

		@Override
		public OrderItem[] newArray(int size) {
			return new OrderItem[size];
		}
	};
}
