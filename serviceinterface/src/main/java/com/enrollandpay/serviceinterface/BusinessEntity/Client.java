package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Client extends BaseBusinessEntity {
	private Integer ClientId; 
	public Integer getClientId() { return this.ClientId; }
	public void setClientId(Integer ClientId) { this.ClientId = ClientId; }
	private String ClientIdent; 
	public String getClientIdent() { return this.ClientIdent; }
	public void setClientIdent(String ClientIdent) { this.ClientIdent = ClientIdent; }
	private String Secret; 
	public String getSecret() { return this.Secret; }
	public void setSecret(String Secret) { this.Secret = Secret; }
	private String Name; 
	public String getName() { return this.Name; }
	public void setName(String Name) { this.Name = Name; }
	private Integer ApplicationType; 
	public Integer getApplicationType() { return this.ApplicationType; }
	public void setApplicationType(Integer ApplicationType) { this.ApplicationType = ApplicationType; }
	private Integer TokenLifeTime; 
	public Integer getTokenLifeTime() { return this.TokenLifeTime; }
	public void setTokenLifeTime(Integer TokenLifeTime) { this.TokenLifeTime = TokenLifeTime; }
	private Integer RefreshTokenLifeTime; 
	public Integer getRefreshTokenLifeTime() { return this.RefreshTokenLifeTime; }
	public void setRefreshTokenLifeTime(Integer RefreshTokenLifeTime) { this.RefreshTokenLifeTime = RefreshTokenLifeTime; }
	private String AllowedOrigin; 
	public String getAllowedOrigin() { return this.AllowedOrigin; }
	public void setAllowedOrigin(String AllowedOrigin) { this.AllowedOrigin = AllowedOrigin; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest> AuthorizationRequests; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess> ClientAccesses; 
    public static Client Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Client entity = new Client();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.ClientId = Entities.ParseIntegerValue(obj, "ClientId");
          entity.ClientIdent = Entities.ParseStringValue(obj, "ClientIdent");
          entity.Secret = Entities.ParseStringValue(obj, "Secret");
          entity.Name = Entities.ParseStringValue(obj, "Name");
          entity.ApplicationType = Entities.ParseIntegerValue(obj, "ApplicationType");
          entity.TokenLifeTime = Entities.ParseIntegerValue(obj, "TokenLifeTime");
          entity.RefreshTokenLifeTime = Entities.ParseIntegerValue(obj, "RefreshTokenLifeTime");
          entity.AllowedOrigin = Entities.ParseStringValue(obj, "AllowedOrigin");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          JSONArray jsonAuthorizationRequests = Entities.ParseJSONArrayValue(obj, "AuthorizationRequests");
                if (jsonAuthorizationRequests != null){
                    for (int index = 0; index < jsonAuthorizationRequests.length();index++){
                        JSONObject lstItem = jsonAuthorizationRequests.getJSONObject(index);
                        entity.AuthorizationRequests.add(com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonClientAccesses = Entities.ParseJSONArrayValue(obj, "ClientAccesses");
                if (jsonClientAccesses != null){
                    for (int index = 0; index < jsonClientAccesses.length();index++){
                        JSONObject lstItem = jsonClientAccesses.getJSONObject(index);
                        entity.ClientAccesses.add(com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Client)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Client() {
		AuthorizationRequests = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest>();
			ClientAccesses = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess>();
			}
}
