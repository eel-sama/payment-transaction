package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class ClientAccess extends BaseBusinessEntity {
	private String ClientAccessId; 
	public String getClientAccessId() { return this.ClientAccessId; }
	public void setClientAccessId(String ClientAccessId) { this.ClientAccessId = ClientAccessId; }
	private Integer ClientId; 
	public Integer getClientId() { return this.ClientId; }
	public void setClientId(Integer ClientId) { this.ClientId = ClientId; }
	private Integer EmployeeId; 
	public Integer getEmployeeId() { return this.EmployeeId; }
	public void setEmployeeId(Integer EmployeeId) { this.EmployeeId = EmployeeId; }
	private Integer DeviceId; 
	public Integer getDeviceId() { return this.DeviceId; }
	public void setDeviceId(Integer DeviceId) { this.DeviceId = DeviceId; }
	private Date LastIssueDateTime; 
	public Date getLastIssueDateTime() { return this.LastIssueDateTime; }
	public void setLastIssueDateTime(Date LastIssueDateTime) { this.LastIssueDateTime = LastIssueDateTime; }
	private Date NextRefreshDateTime; 
	public Date getNextRefreshDateTime() { return this.NextRefreshDateTime; }
	public void setNextRefreshDateTime(Date NextRefreshDateTime) { this.NextRefreshDateTime = NextRefreshDateTime; }
	private Date LastCredentialChangeDateTime; 
	public Date getLastCredentialChangeDateTime() { return this.LastCredentialChangeDateTime; }
	public void setLastCredentialChangeDateTime(Date LastCredentialChangeDateTime) { this.LastCredentialChangeDateTime = LastCredentialChangeDateTime; }
	private String MetaData; 
	public String getMetaData() { return this.MetaData; }
	public void setMetaData(String MetaData) { this.MetaData = MetaData; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private Integer RepresentativeId; 
	public Integer getRepresentativeId() { return this.RepresentativeId; }
	public void setRepresentativeId(Integer RepresentativeId) { this.RepresentativeId = RepresentativeId; }
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private Integer CoalitionId; 
	public Integer getCoalitionId() { return this.CoalitionId; }
	public void setCoalitionId(Integer CoalitionId) { this.CoalitionId = CoalitionId; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Client Client;
	public Client getClient() { return this.Client; }
	public void setClient(Client Client) { this.Client = Client; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Device Device;
	public Device getDevice() { return this.Device; }
	public void setDevice(Device Device) { this.Device = Device; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Employee Employee;
	public Employee getEmployee() { return this.Employee; }
	public void setEmployee(Employee Employee) { this.Employee = Employee; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Representative Representative;
	public Representative getRepresentative() { return this.Representative; }
	public void setRepresentative(Representative Representative) { this.Representative = Representative; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Coalition Coalition;
	public Coalition getCoalition() { return this.Coalition; }
	public void setCoalition(Coalition Coalition) { this.Coalition = Coalition; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.EmployeeImpersonation> EmployeeImpersonations; 
    public static ClientAccess Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                ClientAccess entity = new ClientAccess();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.ClientAccessId = Entities.ParseStringValue(obj, "ClientAccessId");
          entity.ClientId = Entities.ParseIntegerValue(obj, "ClientId");
          entity.EmployeeId = Entities.ParseIntegerValue(obj, "EmployeeId");
          entity.DeviceId = Entities.ParseIntegerValue(obj, "DeviceId");
          entity.LastIssueDateTime = Entities.ParseDateValue(obj, "LastIssueDateTime");
          entity.NextRefreshDateTime = Entities.ParseDateValue(obj, "NextRefreshDateTime");
          entity.LastCredentialChangeDateTime = Entities.ParseDateValue(obj, "LastCredentialChangeDateTime");
          entity.MetaData = Entities.ParseStringValue(obj, "MetaData");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.RepresentativeId = Entities.ParseIntegerValue(obj, "RepresentativeId");
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.CoalitionId = Entities.ParseIntegerValue(obj, "CoalitionId");
          if (obj.has("Client")){  entity.Client = com.enrollandpay.serviceinterface.BusinessEntity.Client.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Client"));}
          if (obj.has("Device")){  entity.Device = com.enrollandpay.serviceinterface.BusinessEntity.Device.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Device"));}
          if (obj.has("Employee")){  entity.Employee = com.enrollandpay.serviceinterface.BusinessEntity.Employee.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Employee"));}
          if (obj.has("Representative")){  entity.Representative = com.enrollandpay.serviceinterface.BusinessEntity.Representative.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Representative"));}
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
          if (obj.has("Coalition")){  entity.Coalition = com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Coalition"));}
          JSONArray jsonEmployeeImpersonations = Entities.ParseJSONArrayValue(obj, "EmployeeImpersonations");
                if (jsonEmployeeImpersonations != null){
                    for (int index = 0; index < jsonEmployeeImpersonations.length();index++){
                        JSONObject lstItem = jsonEmployeeImpersonations.getJSONObject(index);
                        entity.EmployeeImpersonations.add(com.enrollandpay.serviceinterface.BusinessEntity.EmployeeImpersonation.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (ClientAccess)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public ClientAccess() {
		EmployeeImpersonations = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.EmployeeImpersonation>();
			}
}
