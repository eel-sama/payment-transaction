package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class LoyaltyProgramParticipant extends BaseBusinessEntity {
	private Integer LoyaltyProgramParticipantId; 
	public Integer getLoyaltyProgramParticipantId() { return this.LoyaltyProgramParticipantId; }
	public void setLoyaltyProgramParticipantId(Integer LoyaltyProgramParticipantId) { this.LoyaltyProgramParticipantId = LoyaltyProgramParticipantId; }
	private Integer LoyaltyProgramId; 
	public Integer getLoyaltyProgramId() { return this.LoyaltyProgramId; }
	public void setLoyaltyProgramId(Integer LoyaltyProgramId) { this.LoyaltyProgramId = LoyaltyProgramId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String POSRewardIdent; 
	public String getPOSRewardIdent() { return this.POSRewardIdent; }
	public void setPOSRewardIdent(String POSRewardIdent) { this.POSRewardIdent = POSRewardIdent; }
	private String POSProductIdent; 
	public String getPOSProductIdent() { return this.POSProductIdent; }
	public void setPOSProductIdent(String POSProductIdent) { this.POSProductIdent = POSProductIdent; }
	private Integer ApplyRewardOfferType; 
	public Integer getApplyRewardOfferType() { return this.ApplyRewardOfferType; }
	public void setApplyRewardOfferType(Integer ApplyRewardOfferType) { this.ApplyRewardOfferType = ApplyRewardOfferType; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
	public Location getLocation() { return this.Location; }
	public void setLocation(Location Location) { this.Location = Location; }
    private com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram LoyaltyProgram;
	public LoyaltyProgram getLoyaltyProgram() { return this.LoyaltyProgram; }
	public void setLoyaltyProgram(LoyaltyProgram LoyaltyProgram) { this.LoyaltyProgram = LoyaltyProgram; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    public static LoyaltyProgramParticipant Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                LoyaltyProgramParticipant entity = new LoyaltyProgramParticipant();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.LoyaltyProgramParticipantId = Entities.ParseIntegerValue(obj, "LoyaltyProgramParticipantId");
          entity.LoyaltyProgramId = Entities.ParseIntegerValue(obj, "LoyaltyProgramId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.POSRewardIdent = Entities.ParseStringValue(obj, "POSRewardIdent");
          entity.POSProductIdent = Entities.ParseStringValue(obj, "POSProductIdent");
          entity.ApplyRewardOfferType = Entities.ParseIntegerValue(obj, "ApplyRewardOfferType");
          if (obj.has("Location")){  entity.Location = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Location"));}
          if (obj.has("LoyaltyProgram")){  entity.LoyaltyProgram = com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LoyaltyProgram"));}
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (LoyaltyProgramParticipant)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public LoyaltyProgramParticipant() {
		}
}
