package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Employee extends BaseBusinessEntity {
	private Integer EmployeeId; 
	public Integer getEmployeeId() { return this.EmployeeId; }
	public void setEmployeeId(Integer EmployeeId) { this.EmployeeId = EmployeeId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private String NamePrefix; 
	public String getNamePrefix() { return this.NamePrefix; }
	public void setNamePrefix(String NamePrefix) { this.NamePrefix = NamePrefix; }
	private String NameFirst; 
	public String getNameFirst() { return this.NameFirst; }
	public void setNameFirst(String NameFirst) { this.NameFirst = NameFirst; }
	private String NameMiddle; 
	public String getNameMiddle() { return this.NameMiddle; }
	public void setNameMiddle(String NameMiddle) { this.NameMiddle = NameMiddle; }
	private String NameLast; 
	public String getNameLast() { return this.NameLast; }
	public void setNameLast(String NameLast) { this.NameLast = NameLast; }
	private String NameSuffix; 
	public String getNameSuffix() { return this.NameSuffix; }
	public void setNameSuffix(String NameSuffix) { this.NameSuffix = NameSuffix; }
	private String EmailAddress; 
	public String getEmailAddress() { return this.EmailAddress; }
	public void setEmailAddress(String EmailAddress) { this.EmailAddress = EmailAddress; }
	private String PhoneNumber; 
	public String getPhoneNumber() { return this.PhoneNumber; }
	public void setPhoneNumber(String PhoneNumber) { this.PhoneNumber = PhoneNumber; }
	private Integer AccessType; 
	public Integer getAccessType() { return this.AccessType; }
	public void setAccessType(Integer AccessType) { this.AccessType = AccessType; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer LanguageType; 
	public Integer getLanguageType() { return this.LanguageType; }
	public void setLanguageType(Integer LanguageType) { this.LanguageType = LanguageType; }
    private com.enrollandpay.serviceinterface.BusinessEntity.EmployeeAuthentication EmployeeAuthentication;
	public EmployeeAuthentication getEmployeeAuthentication() { return this.EmployeeAuthentication; }
	public void setEmployeeAuthentication(EmployeeAuthentication EmployeeAuthentication) { this.EmployeeAuthentication = EmployeeAuthentication; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess> ClientAccesses; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram> ConsumerLoyaltyPrograms; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.EmployeeLocation> EmployeeLocations; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Order> SaleOrders; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.EmployeeImpersonation> EmployeeImpersonations; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Message> Messages; 
    public static Employee Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Employee entity = new Employee();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.EmployeeId = Entities.ParseIntegerValue(obj, "EmployeeId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.NamePrefix = Entities.ParseStringValue(obj, "NamePrefix");
          entity.NameFirst = Entities.ParseStringValue(obj, "NameFirst");
          entity.NameMiddle = Entities.ParseStringValue(obj, "NameMiddle");
          entity.NameLast = Entities.ParseStringValue(obj, "NameLast");
          entity.NameSuffix = Entities.ParseStringValue(obj, "NameSuffix");
          entity.EmailAddress = Entities.ParseStringValue(obj, "EmailAddress");
          entity.PhoneNumber = Entities.ParseStringValue(obj, "PhoneNumber");
          entity.AccessType = Entities.ParseIntegerValue(obj, "AccessType");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.LanguageType = Entities.ParseIntegerValue(obj, "LanguageType");
          if (obj.has("EmployeeAuthentication")){  entity.EmployeeAuthentication = com.enrollandpay.serviceinterface.BusinessEntity.EmployeeAuthentication.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"EmployeeAuthentication"));}
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
          JSONArray jsonClientAccesses = Entities.ParseJSONArrayValue(obj, "ClientAccesses");
                if (jsonClientAccesses != null){
                    for (int index = 0; index < jsonClientAccesses.length();index++){
                        JSONObject lstItem = jsonClientAccesses.getJSONObject(index);
                        entity.ClientAccesses.add(com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerLoyaltyPrograms = Entities.ParseJSONArrayValue(obj, "ConsumerLoyaltyPrograms");
                if (jsonConsumerLoyaltyPrograms != null){
                    for (int index = 0; index < jsonConsumerLoyaltyPrograms.length();index++){
                        JSONObject lstItem = jsonConsumerLoyaltyPrograms.getJSONObject(index);
                        entity.ConsumerLoyaltyPrograms.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonEmployeeLocations = Entities.ParseJSONArrayValue(obj, "EmployeeLocations");
                if (jsonEmployeeLocations != null){
                    for (int index = 0; index < jsonEmployeeLocations.length();index++){
                        JSONObject lstItem = jsonEmployeeLocations.getJSONObject(index);
                        entity.EmployeeLocations.add(com.enrollandpay.serviceinterface.BusinessEntity.EmployeeLocation.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonSaleOrders = Entities.ParseJSONArrayValue(obj, "SaleOrders");
                if (jsonSaleOrders != null){
                    for (int index = 0; index < jsonSaleOrders.length();index++){
                        JSONObject lstItem = jsonSaleOrders.getJSONObject(index);
                        entity.SaleOrders.add(com.enrollandpay.serviceinterface.BusinessEntity.Order.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonEmployeeImpersonations = Entities.ParseJSONArrayValue(obj, "EmployeeImpersonations");
                if (jsonEmployeeImpersonations != null){
                    for (int index = 0; index < jsonEmployeeImpersonations.length();index++){
                        JSONObject lstItem = jsonEmployeeImpersonations.getJSONObject(index);
                        entity.EmployeeImpersonations.add(com.enrollandpay.serviceinterface.BusinessEntity.EmployeeImpersonation.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMessages = Entities.ParseJSONArrayValue(obj, "Messages");
                if (jsonMessages != null){
                    for (int index = 0; index < jsonMessages.length();index++){
                        JSONObject lstItem = jsonMessages.getJSONObject(index);
                        entity.Messages.add(com.enrollandpay.serviceinterface.BusinessEntity.Message.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Employee)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public String NameFull() { 
		if(NameFirst != null && NameLast != null) { 
			return NameFirst + " " + NameLast; 
		} else { return null; } 
	}
	public String NameAbbr() { 
		if (NameFirst != null && NameLast != null) { 
			return NameLast + ", " + NameFirst.substring(0,1); 
		} else if (NameLast != null) {
			return NameLast;
		} else { return null; } 
	}

    @Override
    public String toString() {
        return this.NameFirst+ " " +this.NameLast;
    }

    public Employee() {
		ClientAccesses = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess>();
			ConsumerLoyaltyPrograms = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram>();
			EmployeeLocations = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.EmployeeLocation>();
			SaleOrders = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Order>();
			EmployeeImpersonations = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.EmployeeImpersonation>();
			Messages = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Message>();
			}
}
