package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class OrderReviewQuestion extends BaseBusinessEntity {
	private Integer OrderReviewQuestionId; 
	public Integer getOrderReviewQuestionId() { return this.OrderReviewQuestionId; }
	public void setOrderReviewQuestionId(Integer OrderReviewQuestionId) { this.OrderReviewQuestionId = OrderReviewQuestionId; }
	private String OrderReviewId; 
	public String getOrderReviewId() { return this.OrderReviewId; }
	public void setOrderReviewId(String OrderReviewId) { this.OrderReviewId = OrderReviewId; }
	private Integer QuestionId; 
	public Integer getQuestionId() { return this.QuestionId; }
	public void setQuestionId(Integer QuestionId) { this.QuestionId = QuestionId; }
	private String Display; 
	public String getDisplay() { return this.Display; }
	public void setDisplay(String Display) { this.Display = Display; }
	private Integer QuestionType; 
	public Integer getQuestionType() { return this.QuestionType; }
	public void setQuestionType(Integer QuestionType) { this.QuestionType = QuestionType; }
	private Double Score; 
	public Double getScore() { return this.Score; }
	public void setScore(Double Score) { this.Score = Score; }
	private Date UpdateDateTime; 
	public Date getUpdateDateTime() { return this.UpdateDateTime; }
	public void setUpdateDateTime(Date UpdateDateTime) { this.UpdateDateTime = UpdateDateTime; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
    private com.enrollandpay.serviceinterface.BusinessEntity.OrderReview OrderReview;
	public OrderReview getOrderReview() { return this.OrderReview; }
	public void setOrderReview(OrderReview OrderReview) { this.OrderReview = OrderReview; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Question Question;
	public Question getQuestion() { return this.Question; }
	public void setQuestion(Question Question) { this.Question = Question; }
    public static OrderReviewQuestion Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                OrderReviewQuestion entity = new OrderReviewQuestion();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.OrderReviewQuestionId = Entities.ParseIntegerValue(obj, "OrderReviewQuestionId");
          entity.OrderReviewId = Entities.ParseStringValue(obj, "OrderReviewId");
          entity.QuestionId = Entities.ParseIntegerValue(obj, "QuestionId");
          entity.Display = Entities.ParseStringValue(obj, "Display");
          entity.QuestionType = Entities.ParseIntegerValue(obj, "QuestionType");
          entity.Score = Entities.ParseDoubleValue(obj, "Score");
          entity.UpdateDateTime = Entities.ParseDateValue(obj, "UpdateDateTime");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          if (obj.has("OrderReview")){  entity.OrderReview = com.enrollandpay.serviceinterface.BusinessEntity.OrderReview.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"OrderReview"));}
          if (obj.has("Question")){  entity.Question = com.enrollandpay.serviceinterface.BusinessEntity.Question.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Question"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (OrderReviewQuestion)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public OrderReviewQuestion() {
		}
}
