package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class OfferAssociation extends BaseBusinessEntity {
	private Integer OfferAssociationId; 
	public Integer getOfferAssociationId() { return this.OfferAssociationId; }
	public void setOfferAssociationId(Integer OfferAssociationId) { this.OfferAssociationId = OfferAssociationId; }
	private Integer OfferId; 
	public Integer getOfferId() { return this.OfferId; }
	public void setOfferId(Integer OfferId) { this.OfferId = OfferId; }
	private Integer ProductId; 
	public Integer getProductId() { return this.ProductId; }
	public void setProductId(Integer ProductId) { this.ProductId = ProductId; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Offer Offer;
	public Offer getOffer() { return this.Offer; }
	public void setOffer(Offer Offer) { this.Offer = Offer; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Product Product;
	public Product getProduct() { return this.Product; }
	public void setProduct(Product Product) { this.Product = Product; }
    public static OfferAssociation Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                OfferAssociation entity = new OfferAssociation();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.OfferAssociationId = Entities.ParseIntegerValue(obj, "OfferAssociationId");
          entity.OfferId = Entities.ParseIntegerValue(obj, "OfferId");
          entity.ProductId = Entities.ParseIntegerValue(obj, "ProductId");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Offer")){  entity.Offer = com.enrollandpay.serviceinterface.BusinessEntity.Offer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Offer"));}
          if (obj.has("Product")){  entity.Product = com.enrollandpay.serviceinterface.BusinessEntity.Product.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Product"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (OfferAssociation)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public OfferAssociation() {
		}
}
