package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class ConsumerLoyaltyProgram extends BaseBusinessEntity {
	private Integer ConsumerLoyaltyProgramId; 
	public Integer getConsumerLoyaltyProgramId() { return this.ConsumerLoyaltyProgramId; }
	public void setConsumerLoyaltyProgramId(Integer ConsumerLoyaltyProgramId) { this.ConsumerLoyaltyProgramId = ConsumerLoyaltyProgramId; }
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private Integer LoyaltyProgramId; 
	public Integer getLoyaltyProgramId() { return this.LoyaltyProgramId; }
	public void setLoyaltyProgramId(Integer LoyaltyProgramId) { this.LoyaltyProgramId = LoyaltyProgramId; }
	private Integer FreeProductCurrent; 
	public Integer getFreeProductCurrent() { return this.FreeProductCurrent; }
	public void setFreeProductCurrent(Integer FreeProductCurrent) { this.FreeProductCurrent = FreeProductCurrent; }
	private Double PointBalanceCurrent; 
	public Double getPointBalanceCurrent() { return this.PointBalanceCurrent; }
	public void setPointBalanceCurrent(Double PointBalanceCurrent) { this.PointBalanceCurrent = PointBalanceCurrent; }
	private Double PointBalanceLifetime; 
	public Double getPointBalanceLifetime() { return this.PointBalanceLifetime; }
	public void setPointBalanceLifetime(Double PointBalanceLifetime) { this.PointBalanceLifetime = PointBalanceLifetime; }
	private Double RewardBalanceCurrent; 
	public Double getRewardBalanceCurrent() { return this.RewardBalanceCurrent; }
	public void setRewardBalanceCurrent(Double RewardBalanceCurrent) { this.RewardBalanceCurrent = RewardBalanceCurrent; }
	private Double RewardBalanceLifetime; 
	public Double getRewardBalanceLifetime() { return this.RewardBalanceLifetime; }
	public void setRewardBalanceLifetime(Double RewardBalanceLifetime) { this.RewardBalanceLifetime = RewardBalanceLifetime; }
	private Date EnrollDateTime; 
	public Date getEnrollDateTime() { return this.EnrollDateTime; }
	public void setEnrollDateTime(Date EnrollDateTime) { this.EnrollDateTime = EnrollDateTime; }
	private Integer EnrollMerchantId; 
	public Integer getEnrollMerchantId() { return this.EnrollMerchantId; }
	public void setEnrollMerchantId(Integer EnrollMerchantId) { this.EnrollMerchantId = EnrollMerchantId; }
	private Integer EnrollLocationId; 
	public Integer getEnrollLocationId() { return this.EnrollLocationId; }
	public void setEnrollLocationId(Integer EnrollLocationId) { this.EnrollLocationId = EnrollLocationId; }
	private Integer EnrollEmployeeId; 
	public Integer getEnrollEmployeeId() { return this.EnrollEmployeeId; }
	public void setEnrollEmployeeId(Integer EnrollEmployeeId) { this.EnrollEmployeeId = EnrollEmployeeId; }
	private Integer OptInType; 
	public Integer getOptInType() { return this.OptInType; }
	public void setOptInType(Integer OptInType) { this.OptInType = OptInType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer LifetimeOrderCount; 
	public Integer getLifetimeOrderCount() { return this.LifetimeOrderCount; }
	public void setLifetimeOrderCount(Integer LifetimeOrderCount) { this.LifetimeOrderCount = LifetimeOrderCount; }
	private Double LifetimeSpendAmount; 
	public Double getLifetimeSpendAmount() { return this.LifetimeSpendAmount; }
	public void setLifetimeSpendAmount(Double LifetimeSpendAmount) { this.LifetimeSpendAmount = LifetimeSpendAmount; }
	private String Ident; 
	public String getIdent() { return this.Ident; }
	public void setIdent(String Ident) { this.Ident = Ident; }
	private Double DiscountBalanceLifetime; 
	public Double getDiscountBalanceLifetime() { return this.DiscountBalanceLifetime; }
	public void setDiscountBalanceLifetime(Double DiscountBalanceLifetime) { this.DiscountBalanceLifetime = DiscountBalanceLifetime; }
	private Integer SharedFreeProductCurrent; 
	public Integer getSharedFreeProductCurrent() { return this.SharedFreeProductCurrent; }
	public void setSharedFreeProductCurrent(Integer SharedFreeProductCurrent) { this.SharedFreeProductCurrent = SharedFreeProductCurrent; }
	private Double SharedRewardBalanceCurrent; 
	public Double getSharedRewardBalanceCurrent() { return this.SharedRewardBalanceCurrent; }
	public void setSharedRewardBalanceCurrent(Double SharedRewardBalanceCurrent) { this.SharedRewardBalanceCurrent = SharedRewardBalanceCurrent; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Employee EnrollEmployee;
	public Employee getEnrollEmployee() { return this.EnrollEmployee; }
	public void setEnrollEmployee(Employee EnrollEmployee) { this.EnrollEmployee = EnrollEmployee; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location EnrollLocation;
	public Location getEnrollLocation() { return this.EnrollLocation; }
	public void setEnrollLocation(Location EnrollLocation) { this.EnrollLocation = EnrollLocation; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant EnrollMerchant;
	public Merchant getEnrollMerchant() { return this.EnrollMerchant; }
	public void setEnrollMerchant(Merchant EnrollMerchant) { this.EnrollMerchant = EnrollMerchant; }
    private com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram LoyaltyProgram;
	public LoyaltyProgram getLoyaltyProgram() { return this.LoyaltyProgram; }
	public void setLoyaltyProgram(LoyaltyProgram LoyaltyProgram) { this.LoyaltyProgram = LoyaltyProgram; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction> ConsumerLoyaltyProgramTransactions; 
    public static ConsumerLoyaltyProgram Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                ConsumerLoyaltyProgram entity = new ConsumerLoyaltyProgram();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.ConsumerLoyaltyProgramId = Entities.ParseIntegerValue(obj, "ConsumerLoyaltyProgramId");
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.LoyaltyProgramId = Entities.ParseIntegerValue(obj, "LoyaltyProgramId");
          entity.FreeProductCurrent = Entities.ParseIntegerValue(obj, "FreeProductCurrent");
          entity.PointBalanceCurrent = Entities.ParseDoubleValue(obj, "PointBalanceCurrent");
          entity.PointBalanceLifetime = Entities.ParseDoubleValue(obj, "PointBalanceLifetime");
          entity.RewardBalanceCurrent = Entities.ParseDoubleValue(obj, "RewardBalanceCurrent");
          entity.RewardBalanceLifetime = Entities.ParseDoubleValue(obj, "RewardBalanceLifetime");
          entity.EnrollDateTime = Entities.ParseDateValue(obj, "EnrollDateTime");
          entity.EnrollMerchantId = Entities.ParseIntegerValue(obj, "EnrollMerchantId");
          entity.EnrollLocationId = Entities.ParseIntegerValue(obj, "EnrollLocationId");
          entity.EnrollEmployeeId = Entities.ParseIntegerValue(obj, "EnrollEmployeeId");
          entity.OptInType = Entities.ParseIntegerValue(obj, "OptInType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.LifetimeOrderCount = Entities.ParseIntegerValue(obj, "LifetimeOrderCount");
          entity.LifetimeSpendAmount = Entities.ParseDoubleValue(obj, "LifetimeSpendAmount");
          entity.Ident = Entities.ParseStringValue(obj, "Ident");
          entity.DiscountBalanceLifetime = Entities.ParseDoubleValue(obj, "DiscountBalanceLifetime");
          entity.SharedFreeProductCurrent = Entities.ParseIntegerValue(obj, "SharedFreeProductCurrent");
          entity.SharedRewardBalanceCurrent = Entities.ParseDoubleValue(obj, "SharedRewardBalanceCurrent");
          if (obj.has("EnrollEmployee")){  entity.EnrollEmployee = com.enrollandpay.serviceinterface.BusinessEntity.Employee.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"EnrollEmployee"));}
          if (obj.has("EnrollLocation")){  entity.EnrollLocation = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"EnrollLocation"));}
          if (obj.has("EnrollMerchant")){  entity.EnrollMerchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"EnrollMerchant"));}
          if (obj.has("LoyaltyProgram")){  entity.LoyaltyProgram = com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LoyaltyProgram"));}
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
          JSONArray jsonConsumerLoyaltyProgramTransactions = Entities.ParseJSONArrayValue(obj, "ConsumerLoyaltyProgramTransactions");
                if (jsonConsumerLoyaltyProgramTransactions != null){
                    for (int index = 0; index < jsonConsumerLoyaltyProgramTransactions.length();index++){
                        JSONObject lstItem = jsonConsumerLoyaltyProgramTransactions.getJSONObject(index);
                        entity.ConsumerLoyaltyProgramTransactions.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (ConsumerLoyaltyProgram)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public ConsumerLoyaltyProgram() {
		ConsumerLoyaltyProgramTransactions = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction>();
			}
}
