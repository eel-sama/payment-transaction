package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Location {
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private String Name; 
	public String getName() { return this.Name; }
	public void setName(String Name) { this.Name = Name; }
	private String Address; 
	public String getAddress() { return this.Address; }
	public void setAddress(String Address) { this.Address = Address; }
	private Long PhoneNumber; 
	public Long getPhoneNumber() { return this.PhoneNumber; }
	public void setPhoneNumber(Long PhoneNumber) { this.PhoneNumber = PhoneNumber; }
	private Integer PosType; 
	public Integer getPosType() { return this.PosType; }
	public void setPosType(Integer PosType) { this.PosType = PosType; }
	private String ExternalIdent; 
	public String getExternalIdent() { return this.ExternalIdent; }
	public void setExternalIdent(String ExternalIdent) { this.ExternalIdent = ExternalIdent; }
	private Double UtcOffsetHour; 
	public Double getUtcOffsetHour() { return this.UtcOffsetHour; }
	public void setUtcOffsetHour(Double UtcOffsetHour) { this.UtcOffsetHour = UtcOffsetHour; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Boolean HasThirdPartyPos;
    public Boolean getHasThirdPartyPos() { return this.HasThirdPartyPos; }
    public void setHasThirdPartyPos(Boolean hasThirdPartyPos) {this.HasThirdPartyPos = hasThirdPartyPos; }
    private Integer TimeZoneType;
	public Integer getTimeZoneType() { return this.TimeZoneType; }
	public void setTimeZoneType(Integer TimeZoneType) { this.TimeZoneType = TimeZoneType; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String PosIdent; 
	public String getPosIdent() { return this.PosIdent; }
	public void setPosIdent(String PosIdent) { this.PosIdent = PosIdent; }
	private Integer RewardOfferSupportType; 
	public Integer getRewardOfferSupportType() { return this.RewardOfferSupportType; }
	public void setRewardOfferSupportType(Integer RewardOfferSupportType) { this.RewardOfferSupportType = RewardOfferSupportType; }
	private Integer LanguageType; 
	public Integer getLanguageType() { return this.LanguageType; }
	public void setLanguageType(Integer LanguageType) { this.LanguageType = LanguageType; }
	private Integer CurrencyType; 
	public Integer getCurrencyType() { return this.CurrencyType; }
	public void setCurrencyType(Integer CurrencyType) { this.CurrencyType = CurrencyType; }
	private Date CreateDate; 
	public Date getCreateDate() { return this.CreateDate; }
	public void setCreateDate(Date CreateDate) { this.CreateDate = CreateDate; }
	private Integer PhoneNumberId; 
	public Integer getPhoneNumberId() { return this.PhoneNumberId; }
	public void setPhoneNumberId(Integer PhoneNumberId) { this.PhoneNumberId = PhoneNumberId; }
	private String BotSettings; 
	public String getBotSettings() { return this.BotSettings; }
	public void setBotSettings(String BotSettings) { this.BotSettings = BotSettings; }
	private Double Longitude; 
	public Double getLongitude() { return this.Longitude; }
	public void setLongitude(Double Longitude) { this.Longitude = Longitude; }
	private Double Latitude; 
	public Double getLatitude() { return this.Latitude; }
	public void setLatitude(Double Latitude) { this.Latitude = Latitude; }
    private com.enrollandpay.serviceinterface.BusinessEntity.LocationConfiguration LocationConfiguration;
	public LocationConfiguration getLocationConfiguration() { return this.LocationConfiguration; }
	public void setLocationConfiguration(LocationConfiguration LocationConfiguration) { this.LocationConfiguration = LocationConfiguration; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    private com.enrollandpay.serviceinterface.BusinessEntity.PhoneNumber PhoneNumber1;
	public PhoneNumber getPhoneNumber1() { return this.PhoneNumber1; }
	public void setPhoneNumber1(PhoneNumber PhoneNumber1) { this.PhoneNumber1 = PhoneNumber1; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest> AuthorizationRequests; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Card> Cards; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram> ConsumerLoyaltyPrograms; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Device> Devices; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramParticipant> LoyaltyProgramParticipants; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Offer> Offers; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.EmployeeLocation> EmployeeLocations; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Order> Orders; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Product> Products; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship> ConsumerRelationships; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.DailySaleOverview> DailySaleOverviews; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.SaleOverview> SaleOverviews; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.DailyOfferOverview> DailyOfferOverviews; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OfferOverview> OfferOverviews; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.MessageThread> MessageThreads; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Tax> Taxes; 
    public static Location Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Location entity = new Location();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.Name = Entities.ParseStringValue(obj, "Name");
          entity.Address = Entities.ParseStringValue(obj, "Address");
          entity.PhoneNumber = Entities.ParseLongValue(obj, "PhoneNumber");
          entity.PosType = Entities.ParseIntegerValue(obj, "PosType");
          entity.ExternalIdent = Entities.ParseStringValue(obj, "ExternalIdent");
          entity.UtcOffsetHour = Entities.ParseDoubleValue(obj, "UtcOffsetHour");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.TimeZoneType = Entities.ParseIntegerValue(obj, "TimeZoneType");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.PosIdent = Entities.ParseStringValue(obj, "PosIdent");
          entity.RewardOfferSupportType = Entities.ParseIntegerValue(obj, "RewardOfferSupportType");
          entity.LanguageType = Entities.ParseIntegerValue(obj, "LanguageType");
          entity.HasThirdPartyPos = Entities.ParseBooleanValue(obj, "HasThirdPartyPos");
          entity.CurrencyType = Entities.ParseIntegerValue(obj, "CurrencyType");
          entity.CreateDate = Entities.ParseDateValue(obj, "CreateDate");
          entity.PhoneNumberId = Entities.ParseIntegerValue(obj, "PhoneNumberId");
          entity.BotSettings = Entities.ParseStringValue(obj, "BotSettings");
          entity.Longitude = Entities.ParseDoubleValue(obj, "Longitude");
          entity.Latitude = Entities.ParseDoubleValue(obj, "Latitude");
          if (obj.has("LocationConfiguration")){  entity.LocationConfiguration = com.enrollandpay.serviceinterface.BusinessEntity.LocationConfiguration.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LocationConfiguration"));}
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
          if (obj.has("PhoneNumber1")){  entity.PhoneNumber1 = com.enrollandpay.serviceinterface.BusinessEntity.PhoneNumber.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"PhoneNumber1"));}
          JSONArray jsonAuthorizationRequests = Entities.ParseJSONArrayValue(obj, "AuthorizationRequests");
                if (jsonAuthorizationRequests != null){
                    for (int index = 0; index < jsonAuthorizationRequests.length();index++){
                        JSONObject lstItem = jsonAuthorizationRequests.getJSONObject(index);
                        entity.AuthorizationRequests.add(com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonCards = Entities.ParseJSONArrayValue(obj, "Cards");
                if (jsonCards != null){
                    for (int index = 0; index < jsonCards.length();index++){
                        JSONObject lstItem = jsonCards.getJSONObject(index);
                        entity.Cards.add(com.enrollandpay.serviceinterface.BusinessEntity.Card.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerLoyaltyPrograms = Entities.ParseJSONArrayValue(obj, "ConsumerLoyaltyPrograms");
                if (jsonConsumerLoyaltyPrograms != null){
                    for (int index = 0; index < jsonConsumerLoyaltyPrograms.length();index++){
                        JSONObject lstItem = jsonConsumerLoyaltyPrograms.getJSONObject(index);
                        entity.ConsumerLoyaltyPrograms.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonDevices = Entities.ParseJSONArrayValue(obj, "Devices");
                if (jsonDevices != null){
                    for (int index = 0; index < jsonDevices.length();index++){
                        JSONObject lstItem = jsonDevices.getJSONObject(index);
                        entity.Devices.add(com.enrollandpay.serviceinterface.BusinessEntity.Device.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonLoyaltyProgramParticipants = Entities.ParseJSONArrayValue(obj, "LoyaltyProgramParticipants");
                if (jsonLoyaltyProgramParticipants != null){
                    for (int index = 0; index < jsonLoyaltyProgramParticipants.length();index++){
                        JSONObject lstItem = jsonLoyaltyProgramParticipants.getJSONObject(index);
                        entity.LoyaltyProgramParticipants.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramParticipant.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOffers = Entities.ParseJSONArrayValue(obj, "Offers");
                if (jsonOffers != null){
                    for (int index = 0; index < jsonOffers.length();index++){
                        JSONObject lstItem = jsonOffers.getJSONObject(index);
                        entity.Offers.add(com.enrollandpay.serviceinterface.BusinessEntity.Offer.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonEmployeeLocations = Entities.ParseJSONArrayValue(obj, "EmployeeLocations");
                if (jsonEmployeeLocations != null){
                    for (int index = 0; index < jsonEmployeeLocations.length();index++){
                        JSONObject lstItem = jsonEmployeeLocations.getJSONObject(index);
                        entity.EmployeeLocations.add(com.enrollandpay.serviceinterface.BusinessEntity.EmployeeLocation.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOrders = Entities.ParseJSONArrayValue(obj, "Orders");
                if (jsonOrders != null){
                    for (int index = 0; index < jsonOrders.length();index++){
                        JSONObject lstItem = jsonOrders.getJSONObject(index);
                        entity.Orders.add(com.enrollandpay.serviceinterface.BusinessEntity.Order.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonProducts = Entities.ParseJSONArrayValue(obj, "Products");
                if (jsonProducts != null){
                    for (int index = 0; index < jsonProducts.length();index++){
                        JSONObject lstItem = jsonProducts.getJSONObject(index);
                        entity.Products.add(com.enrollandpay.serviceinterface.BusinessEntity.Product.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerRelationships = Entities.ParseJSONArrayValue(obj, "ConsumerRelationships");
                if (jsonConsumerRelationships != null){
                    for (int index = 0; index < jsonConsumerRelationships.length();index++){
                        JSONObject lstItem = jsonConsumerRelationships.getJSONObject(index);
                        entity.ConsumerRelationships.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonDailySaleOverviews = Entities.ParseJSONArrayValue(obj, "DailySaleOverviews");
                if (jsonDailySaleOverviews != null){
                    for (int index = 0; index < jsonDailySaleOverviews.length();index++){
                        JSONObject lstItem = jsonDailySaleOverviews.getJSONObject(index);
                        entity.DailySaleOverviews.add(com.enrollandpay.serviceinterface.BusinessEntity.DailySaleOverview.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonSaleOverviews = Entities.ParseJSONArrayValue(obj, "SaleOverviews");
                if (jsonSaleOverviews != null){
                    for (int index = 0; index < jsonSaleOverviews.length();index++){
                        JSONObject lstItem = jsonSaleOverviews.getJSONObject(index);
                        entity.SaleOverviews.add(com.enrollandpay.serviceinterface.BusinessEntity.SaleOverview.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonDailyOfferOverviews = Entities.ParseJSONArrayValue(obj, "DailyOfferOverviews");
                if (jsonDailyOfferOverviews != null){
                    for (int index = 0; index < jsonDailyOfferOverviews.length();index++){
                        JSONObject lstItem = jsonDailyOfferOverviews.getJSONObject(index);
                        entity.DailyOfferOverviews.add(com.enrollandpay.serviceinterface.BusinessEntity.DailyOfferOverview.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOfferOverviews = Entities.ParseJSONArrayValue(obj, "OfferOverviews");
                if (jsonOfferOverviews != null){
                    for (int index = 0; index < jsonOfferOverviews.length();index++){
                        JSONObject lstItem = jsonOfferOverviews.getJSONObject(index);
                        entity.OfferOverviews.add(com.enrollandpay.serviceinterface.BusinessEntity.OfferOverview.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMessageThreads = Entities.ParseJSONArrayValue(obj, "MessageThreads");
                if (jsonMessageThreads != null){
                    for (int index = 0; index < jsonMessageThreads.length();index++){
                        JSONObject lstItem = jsonMessageThreads.getJSONObject(index);
                        entity.MessageThreads.add(com.enrollandpay.serviceinterface.BusinessEntity.MessageThread.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonTaxes = Entities.ParseJSONArrayValue(obj, "Taxes");
                if (jsonTaxes != null){
                    for (int index = 0; index < jsonTaxes.length();index++){
                        JSONObject lstItem = jsonTaxes.getJSONObject(index);
                        entity.Taxes.add(com.enrollandpay.serviceinterface.BusinessEntity.Tax.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Location)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Location() {
		AuthorizationRequests = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest>();
			Cards = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Card>();
			ConsumerLoyaltyPrograms = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram>();
			Devices = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Device>();
			LoyaltyProgramParticipants = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramParticipant>();
			Offers = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Offer>();
			EmployeeLocations = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.EmployeeLocation>();
			Orders = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Order>();
			Products = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Product>();
			ConsumerRelationships = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship>();
			DailySaleOverviews = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.DailySaleOverview>();
			SaleOverviews = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.SaleOverview>();
			DailyOfferOverviews = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.DailyOfferOverview>();
			OfferOverviews = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OfferOverview>();
			MessageThreads = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.MessageThread>();
			Taxes = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Tax>();
			}
}
