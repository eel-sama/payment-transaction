package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class LoyaltyProgramShare extends BaseBusinessEntity {
	private String LoyaltyProgramShareId; 
	public String getLoyaltyProgramShareId() { return this.LoyaltyProgramShareId; }
	public void setLoyaltyProgramShareId(String LoyaltyProgramShareId) { this.LoyaltyProgramShareId = LoyaltyProgramShareId; }
	private Integer LoyaltyProgramId; 
	public Integer getLoyaltyProgramId() { return this.LoyaltyProgramId; }
	public void setLoyaltyProgramId(Integer LoyaltyProgramId) { this.LoyaltyProgramId = LoyaltyProgramId; }
	private Integer OfferId; 
	public Integer getOfferId() { return this.OfferId; }
	public void setOfferId(Integer OfferId) { this.OfferId = OfferId; }
	private Double ProductValuedUpTo; 
	public Double getProductValuedUpTo() { return this.ProductValuedUpTo; }
	public void setProductValuedUpTo(Double ProductValuedUpTo) { this.ProductValuedUpTo = ProductValuedUpTo; }
	private Double RewardBalanceLifetime; 
	public Double getRewardBalanceLifetime() { return this.RewardBalanceLifetime; }
	public void setRewardBalanceLifetime(Double RewardBalanceLifetime) { this.RewardBalanceLifetime = RewardBalanceLifetime; }
	private Double RewardBalanceCurrent; 
	public Double getRewardBalanceCurrent() { return this.RewardBalanceCurrent; }
	public void setRewardBalanceCurrent(Double RewardBalanceCurrent) { this.RewardBalanceCurrent = RewardBalanceCurrent; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer ShareTypes; 
	public Integer getShareTypes() { return this.ShareTypes; }
	public void setShareTypes(Integer ShareTypes) { this.ShareTypes = ShareTypes; }
    private com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram LoyaltyProgram;
	public LoyaltyProgram getLoyaltyProgram() { return this.LoyaltyProgram; }
	public void setLoyaltyProgram(LoyaltyProgram LoyaltyProgram) { this.LoyaltyProgram = LoyaltyProgram; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Offer Offer;
	public Offer getOffer() { return this.Offer; }
	public void setOffer(Offer Offer) { this.Offer = Offer; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction> ConsumerLoyaltyProgramTransactions; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareConsumer> LoyaltyProgramShareConsumers; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareRecipient> LoyaltyProgramShareRecipients; 
    public static LoyaltyProgramShare Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                LoyaltyProgramShare entity = new LoyaltyProgramShare();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.LoyaltyProgramShareId = Entities.ParseStringValue(obj, "LoyaltyProgramShareId");
          entity.LoyaltyProgramId = Entities.ParseIntegerValue(obj, "LoyaltyProgramId");
          entity.OfferId = Entities.ParseIntegerValue(obj, "OfferId");
          entity.ProductValuedUpTo = Entities.ParseDoubleValue(obj, "ProductValuedUpTo");
          entity.RewardBalanceLifetime = Entities.ParseDoubleValue(obj, "RewardBalanceLifetime");
          entity.RewardBalanceCurrent = Entities.ParseDoubleValue(obj, "RewardBalanceCurrent");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.ShareTypes = Entities.ParseIntegerValue(obj, "ShareTypes");
          if (obj.has("LoyaltyProgram")){  entity.LoyaltyProgram = com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LoyaltyProgram"));}
          if (obj.has("Offer")){  entity.Offer = com.enrollandpay.serviceinterface.BusinessEntity.Offer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Offer"));}
          JSONArray jsonConsumerLoyaltyProgramTransactions = Entities.ParseJSONArrayValue(obj, "ConsumerLoyaltyProgramTransactions");
                if (jsonConsumerLoyaltyProgramTransactions != null){
                    for (int index = 0; index < jsonConsumerLoyaltyProgramTransactions.length();index++){
                        JSONObject lstItem = jsonConsumerLoyaltyProgramTransactions.getJSONObject(index);
                        entity.ConsumerLoyaltyProgramTransactions.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonLoyaltyProgramShareConsumers = Entities.ParseJSONArrayValue(obj, "LoyaltyProgramShareConsumers");
                if (jsonLoyaltyProgramShareConsumers != null){
                    for (int index = 0; index < jsonLoyaltyProgramShareConsumers.length();index++){
                        JSONObject lstItem = jsonLoyaltyProgramShareConsumers.getJSONObject(index);
                        entity.LoyaltyProgramShareConsumers.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareConsumer.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonLoyaltyProgramShareRecipients = Entities.ParseJSONArrayValue(obj, "LoyaltyProgramShareRecipients");
                if (jsonLoyaltyProgramShareRecipients != null){
                    for (int index = 0; index < jsonLoyaltyProgramShareRecipients.length();index++){
                        JSONObject lstItem = jsonLoyaltyProgramShareRecipients.getJSONObject(index);
                        entity.LoyaltyProgramShareRecipients.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareRecipient.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (LoyaltyProgramShare)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public LoyaltyProgramShare() {
		ConsumerLoyaltyProgramTransactions = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction>();
			LoyaltyProgramShareConsumers = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareConsumer>();
			LoyaltyProgramShareRecipients = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareRecipient>();
			}
}
