package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class ProductAddOn implements Serializable {
	private Integer ProductId; 
	public Integer getProductId() { return this.ProductId; }
	public void setProductId(Integer ProductId) { this.ProductId = ProductId; }
	private Integer AddOnProductId; 
	public Integer getAddOnProductId() { return this.AddOnProductId; }
	public void setAddOnProductId(Integer AddOnProductId) { this.AddOnProductId = AddOnProductId; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Product AddOn;
	public Product getAddOn() { return this.AddOn; }
	public void setAddOn(Product AddOn) { this.AddOn = AddOn; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Product Product;
	public Product getProduct() { return this.Product; }
	public void setProduct(Product Product) { this.Product = Product; }
    public static ProductAddOn Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                ProductAddOn entity = new ProductAddOn();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.ProductId = Entities.ParseIntegerValue(obj, "ProductId");
          entity.AddOnProductId = Entities.ParseIntegerValue(obj, "AddOnProductId");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("AddOn")){  entity.AddOn = com.enrollandpay.serviceinterface.BusinessEntity.Product.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"AddOn"));}
          if (obj.has("Product")){  entity.Product = com.enrollandpay.serviceinterface.BusinessEntity.Product.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Product"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (ProductAddOn)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public ProductAddOn() {
		}
}
