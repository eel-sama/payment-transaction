package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class ProductRelationshipHolder extends BaseBusinessEntity {
	private Integer ProductRelationshipId; 
	public Integer getProductRelationshipId() { return this.ProductRelationshipId; }
	public void setProductRelationshipId(Integer ProductRelationshipId) { this.ProductRelationshipId = ProductRelationshipId; }
	private Integer ProductIdA; 
	public Integer getProductIdA() { return this.ProductIdA; }
	public void setProductIdA(Integer ProductIdA) { this.ProductIdA = ProductIdA; }
	private Integer ProductIdB; 
	public Integer getProductIdB() { return this.ProductIdB; }
	public void setProductIdB(Integer ProductIdB) { this.ProductIdB = ProductIdB; }
	private String EndTime; 
	public String getEndTime() { return this.EndTime; }
	public void setEndTime(String EndTime) { this.EndTime = EndTime; }
	private Date SaleDate; 
	public Date getSaleDate() { return this.SaleDate; }
	public void setSaleDate(Date SaleDate) { this.SaleDate = SaleDate; }
	private Double ProductAQuantitySold; 
	public Double getProductAQuantitySold() { return this.ProductAQuantitySold; }
	public void setProductAQuantitySold(Double ProductAQuantitySold) { this.ProductAQuantitySold = ProductAQuantitySold; }
	private Double ProductBQuantitySold; 
	public Double getProductBQuantitySold() { return this.ProductBQuantitySold; }
	public void setProductBQuantitySold(Double ProductBQuantitySold) { this.ProductBQuantitySold = ProductBQuantitySold; }
	private Double ProductAQuantitySoldLoyalty; 
	public Double getProductAQuantitySoldLoyalty() { return this.ProductAQuantitySoldLoyalty; }
	public void setProductAQuantitySoldLoyalty(Double ProductAQuantitySoldLoyalty) { this.ProductAQuantitySoldLoyalty = ProductAQuantitySoldLoyalty; }
	private Double ProductBQuantitySoldLoyalty; 
	public Double getProductBQuantitySoldLoyalty() { return this.ProductBQuantitySoldLoyalty; }
	public void setProductBQuantitySoldLoyalty(Double ProductBQuantitySoldLoyalty) { this.ProductBQuantitySoldLoyalty = ProductBQuantitySoldLoyalty; }
	private Double ProductAAmount; 
	public Double getProductAAmount() { return this.ProductAAmount; }
	public void setProductAAmount(Double ProductAAmount) { this.ProductAAmount = ProductAAmount; }
	private Double ProductBAmount; 
	public Double getProductBAmount() { return this.ProductBAmount; }
	public void setProductBAmount(Double ProductBAmount) { this.ProductBAmount = ProductBAmount; }
	private Double ProductAAmountLoyalty; 
	public Double getProductAAmountLoyalty() { return this.ProductAAmountLoyalty; }
	public void setProductAAmountLoyalty(Double ProductAAmountLoyalty) { this.ProductAAmountLoyalty = ProductAAmountLoyalty; }
	private Double ProductBAmountLoyalty; 
	public Double getProductBAmountLoyalty() { return this.ProductBAmountLoyalty; }
	public void setProductBAmountLoyalty(Double ProductBAmountLoyalty) { this.ProductBAmountLoyalty = ProductBAmountLoyalty; }
	private Integer OrderCount; 
	public Integer getOrderCount() { return this.OrderCount; }
	public void setOrderCount(Integer OrderCount) { this.OrderCount = OrderCount; }
	private Integer OrderCountLoyalty; 
	public Integer getOrderCountLoyalty() { return this.OrderCountLoyalty; }
	public void setOrderCountLoyalty(Integer OrderCountLoyalty) { this.OrderCountLoyalty = OrderCountLoyalty; }
	private Double OrderAmountTotal; 
	public Double getOrderAmountTotal() { return this.OrderAmountTotal; }
	public void setOrderAmountTotal(Double OrderAmountTotal) { this.OrderAmountTotal = OrderAmountTotal; }
	private Double OrderAmountTotalLoyalty; 
	public Double getOrderAmountTotalLoyalty() { return this.OrderAmountTotalLoyalty; }
	public void setOrderAmountTotalLoyalty(Double OrderAmountTotalLoyalty) { this.OrderAmountTotalLoyalty = OrderAmountTotalLoyalty; }
	private Date UpdateDateTime; 
	public Date getUpdateDateTime() { return this.UpdateDateTime; }
	public void setUpdateDateTime(Date UpdateDateTime) { this.UpdateDateTime = UpdateDateTime; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    public static ProductRelationshipHolder Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                ProductRelationshipHolder entity = new ProductRelationshipHolder();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.ProductRelationshipId = Entities.ParseIntegerValue(obj, "ProductRelationshipId");
          entity.ProductIdA = Entities.ParseIntegerValue(obj, "ProductIdA");
          entity.ProductIdB = Entities.ParseIntegerValue(obj, "ProductIdB");
          entity.EndTime = Entities.ParseStringValue(obj, "EndTime");
          entity.SaleDate = Entities.ParseDateValue(obj, "SaleDate");
          entity.ProductAQuantitySold = Entities.ParseDoubleValue(obj, "ProductAQuantitySold");
          entity.ProductBQuantitySold = Entities.ParseDoubleValue(obj, "ProductBQuantitySold");
          entity.ProductAQuantitySoldLoyalty = Entities.ParseDoubleValue(obj, "ProductAQuantitySoldLoyalty");
          entity.ProductBQuantitySoldLoyalty = Entities.ParseDoubleValue(obj, "ProductBQuantitySoldLoyalty");
          entity.ProductAAmount = Entities.ParseDoubleValue(obj, "ProductAAmount");
          entity.ProductBAmount = Entities.ParseDoubleValue(obj, "ProductBAmount");
          entity.ProductAAmountLoyalty = Entities.ParseDoubleValue(obj, "ProductAAmountLoyalty");
          entity.ProductBAmountLoyalty = Entities.ParseDoubleValue(obj, "ProductBAmountLoyalty");
          entity.OrderCount = Entities.ParseIntegerValue(obj, "OrderCount");
          entity.OrderCountLoyalty = Entities.ParseIntegerValue(obj, "OrderCountLoyalty");
          entity.OrderAmountTotal = Entities.ParseDoubleValue(obj, "OrderAmountTotal");
          entity.OrderAmountTotalLoyalty = Entities.ParseDoubleValue(obj, "OrderAmountTotalLoyalty");
          entity.UpdateDateTime = Entities.ParseDateValue(obj, "UpdateDateTime");
          entity.TS = Entities.ParseStringValue(obj, "TS");
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (ProductRelationshipHolder)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public ProductRelationshipHolder() {
		}
}
