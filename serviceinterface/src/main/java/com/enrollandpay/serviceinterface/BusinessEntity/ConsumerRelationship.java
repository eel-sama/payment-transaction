package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class ConsumerRelationship extends BaseBusinessEntity {
	private Integer ConsumerRelationshipId; 
	public Integer getConsumerRelationshipId() { return this.ConsumerRelationshipId; }
	public void setConsumerRelationshipId(Integer ConsumerRelationshipId) { this.ConsumerRelationshipId = ConsumerRelationshipId; }
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private Date AssignDateTime; 
	public Date getAssignDateTime() { return this.AssignDateTime; }
	public void setAssignDateTime(Date AssignDateTime) { this.AssignDateTime = AssignDateTime; }
	private Integer OptInType; 
	public Integer getOptInType() { return this.OptInType; }
	public void setOptInType(Integer OptInType) { this.OptInType = OptInType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer OrderId; 
	public Integer getOrderId() { return this.OrderId; }
	public void setOrderId(Integer OrderId) { this.OrderId = OrderId; }
	private Integer TotalOrderCount; 
	public Integer getTotalOrderCount() { return this.TotalOrderCount; }
	public void setTotalOrderCount(Integer TotalOrderCount) { this.TotalOrderCount = TotalOrderCount; }
	private Double TotalSpendAmount; 
	public Double getTotalSpendAmount() { return this.TotalSpendAmount; }
	public void setTotalSpendAmount(Double TotalSpendAmount) { this.TotalSpendAmount = TotalSpendAmount; }
	private Integer CoalitionId; 
	public Integer getCoalitionId() { return this.CoalitionId; }
	public void setCoalitionId(Integer CoalitionId) { this.CoalitionId = CoalitionId; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
	public Location getLocation() { return this.Location; }
	public void setLocation(Location Location) { this.Location = Location; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Order Order;
	public Order getOrder() { return this.Order; }
	public void setOrder(Order Order) { this.Order = Order; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Coalition Coalition;
	public Coalition getCoalition() { return this.Coalition; }
	public void setCoalition(Coalition Coalition) { this.Coalition = Coalition; }
    public static ConsumerRelationship Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                ConsumerRelationship entity = new ConsumerRelationship();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.ConsumerRelationshipId = Entities.ParseIntegerValue(obj, "ConsumerRelationshipId");
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.AssignDateTime = Entities.ParseDateValue(obj, "AssignDateTime");
          entity.OptInType = Entities.ParseIntegerValue(obj, "OptInType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.OrderId = Entities.ParseIntegerValue(obj, "OrderId");
          entity.TotalOrderCount = Entities.ParseIntegerValue(obj, "TotalOrderCount");
          entity.TotalSpendAmount = Entities.ParseDoubleValue(obj, "TotalSpendAmount");
          entity.CoalitionId = Entities.ParseIntegerValue(obj, "CoalitionId");
          if (obj.has("Location")){  entity.Location = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Location"));}
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
          if (obj.has("Order")){  entity.Order = com.enrollandpay.serviceinterface.BusinessEntity.Order.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Order"));}
          if (obj.has("Coalition")){  entity.Coalition = com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Coalition"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (ConsumerRelationship)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public ConsumerRelationship() {
		}
}
