package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Card extends BaseBusinessEntity {
	private Integer CardId; 
	public Integer getCardId() { return this.CardId; }
	public void setCardId(Integer CardId) { this.CardId = CardId; }
	private Integer CardType; 
	public Integer getCardType() { return this.CardType; }
	public void setCardType(Integer CardType) { this.CardType = CardType; }
	private String NameOnCard; 
	public String getNameOnCard() { return this.NameOnCard; }
	public void setNameOnCard(String NameOnCard) { this.NameOnCard = NameOnCard; }
	private String PanMasked; 
	public String getPanMasked() { return this.PanMasked; }
	public void setPanMasked(String PanMasked) { this.PanMasked = PanMasked; }
	private String PAR; 
	public String getPAR() { return this.PAR; }
	public void setPAR(String PAR) { this.PAR = PAR; }
	private Date ExpirationDate; 
	public Date getExpirationDate() { return this.ExpirationDate; }
	public void setExpirationDate(Date ExpirationDate) { this.ExpirationDate = ExpirationDate; }
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private String ConsumerApprovalIdent; 
	public String getConsumerApprovalIdent() { return this.ConsumerApprovalIdent; }
	public void setConsumerApprovalIdent(String ConsumerApprovalIdent) { this.ConsumerApprovalIdent = ConsumerApprovalIdent; }
	private Integer LinkedLocationId; 
	public Integer getLinkedLocationId() { return this.LinkedLocationId; }
	public void setLinkedLocationId(Integer LinkedLocationId) { this.LinkedLocationId = LinkedLocationId; }
	private Date LinkedDateTime; 
	public Date getLinkedDateTime() { return this.LinkedDateTime; }
	public void setLinkedDateTime(Date LinkedDateTime) { this.LinkedDateTime = LinkedDateTime; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location LinkedLocation;
	public Location getLinkedLocation() { return this.LinkedLocation; }
	public void setLinkedLocation(Location LinkedLocation) { this.LinkedLocation = LinkedLocation; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.CardEncrypted> CardEncrypteds; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment> OrderPayments; 
    public static Card Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Card entity = new Card();
                lst.put(obj.getString("$id"),entity);
        //Primary
          entity.CardId = Entities.ParseIntegerValue(obj, "CardId");
          entity.CardType = Entities.ParseIntegerValue(obj, "CardType");
          entity.NameOnCard = Entities.ParseStringValue(obj, "NameOnCard");
          entity.PanMasked = Entities.ParseStringValue(obj, "PanMasked");
          entity.PAR = Entities.ParseStringValue(obj, "PAR");
          entity.ExpirationDate = Entities.ParseDateValue(obj, "ExpirationDate");
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.ConsumerApprovalIdent = Entities.ParseStringValue(obj, "ConsumerApprovalIdent");
          entity.LinkedLocationId = Entities.ParseIntegerValue(obj, "LinkedLocationId");
          entity.LinkedDateTime = Entities.ParseDateValue(obj, "LinkedDateTime");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("LinkedLocation")){  entity.LinkedLocation = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LinkedLocation"));}
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
          JSONArray jsonCardEncrypteds = Entities.ParseJSONArrayValue(obj, "CardEncrypteds");
                if (jsonCardEncrypteds != null){
                    for (int index = 0; index < jsonCardEncrypteds.length();index++){
                        JSONObject lstItem = jsonCardEncrypteds.getJSONObject(index);
                        entity.CardEncrypteds.add(com.enrollandpay.serviceinterface.BusinessEntity.CardEncrypted.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOrderPayments = Entities.ParseJSONArrayValue(obj, "OrderPayments");
                if (jsonOrderPayments != null){
                    for (int index = 0; index < jsonOrderPayments.length();index++){
                        JSONObject lstItem = jsonOrderPayments.getJSONObject(index);
                        entity.OrderPayments.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Card)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Card() {
		CardEncrypteds = new ArrayList<>();
        OrderPayments = new ArrayList<>();
    }
}
