package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Merchant extends BaseBusinessEntity {
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private String Name; 
	public String getName() { return this.Name; }
	public void setName(String Name) { this.Name = Name; }
	private String NameNewsletter; 
	public String getNameNewsletter() { return this.NameNewsletter; }
	public void setNameNewsletter(String NameNewsletter) { this.NameNewsletter = NameNewsletter; }
	private String ExternalIdent; 
	public String getExternalIdent() { return this.ExternalIdent; }
	public void setExternalIdent(String ExternalIdent) { this.ExternalIdent = ExternalIdent; }
	private Boolean HasMultipleLocations; 
	public Boolean getHasMultipleLocations() { return this.HasMultipleLocations; }
	public void setHasMultipleLocations(Boolean HasMultipleLocations) { this.HasMultipleLocations = HasMultipleLocations; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private Integer AskQuestionCount; 
	public Integer getAskQuestionCount() { return this.AskQuestionCount; }
	public void setAskQuestionCount(Integer AskQuestionCount) { this.AskQuestionCount = AskQuestionCount; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer TimeZoneType; 
	public Integer getTimeZoneType() { return this.TimeZoneType; }
	public void setTimeZoneType(Integer TimeZoneType) { this.TimeZoneType = TimeZoneType; }
	private Double UtcOffsetHour; 
	public Double getUtcOffsetHour() { return this.UtcOffsetHour; }
	public void setUtcOffsetHour(Double UtcOffsetHour) { this.UtcOffsetHour = UtcOffsetHour; }
	private Integer CurrencyType; 
	public Integer getCurrencyType() { return this.CurrencyType; }
	public void setCurrencyType(Integer CurrencyType) { this.CurrencyType = CurrencyType; }
	private String PortalDomain; 
	public String getPortalDomain() { return this.PortalDomain; }
	public void setPortalDomain(String PortalDomain) { this.PortalDomain = PortalDomain; }
	private Integer PhoneNumberId; 
	public Integer getPhoneNumberId() { return this.PhoneNumberId; }
	public void setPhoneNumberId(Integer PhoneNumberId) { this.PhoneNumberId = PhoneNumberId; }
	private String BotSettings; 
	public String getBotSettings() { return this.BotSettings; }
	public void setBotSettings(String BotSettings) { this.BotSettings = BotSettings; }
    private com.enrollandpay.serviceinterface.BusinessEntity.MerchantConfiguration MerchantConfiguration;
	public MerchantConfiguration getMerchantConfiguration() { return this.MerchantConfiguration; }
	public void setMerchantConfiguration(MerchantConfiguration MerchantConfiguration) { this.MerchantConfiguration = MerchantConfiguration; }
    private com.enrollandpay.serviceinterface.BusinessEntity.PhoneNumber PhoneNumber;
	public PhoneNumber getPhoneNumber() { return this.PhoneNumber; }
	public void setPhoneNumber(PhoneNumber PhoneNumber) { this.PhoneNumber = PhoneNumber; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest> AuthorizationRequests; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram> ConsumerLoyaltyPrograms; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Device> Devices; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Employee> Employees; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Enrollment> Enrollments; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Location> Locations; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram> LoyaltyPrograms; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramParticipant> LoyaltyProgramParticipants; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Offer> Offers; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Question> Questions; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Product> Products; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship> ConsumerRelationships; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.DailySaleOverview> DailySaleOverviews; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.SaleOverview> SaleOverviews; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.DailyOfferOverview> DailyOfferOverviews; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OfferOverview> OfferOverviews; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.CoalitionMerchant> CoalitionMerchants; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.MerchantAccess> MerchantAccesses; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Invoice> Invoices; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.MessageThread> MessageThreads; 
    public static Merchant Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Merchant entity = new Merchant();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.Name = Entities.ParseStringValue(obj, "Name");
          entity.NameNewsletter = Entities.ParseStringValue(obj, "NameNewsletter");
          entity.ExternalIdent = Entities.ParseStringValue(obj, "ExternalIdent");
          entity.HasMultipleLocations = Entities.ParseBooleanValue(obj, "HasMultipleLocations");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.AskQuestionCount = Entities.ParseIntegerValue(obj, "AskQuestionCount");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.TimeZoneType = Entities.ParseIntegerValue(obj, "TimeZoneType");
          entity.UtcOffsetHour = Entities.ParseDoubleValue(obj, "UtcOffsetHour");
          entity.CurrencyType = Entities.ParseIntegerValue(obj, "CurrencyType");
          entity.PortalDomain = Entities.ParseStringValue(obj, "PortalDomain");
          entity.PhoneNumberId = Entities.ParseIntegerValue(obj, "PhoneNumberId");
          entity.BotSettings = Entities.ParseStringValue(obj, "BotSettings");
          if (obj.has("MerchantConfiguration")){  entity.MerchantConfiguration = com.enrollandpay.serviceinterface.BusinessEntity.MerchantConfiguration.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"MerchantConfiguration"));}
          if (obj.has("PhoneNumber")){  entity.PhoneNumber = com.enrollandpay.serviceinterface.BusinessEntity.PhoneNumber.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"PhoneNumber"));}
          JSONArray jsonAuthorizationRequests = Entities.ParseJSONArrayValue(obj, "AuthorizationRequests");
                if (jsonAuthorizationRequests != null){
                    for (int index = 0; index < jsonAuthorizationRequests.length();index++){
                        JSONObject lstItem = jsonAuthorizationRequests.getJSONObject(index);
                        entity.AuthorizationRequests.add(com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerLoyaltyPrograms = Entities.ParseJSONArrayValue(obj, "ConsumerLoyaltyPrograms");
                if (jsonConsumerLoyaltyPrograms != null){
                    for (int index = 0; index < jsonConsumerLoyaltyPrograms.length();index++){
                        JSONObject lstItem = jsonConsumerLoyaltyPrograms.getJSONObject(index);
                        entity.ConsumerLoyaltyPrograms.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonDevices = Entities.ParseJSONArrayValue(obj, "Devices");
                if (jsonDevices != null){
                    for (int index = 0; index < jsonDevices.length();index++){
                        JSONObject lstItem = jsonDevices.getJSONObject(index);
                        entity.Devices.add(com.enrollandpay.serviceinterface.BusinessEntity.Device.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonEmployees = Entities.ParseJSONArrayValue(obj, "Employees");
                if (jsonEmployees != null){
                    for (int index = 0; index < jsonEmployees.length();index++){
                        JSONObject lstItem = jsonEmployees.getJSONObject(index);
                        entity.Employees.add(com.enrollandpay.serviceinterface.BusinessEntity.Employee.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonEnrollments = Entities.ParseJSONArrayValue(obj, "Enrollments");
                if (jsonEnrollments != null){
                    for (int index = 0; index < jsonEnrollments.length();index++){
                        JSONObject lstItem = jsonEnrollments.getJSONObject(index);
                        entity.Enrollments.add(com.enrollandpay.serviceinterface.BusinessEntity.Enrollment.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonLocations = Entities.ParseJSONArrayValue(obj, "Locations");
                if (jsonLocations != null){
                    for (int index = 0; index < jsonLocations.length();index++){
                        JSONObject lstItem = jsonLocations.getJSONObject(index);
                        entity.Locations.add(com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonLoyaltyPrograms = Entities.ParseJSONArrayValue(obj, "LoyaltyPrograms");
                if (jsonLoyaltyPrograms != null){
                    for (int index = 0; index < jsonLoyaltyPrograms.length();index++){
                        JSONObject lstItem = jsonLoyaltyPrograms.getJSONObject(index);
                        entity.LoyaltyPrograms.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonLoyaltyProgramParticipants = Entities.ParseJSONArrayValue(obj, "LoyaltyProgramParticipants");
                if (jsonLoyaltyProgramParticipants != null){
                    for (int index = 0; index < jsonLoyaltyProgramParticipants.length();index++){
                        JSONObject lstItem = jsonLoyaltyProgramParticipants.getJSONObject(index);
                        entity.LoyaltyProgramParticipants.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramParticipant.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOffers = Entities.ParseJSONArrayValue(obj, "Offers");
                if (jsonOffers != null){
                    for (int index = 0; index < jsonOffers.length();index++){
                        JSONObject lstItem = jsonOffers.getJSONObject(index);
                        entity.Offers.add(com.enrollandpay.serviceinterface.BusinessEntity.Offer.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonQuestions = Entities.ParseJSONArrayValue(obj, "Questions");
                if (jsonQuestions != null){
                    for (int index = 0; index < jsonQuestions.length();index++){
                        JSONObject lstItem = jsonQuestions.getJSONObject(index);
                        entity.Questions.add(com.enrollandpay.serviceinterface.BusinessEntity.Question.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonProducts = Entities.ParseJSONArrayValue(obj, "Products");
                if (jsonProducts != null){
                    for (int index = 0; index < jsonProducts.length();index++){
                        JSONObject lstItem = jsonProducts.getJSONObject(index);
                        entity.Products.add(com.enrollandpay.serviceinterface.BusinessEntity.Product.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerRelationships = Entities.ParseJSONArrayValue(obj, "ConsumerRelationships");
                if (jsonConsumerRelationships != null){
                    for (int index = 0; index < jsonConsumerRelationships.length();index++){
                        JSONObject lstItem = jsonConsumerRelationships.getJSONObject(index);
                        entity.ConsumerRelationships.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonDailySaleOverviews = Entities.ParseJSONArrayValue(obj, "DailySaleOverviews");
                if (jsonDailySaleOverviews != null){
                    for (int index = 0; index < jsonDailySaleOverviews.length();index++){
                        JSONObject lstItem = jsonDailySaleOverviews.getJSONObject(index);
                        entity.DailySaleOverviews.add(com.enrollandpay.serviceinterface.BusinessEntity.DailySaleOverview.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonSaleOverviews = Entities.ParseJSONArrayValue(obj, "SaleOverviews");
                if (jsonSaleOverviews != null){
                    for (int index = 0; index < jsonSaleOverviews.length();index++){
                        JSONObject lstItem = jsonSaleOverviews.getJSONObject(index);
                        entity.SaleOverviews.add(com.enrollandpay.serviceinterface.BusinessEntity.SaleOverview.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonDailyOfferOverviews = Entities.ParseJSONArrayValue(obj, "DailyOfferOverviews");
                if (jsonDailyOfferOverviews != null){
                    for (int index = 0; index < jsonDailyOfferOverviews.length();index++){
                        JSONObject lstItem = jsonDailyOfferOverviews.getJSONObject(index);
                        entity.DailyOfferOverviews.add(com.enrollandpay.serviceinterface.BusinessEntity.DailyOfferOverview.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOfferOverviews = Entities.ParseJSONArrayValue(obj, "OfferOverviews");
                if (jsonOfferOverviews != null){
                    for (int index = 0; index < jsonOfferOverviews.length();index++){
                        JSONObject lstItem = jsonOfferOverviews.getJSONObject(index);
                        entity.OfferOverviews.add(com.enrollandpay.serviceinterface.BusinessEntity.OfferOverview.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonCoalitionMerchants = Entities.ParseJSONArrayValue(obj, "CoalitionMerchants");
                if (jsonCoalitionMerchants != null){
                    for (int index = 0; index < jsonCoalitionMerchants.length();index++){
                        JSONObject lstItem = jsonCoalitionMerchants.getJSONObject(index);
                        entity.CoalitionMerchants.add(com.enrollandpay.serviceinterface.BusinessEntity.CoalitionMerchant.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMerchantAccesses = Entities.ParseJSONArrayValue(obj, "MerchantAccesses");
                if (jsonMerchantAccesses != null){
                    for (int index = 0; index < jsonMerchantAccesses.length();index++){
                        JSONObject lstItem = jsonMerchantAccesses.getJSONObject(index);
                        entity.MerchantAccesses.add(com.enrollandpay.serviceinterface.BusinessEntity.MerchantAccess.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonInvoices = Entities.ParseJSONArrayValue(obj, "Invoices");
                if (jsonInvoices != null){
                    for (int index = 0; index < jsonInvoices.length();index++){
                        JSONObject lstItem = jsonInvoices.getJSONObject(index);
                        entity.Invoices.add(com.enrollandpay.serviceinterface.BusinessEntity.Invoice.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMessageThreads = Entities.ParseJSONArrayValue(obj, "MessageThreads");
                if (jsonMessageThreads != null){
                    for (int index = 0; index < jsonMessageThreads.length();index++){
                        JSONObject lstItem = jsonMessageThreads.getJSONObject(index);
                        entity.MessageThreads.add(com.enrollandpay.serviceinterface.BusinessEntity.MessageThread.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Merchant)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Merchant() {
		AuthorizationRequests = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest>();
			ConsumerLoyaltyPrograms = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram>();
			Devices = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Device>();
			Employees = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Employee>();
			Enrollments = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Enrollment>();
			Locations = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Location>();
			LoyaltyPrograms = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram>();
			LoyaltyProgramParticipants = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramParticipant>();
			Offers = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Offer>();
			Questions = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Question>();
			Products = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Product>();
			ConsumerRelationships = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship>();
			DailySaleOverviews = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.DailySaleOverview>();
			SaleOverviews = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.SaleOverview>();
			DailyOfferOverviews = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.DailyOfferOverview>();
			OfferOverviews = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OfferOverview>();
			CoalitionMerchants = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.CoalitionMerchant>();
			MerchantAccesses = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.MerchantAccess>();
			Invoices = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Invoice>();
			MessageThreads = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.MessageThread>();
			}
}
