package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Device extends BaseBusinessEntity {
	private Integer DeviceId; 
	public Integer getDeviceId() { return this.DeviceId; }
	public void setDeviceId(Integer DeviceId) { this.DeviceId = DeviceId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private String Name; 
	public String getName() { return this.Name; }
	public void setName(String Name) { this.Name = Name; }
	private Integer DeviceType; 
	public Integer getDeviceType() { return this.DeviceType; }
	public void setDeviceType(Integer DeviceType) { this.DeviceType = DeviceType; }
	private String HardwareIdentity; 
	public String getHardwareIdentity() { return this.HardwareIdentity; }
	public void setHardwareIdentity(String HardwareIdentity) { this.HardwareIdentity = HardwareIdentity; }
	private String PairingIdent; 
	public String getPairingIdent() { return this.PairingIdent; }
	public void setPairingIdent(String PairingIdent) { this.PairingIdent = PairingIdent; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer RelayDeviceId; 
	public Integer getRelayDeviceId() { return this.RelayDeviceId; }
	public void setRelayDeviceId(Integer RelayDeviceId) { this.RelayDeviceId = RelayDeviceId; }
	private Integer PaymentDeviceId; 
	public Integer getPaymentDeviceId() { return this.PaymentDeviceId; }
	public void setPaymentDeviceId(Integer PaymentDeviceId) { this.PaymentDeviceId = PaymentDeviceId; }
	private String PosTerminalIdents; 
	public String getPosTerminalIdents() { return this.PosTerminalIdents; }
	public void setPosTerminalIdents(String PosTerminalIdents) { this.PosTerminalIdents = PosTerminalIdents; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
	public Location getLocation() { return this.Location; }
	public void setLocation(Location Location) { this.Location = Location; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Device PaymentDevice;
	public Device getPaymentDevice() { return this.PaymentDevice; }
	public void setPaymentDevice(Device PaymentDevice) { this.PaymentDevice = PaymentDevice; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Device RelayDevice;
	public Device getRelayDevice() { return this.RelayDevice; }
	public void setRelayDevice(Device RelayDevice) { this.RelayDevice = RelayDevice; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess> ClientAccesses; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest> AuthorizationRequests; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Device> PaymentDevices; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Device> RelayDevices; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Order> PaymentOrders; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Order> PosOrders; 
    public static Device Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Device entity = new Device();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.DeviceId = Entities.ParseIntegerValue(obj, "DeviceId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.Name = Entities.ParseStringValue(obj, "Name");
          entity.DeviceType = Entities.ParseIntegerValue(obj, "DeviceType");
          entity.HardwareIdentity = Entities.ParseStringValue(obj, "HardwareIdentity");
          entity.PairingIdent = Entities.ParseStringValue(obj, "PairingIdent");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.RelayDeviceId = Entities.ParseIntegerValue(obj, "RelayDeviceId");
          entity.PaymentDeviceId = Entities.ParseIntegerValue(obj, "PaymentDeviceId");
          entity.PosTerminalIdents = Entities.ParseStringValue(obj, "PosTerminalIdents");
          if (obj.has("Location")){  entity.Location = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Location"));}
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
          if (obj.has("PaymentDevice")){  entity.PaymentDevice = com.enrollandpay.serviceinterface.BusinessEntity.Device.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"PaymentDevice"));}
          if (obj.has("RelayDevice")){  entity.RelayDevice = com.enrollandpay.serviceinterface.BusinessEntity.Device.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"RelayDevice"));}
          JSONArray jsonClientAccesses = Entities.ParseJSONArrayValue(obj, "ClientAccesses");
                if (jsonClientAccesses != null){
                    for (int index = 0; index < jsonClientAccesses.length();index++){
                        JSONObject lstItem = jsonClientAccesses.getJSONObject(index);
                        entity.ClientAccesses.add(com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonAuthorizationRequests = Entities.ParseJSONArrayValue(obj, "AuthorizationRequests");
                if (jsonAuthorizationRequests != null){
                    for (int index = 0; index < jsonAuthorizationRequests.length();index++){
                        JSONObject lstItem = jsonAuthorizationRequests.getJSONObject(index);
                        entity.AuthorizationRequests.add(com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonPaymentDevices = Entities.ParseJSONArrayValue(obj, "PaymentDevices");
                if (jsonPaymentDevices != null){
                    for (int index = 0; index < jsonPaymentDevices.length();index++){
                        JSONObject lstItem = jsonPaymentDevices.getJSONObject(index);
                        entity.PaymentDevices.add(com.enrollandpay.serviceinterface.BusinessEntity.Device.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonRelayDevices = Entities.ParseJSONArrayValue(obj, "RelayDevices");
                if (jsonRelayDevices != null){
                    for (int index = 0; index < jsonRelayDevices.length();index++){
                        JSONObject lstItem = jsonRelayDevices.getJSONObject(index);
                        entity.RelayDevices.add(com.enrollandpay.serviceinterface.BusinessEntity.Device.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonPaymentOrders = Entities.ParseJSONArrayValue(obj, "PaymentOrders");
                if (jsonPaymentOrders != null){
                    for (int index = 0; index < jsonPaymentOrders.length();index++){
                        JSONObject lstItem = jsonPaymentOrders.getJSONObject(index);
                        entity.PaymentOrders.add(com.enrollandpay.serviceinterface.BusinessEntity.Order.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonPosOrders = Entities.ParseJSONArrayValue(obj, "PosOrders");
                if (jsonPosOrders != null){
                    for (int index = 0; index < jsonPosOrders.length();index++){
                        JSONObject lstItem = jsonPosOrders.getJSONObject(index);
                        entity.PosOrders.add(com.enrollandpay.serviceinterface.BusinessEntity.Order.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Device)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Device() {
		ClientAccesses = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess>();
			AuthorizationRequests = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest>();
			PaymentDevices = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Device>();
			RelayDevices = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Device>();
			PaymentOrders = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Order>();
			PosOrders = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Order>();
			}
}
