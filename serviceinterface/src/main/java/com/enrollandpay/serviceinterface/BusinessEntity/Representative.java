package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Representative extends BaseBusinessEntity {
	private Integer RepresentativeId; 
	public Integer getRepresentativeId() { return this.RepresentativeId; }
	public void setRepresentativeId(Integer RepresentativeId) { this.RepresentativeId = RepresentativeId; }
	private Integer CoalitionId; 
	public Integer getCoalitionId() { return this.CoalitionId; }
	public void setCoalitionId(Integer CoalitionId) { this.CoalitionId = CoalitionId; }
	private String NamePrefix; 
	public String getNamePrefix() { return this.NamePrefix; }
	public void setNamePrefix(String NamePrefix) { this.NamePrefix = NamePrefix; }
	private String NameFirst; 
	public String getNameFirst() { return this.NameFirst; }
	public void setNameFirst(String NameFirst) { this.NameFirst = NameFirst; }
	private String NameMiddle; 
	public String getNameMiddle() { return this.NameMiddle; }
	public void setNameMiddle(String NameMiddle) { this.NameMiddle = NameMiddle; }
	private String NameLast; 
	public String getNameLast() { return this.NameLast; }
	public void setNameLast(String NameLast) { this.NameLast = NameLast; }
	private String NameSuffix; 
	public String getNameSuffix() { return this.NameSuffix; }
	public void setNameSuffix(String NameSuffix) { this.NameSuffix = NameSuffix; }
	private String EmailAddress; 
	public String getEmailAddress() { return this.EmailAddress; }
	public void setEmailAddress(String EmailAddress) { this.EmailAddress = EmailAddress; }
	private String PhoneNumber; 
	public String getPhoneNumber() { return this.PhoneNumber; }
	public void setPhoneNumber(String PhoneNumber) { this.PhoneNumber = PhoneNumber; }
	private Integer AccessType; 
	public Integer getAccessType() { return this.AccessType; }
	public void setAccessType(Integer AccessType) { this.AccessType = AccessType; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private Integer LanguageType; 
	public Integer getLanguageType() { return this.LanguageType; }
	public void setLanguageType(Integer LanguageType) { this.LanguageType = LanguageType; }
	private Integer RepresentativeType; 
	public Integer getRepresentativeType() { return this.RepresentativeType; }
	public void setRepresentativeType(Integer RepresentativeType) { this.RepresentativeType = RepresentativeType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Coalition Coalition;
	public Coalition getCoalition() { return this.Coalition; }
	public void setCoalition(Coalition Coalition) { this.Coalition = Coalition; }
    private com.enrollandpay.serviceinterface.BusinessEntity.RepresentativeAuthentication RepresentativeAuthentication;
	public RepresentativeAuthentication getRepresentativeAuthentication() { return this.RepresentativeAuthentication; }
	public void setRepresentativeAuthentication(RepresentativeAuthentication RepresentativeAuthentication) { this.RepresentativeAuthentication = RepresentativeAuthentication; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.TeamRepresentative> TeamRepresentatives; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess> ClientAccesses; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.MerchantAccess> MerchantAccesses; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.EmployeeImpersonation> EmployeeImpersonations; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.CoalitionMerchant> CoalitionMerchants; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Message> Messages; 
    public static Representative Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Representative entity = new Representative();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.RepresentativeId = Entities.ParseIntegerValue(obj, "RepresentativeId");
          entity.CoalitionId = Entities.ParseIntegerValue(obj, "CoalitionId");
          entity.NamePrefix = Entities.ParseStringValue(obj, "NamePrefix");
          entity.NameFirst = Entities.ParseStringValue(obj, "NameFirst");
          entity.NameMiddle = Entities.ParseStringValue(obj, "NameMiddle");
          entity.NameLast = Entities.ParseStringValue(obj, "NameLast");
          entity.NameSuffix = Entities.ParseStringValue(obj, "NameSuffix");
          entity.EmailAddress = Entities.ParseStringValue(obj, "EmailAddress");
          entity.PhoneNumber = Entities.ParseStringValue(obj, "PhoneNumber");
          entity.AccessType = Entities.ParseIntegerValue(obj, "AccessType");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.LanguageType = Entities.ParseIntegerValue(obj, "LanguageType");
          entity.RepresentativeType = Entities.ParseIntegerValue(obj, "RepresentativeType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Coalition")){  entity.Coalition = com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Coalition"));}
          if (obj.has("RepresentativeAuthentication")){  entity.RepresentativeAuthentication = com.enrollandpay.serviceinterface.BusinessEntity.RepresentativeAuthentication.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"RepresentativeAuthentication"));}
          JSONArray jsonTeamRepresentatives = Entities.ParseJSONArrayValue(obj, "TeamRepresentatives");
                if (jsonTeamRepresentatives != null){
                    for (int index = 0; index < jsonTeamRepresentatives.length();index++){
                        JSONObject lstItem = jsonTeamRepresentatives.getJSONObject(index);
                        entity.TeamRepresentatives.add(com.enrollandpay.serviceinterface.BusinessEntity.TeamRepresentative.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonClientAccesses = Entities.ParseJSONArrayValue(obj, "ClientAccesses");
                if (jsonClientAccesses != null){
                    for (int index = 0; index < jsonClientAccesses.length();index++){
                        JSONObject lstItem = jsonClientAccesses.getJSONObject(index);
                        entity.ClientAccesses.add(com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMerchantAccesses = Entities.ParseJSONArrayValue(obj, "MerchantAccesses");
                if (jsonMerchantAccesses != null){
                    for (int index = 0; index < jsonMerchantAccesses.length();index++){
                        JSONObject lstItem = jsonMerchantAccesses.getJSONObject(index);
                        entity.MerchantAccesses.add(com.enrollandpay.serviceinterface.BusinessEntity.MerchantAccess.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonEmployeeImpersonations = Entities.ParseJSONArrayValue(obj, "EmployeeImpersonations");
                if (jsonEmployeeImpersonations != null){
                    for (int index = 0; index < jsonEmployeeImpersonations.length();index++){
                        JSONObject lstItem = jsonEmployeeImpersonations.getJSONObject(index);
                        entity.EmployeeImpersonations.add(com.enrollandpay.serviceinterface.BusinessEntity.EmployeeImpersonation.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonCoalitionMerchants = Entities.ParseJSONArrayValue(obj, "CoalitionMerchants");
                if (jsonCoalitionMerchants != null){
                    for (int index = 0; index < jsonCoalitionMerchants.length();index++){
                        JSONObject lstItem = jsonCoalitionMerchants.getJSONObject(index);
                        entity.CoalitionMerchants.add(com.enrollandpay.serviceinterface.BusinessEntity.CoalitionMerchant.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMessages = Entities.ParseJSONArrayValue(obj, "Messages");
                if (jsonMessages != null){
                    for (int index = 0; index < jsonMessages.length();index++){
                        JSONObject lstItem = jsonMessages.getJSONObject(index);
                        entity.Messages.add(com.enrollandpay.serviceinterface.BusinessEntity.Message.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Representative)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Representative() {
		TeamRepresentatives = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.TeamRepresentative>();
			ClientAccesses = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess>();
			MerchantAccesses = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.MerchantAccess>();
			EmployeeImpersonations = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.EmployeeImpersonation>();
			CoalitionMerchants = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.CoalitionMerchant>();
			Messages = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Message>();
			}
}
