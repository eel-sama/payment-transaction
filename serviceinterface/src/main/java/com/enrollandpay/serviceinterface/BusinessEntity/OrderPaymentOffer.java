package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class OrderPaymentOffer extends BaseBusinessEntity {
	private Integer OrderPaymentOfferId; 
	public Integer getOrderPaymentOfferId() { return this.OrderPaymentOfferId; }
	public void setOrderPaymentOfferId(Integer OrderPaymentOfferId) { this.OrderPaymentOfferId = OrderPaymentOfferId; }
	private Integer OrderPaymentId; 
	public Integer getOrderPaymentId() { return this.OrderPaymentId; }
	public void setOrderPaymentId(Integer OrderPaymentId) { this.OrderPaymentId = OrderPaymentId; }
	private Integer OfferId; 
	public Integer getOfferId() { return this.OfferId; }
	public void setOfferId(Integer OfferId) { this.OfferId = OfferId; }
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private String StatusInformation; 
	public String getStatusInformation() { return this.StatusInformation; }
	public void setStatusInformation(String StatusInformation) { this.StatusInformation = StatusInformation; }
	private String Display; 
	public String getDisplay() { return this.Display; }
	public void setDisplay(String Display) { this.Display = Display; }
	private Integer ReceiveType; 
	public Integer getReceiveType() { return this.ReceiveType; }
	public void setReceiveType(Integer ReceiveType) { this.ReceiveType = ReceiveType; }
	private Double ReceiveAmount; 
	public Double getReceiveAmount() { return this.ReceiveAmount; }
	public void setReceiveAmount(Double ReceiveAmount) { this.ReceiveAmount = ReceiveAmount; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Offer Offer;
	public Offer getOffer() { return this.Offer; }
	public void setOffer(Offer Offer) { this.Offer = Offer; }
    private com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment OrderPayment;
	public OrderPayment getOrderPayment() { return this.OrderPayment; }
	public void setOrderPayment(OrderPayment OrderPayment) { this.OrderPayment = OrderPayment; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    public static OrderPaymentOffer Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                OrderPaymentOffer entity = new OrderPaymentOffer();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.OrderPaymentOfferId = Entities.ParseIntegerValue(obj, "OrderPaymentOfferId");
          entity.OrderPaymentId = Entities.ParseIntegerValue(obj, "OrderPaymentId");
          entity.OfferId = Entities.ParseIntegerValue(obj, "OfferId");
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.StatusInformation = Entities.ParseStringValue(obj, "StatusInformation");
          entity.Display = Entities.ParseStringValue(obj, "Display");
          entity.ReceiveType = Entities.ParseIntegerValue(obj, "ReceiveType");
          entity.ReceiveAmount = Entities.ParseDoubleValue(obj, "ReceiveAmount");
          if (obj.has("Offer")){  entity.Offer = com.enrollandpay.serviceinterface.BusinessEntity.Offer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Offer"));}
          if (obj.has("OrderPayment")){  entity.OrderPayment = com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"OrderPayment"));}
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (OrderPaymentOffer)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public OrderPaymentOffer() {
		}
}
