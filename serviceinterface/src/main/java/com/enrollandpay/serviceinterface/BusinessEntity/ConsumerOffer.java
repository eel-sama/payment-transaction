package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class ConsumerOffer extends BaseBusinessEntity {
	private Integer ConsumerOfferId; 
	public Integer getConsumerOfferId() { return this.ConsumerOfferId; }
	public void setConsumerOfferId(Integer ConsumerOfferId) { this.ConsumerOfferId = ConsumerOfferId; }
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private Integer OfferId; 
	public Integer getOfferId() { return this.OfferId; }
	public void setOfferId(Integer OfferId) { this.OfferId = OfferId; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer AvailableUses; 
	public Integer getAvailableUses() { return this.AvailableUses; }
	public void setAvailableUses(Integer AvailableUses) { this.AvailableUses = AvailableUses; }
	private String OrderIds; 
	public String getOrderIds() { return this.OrderIds; }
	public void setOrderIds(String OrderIds) { this.OrderIds = OrderIds; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Offer Offer;
	public Offer getOffer() { return this.Offer; }
	public void setOffer(Offer Offer) { this.Offer = Offer; }
    public static ConsumerOffer Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                ConsumerOffer entity = new ConsumerOffer();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.ConsumerOfferId = Entities.ParseIntegerValue(obj, "ConsumerOfferId");
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.OfferId = Entities.ParseIntegerValue(obj, "OfferId");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.AvailableUses = Entities.ParseIntegerValue(obj, "AvailableUses");
          entity.OrderIds = Entities.ParseStringValue(obj, "OrderIds");
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
          if (obj.has("Offer")){  entity.Offer = com.enrollandpay.serviceinterface.BusinessEntity.Offer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Offer"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (ConsumerOffer)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public ConsumerOffer() {
		}
}
