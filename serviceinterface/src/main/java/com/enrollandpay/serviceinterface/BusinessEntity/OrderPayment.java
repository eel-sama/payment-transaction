package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;
import java.util.Map;

public class OrderPayment extends BaseBusinessEntity {
	private Integer OrderPaymentId; 
	public Integer getOrderPaymentId() { return this.OrderPaymentId; }
	public void setOrderPaymentId(Integer OrderPaymentId) { this.OrderPaymentId = OrderPaymentId; }
	private Integer OrderId; 
	public Integer getOrderId() { return this.OrderId; }
	public void setOrderId(Integer OrderId) { this.OrderId = OrderId; }
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private Integer CardId; 
	public Integer getCardId() { return this.CardId; }
	public void setCardId(Integer CardId) { this.CardId = CardId; }
	private Double AmountTotal; 
	public Double getAmountTotal() { return this.AmountTotal; }
	public void setAmountTotal(Double AmountTotal) { this.AmountTotal = AmountTotal; }
	private Double AmountTax; 
	public Double getAmountTax() { return this.AmountTax; }
	public void setAmountTax(Double AmountTax) { this.AmountTax = AmountTax; }
	private Double AmountGratuity; 
	public Double getAmountGratuity() { return this.AmountGratuity; }
	public void setAmountGratuity(Double AmountGratuity) { this.AmountGratuity = AmountGratuity; }
	private Double AmountTendered; 
	public Double getAmountTendered() { return this.AmountTendered; }
	public void setAmountTendered(Double AmountTendered) { this.AmountTendered = AmountTendered; }
	private Double AmountReward; 
	public Double getAmountReward() { return this.AmountReward; }
	public void setAmountReward(Double AmountReward) { this.AmountReward = AmountReward; }
    private Double AmountReturned;
    public Double getAmountReturned() { return this.AmountReturned; }
    public void setAmountReturned(Double AmountReturned) { this.AmountReturned = AmountReturned; }
	private String PosIdent; 
	public String getPosIdent() { return this.PosIdent; }
	public void setPosIdent(String PosIdent) { this.PosIdent = PosIdent; }
	private String ReferenceIdent; 
	public String getReferenceIdent() { return this.ReferenceIdent; }
	public void setReferenceIdent(String ReferenceIdent) { this.ReferenceIdent = ReferenceIdent; }
	private String ApprovalIdent; 
	public String getApprovalIdent() { return this.ApprovalIdent; }
	public void setApprovalIdent(String ApprovalIdent) { this.ApprovalIdent = ApprovalIdent; }
	private String VoidIdent; 
	public String getVoidIdent() { return this.VoidIdent; }
	public void setVoidIdent(String VoidIdent) { this.VoidIdent = VoidIdent; }
	private Integer PaymentType; 
	public Integer getPaymentType() { return this.PaymentType; }
	public void setPaymentType(Integer PaymentType) { this.PaymentType = PaymentType; }
	private Integer ApprovalType; 
	public Integer getApprovalType() { return this.ApprovalType; }
	public void setApprovalType(Integer ApprovalType) { this.ApprovalType = ApprovalType; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Double AmountDue; 
	public Double getAmountDue() { return this.AmountDue; }
	public void setAmountDue(Double AmountDue) { this.AmountDue = AmountDue; }
	private String StatusInformation; 
	public String getStatusInformation() { return this.StatusInformation; }
	public void setStatusInformation(String StatusInformation) { this.StatusInformation = StatusInformation; }
	private String Signature;
    public String getSignature() {        return Signature;    }
    public void setSignature(String signature) {        Signature = signature;    }
    private String Commands;
	public String getCommands() { return this.Commands; }
	public void setCommands(String Commands) { this.Commands = Commands; }
	private String CardEncryptedIdent; 
	public String getCardEncryptedIdent() { return this.CardEncryptedIdent; }
	public void setCardEncryptedIdent(String CardEncryptedIdent) { this.CardEncryptedIdent = CardEncryptedIdent; }
	private Double AmountLoyaltyUsed; 
	public Double getAmountLoyaltyUsed() { return this.AmountLoyaltyUsed; }
	public void setAmountLoyaltyUsed(Double AmountLoyaltyUsed) { this.AmountLoyaltyUsed = AmountLoyaltyUsed; }
	private Double AmountDiscount; 
	public Double getAmountDiscount() { return this.AmountDiscount; }
	public void setAmountDiscount(Double AmountDiscount) { this.AmountDiscount = AmountDiscount; }
	private String DiscountLog; 
	public String getDiscountLog() { return this.DiscountLog; }
	public void setDiscountLog(String DiscountLog) { this.DiscountLog = DiscountLog; }
	private Double AmountGratuity2; 
	public Double getAmountGratuity2() { return this.AmountGratuity2; }
	public void setAmountGratuity2(Double AmountGratuity2) { this.AmountGratuity2 = AmountGratuity2; }
	private Double AmountGratuity3; 
	public Double getAmountGratuity3() { return this.AmountGratuity3; }
	public void setAmountGratuity3(Double AmountGratuity3) { this.AmountGratuity3 = AmountGratuity3; }
	private Double AmountSurcharge; 
	public Double getAmountSurcharge() { return this.AmountSurcharge; }
	public void setAmountSurcharge(Double AmountSurcharge) { this.AmountSurcharge = AmountSurcharge; }
	private Integer RequestType; 
	public Integer getRequestType() { return this.RequestType; }
	public void setRequestType(Integer RequestType) { this.RequestType = RequestType; }
	private String BatchIdent; 
	public String getBatchIdent() { return this.BatchIdent; }
	public void setBatchIdent(String BatchIdent) { this.BatchIdent = BatchIdent; }
	private String ReceiptAddress; 
	public String getReceiptAddress() { return this.ReceiptAddress; }
	public void setReceiptAddress(String ReceiptAddress) { this.ReceiptAddress = ReceiptAddress; }
	private Integer ReceiptType; 
	public Integer getReceiptType() { return this.ReceiptType; }
	public void setReceiptType(Integer ReceiptType) { this.ReceiptType = ReceiptType; }
	private Map<String,Object> AdditionalAttributes;
    public Map<String, Object> getAdditionalAttributes() {        return AdditionalAttributes;    }
    public void setAdditionalAttributes(Map<String, Object> additionalAttributes) {        AdditionalAttributes = additionalAttributes;    }
    private com.enrollandpay.serviceinterface.BusinessEntity.Card Card;
	public Card getCard() { return this.Card; }
	public void setCard(Card Card) { this.Card = Card; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Order Order;
	public Order getOrder() { return this.Order; }
	public void setOrder(Order Order) { this.Order = Order; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction> ConsumerLoyaltyProgramTransactions; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentOffer> OrderPaymentOffers; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentLoyaltyProgram> OrderPaymentLoyaltyPrograms; 
    public static OrderPayment Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                OrderPayment entity = new OrderPayment();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.OrderPaymentId = Entities.ParseIntegerValue(obj, "OrderPaymentId");
          entity.OrderId = Entities.ParseIntegerValue(obj, "OrderId");
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.CardId = Entities.ParseIntegerValue(obj, "CardId");
          entity.AmountTotal = Entities.ParseDoubleValue(obj, "AmountTotal");
          entity.AmountTax = Entities.ParseDoubleValue(obj, "AmountTax");
          entity.AmountGratuity = Entities.ParseDoubleValue(obj, "AmountGratuity");
          entity.AmountTendered = Entities.ParseDoubleValue(obj, "AmountTendered");
          entity.AmountReward = Entities.ParseDoubleValue(obj, "AmountReward");
          entity.PosIdent = Entities.ParseStringValue(obj, "PosIdent");
          entity.ReferenceIdent = Entities.ParseStringValue(obj, "ReferenceIdent");
          entity.ApprovalIdent = Entities.ParseStringValue(obj, "ApprovalIdent");
          entity.VoidIdent = Entities.ParseStringValue(obj, "VoidIdent");
          entity.PaymentType = Entities.ParseIntegerValue(obj, "PaymentType");
          entity.ApprovalType = Entities.ParseIntegerValue(obj, "ApprovalType");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.AmountDue = Entities.ParseDoubleValue(obj, "AmountDue");
          entity.StatusInformation = Entities.ParseStringValue(obj, "StatusInformation");
          entity.Commands = Entities.ParseStringValue(obj, "Commands");
          entity.CardEncryptedIdent = Entities.ParseStringValue(obj, "CardEncryptedIdent");
          entity.AmountLoyaltyUsed = Entities.ParseDoubleValue(obj, "AmountLoyaltyUsed");
          entity.AmountDiscount = Entities.ParseDoubleValue(obj, "AmountDiscount");
          entity.DiscountLog = Entities.ParseStringValue(obj, "DiscountLog");
          entity.AmountGratuity2 = Entities.ParseDoubleValue(obj, "AmountGratuity2");
          entity.AmountGratuity3 = Entities.ParseDoubleValue(obj, "AmountGratuity3");
          entity.AmountSurcharge = Entities.ParseDoubleValue(obj, "AmountSurcharge");
          entity.RequestType = Entities.ParseIntegerValue(obj, "RequestType");
          entity.BatchIdent = Entities.ParseStringValue(obj, "BatchIdent");
          entity.ReceiptAddress = Entities.ParseStringValue(obj, "ReceiptAddress");
          entity.AdditionalAttributes = Entities.ParseMapValue(obj,"AdditionalAttributes");
          entity.ReceiptType = Entities.ParseIntegerValue(obj, "ReceiptType");
          entity.Signature = Entities.ParseStringValue(obj, "Signature");
          if (obj.has("Card")){  entity.Card = com.enrollandpay.serviceinterface.BusinessEntity.Card.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Card"));}
          if (obj.has("Order")){  entity.Order = com.enrollandpay.serviceinterface.BusinessEntity.Order.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Order"));}
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
          JSONArray jsonConsumerLoyaltyProgramTransactions = Entities.ParseJSONArrayValue(obj, "ConsumerLoyaltyProgramTransactions");
                if (jsonConsumerLoyaltyProgramTransactions != null){
                    for (int index = 0; index < jsonConsumerLoyaltyProgramTransactions.length();index++){
                        JSONObject lstItem = jsonConsumerLoyaltyProgramTransactions.getJSONObject(index);
                        entity.ConsumerLoyaltyProgramTransactions.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOrderPaymentOffers = Entities.ParseJSONArrayValue(obj, "OrderPaymentOffers");
                if (jsonOrderPaymentOffers != null){
                    for (int index = 0; index < jsonOrderPaymentOffers.length();index++){
                        JSONObject lstItem = jsonOrderPaymentOffers.getJSONObject(index);
                        entity.OrderPaymentOffers.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentOffer.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOrderPaymentLoyaltyPrograms = Entities.ParseJSONArrayValue(obj, "OrderPaymentLoyaltyPrograms");
                if (jsonOrderPaymentLoyaltyPrograms != null){
                    for (int index = 0; index < jsonOrderPaymentLoyaltyPrograms.length();index++){
                        JSONObject lstItem = jsonOrderPaymentLoyaltyPrograms.getJSONObject(index);
                        entity.OrderPaymentLoyaltyPrograms.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentLoyaltyProgram.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (OrderPayment)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public OrderPayment() {
		ConsumerLoyaltyProgramTransactions = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction>();
			OrderPaymentOffers = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentOffer>();
			OrderPaymentLoyaltyPrograms = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentLoyaltyProgram>();
			}
}
