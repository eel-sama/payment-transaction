package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Message extends BaseBusinessEntity {
	private String MessageId; 
	public String getMessageId() { return this.MessageId; }
	public void setMessageId(String MessageId) { this.MessageId = MessageId; }
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private Integer OrderId; 
	public Integer getOrderId() { return this.OrderId; }
	public void setOrderId(Integer OrderId) { this.OrderId = OrderId; }
	private String EmailAddress; 
	public String getEmailAddress() { return this.EmailAddress; }
	public void setEmailAddress(String EmailAddress) { this.EmailAddress = EmailAddress; }
	private String PhoneNumber; 
	public String getPhoneNumber() { return this.PhoneNumber; }
	public void setPhoneNumber(String PhoneNumber) { this.PhoneNumber = PhoneNumber; }
	private Integer MessageType; 
	public Integer getMessageType() { return this.MessageType; }
	public void setMessageType(Integer MessageType) { this.MessageType = MessageType; }
	private Date SendDateTime; 
	public Date getSendDateTime() { return this.SendDateTime; }
	public void setSendDateTime(Date SendDateTime) { this.SendDateTime = SendDateTime; }
	private Date FirstReadDateTime; 
	public Date getFirstReadDateTime() { return this.FirstReadDateTime; }
	public void setFirstReadDateTime(Date FirstReadDateTime) { this.FirstReadDateTime = FirstReadDateTime; }
	private Date LastReadDateTime; 
	public Date getLastReadDateTime() { return this.LastReadDateTime; }
	public void setLastReadDateTime(Date LastReadDateTime) { this.LastReadDateTime = LastReadDateTime; }
	private String ReadDateTimes; 
	public String getReadDateTimes() { return this.ReadDateTimes; }
	public void setReadDateTimes(String ReadDateTimes) { this.ReadDateTimes = ReadDateTimes; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer LoyaltyProgramId; 
	public Integer getLoyaltyProgramId() { return this.LoyaltyProgramId; }
	public void setLoyaltyProgramId(Integer LoyaltyProgramId) { this.LoyaltyProgramId = LoyaltyProgramId; }
	private String Details; 
	public String getDetails() { return this.Details; }
	public void setDetails(String Details) { this.Details = Details; }
	private Integer OfferId; 
	public Integer getOfferId() { return this.OfferId; }
	public void setOfferId(Integer OfferId) { this.OfferId = OfferId; }
	private String MessageThreadId; 
	public String getMessageThreadId() { return this.MessageThreadId; }
	public void setMessageThreadId(String MessageThreadId) { this.MessageThreadId = MessageThreadId; }
	private Integer EmployeeId; 
	public Integer getEmployeeId() { return this.EmployeeId; }
	public void setEmployeeId(Integer EmployeeId) { this.EmployeeId = EmployeeId; }
	private Integer RepresentativeId; 
	public Integer getRepresentativeId() { return this.RepresentativeId; }
	public void setRepresentativeId(Integer RepresentativeId) { this.RepresentativeId = RepresentativeId; }
	private Integer MessageSenderType; 
	public Integer getMessageSenderType() { return this.MessageSenderType; }
	public void setMessageSenderType(Integer MessageSenderType) { this.MessageSenderType = MessageSenderType; }
	private Integer MessageInteractionTypes; 
	public Integer getMessageInteractionTypes() { return this.MessageInteractionTypes; }
	public void setMessageInteractionTypes(Integer MessageInteractionTypes) { this.MessageInteractionTypes = MessageInteractionTypes; }
	private Integer MessageChannelType; 
	public Integer getMessageChannelType() { return this.MessageChannelType; }
	public void setMessageChannelType(Integer MessageChannelType) { this.MessageChannelType = MessageChannelType; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Order Order;
	public Order getOrder() { return this.Order; }
	public void setOrder(Order Order) { this.Order = Order; }
    private com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram LoyaltyProgram;
	public LoyaltyProgram getLoyaltyProgram() { return this.LoyaltyProgram; }
	public void setLoyaltyProgram(LoyaltyProgram LoyaltyProgram) { this.LoyaltyProgram = LoyaltyProgram; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Offer Offer;
	public Offer getOffer() { return this.Offer; }
	public void setOffer(Offer Offer) { this.Offer = Offer; }
    private com.enrollandpay.serviceinterface.BusinessEntity.MessageThread MessageThread;
	public MessageThread getMessageThread() { return this.MessageThread; }
	public void setMessageThread(MessageThread MessageThread) { this.MessageThread = MessageThread; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Employee Employee;
	public Employee getEmployee() { return this.Employee; }
	public void setEmployee(Employee Employee) { this.Employee = Employee; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Representative Representative;
	public Representative getRepresentative() { return this.Representative; }
	public void setRepresentative(Representative Representative) { this.Representative = Representative; }
    public static Message Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Message entity = new Message();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.MessageId = Entities.ParseStringValue(obj, "MessageId");
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.OrderId = Entities.ParseIntegerValue(obj, "OrderId");
          entity.EmailAddress = Entities.ParseStringValue(obj, "EmailAddress");
          entity.PhoneNumber = Entities.ParseStringValue(obj, "PhoneNumber");
          entity.MessageType = Entities.ParseIntegerValue(obj, "MessageType");
          entity.SendDateTime = Entities.ParseDateValue(obj, "SendDateTime");
          entity.FirstReadDateTime = Entities.ParseDateValue(obj, "FirstReadDateTime");
          entity.LastReadDateTime = Entities.ParseDateValue(obj, "LastReadDateTime");
          entity.ReadDateTimes = Entities.ParseStringValue(obj, "ReadDateTimes");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.LoyaltyProgramId = Entities.ParseIntegerValue(obj, "LoyaltyProgramId");
          entity.Details = Entities.ParseStringValue(obj, "Details");
          entity.OfferId = Entities.ParseIntegerValue(obj, "OfferId");
          entity.MessageThreadId = Entities.ParseStringValue(obj, "MessageThreadId");
          entity.EmployeeId = Entities.ParseIntegerValue(obj, "EmployeeId");
          entity.RepresentativeId = Entities.ParseIntegerValue(obj, "RepresentativeId");
          entity.MessageSenderType = Entities.ParseIntegerValue(obj, "MessageSenderType");
          entity.MessageInteractionTypes = Entities.ParseIntegerValue(obj, "MessageInteractionTypes");
          entity.MessageChannelType = Entities.ParseIntegerValue(obj, "MessageChannelType");
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
          if (obj.has("Order")){  entity.Order = com.enrollandpay.serviceinterface.BusinessEntity.Order.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Order"));}
          if (obj.has("LoyaltyProgram")){  entity.LoyaltyProgram = com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LoyaltyProgram"));}
          if (obj.has("Offer")){  entity.Offer = com.enrollandpay.serviceinterface.BusinessEntity.Offer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Offer"));}
          if (obj.has("MessageThread")){  entity.MessageThread = com.enrollandpay.serviceinterface.BusinessEntity.MessageThread.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"MessageThread"));}
          if (obj.has("Employee")){  entity.Employee = com.enrollandpay.serviceinterface.BusinessEntity.Employee.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Employee"));}
          if (obj.has("Representative")){  entity.Representative = com.enrollandpay.serviceinterface.BusinessEntity.Representative.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Representative"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Message)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Message() {
		}
}
