package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Offer extends BaseBusinessEntity {
	private Integer OfferId; 
	public Integer getOfferId() { return this.OfferId; }
	public void setOfferId(Integer OfferId) { this.OfferId = OfferId; }
	private Integer LoyaltyProgramId; 
	public Integer getLoyaltyProgramId() { return this.LoyaltyProgramId; }
	public void setLoyaltyProgramId(Integer LoyaltyProgramId) { this.LoyaltyProgramId = LoyaltyProgramId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private String NamePrivate; 
	public String getNamePrivate() { return this.NamePrivate; }
	public void setNamePrivate(String NamePrivate) { this.NamePrivate = NamePrivate; }
	private String Display; 
	public String getDisplay() { return this.Display; }
	public void setDisplay(String Display) { this.Display = Display; }
	private Integer ReceiveType; 
	public Integer getReceiveType() { return this.ReceiveType; }
	public void setReceiveType(Integer ReceiveType) { this.ReceiveType = ReceiveType; }
	private Double ReceiveAmount; 
	public Double getReceiveAmount() { return this.ReceiveAmount; }
	public void setReceiveAmount(Double ReceiveAmount) { this.ReceiveAmount = ReceiveAmount; }
	private Integer ReceiveExpireType; 
	public Integer getReceiveExpireType() { return this.ReceiveExpireType; }
	public void setReceiveExpireType(Integer ReceiveExpireType) { this.ReceiveExpireType = ReceiveExpireType; }
	private Integer ReceiveExpireAmount; 
	public Integer getReceiveExpireAmount() { return this.ReceiveExpireAmount; }
	public void setReceiveExpireAmount(Integer ReceiveExpireAmount) { this.ReceiveExpireAmount = ReceiveExpireAmount; }
	private Integer ValidForType; 
	public Integer getValidForType() { return this.ValidForType; }
	public void setValidForType(Integer ValidForType) { this.ValidForType = ValidForType; }
	private Integer QualifierType; 
	public Integer getQualifierType() { return this.QualifierType; }
	public void setQualifierType(Integer QualifierType) { this.QualifierType = QualifierType; }
	private Double QualifierAmount; 
	public Double getQualifierAmount() { return this.QualifierAmount; }
	public void setQualifierAmount(Double QualifierAmount) { this.QualifierAmount = QualifierAmount; }
	private Double MinOrderAmount; 
	public Double getMinOrderAmount() { return this.MinOrderAmount; }
	public void setMinOrderAmount(Double MinOrderAmount) { this.MinOrderAmount = MinOrderAmount; }
	private Double MaxOrderAmount; 
	public Double getMaxOrderAmount() { return this.MaxOrderAmount; }
	public void setMaxOrderAmount(Double MaxOrderAmount) { this.MaxOrderAmount = MaxOrderAmount; }
	private String QualifierRuleset; 
	public String getQualifierRuleset() { return this.QualifierRuleset; }
	public void setQualifierRuleset(String QualifierRuleset) { this.QualifierRuleset = QualifierRuleset; }
	private Date StartDate; 
	public Date getStartDate() { return this.StartDate; }
	public void setStartDate(Date StartDate) { this.StartDate = StartDate; }
	private Date EndDate; 
	public Date getEndDate() { return this.EndDate; }
	public void setEndDate(Date EndDate) { this.EndDate = EndDate; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Double MaxReceiveAmount; 
	public Double getMaxReceiveAmount() { return this.MaxReceiveAmount; }
	public void setMaxReceiveAmount(Double MaxReceiveAmount) { this.MaxReceiveAmount = MaxReceiveAmount; }
	private String ApproveIdent; 
	public String getApproveIdent() { return this.ApproveIdent; }
	public void setApproveIdent(String ApproveIdent) { this.ApproveIdent = ApproveIdent; }
	private Boolean IsDisplayAuto; 
	public Boolean getIsDisplayAuto() { return this.IsDisplayAuto; }
	public void setIsDisplayAuto(Boolean IsDisplayAuto) { this.IsDisplayAuto = IsDisplayAuto; }
	private String PaymentRuleset; 
	public String getPaymentRuleset() { return this.PaymentRuleset; }
	public void setPaymentRuleset(String PaymentRuleset) { this.PaymentRuleset = PaymentRuleset; }
	private Integer OfferType; 
	public Integer getOfferType() { return this.OfferType; }
	public void setOfferType(Integer OfferType) { this.OfferType = OfferType; }
	private Integer MaxRedemptionsPerOrder; 
	public Integer getMaxRedemptionsPerOrder() { return this.MaxRedemptionsPerOrder; }
	public void setMaxRedemptionsPerOrder(Integer MaxRedemptionsPerOrder) { this.MaxRedemptionsPerOrder = MaxRedemptionsPerOrder; }
	private String TargetedRecipients; 
	public String getTargetedRecipients() { return this.TargetedRecipients; }
	public void setTargetedRecipients(String TargetedRecipients) { this.TargetedRecipients = TargetedRecipients; }
	private Integer SponsorId; 
	public Integer getSponsorId() { return this.SponsorId; }
	public void setSponsorId(Integer SponsorId) { this.SponsorId = SponsorId; }
	private String Settings; 
	public String getSettings() { return this.Settings; }
	public void setSettings(String Settings) { this.Settings = Settings; }
	private Integer PromotionalType; 
	public Integer getPromotionalType() { return this.PromotionalType; }
	public void setPromotionalType(Integer PromotionalType) { this.PromotionalType = PromotionalType; }
	private Integer UseCount; 
	public Integer getUseCount() { return this.UseCount; }
	public void setUseCount(Integer UseCount) { this.UseCount = UseCount; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
	public Location getLocation() { return this.Location; }
	public void setLocation(Location Location) { this.Location = Location; }
    private com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram LoyaltyProgram;
	public LoyaltyProgram getLoyaltyProgram() { return this.LoyaltyProgram; }
	public void setLoyaltyProgram(LoyaltyProgram LoyaltyProgram) { this.LoyaltyProgram = LoyaltyProgram; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Sponsor Sponsor;
	public Sponsor getSponsor() { return this.Sponsor; }
	public void setSponsor(Sponsor Sponsor) { this.Sponsor = Sponsor; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OfferAssociation> OfferAssociations; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentOffer> OrderPaymentOffers; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction> ConsumerLoyaltyProgramTransactions; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.DailyOfferOverview> DailyOfferOverviews; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OfferOverview> OfferOverviews; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Message> Messages; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerOffer> ConsumerOffers; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShare> LoyaltyProgramShares; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.MessageThread> MessageThreads; 
    public static Offer Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Offer entity = new Offer();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.OfferId = Entities.ParseIntegerValue(obj, "OfferId");
          entity.LoyaltyProgramId = Entities.ParseIntegerValue(obj, "LoyaltyProgramId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.NamePrivate = Entities.ParseStringValue(obj, "NamePrivate");
          entity.Display = Entities.ParseStringValue(obj, "Display");
          entity.ReceiveType = Entities.ParseIntegerValue(obj, "ReceiveType");
          entity.ReceiveAmount = Entities.ParseDoubleValue(obj, "ReceiveAmount");
          entity.ReceiveExpireType = Entities.ParseIntegerValue(obj, "ReceiveExpireType");
          entity.ReceiveExpireAmount = Entities.ParseIntegerValue(obj, "ReceiveExpireAmount");
          entity.ValidForType = Entities.ParseIntegerValue(obj, "ValidForType");
          entity.QualifierType = Entities.ParseIntegerValue(obj, "QualifierType");
          entity.QualifierAmount = Entities.ParseDoubleValue(obj, "QualifierAmount");
          entity.MinOrderAmount = Entities.ParseDoubleValue(obj, "MinOrderAmount");
          entity.MaxOrderAmount = Entities.ParseDoubleValue(obj, "MaxOrderAmount");
          entity.QualifierRuleset = Entities.ParseStringValue(obj, "QualifierRuleset");
          entity.StartDate = Entities.ParseDateValue(obj, "StartDate");
          entity.EndDate = Entities.ParseDateValue(obj, "EndDate");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.MaxReceiveAmount = Entities.ParseDoubleValue(obj, "MaxReceiveAmount");
          entity.ApproveIdent = Entities.ParseStringValue(obj, "ApproveIdent");
          entity.IsDisplayAuto = Entities.ParseBooleanValue(obj, "IsDisplayAuto");
          entity.PaymentRuleset = Entities.ParseStringValue(obj, "PaymentRuleset");
          entity.OfferType = Entities.ParseIntegerValue(obj, "OfferType");
          entity.MaxRedemptionsPerOrder = Entities.ParseIntegerValue(obj, "MaxRedemptionsPerOrder");
          entity.TargetedRecipients = Entities.ParseStringValue(obj, "TargetedRecipients");
          entity.SponsorId = Entities.ParseIntegerValue(obj, "SponsorId");
          entity.Settings = Entities.ParseStringValue(obj, "Settings");
          entity.PromotionalType = Entities.ParseIntegerValue(obj, "PromotionalType");
          entity.UseCount = Entities.ParseIntegerValue(obj, "UseCount");
          if (obj.has("Location")){  entity.Location = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Location"));}
          if (obj.has("LoyaltyProgram")){  entity.LoyaltyProgram = com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LoyaltyProgram"));}
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
          if (obj.has("Sponsor")){  entity.Sponsor = com.enrollandpay.serviceinterface.BusinessEntity.Sponsor.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Sponsor"));}
          JSONArray jsonOfferAssociations = Entities.ParseJSONArrayValue(obj, "OfferAssociations");
                if (jsonOfferAssociations != null){
                    for (int index = 0; index < jsonOfferAssociations.length();index++){
                        JSONObject lstItem = jsonOfferAssociations.getJSONObject(index);
                        entity.OfferAssociations.add(com.enrollandpay.serviceinterface.BusinessEntity.OfferAssociation.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOrderPaymentOffers = Entities.ParseJSONArrayValue(obj, "OrderPaymentOffers");
                if (jsonOrderPaymentOffers != null){
                    for (int index = 0; index < jsonOrderPaymentOffers.length();index++){
                        JSONObject lstItem = jsonOrderPaymentOffers.getJSONObject(index);
                        entity.OrderPaymentOffers.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentOffer.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerLoyaltyProgramTransactions = Entities.ParseJSONArrayValue(obj, "ConsumerLoyaltyProgramTransactions");
                if (jsonConsumerLoyaltyProgramTransactions != null){
                    for (int index = 0; index < jsonConsumerLoyaltyProgramTransactions.length();index++){
                        JSONObject lstItem = jsonConsumerLoyaltyProgramTransactions.getJSONObject(index);
                        entity.ConsumerLoyaltyProgramTransactions.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonDailyOfferOverviews = Entities.ParseJSONArrayValue(obj, "DailyOfferOverviews");
                if (jsonDailyOfferOverviews != null){
                    for (int index = 0; index < jsonDailyOfferOverviews.length();index++){
                        JSONObject lstItem = jsonDailyOfferOverviews.getJSONObject(index);
                        entity.DailyOfferOverviews.add(com.enrollandpay.serviceinterface.BusinessEntity.DailyOfferOverview.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOfferOverviews = Entities.ParseJSONArrayValue(obj, "OfferOverviews");
                if (jsonOfferOverviews != null){
                    for (int index = 0; index < jsonOfferOverviews.length();index++){
                        JSONObject lstItem = jsonOfferOverviews.getJSONObject(index);
                        entity.OfferOverviews.add(com.enrollandpay.serviceinterface.BusinessEntity.OfferOverview.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMessages = Entities.ParseJSONArrayValue(obj, "Messages");
                if (jsonMessages != null){
                    for (int index = 0; index < jsonMessages.length();index++){
                        JSONObject lstItem = jsonMessages.getJSONObject(index);
                        entity.Messages.add(com.enrollandpay.serviceinterface.BusinessEntity.Message.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerOffers = Entities.ParseJSONArrayValue(obj, "ConsumerOffers");
                if (jsonConsumerOffers != null){
                    for (int index = 0; index < jsonConsumerOffers.length();index++){
                        JSONObject lstItem = jsonConsumerOffers.getJSONObject(index);
                        entity.ConsumerOffers.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerOffer.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonLoyaltyProgramShares = Entities.ParseJSONArrayValue(obj, "LoyaltyProgramShares");
                if (jsonLoyaltyProgramShares != null){
                    for (int index = 0; index < jsonLoyaltyProgramShares.length();index++){
                        JSONObject lstItem = jsonLoyaltyProgramShares.getJSONObject(index);
                        entity.LoyaltyProgramShares.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShare.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMessageThreads = Entities.ParseJSONArrayValue(obj, "MessageThreads");
                if (jsonMessageThreads != null){
                    for (int index = 0; index < jsonMessageThreads.length();index++){
                        JSONObject lstItem = jsonMessageThreads.getJSONObject(index);
                        entity.MessageThreads.add(com.enrollandpay.serviceinterface.BusinessEntity.MessageThread.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Offer)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Offer() {
		OfferAssociations = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OfferAssociation>();
			OrderPaymentOffers = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentOffer>();
			ConsumerLoyaltyProgramTransactions = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction>();
			DailyOfferOverviews = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.DailyOfferOverview>();
			OfferOverviews = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OfferOverview>();
			Messages = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Message>();
			ConsumerOffers = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerOffer>();
			LoyaltyProgramShares = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShare>();
			MessageThreads = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.MessageThread>();
			}
}
