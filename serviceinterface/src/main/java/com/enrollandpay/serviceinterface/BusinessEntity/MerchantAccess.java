package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class MerchantAccess extends BaseBusinessEntity {
	private Integer MerchantAccessId; 
	public Integer getMerchantAccessId() { return this.MerchantAccessId; }
	public void setMerchantAccessId(Integer MerchantAccessId) { this.MerchantAccessId = MerchantAccessId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private Integer SalesRepresentativeId; 
	public Integer getSalesRepresentativeId() { return this.SalesRepresentativeId; }
	public void setSalesRepresentativeId(Integer SalesRepresentativeId) { this.SalesRepresentativeId = SalesRepresentativeId; }
	private Integer TeamId; 
	public Integer getTeamId() { return this.TeamId; }
	public void setTeamId(Integer TeamId) { this.TeamId = TeamId; }
	private Integer AccessType; 
	public Integer getAccessType() { return this.AccessType; }
	public void setAccessType(Integer AccessType) { this.AccessType = AccessType; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private Boolean IsPrimary; 
	public Boolean getIsPrimary() { return this.IsPrimary; }
	public void setIsPrimary(Boolean IsPrimary) { this.IsPrimary = IsPrimary; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Representative Representative;
	public Representative getRepresentative() { return this.Representative; }
	public void setRepresentative(Representative Representative) { this.Representative = Representative; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Team Team;
	public Team getTeam() { return this.Team; }
	public void setTeam(Team Team) { this.Team = Team; }
    public static MerchantAccess Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                MerchantAccess entity = new MerchantAccess();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.MerchantAccessId = Entities.ParseIntegerValue(obj, "MerchantAccessId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.SalesRepresentativeId = Entities.ParseIntegerValue(obj, "SalesRepresentativeId");
          entity.TeamId = Entities.ParseIntegerValue(obj, "TeamId");
          entity.AccessType = Entities.ParseIntegerValue(obj, "AccessType");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.IsPrimary = Entities.ParseBooleanValue(obj, "IsPrimary");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
          if (obj.has("Representative")){  entity.Representative = com.enrollandpay.serviceinterface.BusinessEntity.Representative.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Representative"));}
          if (obj.has("Team")){  entity.Team = com.enrollandpay.serviceinterface.BusinessEntity.Team.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Team"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (MerchantAccess)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public MerchantAccess() {
		}
}
