package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class OfferOverview extends BaseBusinessEntity {
	private Integer OfferOverviewId; 
	public Integer getOfferOverviewId() { return this.OfferOverviewId; }
	public void setOfferOverviewId(Integer OfferOverviewId) { this.OfferOverviewId = OfferOverviewId; }
	private Integer OfferId; 
	public Integer getOfferId() { return this.OfferId; }
	public void setOfferId(Integer OfferId) { this.OfferId = OfferId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private Integer PointsEarned; 
	public Integer getPointsEarned() { return this.PointsEarned; }
	public void setPointsEarned(Integer PointsEarned) { this.PointsEarned = PointsEarned; }
	private Integer PointsSpent; 
	public Integer getPointsSpent() { return this.PointsSpent; }
	public void setPointsSpent(Integer PointsSpent) { this.PointsSpent = PointsSpent; }
	private Double RewardsEarned; 
	public Double getRewardsEarned() { return this.RewardsEarned; }
	public void setRewardsEarned(Double RewardsEarned) { this.RewardsEarned = RewardsEarned; }
	private Double RewardsSpent; 
	public Double getRewardsSpent() { return this.RewardsSpent; }
	public void setRewardsSpent(Double RewardsSpent) { this.RewardsSpent = RewardsSpent; }
	private Double InstantRewardsEarned; 
	public Double getInstantRewardsEarned() { return this.InstantRewardsEarned; }
	public void setInstantRewardsEarned(Double InstantRewardsEarned) { this.InstantRewardsEarned = InstantRewardsEarned; }
	private Double InstantRewardsSpent; 
	public Double getInstantRewardsSpent() { return this.InstantRewardsSpent; }
	public void setInstantRewardsSpent(Double InstantRewardsSpent) { this.InstantRewardsSpent = InstantRewardsSpent; }
	private Double ProductsEarnedValuedUpTo; 
	public Double getProductsEarnedValuedUpTo() { return this.ProductsEarnedValuedUpTo; }
	public void setProductsEarnedValuedUpTo(Double ProductsEarnedValuedUpTo) { this.ProductsEarnedValuedUpTo = ProductsEarnedValuedUpTo; }
	private Integer ProductsEarnedCount; 
	public Integer getProductsEarnedCount() { return this.ProductsEarnedCount; }
	public void setProductsEarnedCount(Integer ProductsEarnedCount) { this.ProductsEarnedCount = ProductsEarnedCount; }
	private Double ProductsSpentValuedUpTo; 
	public Double getProductsSpentValuedUpTo() { return this.ProductsSpentValuedUpTo; }
	public void setProductsSpentValuedUpTo(Double ProductsSpentValuedUpTo) { this.ProductsSpentValuedUpTo = ProductsSpentValuedUpTo; }
	private Integer ProductsSpentCount; 
	public Integer getProductsSpentCount() { return this.ProductsSpentCount; }
	public void setProductsSpentCount(Integer ProductsSpentCount) { this.ProductsSpentCount = ProductsSpentCount; }
	private Double InstantProductsEarnedValuedUpTo; 
	public Double getInstantProductsEarnedValuedUpTo() { return this.InstantProductsEarnedValuedUpTo; }
	public void setInstantProductsEarnedValuedUpTo(Double InstantProductsEarnedValuedUpTo) { this.InstantProductsEarnedValuedUpTo = InstantProductsEarnedValuedUpTo; }
	private Integer InstantProductsEarnedCount; 
	public Integer getInstantProductsEarnedCount() { return this.InstantProductsEarnedCount; }
	public void setInstantProductsEarnedCount(Integer InstantProductsEarnedCount) { this.InstantProductsEarnedCount = InstantProductsEarnedCount; }
	private Double InstantProductsSpentValuedUpTo; 
	public Double getInstantProductsSpentValuedUpTo() { return this.InstantProductsSpentValuedUpTo; }
	public void setInstantProductsSpentValuedUpTo(Double InstantProductsSpentValuedUpTo) { this.InstantProductsSpentValuedUpTo = InstantProductsSpentValuedUpTo; }
	private Integer InstantProductsSpentCount; 
	public Integer getInstantProductsSpentCount() { return this.InstantProductsSpentCount; }
	public void setInstantProductsSpentCount(Integer InstantProductsSpentCount) { this.InstantProductsSpentCount = InstantProductsSpentCount; }
	private Integer OrderCount; 
	public Integer getOrderCount() { return this.OrderCount; }
	public void setOrderCount(Integer OrderCount) { this.OrderCount = OrderCount; }
	private Double OrderAmount; 
	public Double getOrderAmount() { return this.OrderAmount; }
	public void setOrderAmount(Double OrderAmount) { this.OrderAmount = OrderAmount; }
	private Integer ProposedOrderCount; 
	public Integer getProposedOrderCount() { return this.ProposedOrderCount; }
	public void setProposedOrderCount(Integer ProposedOrderCount) { this.ProposedOrderCount = ProposedOrderCount; }
	private Double ProposedOrderAmount; 
	public Double getProposedOrderAmount() { return this.ProposedOrderAmount; }
	public void setProposedOrderAmount(Double ProposedOrderAmount) { this.ProposedOrderAmount = ProposedOrderAmount; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer ShareCount; 
	public Integer getShareCount() { return this.ShareCount; }
	public void setShareCount(Integer ShareCount) { this.ShareCount = ShareCount; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
	public Location getLocation() { return this.Location; }
	public void setLocation(Location Location) { this.Location = Location; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Offer Offer;
	public Offer getOffer() { return this.Offer; }
	public void setOffer(Offer Offer) { this.Offer = Offer; }
    public static OfferOverview Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                OfferOverview entity = new OfferOverview();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.OfferOverviewId = Entities.ParseIntegerValue(obj, "OfferOverviewId");
          entity.OfferId = Entities.ParseIntegerValue(obj, "OfferId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.PointsEarned = Entities.ParseIntegerValue(obj, "PointsEarned");
          entity.PointsSpent = Entities.ParseIntegerValue(obj, "PointsSpent");
          entity.RewardsEarned = Entities.ParseDoubleValue(obj, "RewardsEarned");
          entity.RewardsSpent = Entities.ParseDoubleValue(obj, "RewardsSpent");
          entity.InstantRewardsEarned = Entities.ParseDoubleValue(obj, "InstantRewardsEarned");
          entity.InstantRewardsSpent = Entities.ParseDoubleValue(obj, "InstantRewardsSpent");
          entity.ProductsEarnedValuedUpTo = Entities.ParseDoubleValue(obj, "ProductsEarnedValuedUpTo");
          entity.ProductsEarnedCount = Entities.ParseIntegerValue(obj, "ProductsEarnedCount");
          entity.ProductsSpentValuedUpTo = Entities.ParseDoubleValue(obj, "ProductsSpentValuedUpTo");
          entity.ProductsSpentCount = Entities.ParseIntegerValue(obj, "ProductsSpentCount");
          entity.InstantProductsEarnedValuedUpTo = Entities.ParseDoubleValue(obj, "InstantProductsEarnedValuedUpTo");
          entity.InstantProductsEarnedCount = Entities.ParseIntegerValue(obj, "InstantProductsEarnedCount");
          entity.InstantProductsSpentValuedUpTo = Entities.ParseDoubleValue(obj, "InstantProductsSpentValuedUpTo");
          entity.InstantProductsSpentCount = Entities.ParseIntegerValue(obj, "InstantProductsSpentCount");
          entity.OrderCount = Entities.ParseIntegerValue(obj, "OrderCount");
          entity.OrderAmount = Entities.ParseDoubleValue(obj, "OrderAmount");
          entity.ProposedOrderCount = Entities.ParseIntegerValue(obj, "ProposedOrderCount");
          entity.ProposedOrderAmount = Entities.ParseDoubleValue(obj, "ProposedOrderAmount");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.ShareCount = Entities.ParseIntegerValue(obj, "ShareCount");
          if (obj.has("Location")){  entity.Location = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Location"));}
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
          if (obj.has("Offer")){  entity.Offer = com.enrollandpay.serviceinterface.BusinessEntity.Offer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Offer"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (OfferOverview)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public OfferOverview() {
		}
}
