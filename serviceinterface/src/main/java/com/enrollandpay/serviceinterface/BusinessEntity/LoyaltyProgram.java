package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class LoyaltyProgram extends BaseBusinessEntity {
	private Integer LoyaltyProgramId; 
	public Integer getLoyaltyProgramId() { return this.LoyaltyProgramId; }
	public void setLoyaltyProgramId(Integer LoyaltyProgramId) { this.LoyaltyProgramId = LoyaltyProgramId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private String ProgramName; 
	public String getProgramName() { return this.ProgramName; }
	public void setProgramName(String ProgramName) { this.ProgramName = ProgramName; }
	private String ProgramJoinMessage; 
	public String getProgramJoinMessage() { return this.ProgramJoinMessage; }
	public void setProgramJoinMessage(String ProgramJoinMessage) { this.ProgramJoinMessage = ProgramJoinMessage; }
	private String ProgramMessage; 
	public String getProgramMessage() { return this.ProgramMessage; }
	public void setProgramMessage(String ProgramMessage) { this.ProgramMessage = ProgramMessage; }
	private String ProgramBenefits; 
	public String getProgramBenefits() { return this.ProgramBenefits; }
	public void setProgramBenefits(String ProgramBenefits) { this.ProgramBenefits = ProgramBenefits; }
	private Boolean AllowCardAddedRedemption; 
	public Boolean getAllowCardAddedRedemption() { return this.AllowCardAddedRedemption; }
	public void setAllowCardAddedRedemption(Boolean AllowCardAddedRedemption) { this.AllowCardAddedRedemption = AllowCardAddedRedemption; }
	private Double MinRedemptionOrderAmount; 
	public Double getMinRedemptionOrderAmount() { return this.MinRedemptionOrderAmount; }
	public void setMinRedemptionOrderAmount(Double MinRedemptionOrderAmount) { this.MinRedemptionOrderAmount = MinRedemptionOrderAmount; }
	private Double MinRewardRedemptionAmount; 
	public Double getMinRewardRedemptionAmount() { return this.MinRewardRedemptionAmount; }
	public void setMinRewardRedemptionAmount(Double MinRewardRedemptionAmount) { this.MinRewardRedemptionAmount = MinRewardRedemptionAmount; }
	private Double MaxRewardRedemptionAmount; 
	public Double getMaxRewardRedemptionAmount() { return this.MaxRewardRedemptionAmount; }
	public void setMaxRewardRedemptionAmount(Double MaxRewardRedemptionAmount) { this.MaxRewardRedemptionAmount = MaxRewardRedemptionAmount; }
	private Integer EnrollmentMethodType; 
	public Integer getEnrollmentMethodType() { return this.EnrollmentMethodType; }
	public void setEnrollmentMethodType(Integer EnrollmentMethodType) { this.EnrollmentMethodType = EnrollmentMethodType; }
	private Integer LoyaltyProgramType; 
	public Integer getLoyaltyProgramType() { return this.LoyaltyProgramType; }
	public void setLoyaltyProgramType(Integer LoyaltyProgramType) { this.LoyaltyProgramType = LoyaltyProgramType; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer CurrencyType; 
	public Integer getCurrencyType() { return this.CurrencyType; }
	public void setCurrencyType(Integer CurrencyType) { this.CurrencyType = CurrencyType; }
	private Integer LanguageType; 
	public Integer getLanguageType() { return this.LanguageType; }
	public void setLanguageType(Integer LanguageType) { this.LanguageType = LanguageType; }
	private Double UtcOffsetHour; 
	public Double getUtcOffsetHour() { return this.UtcOffsetHour; }
	public void setUtcOffsetHour(Double UtcOffsetHour) { this.UtcOffsetHour = UtcOffsetHour; }
	private Integer TimeZoneType; 
	public Integer getTimeZoneType() { return this.TimeZoneType; }
	public void setTimeZoneType(Integer TimeZoneType) { this.TimeZoneType = TimeZoneType; }
	private Boolean AutoApplyReward; 
	public Boolean getAutoApplyReward() { return this.AutoApplyReward; }
	public void setAutoApplyReward(Boolean AutoApplyReward) { this.AutoApplyReward = AutoApplyReward; }
	private Integer ApplyRewardOfferType; 
	public Integer getApplyRewardOfferType() { return this.ApplyRewardOfferType; }
	public void setApplyRewardOfferType(Integer ApplyRewardOfferType) { this.ApplyRewardOfferType = ApplyRewardOfferType; }
	private Integer CoalitionId; 
	public Integer getCoalitionId() { return this.CoalitionId; }
	public void setCoalitionId(Integer CoalitionId) { this.CoalitionId = CoalitionId; }
	private Integer RemainingEnrollments; 
	public Integer getRemainingEnrollments() { return this.RemainingEnrollments; }
	public void setRemainingEnrollments(Integer RemainingEnrollments) { this.RemainingEnrollments = RemainingEnrollments; }
	private Integer PhoneNumberId; 
	public Integer getPhoneNumberId() { return this.PhoneNumberId; }
	public void setPhoneNumberId(Integer PhoneNumberId) { this.PhoneNumberId = PhoneNumberId; }
    private com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramConfiguration LoyaltyProgramConfiguration;
	public LoyaltyProgramConfiguration getLoyaltyProgramConfiguration() { return this.LoyaltyProgramConfiguration; }
	public void setLoyaltyProgramConfiguration(LoyaltyProgramConfiguration LoyaltyProgramConfiguration) { this.LoyaltyProgramConfiguration = LoyaltyProgramConfiguration; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Coalition Coalition;
	public Coalition getCoalition() { return this.Coalition; }
	public void setCoalition(Coalition Coalition) { this.Coalition = Coalition; }
    private com.enrollandpay.serviceinterface.BusinessEntity.PhoneNumber PhoneNumber;
	public PhoneNumber getPhoneNumber() { return this.PhoneNumber; }
	public void setPhoneNumber(PhoneNumber PhoneNumber) { this.PhoneNumber = PhoneNumber; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Offer> Offers; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramParticipant> LoyaltyProgramParticipants; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram> ConsumerLoyaltyPrograms; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentLoyaltyProgram> OrderPaymentLoyaltyPrograms; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.DailySaleOverview> DailySaleOverviews; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.SaleOverview> SaleOverviews; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Message> Messages; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShare> LoyaltyProgramShares; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.MessageThread> MessageThreads; 
    public static LoyaltyProgram Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                LoyaltyProgram entity = new LoyaltyProgram();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.LoyaltyProgramId = Entities.ParseIntegerValue(obj, "LoyaltyProgramId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.ProgramName = Entities.ParseStringValue(obj, "ProgramName");
          entity.ProgramJoinMessage = Entities.ParseStringValue(obj, "ProgramJoinMessage");
          entity.ProgramMessage = Entities.ParseStringValue(obj, "ProgramMessage");
          entity.ProgramBenefits = Entities.ParseStringValue(obj, "ProgramBenefits");
          entity.AllowCardAddedRedemption = Entities.ParseBooleanValue(obj, "AllowCardAddedRedemption");
          entity.MinRedemptionOrderAmount = Entities.ParseDoubleValue(obj, "MinRedemptionOrderAmount");
          entity.MinRewardRedemptionAmount = Entities.ParseDoubleValue(obj, "MinRewardRedemptionAmount");
          entity.MaxRewardRedemptionAmount = Entities.ParseDoubleValue(obj, "MaxRewardRedemptionAmount");
          entity.EnrollmentMethodType = Entities.ParseIntegerValue(obj, "EnrollmentMethodType");
          entity.LoyaltyProgramType = Entities.ParseIntegerValue(obj, "LoyaltyProgramType");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.CurrencyType = Entities.ParseIntegerValue(obj, "CurrencyType");
          entity.LanguageType = Entities.ParseIntegerValue(obj, "LanguageType");
          entity.UtcOffsetHour = Entities.ParseDoubleValue(obj, "UtcOffsetHour");
          entity.TimeZoneType = Entities.ParseIntegerValue(obj, "TimeZoneType");
          entity.AutoApplyReward = Entities.ParseBooleanValue(obj, "AutoApplyReward");
          entity.ApplyRewardOfferType = Entities.ParseIntegerValue(obj, "ApplyRewardOfferType");
          entity.CoalitionId = Entities.ParseIntegerValue(obj, "CoalitionId");
          entity.RemainingEnrollments = Entities.ParseIntegerValue(obj, "RemainingEnrollments");
          entity.PhoneNumberId = Entities.ParseIntegerValue(obj, "PhoneNumberId");
          if (obj.has("LoyaltyProgramConfiguration")){  entity.LoyaltyProgramConfiguration = com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramConfiguration.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LoyaltyProgramConfiguration"));}
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
          if (obj.has("Coalition")){  entity.Coalition = com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Coalition"));}
          if (obj.has("PhoneNumber")){  entity.PhoneNumber = com.enrollandpay.serviceinterface.BusinessEntity.PhoneNumber.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"PhoneNumber"));}
          JSONArray jsonOffers = Entities.ParseJSONArrayValue(obj, "Offers");
                if (jsonOffers != null){
                    for (int index = 0; index < jsonOffers.length();index++){
                        JSONObject lstItem = jsonOffers.getJSONObject(index);
                        entity.Offers.add(com.enrollandpay.serviceinterface.BusinessEntity.Offer.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonLoyaltyProgramParticipants = Entities.ParseJSONArrayValue(obj, "LoyaltyProgramParticipants");
                if (jsonLoyaltyProgramParticipants != null){
                    for (int index = 0; index < jsonLoyaltyProgramParticipants.length();index++){
                        JSONObject lstItem = jsonLoyaltyProgramParticipants.getJSONObject(index);
                        entity.LoyaltyProgramParticipants.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramParticipant.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerLoyaltyPrograms = Entities.ParseJSONArrayValue(obj, "ConsumerLoyaltyPrograms");
                if (jsonConsumerLoyaltyPrograms != null){
                    for (int index = 0; index < jsonConsumerLoyaltyPrograms.length();index++){
                        JSONObject lstItem = jsonConsumerLoyaltyPrograms.getJSONObject(index);
                        entity.ConsumerLoyaltyPrograms.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOrderPaymentLoyaltyPrograms = Entities.ParseJSONArrayValue(obj, "OrderPaymentLoyaltyPrograms");
                if (jsonOrderPaymentLoyaltyPrograms != null){
                    for (int index = 0; index < jsonOrderPaymentLoyaltyPrograms.length();index++){
                        JSONObject lstItem = jsonOrderPaymentLoyaltyPrograms.getJSONObject(index);
                        entity.OrderPaymentLoyaltyPrograms.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentLoyaltyProgram.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonDailySaleOverviews = Entities.ParseJSONArrayValue(obj, "DailySaleOverviews");
                if (jsonDailySaleOverviews != null){
                    for (int index = 0; index < jsonDailySaleOverviews.length();index++){
                        JSONObject lstItem = jsonDailySaleOverviews.getJSONObject(index);
                        entity.DailySaleOverviews.add(com.enrollandpay.serviceinterface.BusinessEntity.DailySaleOverview.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonSaleOverviews = Entities.ParseJSONArrayValue(obj, "SaleOverviews");
                if (jsonSaleOverviews != null){
                    for (int index = 0; index < jsonSaleOverviews.length();index++){
                        JSONObject lstItem = jsonSaleOverviews.getJSONObject(index);
                        entity.SaleOverviews.add(com.enrollandpay.serviceinterface.BusinessEntity.SaleOverview.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMessages = Entities.ParseJSONArrayValue(obj, "Messages");
                if (jsonMessages != null){
                    for (int index = 0; index < jsonMessages.length();index++){
                        JSONObject lstItem = jsonMessages.getJSONObject(index);
                        entity.Messages.add(com.enrollandpay.serviceinterface.BusinessEntity.Message.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonLoyaltyProgramShares = Entities.ParseJSONArrayValue(obj, "LoyaltyProgramShares");
                if (jsonLoyaltyProgramShares != null){
                    for (int index = 0; index < jsonLoyaltyProgramShares.length();index++){
                        JSONObject lstItem = jsonLoyaltyProgramShares.getJSONObject(index);
                        entity.LoyaltyProgramShares.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShare.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMessageThreads = Entities.ParseJSONArrayValue(obj, "MessageThreads");
                if (jsonMessageThreads != null){
                    for (int index = 0; index < jsonMessageThreads.length();index++){
                        JSONObject lstItem = jsonMessageThreads.getJSONObject(index);
                        entity.MessageThreads.add(com.enrollandpay.serviceinterface.BusinessEntity.MessageThread.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (LoyaltyProgram)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public LoyaltyProgram() {
		Offers = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Offer>();
			LoyaltyProgramParticipants = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramParticipant>();
			ConsumerLoyaltyPrograms = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram>();
			OrderPaymentLoyaltyPrograms = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentLoyaltyProgram>();
			DailySaleOverviews = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.DailySaleOverview>();
			SaleOverviews = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.SaleOverview>();
			Messages = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Message>();
			LoyaltyProgramShares = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShare>();
			MessageThreads = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.MessageThread>();
			}
}
