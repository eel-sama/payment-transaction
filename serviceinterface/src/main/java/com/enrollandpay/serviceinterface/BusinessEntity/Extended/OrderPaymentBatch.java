package com.enrollandpay.serviceinterface.BusinessEntity.Extended;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderPaymentBatch {
    @SerializedName("BatchIdent")
    @Expose
    private String BatchIdent;
    @SerializedName("CashSaleAmount")
    @Expose
    private Double CashSaleAmount;
    @SerializedName("CashSaleCount")
    @Expose
    private Integer CashSaleCount;
    @SerializedName("CashVoidAmount")
    @Expose
    private Double CashVoidAmount;
    @SerializedName("CashVoidCount")
    @Expose
    private Integer CashVoidCount;
    @SerializedName("CreditSaleAmount")
    @Expose
    private Double CreditSaleAmount;
    @SerializedName("CreditSaleCount")
    @Expose
    private Integer CreditSaleCount;
    @SerializedName("CreditVoidAmount")
    @Expose
    private Double CreditVoidAmount;
    @SerializedName("CreditVoidCount")
    @Expose
    private Integer CreditVoidCount;
    @SerializedName("DebitSaleAmount")
    @Expose
    private Double DebitSaleAmount;
    @SerializedName("DebitSaleCount")
    @Expose
    private Integer DebitSaleCount;
    @SerializedName("DebitVoidAmount")
    @Expose
    private Double DebitVoidAmount;
    @SerializedName("DebitVoidCount")
    @Expose
    private Integer DebitVoidCount;
    @SerializedName("EBTSaleAmount")
    @Expose
    private Double EBTSaleAmount;
    @SerializedName("EBTSaleCount")
    @Expose
    private Integer EBTSaleCount;
    @SerializedName("EBTVoidAmount")
    @Expose
    private Double EBTVoidAmount;
    @SerializedName("EBTVoidCount")
    @Expose
    private Integer EBTVoidCount;
    @SerializedName("OtherSaleAmount")
    @Expose
    private Double OtherSaleAmount;
    @SerializedName("OtherSaleCount")
    @Expose
    private Integer OtherSaleCount;
    @SerializedName("OtherVoidAmount")
    @Expose
    private Double OtherVoidAmount;
    @SerializedName("OtherVoidCount")
    @Expose
    private Integer OtherVoidCount;
    ////////////////////////////////////////

    public String getBatchIdent() {
        return BatchIdent;
    }

    public void setBatchIdent(String batchIdent) {
        BatchIdent = batchIdent;
    }

    public Double getCashSaleAmount() {
        return CashSaleAmount;
    }

    public void setCashSaleAmount(Double cashSaleAmount) {
        CashSaleAmount = cashSaleAmount;
    }

    public Integer getCashSaleCount() {
        return CashSaleCount;
    }

    public void setCashSaleCount(Integer cashSaleCount) {
        CashSaleCount = cashSaleCount;
    }

    public Double getCashVoidAmount() {
        return CashVoidAmount;
    }

    public void setCashVoidAmount(Double cashVoidAmount) {
        CashVoidAmount = cashVoidAmount;
    }

    public Integer getCashVoidCount() {
        return CashVoidCount;
    }

    public void setCashVoidCount(Integer cashVoidCount) {
        CashVoidCount = cashVoidCount;
    }

    public Double getCreditSaleAmount() {
        return CreditSaleAmount;
    }

    public void setCreditSaleAmount(Double creditSaleAmount) {
        CreditSaleAmount = creditSaleAmount;
    }

    public Integer getCreditSaleCount() {
        return CreditSaleCount;
    }

    public void setCreditSaleCount(Integer creditSaleCount) {
        CreditSaleCount = creditSaleCount;
    }

    public Double getCreditVoidAmount() {
        return CreditVoidAmount;
    }

    public void setCreditVoidAmount(Double creditVoidAmount) {
        CreditVoidAmount = creditVoidAmount;
    }

    public Integer getCreditVoidCount() {
        return CreditVoidCount;
    }

    public void setCreditVoidCount(Integer creditVoidCount) {
        CreditVoidCount = creditVoidCount;
    }

    public Double getDebitSaleAmount() {
        return DebitSaleAmount;
    }

    public void setDebitSaleAmount(Double debitSaleAmount) {
        DebitSaleAmount = debitSaleAmount;
    }

    public Integer getDebitSaleCount() {
        return DebitSaleCount;
    }

    public void setDebitSaleCount(Integer debitSaleCount) {
        DebitSaleCount = debitSaleCount;
    }

    public Double getDebitVoidAmount() {
        return DebitVoidAmount;
    }

    public void setDebitVoidAmount(Double debitVoidAmount) {
        DebitVoidAmount = debitVoidAmount;
    }

    public Integer getDebitVoidCount() {
        return DebitVoidCount;
    }

    public void setDebitVoidCount(Integer debitVoidCount) {
        DebitVoidCount = debitVoidCount;
    }

    public Double getEBTSaleAmount() {
        return EBTSaleAmount;
    }

    public void setEBTSaleAmount(Double EBTSaleAmount) {
        this.EBTSaleAmount = EBTSaleAmount;
    }

    public Integer getEBTSaleCount() {
        return EBTSaleCount;
    }

    public void setEBTSaleCount(Integer EBTSaleCount) {
        this.EBTSaleCount = EBTSaleCount;
    }

    public Double getEBTVoidAmount() {
        return EBTVoidAmount;
    }

    public void setEBTVoidAmount(Double EBTVoidAmount) {
        this.EBTVoidAmount = EBTVoidAmount;
    }

    public Integer getEBTVoidCount() {
        return EBTVoidCount;
    }

    public void setEBTVoidCount(Integer EBTVoidCount) {
        this.EBTVoidCount = EBTVoidCount;
    }

    public Double getOtherSaleAmount() {
        return OtherSaleAmount;
    }

    public void setOtherSaleAmount(Double otherSaleAmount) {
        OtherSaleAmount = otherSaleAmount;
    }

    public Integer getOtherSaleCount() {
        return OtherSaleCount;
    }

    public void setOtherSaleCount(Integer otherSaleCount) {
        OtherSaleCount = otherSaleCount;
    }

    public Double getOtherVoidAmount() {
        return OtherVoidAmount;
    }

    public void setOtherVoidAmount(Double otherVoidAmount) {
        OtherVoidAmount = otherVoidAmount;
    }

    public Integer getOtherVoidCount() {
        return OtherVoidCount;
    }

    public void setOtherVoidCount(Integer otherVoidCount) {
        OtherVoidCount = otherVoidCount;
    }
}
