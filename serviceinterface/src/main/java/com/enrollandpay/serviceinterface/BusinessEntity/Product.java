package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Product implements Serializable {
	private Integer ProductId; 
	public Integer getProductId() { return this.ProductId; }
	public void setProductId(Integer ProductId) { this.ProductId = ProductId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private Integer ParentProductId; 
	public Integer getParentProductId() { return this.ParentProductId; }
	public void setParentProductId(Integer ParentProductId) { this.ParentProductId = ParentProductId; }
	private String Name; 
	public String getName() { return this.Name; }
	public void setName(String Name) { this.Name = Name; }
	private String Description; 
	public String getDescription() { return this.Description; }
	public void setDescription(String Description) { this.Description = Description; }
	private String PosIdents; 
	public String getPosIdents() { return this.PosIdents; }
	public void setPosIdents(String PosIdents) { this.PosIdents = PosIdents; }
	public ArrayList<String> getPosIdentsArray(){
	    if (this.PosIdents == null){return null;}
	    return (new Gson()).fromJson(this.PosIdents, ArrayList.class);
    }
	private Double SaleAmount; 
	public Double getSaleAmount() { return this.SaleAmount; }
	public void setSaleAmount(Double SaleAmount) { this.SaleAmount = SaleAmount; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Date CreatedDateTime; 
	public Date getCreatedDateTime() { return this.CreatedDateTime; }
	public void setCreatedDateTime(Date CreatedDateTime) { this.CreatedDateTime = CreatedDateTime; }
	private Integer TaxId; 
	public Integer getTaxId() { return this.TaxId; }
	public void setTaxId(Integer TaxId) { this.TaxId = TaxId; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
	public Location getLocation() { return this.Location; }
	public void setLocation(Location Location) { this.Location = Location; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Product ProductParent;
	public Product getProductParent() { return this.ProductParent; }
	public void setProductParent(Product ProductParent) { this.ProductParent = ProductParent; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Tax Tax;
	public Tax getTax() { return this.Tax; }
	public void setTax(Tax Tax) { this.Tax = Tax; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OfferAssociation> OfferAssociations; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderItem> OrderItems; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Product> ProductChildren; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ProductStatistic> ProductStatistics; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ProductRelationship> ProductRelationshipsA; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ProductRelationship> ProductRelationshipsB; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.CategoryProduct> CategoryProducts; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ProductAddOn> ProductAddOnsAsAddOn; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ProductAddOn> ProductAddOnsAsProduct; 
    public static Product Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Product entity = new Product();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.ProductId = Entities.ParseIntegerValue(obj, "ProductId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.ParentProductId = Entities.ParseIntegerValue(obj, "ParentProductId");
          entity.Name = Entities.ParseStringValue(obj, "Name");
          entity.Description = Entities.ParseStringValue(obj, "Description");
          entity.PosIdents = Entities.ParseStringValue(obj, "PosIdents");
          entity.SaleAmount = Entities.ParseDoubleValue(obj, "SaleAmount");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.CreatedDateTime = Entities.ParseDateValue(obj, "CreatedDateTime");
          entity.TaxId = Entities.ParseIntegerValue(obj, "TaxId");
          if (obj.has("Location")){  entity.Location = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Location"));}
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
          if (obj.has("ProductParent")){  entity.ProductParent = com.enrollandpay.serviceinterface.BusinessEntity.Product.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"ProductParent"));}
          if (obj.has("Tax")){  entity.Tax = com.enrollandpay.serviceinterface.BusinessEntity.Tax.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Tax"));}
          JSONArray jsonOfferAssociations = Entities.ParseJSONArrayValue(obj, "OfferAssociations");
                if (jsonOfferAssociations != null){
                    for (int index = 0; index < jsonOfferAssociations.length();index++){
                        JSONObject lstItem = jsonOfferAssociations.getJSONObject(index);
                        entity.OfferAssociations.add(com.enrollandpay.serviceinterface.BusinessEntity.OfferAssociation.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOrderItems = Entities.ParseJSONArrayValue(obj, "OrderItems");
                if (jsonOrderItems != null){
                    for (int index = 0; index < jsonOrderItems.length();index++){
                        JSONObject lstItem = jsonOrderItems.getJSONObject(index);
                        entity.OrderItems.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderItem.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonProductChildren = Entities.ParseJSONArrayValue(obj, "ProductChildren");
                if (jsonProductChildren != null){
                    for (int index = 0; index < jsonProductChildren.length();index++){
                        JSONObject lstItem = jsonProductChildren.getJSONObject(index);
                        entity.ProductChildren.add(com.enrollandpay.serviceinterface.BusinessEntity.Product.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonProductStatistics = Entities.ParseJSONArrayValue(obj, "ProductStatistics");
                if (jsonProductStatistics != null){
                    for (int index = 0; index < jsonProductStatistics.length();index++){
                        JSONObject lstItem = jsonProductStatistics.getJSONObject(index);
                        entity.ProductStatistics.add(com.enrollandpay.serviceinterface.BusinessEntity.ProductStatistic.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonProductRelationshipsA = Entities.ParseJSONArrayValue(obj, "ProductRelationshipsA");
                if (jsonProductRelationshipsA != null){
                    for (int index = 0; index < jsonProductRelationshipsA.length();index++){
                        JSONObject lstItem = jsonProductRelationshipsA.getJSONObject(index);
                        entity.ProductRelationshipsA.add(com.enrollandpay.serviceinterface.BusinessEntity.ProductRelationship.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonProductRelationshipsB = Entities.ParseJSONArrayValue(obj, "ProductRelationshipsB");
                if (jsonProductRelationshipsB != null){
                    for (int index = 0; index < jsonProductRelationshipsB.length();index++){
                        JSONObject lstItem = jsonProductRelationshipsB.getJSONObject(index);
                        entity.ProductRelationshipsB.add(com.enrollandpay.serviceinterface.BusinessEntity.ProductRelationship.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonCategoryProducts = Entities.ParseJSONArrayValue(obj, "CategoryProducts");
                if (jsonCategoryProducts != null){
                    for (int index = 0; index < jsonCategoryProducts.length();index++){
                        JSONObject lstItem = jsonCategoryProducts.getJSONObject(index);
                        entity.CategoryProducts.add(com.enrollandpay.serviceinterface.BusinessEntity.CategoryProduct.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonProductAddOnsAsAddOn = Entities.ParseJSONArrayValue(obj, "ProductAddOnsAsAddOn");
                if (jsonProductAddOnsAsAddOn != null){
                    for (int index = 0; index < jsonProductAddOnsAsAddOn.length();index++){
                        JSONObject lstItem = jsonProductAddOnsAsAddOn.getJSONObject(index);
                        entity.ProductAddOnsAsAddOn.add(com.enrollandpay.serviceinterface.BusinessEntity.ProductAddOn.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonProductAddOnsAsProduct = Entities.ParseJSONArrayValue(obj, "ProductAddOnsAsProduct");
                if (jsonProductAddOnsAsProduct != null){
                    for (int index = 0; index < jsonProductAddOnsAsProduct.length();index++){
                        JSONObject lstItem = jsonProductAddOnsAsProduct.getJSONObject(index);
                        entity.ProductAddOnsAsProduct.add(com.enrollandpay.serviceinterface.BusinessEntity.ProductAddOn.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Product)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Product() {
		OfferAssociations = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OfferAssociation>();
			OrderItems = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderItem>();
			ProductChildren = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Product>();
			ProductStatistics = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ProductStatistic>();
			ProductRelationshipsA = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ProductRelationship>();
			ProductRelationshipsB = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ProductRelationship>();
			CategoryProducts = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.CategoryProduct>();
			ProductAddOnsAsAddOn = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ProductAddOn>();
			ProductAddOnsAsProduct = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ProductAddOn>();
			}
}
