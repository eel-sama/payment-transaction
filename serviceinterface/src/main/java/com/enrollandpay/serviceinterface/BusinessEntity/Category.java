package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Category implements Serializable{
	private Integer CategoryId; 
	public Integer getCategoryId() { return this.CategoryId; }
	public void setCategoryId(Integer CategoryId) { this.CategoryId = CategoryId; }
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private String Name; 
	public String getName() { return this.Name; }
	public void setName(String Name) { this.Name = Name; }
	private Integer ParentCategoryId; 
	public Integer getParentCategoryId() { return this.ParentCategoryId; }
	public void setParentCategoryId(Integer ParentCategoryId) { this.ParentCategoryId = ParentCategoryId; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Category Parent;
	public Category getParent() { return this.Parent; }
	public void setParent(Category Parent) { this.Parent = Parent; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Category> Children; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.CategoryProduct> CategoryProducts; 
    public static Category Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Category entity = new Category();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.CategoryId = Entities.ParseIntegerValue(obj, "CategoryId");
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.Name = Entities.ParseStringValue(obj, "Name");
          entity.ParentCategoryId = Entities.ParseIntegerValue(obj, "ParentCategoryId");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Parent")){  entity.Parent = com.enrollandpay.serviceinterface.BusinessEntity.Category.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Parent"));}
          JSONArray jsonChildren = Entities.ParseJSONArrayValue(obj, "Children");
                if (jsonChildren != null){
                    for (int index = 0; index < jsonChildren.length();index++){
                        JSONObject lstItem = jsonChildren.getJSONObject(index);
                        entity.Children.add(com.enrollandpay.serviceinterface.BusinessEntity.Category.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonCategoryProducts = Entities.ParseJSONArrayValue(obj, "CategoryProducts");
                if (jsonCategoryProducts != null){
                    for (int index = 0; index < jsonCategoryProducts.length();index++){
                        JSONObject lstItem = jsonCategoryProducts.getJSONObject(index);
                        entity.CategoryProducts.add(com.enrollandpay.serviceinterface.BusinessEntity.CategoryProduct.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Category)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Category() {
		Children = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Category>();
			CategoryProducts = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.CategoryProduct>();
			}
}
