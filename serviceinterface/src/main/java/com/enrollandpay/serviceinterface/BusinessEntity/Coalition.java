package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Coalition extends BaseBusinessEntity {
	private Integer CoalitionId; 
	public Integer getCoalitionId() { return this.CoalitionId; }
	public void setCoalitionId(Integer CoalitionId) { this.CoalitionId = CoalitionId; }
	private String Name; 
	public String getName() { return this.Name; }
	public void setName(String Name) { this.Name = Name; }
	private String InternalPortalDomain; 
	public String getInternalPortalDomain() { return this.InternalPortalDomain; }
	public void setInternalPortalDomain(String InternalPortalDomain) { this.InternalPortalDomain = InternalPortalDomain; }
	private String MerchantPortalDomain; 
	public String getMerchantPortalDomain() { return this.MerchantPortalDomain; }
	public void setMerchantPortalDomain(String MerchantPortalDomain) { this.MerchantPortalDomain = MerchantPortalDomain; }
	private String ConsumerDomain; 
	public String getConsumerDomain() { return this.ConsumerDomain; }
	public void setConsumerDomain(String ConsumerDomain) { this.ConsumerDomain = ConsumerDomain; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer BillingCycleDayOfMonth; 
	public Integer getBillingCycleDayOfMonth() { return this.BillingCycleDayOfMonth; }
	public void setBillingCycleDayOfMonth(Integer BillingCycleDayOfMonth) { this.BillingCycleDayOfMonth = BillingCycleDayOfMonth; }
	private String InternalPortalCSS; 
	public String getInternalPortalCSS() { return this.InternalPortalCSS; }
	public void setInternalPortalCSS(String InternalPortalCSS) { this.InternalPortalCSS = InternalPortalCSS; }
	private String MerchantPortalCSS; 
	public String getMerchantPortalCSS() { return this.MerchantPortalCSS; }
	public void setMerchantPortalCSS(String MerchantPortalCSS) { this.MerchantPortalCSS = MerchantPortalCSS; }
	private String ConsumerCSS; 
	public String getConsumerCSS() { return this.ConsumerCSS; }
	public void setConsumerCSS(String ConsumerCSS) { this.ConsumerCSS = ConsumerCSS; }
	private Integer PhoneNumberId; 
	public Integer getPhoneNumberId() { return this.PhoneNumberId; }
	public void setPhoneNumberId(Integer PhoneNumberId) { this.PhoneNumberId = PhoneNumberId; }
	private String ReferenceIdent; 
	public String getReferenceIdent() { return this.ReferenceIdent; }
	public void setReferenceIdent(String ReferenceIdent) { this.ReferenceIdent = ReferenceIdent; }
    private com.enrollandpay.serviceinterface.BusinessEntity.PhoneNumber PhoneNumber;
	public PhoneNumber getPhoneNumber() { return this.PhoneNumber; }
	public void setPhoneNumber(PhoneNumber PhoneNumber) { this.PhoneNumber = PhoneNumber; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram> LoyaltyPrograms; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Representative> Representatives; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Team> Teams; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.CoalitionMerchant> CoalitionMerchants; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.CoalitionAuthority> Parents; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.CoalitionAuthority> Children; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.BillingCycle> BillingCycles; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Invoice> Invoices; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest> AuthorizationRequests; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship> ConsumerRelationships; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.MessageThread> MessageThreads; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess> ClientAccesses; 
    public static Coalition Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Coalition entity = new Coalition();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.CoalitionId = Entities.ParseIntegerValue(obj, "CoalitionId");
          entity.Name = Entities.ParseStringValue(obj, "Name");
          entity.InternalPortalDomain = Entities.ParseStringValue(obj, "InternalPortalDomain");
          entity.MerchantPortalDomain = Entities.ParseStringValue(obj, "MerchantPortalDomain");
          entity.ConsumerDomain = Entities.ParseStringValue(obj, "ConsumerDomain");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.BillingCycleDayOfMonth = Entities.ParseIntegerValue(obj, "BillingCycleDayOfMonth");
          entity.InternalPortalCSS = Entities.ParseStringValue(obj, "InternalPortalCSS");
          entity.MerchantPortalCSS = Entities.ParseStringValue(obj, "MerchantPortalCSS");
          entity.ConsumerCSS = Entities.ParseStringValue(obj, "ConsumerCSS");
          entity.PhoneNumberId = Entities.ParseIntegerValue(obj, "PhoneNumberId");
          entity.ReferenceIdent = Entities.ParseStringValue(obj, "ReferenceIdent");
          if (obj.has("PhoneNumber")){  entity.PhoneNumber = com.enrollandpay.serviceinterface.BusinessEntity.PhoneNumber.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"PhoneNumber"));}
          JSONArray jsonLoyaltyPrograms = Entities.ParseJSONArrayValue(obj, "LoyaltyPrograms");
                if (jsonLoyaltyPrograms != null){
                    for (int index = 0; index < jsonLoyaltyPrograms.length();index++){
                        JSONObject lstItem = jsonLoyaltyPrograms.getJSONObject(index);
                        entity.LoyaltyPrograms.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonRepresentatives = Entities.ParseJSONArrayValue(obj, "Representatives");
                if (jsonRepresentatives != null){
                    for (int index = 0; index < jsonRepresentatives.length();index++){
                        JSONObject lstItem = jsonRepresentatives.getJSONObject(index);
                        entity.Representatives.add(com.enrollandpay.serviceinterface.BusinessEntity.Representative.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonTeams = Entities.ParseJSONArrayValue(obj, "Teams");
                if (jsonTeams != null){
                    for (int index = 0; index < jsonTeams.length();index++){
                        JSONObject lstItem = jsonTeams.getJSONObject(index);
                        entity.Teams.add(com.enrollandpay.serviceinterface.BusinessEntity.Team.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonCoalitionMerchants = Entities.ParseJSONArrayValue(obj, "CoalitionMerchants");
                if (jsonCoalitionMerchants != null){
                    for (int index = 0; index < jsonCoalitionMerchants.length();index++){
                        JSONObject lstItem = jsonCoalitionMerchants.getJSONObject(index);
                        entity.CoalitionMerchants.add(com.enrollandpay.serviceinterface.BusinessEntity.CoalitionMerchant.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonParents = Entities.ParseJSONArrayValue(obj, "Parents");
                if (jsonParents != null){
                    for (int index = 0; index < jsonParents.length();index++){
                        JSONObject lstItem = jsonParents.getJSONObject(index);
                        entity.Parents.add(com.enrollandpay.serviceinterface.BusinessEntity.CoalitionAuthority.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonChildren = Entities.ParseJSONArrayValue(obj, "Children");
                if (jsonChildren != null){
                    for (int index = 0; index < jsonChildren.length();index++){
                        JSONObject lstItem = jsonChildren.getJSONObject(index);
                        entity.Children.add(com.enrollandpay.serviceinterface.BusinessEntity.CoalitionAuthority.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonBillingCycles = Entities.ParseJSONArrayValue(obj, "BillingCycles");
                if (jsonBillingCycles != null){
                    for (int index = 0; index < jsonBillingCycles.length();index++){
                        JSONObject lstItem = jsonBillingCycles.getJSONObject(index);
                        entity.BillingCycles.add(com.enrollandpay.serviceinterface.BusinessEntity.BillingCycle.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonInvoices = Entities.ParseJSONArrayValue(obj, "Invoices");
                if (jsonInvoices != null){
                    for (int index = 0; index < jsonInvoices.length();index++){
                        JSONObject lstItem = jsonInvoices.getJSONObject(index);
                        entity.Invoices.add(com.enrollandpay.serviceinterface.BusinessEntity.Invoice.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonAuthorizationRequests = Entities.ParseJSONArrayValue(obj, "AuthorizationRequests");
                if (jsonAuthorizationRequests != null){
                    for (int index = 0; index < jsonAuthorizationRequests.length();index++){
                        JSONObject lstItem = jsonAuthorizationRequests.getJSONObject(index);
                        entity.AuthorizationRequests.add(com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerRelationships = Entities.ParseJSONArrayValue(obj, "ConsumerRelationships");
                if (jsonConsumerRelationships != null){
                    for (int index = 0; index < jsonConsumerRelationships.length();index++){
                        JSONObject lstItem = jsonConsumerRelationships.getJSONObject(index);
                        entity.ConsumerRelationships.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMessageThreads = Entities.ParseJSONArrayValue(obj, "MessageThreads");
                if (jsonMessageThreads != null){
                    for (int index = 0; index < jsonMessageThreads.length();index++){
                        JSONObject lstItem = jsonMessageThreads.getJSONObject(index);
                        entity.MessageThreads.add(com.enrollandpay.serviceinterface.BusinessEntity.MessageThread.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonClientAccesses = Entities.ParseJSONArrayValue(obj, "ClientAccesses");
                if (jsonClientAccesses != null){
                    for (int index = 0; index < jsonClientAccesses.length();index++){
                        JSONObject lstItem = jsonClientAccesses.getJSONObject(index);
                        entity.ClientAccesses.add(com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Coalition)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Coalition() {
		LoyaltyPrograms = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram>();
			Representatives = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Representative>();
			Teams = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Team>();
			CoalitionMerchants = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.CoalitionMerchant>();
			Parents = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.CoalitionAuthority>();
			Children = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.CoalitionAuthority>();
			BillingCycles = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.BillingCycle>();
			Invoices = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Invoice>();
			AuthorizationRequests = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest>();
			ConsumerRelationships = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship>();
			MessageThreads = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.MessageThread>();
			ClientAccesses = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess>();
			}
}
