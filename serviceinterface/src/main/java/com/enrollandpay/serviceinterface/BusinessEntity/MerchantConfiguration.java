package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class MerchantConfiguration extends BaseBusinessEntity {
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private String StatusIdents; 
	public String getStatusIdents() { return this.StatusIdents; }
	public void setStatusIdents(String StatusIdents) { this.StatusIdents = StatusIdents; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    public static MerchantConfiguration Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                MerchantConfiguration entity = new MerchantConfiguration();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.StatusIdents = Entities.ParseStringValue(obj, "StatusIdents");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (MerchantConfiguration)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public MerchantConfiguration() {
		}
}
