package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class ProductRelationshipHistory extends BaseBusinessEntity {
	private Integer ProductRelationshipId; 
	public Integer getProductRelationshipId() { return this.ProductRelationshipId; }
	public void setProductRelationshipId(Integer ProductRelationshipId) { this.ProductRelationshipId = ProductRelationshipId; }
	private String History; 
	public String getHistory() { return this.History; }
	public void setHistory(String History) { this.History = History; }
	private Date UpdateDateTime; 
	public Date getUpdateDateTime() { return this.UpdateDateTime; }
	public void setUpdateDateTime(Date UpdateDateTime) { this.UpdateDateTime = UpdateDateTime; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.ProductRelationship ProductRelationship;
	public ProductRelationship getProductRelationship() { return this.ProductRelationship; }
	public void setProductRelationship(ProductRelationship ProductRelationship) { this.ProductRelationship = ProductRelationship; }
    public static ProductRelationshipHistory Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                ProductRelationshipHistory entity = new ProductRelationshipHistory();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.ProductRelationshipId = Entities.ParseIntegerValue(obj, "ProductRelationshipId");
          entity.History = Entities.ParseStringValue(obj, "History");
          entity.UpdateDateTime = Entities.ParseDateValue(obj, "UpdateDateTime");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("ProductRelationship")){  entity.ProductRelationship = com.enrollandpay.serviceinterface.BusinessEntity.ProductRelationship.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"ProductRelationship"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (ProductRelationshipHistory)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public ProductRelationshipHistory() {
		}
}
