package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class QuestionStatistic extends BaseBusinessEntity {
	private Integer QuestionStatisticId; 
	public Integer getQuestionStatisticId() { return this.QuestionStatisticId; }
	public void setQuestionStatisticId(Integer QuestionStatisticId) { this.QuestionStatisticId = QuestionStatisticId; }
	private Integer QuestionId; 
	public Integer getQuestionId() { return this.QuestionId; }
	public void setQuestionId(Integer QuestionId) { this.QuestionId = QuestionId; }
	private Date SaleDate; 
	public Date getSaleDate() { return this.SaleDate; }
	public void setSaleDate(Date SaleDate) { this.SaleDate = SaleDate; }
	private Integer AddedCount; 
	public Integer getAddedCount() { return this.AddedCount; }
	public void setAddedCount(Integer AddedCount) { this.AddedCount = AddedCount; }
	private Integer DisplayedCount; 
	public Integer getDisplayedCount() { return this.DisplayedCount; }
	public void setDisplayedCount(Integer DisplayedCount) { this.DisplayedCount = DisplayedCount; }
	private Integer AnsweredCount; 
	public Integer getAnsweredCount() { return this.AnsweredCount; }
	public void setAnsweredCount(Integer AnsweredCount) { this.AnsweredCount = AnsweredCount; }
	private Date UpdateDateTime; 
	public Date getUpdateDateTime() { return this.UpdateDateTime; }
	public void setUpdateDateTime(Date UpdateDateTime) { this.UpdateDateTime = UpdateDateTime; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Question Question;
	public Question getQuestion() { return this.Question; }
	public void setQuestion(Question Question) { this.Question = Question; }
    public static QuestionStatistic Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                QuestionStatistic entity = new QuestionStatistic();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.QuestionStatisticId = Entities.ParseIntegerValue(obj, "QuestionStatisticId");
          entity.QuestionId = Entities.ParseIntegerValue(obj, "QuestionId");
          entity.SaleDate = Entities.ParseDateValue(obj, "SaleDate");
          entity.AddedCount = Entities.ParseIntegerValue(obj, "AddedCount");
          entity.DisplayedCount = Entities.ParseIntegerValue(obj, "DisplayedCount");
          entity.AnsweredCount = Entities.ParseIntegerValue(obj, "AnsweredCount");
          entity.UpdateDateTime = Entities.ParseDateValue(obj, "UpdateDateTime");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Question")){  entity.Question = com.enrollandpay.serviceinterface.BusinessEntity.Question.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Question"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (QuestionStatistic)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public QuestionStatistic() {
		}
}
