package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class AuthorizationRequest extends BaseBusinessEntity {
	private Integer AuthorizationRequestId; 
	public Integer getAuthorizationRequestId() { return this.AuthorizationRequestId; }
	public void setAuthorizationRequestId(Integer AuthorizationRequestId) { this.AuthorizationRequestId = AuthorizationRequestId; }
	private Integer ClientId; 
	public Integer getClientId() { return this.ClientId; }
	public void setClientId(Integer ClientId) { this.ClientId = ClientId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private String Secret; 
	public String getSecret() { return this.Secret; }
	public void setSecret(String Secret) { this.Secret = Secret; }
	private String RequestInfo; 
	public String getRequestInfo() { return this.RequestInfo; }
	public void setRequestInfo(String RequestInfo) { this.RequestInfo = RequestInfo; }
	private Date RequestDateTime; 
	public Date getRequestDateTime() { return this.RequestDateTime; }
	public void setRequestDateTime(Date RequestDateTime) { this.RequestDateTime = RequestDateTime; }
	private String AuthCode; 
	public String getAuthCode() { return this.AuthCode; }
	public void setAuthCode(String AuthCode) { this.AuthCode = AuthCode; }
	private String DisplayCode; 
	public String getDisplayCode() { return this.DisplayCode; }
	public void setDisplayCode(String DisplayCode) { this.DisplayCode = DisplayCode; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer DeviceId; 
	public Integer getDeviceId() { return this.DeviceId; }
	public void setDeviceId(Integer DeviceId) { this.DeviceId = DeviceId; }
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private Integer CoalitionId; 
	public Integer getCoalitionId() { return this.CoalitionId; }
	public void setCoalitionId(Integer CoalitionId) { this.CoalitionId = CoalitionId; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Client Client;
	public Client getClient() { return this.Client; }
	public void setClient(Client Client) { this.Client = Client; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
	public Location getLocation() { return this.Location; }
	public void setLocation(Location Location) { this.Location = Location; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Device Device;
	public Device getDevice() { return this.Device; }
	public void setDevice(Device Device) { this.Device = Device; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Coalition Coalition;
	public Coalition getCoalition() { return this.Coalition; }
	public void setCoalition(Coalition Coalition) { this.Coalition = Coalition; }
    public static AuthorizationRequest Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                AuthorizationRequest entity = new AuthorizationRequest();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.AuthorizationRequestId = Entities.ParseIntegerValue(obj, "AuthorizationRequestId");
          entity.ClientId = Entities.ParseIntegerValue(obj, "ClientId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.Secret = Entities.ParseStringValue(obj, "Secret");
          entity.RequestInfo = Entities.ParseStringValue(obj, "RequestInfo");
          entity.RequestDateTime = Entities.ParseDateValue(obj, "RequestDateTime");
          entity.AuthCode = Entities.ParseStringValue(obj, "AuthCode");
          entity.DisplayCode = Entities.ParseStringValue(obj, "DisplayCode");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.DeviceId = Entities.ParseIntegerValue(obj, "DeviceId");
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.CoalitionId = Entities.ParseIntegerValue(obj, "CoalitionId");
          if (obj.has("Client")){  entity.Client = com.enrollandpay.serviceinterface.BusinessEntity.Client.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Client"));}
          if (obj.has("Location")){  entity.Location = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Location"));}
          if (obj.has("Device")){  entity.Device = com.enrollandpay.serviceinterface.BusinessEntity.Device.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Device"));}
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
          if (obj.has("Coalition")){  entity.Coalition = com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Coalition"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (AuthorizationRequest)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public AuthorizationRequest() {
		}
}
