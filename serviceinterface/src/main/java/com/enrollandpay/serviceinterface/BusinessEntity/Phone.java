package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Phone extends BaseBusinessEntity {
	private Long PhoneId; 
	public Long getPhoneId() { return this.PhoneId; }
	public void setPhoneId(Long PhoneId) { this.PhoneId = PhoneId; }
	private Long ForwardTo; 
	public Long getForwardTo() { return this.ForwardTo; }
	public void setForwardTo(Long ForwardTo) { this.ForwardTo = ForwardTo; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.LocationConfiguration> LocationConfigurations; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramConfiguration> LoyaltyProgramConfigurations; 
    public static Phone Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Phone entity = new Phone();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.PhoneId = Entities.ParseLongValue(obj, "PhoneId");
          entity.ForwardTo = Entities.ParseLongValue(obj, "ForwardTo");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          JSONArray jsonLocationConfigurations = Entities.ParseJSONArrayValue(obj, "LocationConfigurations");
                if (jsonLocationConfigurations != null){
                    for (int index = 0; index < jsonLocationConfigurations.length();index++){
                        JSONObject lstItem = jsonLocationConfigurations.getJSONObject(index);
                        entity.LocationConfigurations.add(com.enrollandpay.serviceinterface.BusinessEntity.LocationConfiguration.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonLoyaltyProgramConfigurations = Entities.ParseJSONArrayValue(obj, "LoyaltyProgramConfigurations");
                if (jsonLoyaltyProgramConfigurations != null){
                    for (int index = 0; index < jsonLoyaltyProgramConfigurations.length();index++){
                        JSONObject lstItem = jsonLoyaltyProgramConfigurations.getJSONObject(index);
                        entity.LoyaltyProgramConfigurations.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramConfiguration.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Phone)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Phone() {
		LocationConfigurations = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.LocationConfiguration>();
			LoyaltyProgramConfigurations = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramConfiguration>();
			}
}
