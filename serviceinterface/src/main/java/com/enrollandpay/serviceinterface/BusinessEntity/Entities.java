package com.enrollandpay.serviceinterface.BusinessEntity;

import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;


@SuppressWarnings("all")
public class Entities {
	public static Integer ParseIntegerValue(JSONObject object, String propertyName){
        if (object == null || propertyName == null || !object.has(propertyName) || object.isNull(propertyName)){return  null;}
        try {
            return object.getInt(propertyName);
        }
        catch (Exception ex){
            Log.d(GlobalVariables.TAG, ex.toString());
        }
        return null;
    }
    public static Boolean ParseBooleanValue(JSONObject object, String propertyName){
        if (object == null || propertyName == null || !object.has(propertyName) || object.isNull(propertyName)){return  null;}
        try {
            return object.getBoolean(propertyName);
        }
        catch (Exception ex){
            Log.d(GlobalVariables.TAG, ex.toString());
        }
        return null;
    }
    public static JSONObject ParseJSONObjectValue(JSONObject object, String propertyName){
        if (object == null || propertyName == null || !object.has(propertyName) || object.isNull(propertyName)){return  null;}
        try {
            return object.getJSONObject(propertyName);
        }
        catch (Exception ex){
            Log.d(GlobalVariables.TAG, ex.toString());
        }
        return null;
    }
    public static JSONArray ParseJSONArrayValue(JSONObject object, String propertyName){
        if (object == null || propertyName == null || !object.has(propertyName) || object.isNull(propertyName)){return  null;}
        try {
            return object.getJSONArray(propertyName);
        }
        catch (Exception ex){
            Log.d(GlobalVariables.TAG, ex.toString());
        }
        return null;
    }
    public static Long ParseLongValue(JSONObject object, String propertyName){
        if (object == null || propertyName == null || !object.has(propertyName) || object.isNull(propertyName)){return  null;}
        try {
            return object.getLong(propertyName);
        }
        catch (Exception ex){
            Log.d(GlobalVariables.TAG, ex.toString());
        }
        return null;
    }
    public static String ParseStringValue(JSONObject object, String propertyName){
        if (object == null || propertyName == null || !object.has(propertyName) || object.isNull(propertyName)){return  null;}
        try {
            return object.getString(propertyName);
        }
        catch (Exception ex){
            Log.d(GlobalVariables.TAG, ex.toString());
        }
        return null;
    }
    public static Map<String, Object> ParseMapValue(JSONObject object, String propertyName){
        if (object == null || propertyName == null || !object.has(propertyName) || object.isNull(propertyName)){return  null;}
        try {
            Map<String, Object> retMap = new HashMap<String, Object>();
            String value = object.getString(propertyName);
            if(value != null) {
                JSONObject values =  new JSONObject(value);
                Iterator<String> iter = values.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    retMap.put(key, values.get(key));
                }
            }
            return retMap;
        }
        catch (Exception ex){
            Log.d(GlobalVariables.TAG, ex.toString());
        }
        return null;
    }
    public static Double ParseDoubleValue(JSONObject object, String propertyName){
        if (object == null || propertyName == null || !object.has(propertyName) || object.isNull(propertyName)){return  null;}
        try {
            return object.getDouble(propertyName);
        }
        catch (Exception ex){
            Log.d(GlobalVariables.TAG, ex.toString());
        }
        return null;
    }
    public static Date ParseDateValue(JSONObject object, String propertyName){
        if (object == null || propertyName == null || !object.has(propertyName) || object.isNull(propertyName)){return  null;}
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        try {
            Date date = format.parse(object.getString(propertyName));
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        try {
            Date date = format.parse(object.getString(propertyName));
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
    private static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    private static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
    public static AuthorizationRequest GetAuthorizationRequest(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest.Deserialize(lookups,entity);
    }
    public static ArrayList<AuthorizationRequest> GetAuthorizationRequests(JSONArray entities){
        ArrayList<AuthorizationRequest> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static BillingCycle GetBillingCycle(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.BillingCycle.Deserialize(lookups,entity);
    }
    public static ArrayList<BillingCycle> GetBillingCycles(JSONArray entities){
        ArrayList<BillingCycle> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.BillingCycle.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Card GetCard(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Card.Deserialize(lookups,entity);
    }
    public static ArrayList<Card> GetCards(JSONArray entities){
        ArrayList<Card> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Card.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static CardEncrypted GetCardEncrypted(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.CardEncrypted.Deserialize(lookups,entity);
    }
    public static ArrayList<CardEncrypted> GetCardEncrypteds(JSONArray entities){
        ArrayList<CardEncrypted> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.CardEncrypted.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Category GetCategory(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Category.Deserialize(lookups,entity);
    }
    public static ArrayList<Category> GetCategories(JSONArray entities){
        ArrayList<Category> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Category.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static CategoryProduct GetCategoryProduct(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.CategoryProduct.Deserialize(lookups,entity);
    }
    public static ArrayList<CategoryProduct> GetCategoryProducts(JSONArray entities){
        ArrayList<CategoryProduct> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.CategoryProduct.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Client GetClient(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Client.Deserialize(lookups,entity);
    }
    public static ArrayList<Client> GetClients(JSONArray entities){
        ArrayList<Client> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Client.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static ClientAccess GetClientAccess(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess.Deserialize(lookups,entity);
    }
    public static ArrayList<ClientAccess> GetClientAccesses(JSONArray entities){
        ArrayList<ClientAccess> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Coalition GetCoalition(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lookups,entity);
    }
    public static ArrayList<Coalition> GetCoalitions(JSONArray entities){
        ArrayList<Coalition> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static CoalitionAuthority GetCoalitionAuthority(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.CoalitionAuthority.Deserialize(lookups,entity);
    }
    public static ArrayList<CoalitionAuthority> GetCoalitionAuthorities(JSONArray entities){
        ArrayList<CoalitionAuthority> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.CoalitionAuthority.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static CoalitionMerchant GetCoalitionMerchant(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.CoalitionMerchant.Deserialize(lookups,entity);
    }
    public static ArrayList<CoalitionMerchant> GetCoalitionMerchants(JSONArray entities){
        ArrayList<CoalitionMerchant> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.CoalitionMerchant.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Consumer GetConsumer(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lookups,entity);
    }
    public static ArrayList<Consumer> GetConsumers(JSONArray entities){
        ArrayList<Consumer> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static ConsumerContactInformation GetConsumerContactInformation(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.ConsumerContactInformation.Deserialize(lookups,entity);
    }
    public static ArrayList<ConsumerContactInformation> GetConsumerContactInformations(JSONArray entities){
        ArrayList<ConsumerContactInformation> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerContactInformation.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static ConsumerLoyaltyProgram GetConsumerLoyaltyProgram(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram.Deserialize(lookups,entity);
    }
    public static ArrayList<ConsumerLoyaltyProgram> GetConsumerLoyaltyPrograms(JSONArray entities){
        ArrayList<ConsumerLoyaltyProgram> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static ConsumerLoyaltyProgramTransaction GetConsumerLoyaltyProgramTransaction(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction.Deserialize(lookups,entity);
    }
    public static ArrayList<ConsumerLoyaltyProgramTransaction> GetConsumerLoyaltyProgramTransactions(JSONArray entities){
        ArrayList<ConsumerLoyaltyProgramTransaction> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static ConsumerOffer GetConsumerOffer(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.ConsumerOffer.Deserialize(lookups,entity);
    }
    public static ArrayList<ConsumerOffer> GetConsumerOffers(JSONArray entities){
        ArrayList<ConsumerOffer> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerOffer.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static ConsumerRelationship GetConsumerRelationship(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship.Deserialize(lookups,entity);
    }
    public static ArrayList<ConsumerRelationship> GetConsumerRelationships(JSONArray entities){
        ArrayList<ConsumerRelationship> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static DailyOfferOverview GetDailyOfferOverview(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.DailyOfferOverview.Deserialize(lookups,entity);
    }
    public static ArrayList<DailyOfferOverview> GetDailyOfferOverviews(JSONArray entities){
        ArrayList<DailyOfferOverview> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.DailyOfferOverview.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static DailySaleOverview GetDailySaleOverview(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.DailySaleOverview.Deserialize(lookups,entity);
    }
    public static ArrayList<DailySaleOverview> GetDailySaleOverviews(JSONArray entities){
        ArrayList<DailySaleOverview> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.DailySaleOverview.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Device GetDevice(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Device.Deserialize(lookups,entity);
    }
    public static ArrayList<Device> GetDevices(JSONArray entities){
        ArrayList<Device> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Device.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Employee GetEmployee(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Employee.Deserialize(lookups,entity);
    }
    public static ArrayList<Employee> GetEmployees(JSONArray entities){
        ArrayList<Employee> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Employee.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static EmployeeAuthentication GetEmployeeAuthentication(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.EmployeeAuthentication.Deserialize(lookups,entity);
    }
    public static ArrayList<EmployeeAuthentication> GetEmployeeAuthentications(JSONArray entities){
        ArrayList<EmployeeAuthentication> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.EmployeeAuthentication.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static EmployeeImpersonation GetEmployeeImpersonation(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.EmployeeImpersonation.Deserialize(lookups,entity);
    }
    public static ArrayList<EmployeeImpersonation> GetEmployeeImpersonations(JSONArray entities){
        ArrayList<EmployeeImpersonation> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.EmployeeImpersonation.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static EmployeeLocation GetEmployeeLocation(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.EmployeeLocation.Deserialize(lookups,entity);
    }
    public static ArrayList<EmployeeLocation> GetEmployeeLocations(JSONArray entities){
        ArrayList<EmployeeLocation> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.EmployeeLocation.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Enrollment GetEnrollment(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Enrollment.Deserialize(lookups,entity);
    }
    public static ArrayList<Enrollment> GetEnrollments(JSONArray entities){
        ArrayList<Enrollment> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Enrollment.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Invoice GetInvoice(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Invoice.Deserialize(lookups,entity);
    }
    public static ArrayList<Invoice> GetInvoices(JSONArray entities){
        ArrayList<Invoice> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Invoice.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static InvoiceItem GetInvoiceItem(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.InvoiceItem.Deserialize(lookups,entity);
    }
    public static ArrayList<InvoiceItem> GetInvoiceItems(JSONArray entities){
        ArrayList<InvoiceItem> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.InvoiceItem.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Location GetLocation(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lookups,entity);
    }
    public static ArrayList<Location> GetLocations(JSONArray entities){
        ArrayList<Location> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static LocationConfiguration GetLocationConfiguration(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.LocationConfiguration.Deserialize(lookups,entity);
    }
    public static ArrayList<LocationConfiguration> GetLocationConfigurations(JSONArray entities){
        ArrayList<LocationConfiguration> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.LocationConfiguration.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static LogError GetLogError(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.LogError.Deserialize(lookups,entity);
    }
    public static ArrayList<LogError> GetLogErrors(JSONArray entities){
        ArrayList<LogError> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.LogError.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static LoyaltyProgram GetLoyaltyProgram(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram.Deserialize(lookups,entity);
    }
    public static ArrayList<LoyaltyProgram> GetLoyaltyPrograms(JSONArray entities){
        ArrayList<LoyaltyProgram> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static LoyaltyProgramConfiguration GetLoyaltyProgramConfiguration(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramConfiguration.Deserialize(lookups,entity);
    }
    public static ArrayList<LoyaltyProgramConfiguration> GetLoyaltyProgramConfigurations(JSONArray entities){
        ArrayList<LoyaltyProgramConfiguration> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramConfiguration.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static LoyaltyProgramParticipant GetLoyaltyProgramParticipant(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramParticipant.Deserialize(lookups,entity);
    }
    public static ArrayList<LoyaltyProgramParticipant> GetLoyaltyProgramParticipants(JSONArray entities){
        ArrayList<LoyaltyProgramParticipant> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramParticipant.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static LoyaltyProgramShare GetLoyaltyProgramShare(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShare.Deserialize(lookups,entity);
    }
    public static ArrayList<LoyaltyProgramShare> GetLoyaltyProgramShares(JSONArray entities){
        ArrayList<LoyaltyProgramShare> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShare.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static LoyaltyProgramShareConsumer GetLoyaltyProgramShareConsumer(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareConsumer.Deserialize(lookups,entity);
    }
    public static ArrayList<LoyaltyProgramShareConsumer> GetLoyaltyProgramShareConsumers(JSONArray entities){
        ArrayList<LoyaltyProgramShareConsumer> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareConsumer.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static LoyaltyProgramShareRecipient GetLoyaltyProgramShareRecipient(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareRecipient.Deserialize(lookups,entity);
    }
    public static ArrayList<LoyaltyProgramShareRecipient> GetLoyaltyProgramShareRecipients(JSONArray entities){
        ArrayList<LoyaltyProgramShareRecipient> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareRecipient.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Merchant GetMerchant(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lookups,entity);
    }
    public static ArrayList<Merchant> GetMerchants(JSONArray entities){
        ArrayList<Merchant> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static MerchantAccess GetMerchantAccess(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.MerchantAccess.Deserialize(lookups,entity);
    }
    public static ArrayList<MerchantAccess> GetMerchantAccesses(JSONArray entities){
        ArrayList<MerchantAccess> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.MerchantAccess.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static MerchantConfiguration GetMerchantConfiguration(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.MerchantConfiguration.Deserialize(lookups,entity);
    }
    public static ArrayList<MerchantConfiguration> GetMerchantConfigurations(JSONArray entities){
        ArrayList<MerchantConfiguration> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.MerchantConfiguration.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Message GetMessage(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Message.Deserialize(lookups,entity);
    }
    public static ArrayList<Message> GetMessages(JSONArray entities){
        ArrayList<Message> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Message.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static MessageTemplate GetMessageTemplate(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.MessageTemplate.Deserialize(lookups,entity);
    }
    public static ArrayList<MessageTemplate> GetMessageTemplates(JSONArray entities){
        ArrayList<MessageTemplate> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.MessageTemplate.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static MessageThread GetMessageThread(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.MessageThread.Deserialize(lookups,entity);
    }
    public static ArrayList<MessageThread> GetMessageThreads(JSONArray entities){
        ArrayList<MessageThread> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.MessageThread.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Offer GetOffer(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Offer.Deserialize(lookups,entity);
    }
    public static ArrayList<Offer> GetOffers(JSONArray entities){
        ArrayList<Offer> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Offer.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static OfferAssociation GetOfferAssociation(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.OfferAssociation.Deserialize(lookups,entity);
    }
    public static ArrayList<OfferAssociation> GetOfferAssociations(JSONArray entities){
        ArrayList<OfferAssociation> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.OfferAssociation.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static OfferOverview GetOfferOverview(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.OfferOverview.Deserialize(lookups,entity);
    }
    public static ArrayList<OfferOverview> GetOfferOverviews(JSONArray entities){
        ArrayList<OfferOverview> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.OfferOverview.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Order GetOrder(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Order.Deserialize(lookups,entity);
    }
    public static ArrayList<Order> GetOrders(JSONArray entities){
        ArrayList<Order> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Order.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static OrderItem GetOrderItem(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.OrderItem.Deserialize(lookups,entity);
    }
    public static ArrayList<OrderItem> GetOrderItems(JSONArray entities){
        ArrayList<OrderItem> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderItem.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static OrderPayment GetOrderPayment(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment.Deserialize(lookups,entity);
    }
    public static ArrayList<OrderPayment> GetOrderPayments(JSONArray entities){
        ArrayList<OrderPayment> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static OrderPaymentLoyaltyProgram GetOrderPaymentLoyaltyProgram(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentLoyaltyProgram.Deserialize(lookups,entity);
    }
    public static ArrayList<OrderPaymentLoyaltyProgram> GetOrderPaymentLoyaltyPrograms(JSONArray entities){
        ArrayList<OrderPaymentLoyaltyProgram> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentLoyaltyProgram.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static OrderPaymentOffer GetOrderPaymentOffer(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentOffer.Deserialize(lookups,entity);
    }
    public static ArrayList<OrderPaymentOffer> GetOrderPaymentOffers(JSONArray entities){
        ArrayList<OrderPaymentOffer> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentOffer.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static OrderReview GetOrderReview(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.OrderReview.Deserialize(lookups,entity);
    }
    public static ArrayList<OrderReview> GetOrderReviews(JSONArray entities){
        ArrayList<OrderReview> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderReview.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static OrderReviewQuestion GetOrderReviewQuestion(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.OrderReviewQuestion.Deserialize(lookups,entity);
    }
    public static ArrayList<OrderReviewQuestion> GetOrderReviewQuestions(JSONArray entities){
        ArrayList<OrderReviewQuestion> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderReviewQuestion.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static PasswordReset GetPasswordReset(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.PasswordReset.Deserialize(lookups,entity);
    }
    public static ArrayList<PasswordReset> GetPasswordResets(JSONArray entities){
        ArrayList<PasswordReset> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.PasswordReset.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Phone GetPhone(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Phone.Deserialize(lookups,entity);
    }
    public static ArrayList<Phone> GetPhones(JSONArray entities){
        ArrayList<Phone> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Phone.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static PhoneNumber GetPhoneNumber(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.PhoneNumber.Deserialize(lookups,entity);
    }
    public static ArrayList<PhoneNumber> GetPhoneNumbers(JSONArray entities){
        ArrayList<PhoneNumber> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.PhoneNumber.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Product GetProduct(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Product.Deserialize(lookups,entity);
    }
    public static ArrayList<Product> GetProducts(JSONArray entities){
        ArrayList<Product> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Product.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static ProductAddOn GetProductAddOn(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.ProductAddOn.Deserialize(lookups,entity);
    }
    public static ArrayList<ProductAddOn> GetProductAddOns(JSONArray entities){
        ArrayList<ProductAddOn> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.ProductAddOn.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static ProductRelationship GetProductRelationship(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.ProductRelationship.Deserialize(lookups,entity);
    }
    public static ArrayList<ProductRelationship> GetProductRelationships(JSONArray entities){
        ArrayList<ProductRelationship> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.ProductRelationship.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static ProductRelationshipHistory GetProductRelationshipHistory(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.ProductRelationshipHistory.Deserialize(lookups,entity);
    }
    public static ArrayList<ProductRelationshipHistory> GetProductRelationshipHistories(JSONArray entities){
        ArrayList<ProductRelationshipHistory> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.ProductRelationshipHistory.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static ProductRelationshipHolder GetProductRelationshipHolder(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.ProductRelationshipHolder.Deserialize(lookups,entity);
    }
    public static ArrayList<ProductRelationshipHolder> GetProductRelationshipHolders(JSONArray entities){
        ArrayList<ProductRelationshipHolder> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.ProductRelationshipHolder.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static ProductStatistic GetProductStatistic(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.ProductStatistic.Deserialize(lookups,entity);
    }
    public static ArrayList<ProductStatistic> GetProductStatistics(JSONArray entities){
        ArrayList<ProductStatistic> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.ProductStatistic.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Question GetQuestion(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Question.Deserialize(lookups,entity);
    }
    public static ArrayList<Question> GetQuestions(JSONArray entities){
        ArrayList<Question> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Question.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static QuestionStatistic GetQuestionStatistic(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.QuestionStatistic.Deserialize(lookups,entity);
    }
    public static ArrayList<QuestionStatistic> GetQuestionStatistics(JSONArray entities){
        ArrayList<QuestionStatistic> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.QuestionStatistic.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Representative GetRepresentative(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Representative.Deserialize(lookups,entity);
    }
    public static ArrayList<Representative> GetRepresentatives(JSONArray entities){
        ArrayList<Representative> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Representative.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static RepresentativeAuthentication GetRepresentativeAuthentication(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.RepresentativeAuthentication.Deserialize(lookups,entity);
    }
    public static ArrayList<RepresentativeAuthentication> GetRepresentativeAuthentications(JSONArray entities){
        ArrayList<RepresentativeAuthentication> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.RepresentativeAuthentication.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static SaleOverview GetSaleOverview(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.SaleOverview.Deserialize(lookups,entity);
    }
    public static ArrayList<SaleOverview> GetSaleOverviews(JSONArray entities){
        ArrayList<SaleOverview> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.SaleOverview.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Sponsor GetSponsor(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Sponsor.Deserialize(lookups,entity);
    }
    public static ArrayList<Sponsor> GetSponsors(JSONArray entities){
        ArrayList<Sponsor> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Sponsor.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Tabletop GetTabletop(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Tabletop.Deserialize(lookups,entity);
    }
    public static ArrayList<Tabletop> GetTabletops(JSONArray entities){
        ArrayList<Tabletop> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Tabletop.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Tax GetTax(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Tax.Deserialize(lookups,entity);
    }
    public static ArrayList<Tax> GetTaxes(JSONArray entities){
        ArrayList<Tax> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Tax.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static Team GetTeam(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.Team.Deserialize(lookups,entity);
    }
    public static ArrayList<Team> GetTeams(JSONArray entities){
        ArrayList<Team> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.Team.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
    public static TeamRepresentative GetTeamRepresentative(JSONObject entity){
        HashMap<String,Object> lookups = new HashMap();
        if (entity == null){return null;}
        return com.enrollandpay.serviceinterface.BusinessEntity.TeamRepresentative.Deserialize(lookups,entity);
    }
    public static ArrayList<TeamRepresentative> GetTeamRepresentatives(JSONArray entities){
        ArrayList<TeamRepresentative> lst = new ArrayList<>();
        HashMap<String,Object> lookups = new HashMap();
        if (entities == null){return lst;}
        for (int iter=0;iter<entities.length();iter++){
            try{
                lst.add(com.enrollandpay.serviceinterface.BusinessEntity.TeamRepresentative.Deserialize(lookups, entities.getJSONObject(iter)));
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return lst;
    }
		
}
