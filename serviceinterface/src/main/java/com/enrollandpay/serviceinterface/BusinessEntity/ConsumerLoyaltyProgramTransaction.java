package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class ConsumerLoyaltyProgramTransaction extends BaseBusinessEntity {
	private Integer ConsumerLoyaltyProgramTransactionId; 
	public Integer getConsumerLoyaltyProgramTransactionId() { return this.ConsumerLoyaltyProgramTransactionId; }
	public void setConsumerLoyaltyProgramTransactionId(Integer ConsumerLoyaltyProgramTransactionId) { this.ConsumerLoyaltyProgramTransactionId = ConsumerLoyaltyProgramTransactionId; }
	private Integer ConsumerLoyaltyProgramId; 
	public Integer getConsumerLoyaltyProgramId() { return this.ConsumerLoyaltyProgramId; }
	public void setConsumerLoyaltyProgramId(Integer ConsumerLoyaltyProgramId) { this.ConsumerLoyaltyProgramId = ConsumerLoyaltyProgramId; }
	private Integer OrderPaymentId; 
	public Integer getOrderPaymentId() { return this.OrderPaymentId; }
	public void setOrderPaymentId(Integer OrderPaymentId) { this.OrderPaymentId = OrderPaymentId; }
	private Double Points; 
	public Double getPoints() { return this.Points; }
	public void setPoints(Double Points) { this.Points = Points; }
	private Double RewardAmount; 
	public Double getRewardAmount() { return this.RewardAmount; }
	public void setRewardAmount(Double RewardAmount) { this.RewardAmount = RewardAmount; }
	private Double ProductValuedUpTo; 
	public Double getProductValuedUpTo() { return this.ProductValuedUpTo; }
	public void setProductValuedUpTo(Double ProductValuedUpTo) { this.ProductValuedUpTo = ProductValuedUpTo; }
	private Date TransactionDateTime; 
	public Date getTransactionDateTime() { return this.TransactionDateTime; }
	public void setTransactionDateTime(Date TransactionDateTime) { this.TransactionDateTime = TransactionDateTime; }
	private Date ExpirationDateTime; 
	public Date getExpirationDateTime() { return this.ExpirationDateTime; }
	public void setExpirationDateTime(Date ExpirationDateTime) { this.ExpirationDateTime = ExpirationDateTime; }
	private Integer TransactionType; 
	public Integer getTransactionType() { return this.TransactionType; }
	public void setTransactionType(Integer TransactionType) { this.TransactionType = TransactionType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private Integer OfferId; 
	public Integer getOfferId() { return this.OfferId; }
	public void setOfferId(Integer OfferId) { this.OfferId = OfferId; }
	private String StatusInformation; 
	public String getStatusInformation() { return this.StatusInformation; }
	public void setStatusInformation(String StatusInformation) { this.StatusInformation = StatusInformation; }
	private Integer OrderId; 
	public Integer getOrderId() { return this.OrderId; }
	public void setOrderId(Integer OrderId) { this.OrderId = OrderId; }
	private Double PointsSpent; 
	public Double getPointsSpent() { return this.PointsSpent; }
	public void setPointsSpent(Double PointsSpent) { this.PointsSpent = PointsSpent; }
	private Double RewardAmountSpent; 
	public Double getRewardAmountSpent() { return this.RewardAmountSpent; }
	public void setRewardAmountSpent(Double RewardAmountSpent) { this.RewardAmountSpent = RewardAmountSpent; }
	private String LoyaltyProgramShareId; 
	public String getLoyaltyProgramShareId() { return this.LoyaltyProgramShareId; }
	public void setLoyaltyProgramShareId(String LoyaltyProgramShareId) { this.LoyaltyProgramShareId = LoyaltyProgramShareId; }
    private com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram ConsumerLoyaltyProgram;
	public ConsumerLoyaltyProgram getConsumerLoyaltyProgram() { return this.ConsumerLoyaltyProgram; }
	public void setConsumerLoyaltyProgram(ConsumerLoyaltyProgram ConsumerLoyaltyProgram) { this.ConsumerLoyaltyProgram = ConsumerLoyaltyProgram; }
    private com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment OrderPayment;
	public OrderPayment getOrderPayment() { return this.OrderPayment; }
	public void setOrderPayment(OrderPayment OrderPayment) { this.OrderPayment = OrderPayment; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Offer Offer;
	public Offer getOffer() { return this.Offer; }
	public void setOffer(Offer Offer) { this.Offer = Offer; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Order Order;
	public Order getOrder() { return this.Order; }
	public void setOrder(Order Order) { this.Order = Order; }
    private com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShare LoyaltyProgramShare;
	public LoyaltyProgramShare getLoyaltyProgramShare() { return this.LoyaltyProgramShare; }
	public void setLoyaltyProgramShare(LoyaltyProgramShare LoyaltyProgramShare) { this.LoyaltyProgramShare = LoyaltyProgramShare; }
    public static ConsumerLoyaltyProgramTransaction Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                ConsumerLoyaltyProgramTransaction entity = new ConsumerLoyaltyProgramTransaction();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.ConsumerLoyaltyProgramTransactionId = Entities.ParseIntegerValue(obj, "ConsumerLoyaltyProgramTransactionId");
          entity.ConsumerLoyaltyProgramId = Entities.ParseIntegerValue(obj, "ConsumerLoyaltyProgramId");
          entity.OrderPaymentId = Entities.ParseIntegerValue(obj, "OrderPaymentId");
          entity.Points = Entities.ParseDoubleValue(obj, "Points");
          entity.RewardAmount = Entities.ParseDoubleValue(obj, "RewardAmount");
          entity.ProductValuedUpTo = Entities.ParseDoubleValue(obj, "ProductValuedUpTo");
          entity.TransactionDateTime = Entities.ParseDateValue(obj, "TransactionDateTime");
          entity.ExpirationDateTime = Entities.ParseDateValue(obj, "ExpirationDateTime");
          entity.TransactionType = Entities.ParseIntegerValue(obj, "TransactionType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.OfferId = Entities.ParseIntegerValue(obj, "OfferId");
          entity.StatusInformation = Entities.ParseStringValue(obj, "StatusInformation");
          entity.OrderId = Entities.ParseIntegerValue(obj, "OrderId");
          entity.PointsSpent = Entities.ParseDoubleValue(obj, "PointsSpent");
          entity.RewardAmountSpent = Entities.ParseDoubleValue(obj, "RewardAmountSpent");
          entity.LoyaltyProgramShareId = Entities.ParseStringValue(obj, "LoyaltyProgramShareId");
          if (obj.has("ConsumerLoyaltyProgram")){  entity.ConsumerLoyaltyProgram = com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"ConsumerLoyaltyProgram"));}
          if (obj.has("OrderPayment")){  entity.OrderPayment = com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"OrderPayment"));}
          if (obj.has("Offer")){  entity.Offer = com.enrollandpay.serviceinterface.BusinessEntity.Offer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Offer"));}
          if (obj.has("Order")){  entity.Order = com.enrollandpay.serviceinterface.BusinessEntity.Order.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Order"));}
          if (obj.has("LoyaltyProgramShare")){  entity.LoyaltyProgramShare = com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShare.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LoyaltyProgramShare"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (ConsumerLoyaltyProgramTransaction)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public ConsumerLoyaltyProgramTransaction() {
		}
}
