package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class LocationConfiguration extends BaseBusinessEntity {
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private String EmailAddressDomain; 
	public String getEmailAddressDomain() { return this.EmailAddressDomain; }
	public void setEmailAddressDomain(String EmailAddressDomain) { this.EmailAddressDomain = EmailAddressDomain; }
	private String POSConfiguration; 
	public String getPOSConfiguration() { return this.POSConfiguration; }
	public void setPOSConfiguration(String POSConfiguration) { this.POSConfiguration = POSConfiguration; }
	private Long PhoneId; 
	public Long getPhoneId() { return this.PhoneId; }
	public void setPhoneId(Long PhoneId) { this.PhoneId = PhoneId; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private String DefaultPOSRewardIdent; 
	public String getDefaultPOSRewardIdent() { return this.DefaultPOSRewardIdent; }
	public void setDefaultPOSRewardIdent(String DefaultPOSRewardIdent) { this.DefaultPOSRewardIdent = DefaultPOSRewardIdent; }
	private Integer DefaultApplyRewardOfferType; 
	public Integer getDefaultApplyRewardOfferType() { return this.DefaultApplyRewardOfferType; }
	public void setDefaultApplyRewardOfferType(Integer DefaultApplyRewardOfferType) { this.DefaultApplyRewardOfferType = DefaultApplyRewardOfferType; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
	public Location getLocation() { return this.Location; }
	public void setLocation(Location Location) { this.Location = Location; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Phone Phone;
	public Phone getPhone() { return this.Phone; }
	public void setPhone(Phone Phone) { this.Phone = Phone; }
    public static LocationConfiguration Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                LocationConfiguration entity = new LocationConfiguration();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.EmailAddressDomain = Entities.ParseStringValue(obj, "EmailAddressDomain");
          entity.POSConfiguration = Entities.ParseStringValue(obj, "POSConfiguration");
          entity.PhoneId = Entities.ParseLongValue(obj, "PhoneId");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.DefaultPOSRewardIdent = Entities.ParseStringValue(obj, "DefaultPOSRewardIdent");
          entity.DefaultApplyRewardOfferType = Entities.ParseIntegerValue(obj, "DefaultApplyRewardOfferType");
          if (obj.has("Location")){  entity.Location = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Location"));}
          if (obj.has("Phone")){  entity.Phone = com.enrollandpay.serviceinterface.BusinessEntity.Phone.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Phone"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (LocationConfiguration)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public LocationConfiguration() {
		}
}
