package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class EmployeeAuthentication extends BaseBusinessEntity {
	private Integer EmployeeId; 
	public Integer getEmployeeId() { return this.EmployeeId; }
	public void setEmployeeId(Integer EmployeeId) { this.EmployeeId = EmployeeId; }
	private String EncryptedPassword; 
	public String getEncryptedPassword() { return this.EncryptedPassword; }
	public void setEncryptedPassword(String EncryptedPassword) { this.EncryptedPassword = EncryptedPassword; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Employee Employee;
	public Employee getEmployee() { return this.Employee; }
	public void setEmployee(Employee Employee) { this.Employee = Employee; }
    public static EmployeeAuthentication Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                EmployeeAuthentication entity = new EmployeeAuthentication();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.EmployeeId = Entities.ParseIntegerValue(obj, "EmployeeId");
          entity.EncryptedPassword = Entities.ParseStringValue(obj, "EncryptedPassword");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Employee")){  entity.Employee = com.enrollandpay.serviceinterface.BusinessEntity.Employee.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Employee"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (EmployeeAuthentication)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public EmployeeAuthentication() {
		}
}
