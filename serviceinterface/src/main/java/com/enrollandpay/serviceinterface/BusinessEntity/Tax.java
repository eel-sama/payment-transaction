package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Tax implements Serializable {
	private Integer TaxId; 
	public Integer getTaxId() { return this.TaxId; }
	public void setTaxId(Integer TaxId) { this.TaxId = TaxId; }
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private String Name; 
	public String getName() { return this.Name; }
	public void setName(String Name) { this.Name = Name; }
	private Double Percentage; 
	public Double getPercentage() { return this.Percentage; }
	public void setPercentage(Double Percentage) { this.Percentage = Percentage; }
	private Integer StatusType;
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
	public Location getLocation() { return this.Location; }
	public void setLocation(Location Location) { this.Location = Location; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderItem> OrderItems; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Product> Products; 
    public static Tax Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Tax entity = new Tax();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.TaxId = Entities.ParseIntegerValue(obj, "TaxId");
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.Name = Entities.ParseStringValue(obj, "Name");
          entity.Percentage = Entities.ParseDoubleValue(obj, "Percentage");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Location")){  entity.Location = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Location"));}
          JSONArray jsonOrderItems = Entities.ParseJSONArrayValue(obj, "OrderItems");
                if (jsonOrderItems != null){
                    for (int index = 0; index < jsonOrderItems.length();index++){
                        JSONObject lstItem = jsonOrderItems.getJSONObject(index);
                        entity.OrderItems.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderItem.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonProducts = Entities.ParseJSONArrayValue(obj, "Products");
                if (jsonProducts != null){
                    for (int index = 0; index < jsonProducts.length();index++){
                        JSONObject lstItem = jsonProducts.getJSONObject(index);
                        entity.Products.add(com.enrollandpay.serviceinterface.BusinessEntity.Product.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Tax)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Tax() {
		OrderItems = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderItem>();
			Products = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Product>();
			}
}
