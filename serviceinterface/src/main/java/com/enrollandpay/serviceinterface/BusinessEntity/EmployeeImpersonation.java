package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class EmployeeImpersonation extends BaseBusinessEntity {
	private Integer EmployeeImpersonationId; 
	public Integer getEmployeeImpersonationId() { return this.EmployeeImpersonationId; }
	public void setEmployeeImpersonationId(Integer EmployeeImpersonationId) { this.EmployeeImpersonationId = EmployeeImpersonationId; }
	private Integer RepresentativeId; 
	public Integer getRepresentativeId() { return this.RepresentativeId; }
	public void setRepresentativeId(Integer RepresentativeId) { this.RepresentativeId = RepresentativeId; }
	private Integer EmployeeId; 
	public Integer getEmployeeId() { return this.EmployeeId; }
	public void setEmployeeId(Integer EmployeeId) { this.EmployeeId = EmployeeId; }
	private String ClientAccessId; 
	public String getClientAccessId() { return this.ClientAccessId; }
	public void setClientAccessId(String ClientAccessId) { this.ClientAccessId = ClientAccessId; }
	private Date StartDate; 
	public Date getStartDate() { return this.StartDate; }
	public void setStartDate(Date StartDate) { this.StartDate = StartDate; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess ClientAccess;
	public ClientAccess getClientAccess() { return this.ClientAccess; }
	public void setClientAccess(ClientAccess ClientAccess) { this.ClientAccess = ClientAccess; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Employee Employee;
	public Employee getEmployee() { return this.Employee; }
	public void setEmployee(Employee Employee) { this.Employee = Employee; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Representative Representative;
	public Representative getRepresentative() { return this.Representative; }
	public void setRepresentative(Representative Representative) { this.Representative = Representative; }
    public static EmployeeImpersonation Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                EmployeeImpersonation entity = new EmployeeImpersonation();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.EmployeeImpersonationId = Entities.ParseIntegerValue(obj, "EmployeeImpersonationId");
          entity.RepresentativeId = Entities.ParseIntegerValue(obj, "RepresentativeId");
          entity.EmployeeId = Entities.ParseIntegerValue(obj, "EmployeeId");
          entity.ClientAccessId = Entities.ParseStringValue(obj, "ClientAccessId");
          entity.StartDate = Entities.ParseDateValue(obj, "StartDate");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("ClientAccess")){  entity.ClientAccess = com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"ClientAccess"));}
          if (obj.has("Employee")){  entity.Employee = com.enrollandpay.serviceinterface.BusinessEntity.Employee.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Employee"));}
          if (obj.has("Representative")){  entity.Representative = com.enrollandpay.serviceinterface.BusinessEntity.Representative.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Representative"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (EmployeeImpersonation)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public EmployeeImpersonation() {
		}
}
