package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class RepresentativeAuthentication extends BaseBusinessEntity {
	private Integer RepresentativeId; 
	public Integer getRepresentativeId() { return this.RepresentativeId; }
	public void setRepresentativeId(Integer RepresentativeId) { this.RepresentativeId = RepresentativeId; }
	private String EncryptedPassword; 
	public String getEncryptedPassword() { return this.EncryptedPassword; }
	public void setEncryptedPassword(String EncryptedPassword) { this.EncryptedPassword = EncryptedPassword; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Representative Representative;
	public Representative getRepresentative() { return this.Representative; }
	public void setRepresentative(Representative Representative) { this.Representative = Representative; }
    public static RepresentativeAuthentication Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                RepresentativeAuthentication entity = new RepresentativeAuthentication();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.RepresentativeId = Entities.ParseIntegerValue(obj, "RepresentativeId");
          entity.EncryptedPassword = Entities.ParseStringValue(obj, "EncryptedPassword");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Representative")){  entity.Representative = com.enrollandpay.serviceinterface.BusinessEntity.Representative.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Representative"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (RepresentativeAuthentication)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public RepresentativeAuthentication() {
		}
}
