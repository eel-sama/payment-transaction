package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Question extends BaseBusinessEntity {
	private Integer QuestionId; 
	public Integer getQuestionId() { return this.QuestionId; }
	public void setQuestionId(Integer QuestionId) { this.QuestionId = QuestionId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private String Label; 
	public String getLabel() { return this.Label; }
	public void setLabel(String Label) { this.Label = Label; }
	private String Display; 
	public String getDisplay() { return this.Display; }
	public void setDisplay(String Display) { this.Display = Display; }
	private Integer QuestionType; 
	public Integer getQuestionType() { return this.QuestionType; }
	public void setQuestionType(Integer QuestionType) { this.QuestionType = QuestionType; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderReviewQuestion> OrderReviewQuestions; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.QuestionStatistic> QuestionStatistics; 
    public static Question Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Question entity = new Question();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.QuestionId = Entities.ParseIntegerValue(obj, "QuestionId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.Label = Entities.ParseStringValue(obj, "Label");
          entity.Display = Entities.ParseStringValue(obj, "Display");
          entity.QuestionType = Entities.ParseIntegerValue(obj, "QuestionType");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
          JSONArray jsonOrderReviewQuestions = Entities.ParseJSONArrayValue(obj, "OrderReviewQuestions");
                if (jsonOrderReviewQuestions != null){
                    for (int index = 0; index < jsonOrderReviewQuestions.length();index++){
                        JSONObject lstItem = jsonOrderReviewQuestions.getJSONObject(index);
                        entity.OrderReviewQuestions.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderReviewQuestion.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonQuestionStatistics = Entities.ParseJSONArrayValue(obj, "QuestionStatistics");
                if (jsonQuestionStatistics != null){
                    for (int index = 0; index < jsonQuestionStatistics.length();index++){
                        JSONObject lstItem = jsonQuestionStatistics.getJSONObject(index);
                        entity.QuestionStatistics.add(com.enrollandpay.serviceinterface.BusinessEntity.QuestionStatistic.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Question)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Question() {
		OrderReviewQuestions = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderReviewQuestion>();
			QuestionStatistics = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.QuestionStatistic>();
			}
}
