package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class LoyaltyProgramShareRecipient extends BaseBusinessEntity {
	private String LoyaltyProgramShareRecipientId; 
	public String getLoyaltyProgramShareRecipientId() { return this.LoyaltyProgramShareRecipientId; }
	public void setLoyaltyProgramShareRecipientId(String LoyaltyProgramShareRecipientId) { this.LoyaltyProgramShareRecipientId = LoyaltyProgramShareRecipientId; }
	private String LoyaltyProgramShareId; 
	public String getLoyaltyProgramShareId() { return this.LoyaltyProgramShareId; }
	public void setLoyaltyProgramShareId(String LoyaltyProgramShareId) { this.LoyaltyProgramShareId = LoyaltyProgramShareId; }
	private Integer SenderId; 
	public Integer getSenderId() { return this.SenderId; }
	public void setSenderId(Integer SenderId) { this.SenderId = SenderId; }
	private String EmailAddress; 
	public String getEmailAddress() { return this.EmailAddress; }
	public void setEmailAddress(String EmailAddress) { this.EmailAddress = EmailAddress; }
	private String PhoneNumber; 
	public String getPhoneNumber() { return this.PhoneNumber; }
	public void setPhoneNumber(String PhoneNumber) { this.PhoneNumber = PhoneNumber; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    private com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShare LoyaltyProgramShare;
	public LoyaltyProgramShare getLoyaltyProgramShare() { return this.LoyaltyProgramShare; }
	public void setLoyaltyProgramShare(LoyaltyProgramShare LoyaltyProgramShare) { this.LoyaltyProgramShare = LoyaltyProgramShare; }
    public static LoyaltyProgramShareRecipient Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                LoyaltyProgramShareRecipient entity = new LoyaltyProgramShareRecipient();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.LoyaltyProgramShareRecipientId = Entities.ParseStringValue(obj, "LoyaltyProgramShareRecipientId");
          entity.LoyaltyProgramShareId = Entities.ParseStringValue(obj, "LoyaltyProgramShareId");
          entity.SenderId = Entities.ParseIntegerValue(obj, "SenderId");
          entity.EmailAddress = Entities.ParseStringValue(obj, "EmailAddress");
          entity.PhoneNumber = Entities.ParseStringValue(obj, "PhoneNumber");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
          if (obj.has("LoyaltyProgramShare")){  entity.LoyaltyProgramShare = com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShare.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LoyaltyProgramShare"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (LoyaltyProgramShareRecipient)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public LoyaltyProgramShareRecipient() {
		}
}
