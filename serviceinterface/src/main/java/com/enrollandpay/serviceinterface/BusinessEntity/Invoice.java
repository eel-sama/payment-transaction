package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Invoice extends BaseBusinessEntity {
	private Integer InvoiceId; 
	public Integer getInvoiceId() { return this.InvoiceId; }
	public void setInvoiceId(Integer InvoiceId) { this.InvoiceId = InvoiceId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private Integer CoalitionId; 
	public Integer getCoalitionId() { return this.CoalitionId; }
	public void setCoalitionId(Integer CoalitionId) { this.CoalitionId = CoalitionId; }
	private Integer BillingCycleId; 
	public Integer getBillingCycleId() { return this.BillingCycleId; }
	public void setBillingCycleId(Integer BillingCycleId) { this.BillingCycleId = BillingCycleId; }
	private Double AmountDue; 
	public Double getAmountDue() { return this.AmountDue; }
	public void setAmountDue(Double AmountDue) { this.AmountDue = AmountDue; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer PrepaidCampaigns; 
	public Integer getPrepaidCampaigns() { return this.PrepaidCampaigns; }
	public void setPrepaidCampaigns(Integer PrepaidCampaigns) { this.PrepaidCampaigns = PrepaidCampaigns; }
	private Integer PrepaidCampaignsUsed; 
	public Integer getPrepaidCampaignsUsed() { return this.PrepaidCampaignsUsed; }
	public void setPrepaidCampaignsUsed(Integer PrepaidCampaignsUsed) { this.PrepaidCampaignsUsed = PrepaidCampaignsUsed; }
    private com.enrollandpay.serviceinterface.BusinessEntity.BillingCycle BillingCycle;
	public BillingCycle getBillingCycle() { return this.BillingCycle; }
	public void setBillingCycle(BillingCycle BillingCycle) { this.BillingCycle = BillingCycle; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Coalition Coalition;
	public Coalition getCoalition() { return this.Coalition; }
	public void setCoalition(Coalition Coalition) { this.Coalition = Coalition; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.InvoiceItem> InvoiceItems; 
    public static Invoice Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Invoice entity = new Invoice();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.InvoiceId = Entities.ParseIntegerValue(obj, "InvoiceId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.CoalitionId = Entities.ParseIntegerValue(obj, "CoalitionId");
          entity.BillingCycleId = Entities.ParseIntegerValue(obj, "BillingCycleId");
          entity.AmountDue = Entities.ParseDoubleValue(obj, "AmountDue");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.PrepaidCampaigns = Entities.ParseIntegerValue(obj, "PrepaidCampaigns");
          entity.PrepaidCampaignsUsed = Entities.ParseIntegerValue(obj, "PrepaidCampaignsUsed");
          if (obj.has("BillingCycle")){  entity.BillingCycle = com.enrollandpay.serviceinterface.BusinessEntity.BillingCycle.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"BillingCycle"));}
          if (obj.has("Coalition")){  entity.Coalition = com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Coalition"));}
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
          JSONArray jsonInvoiceItems = Entities.ParseJSONArrayValue(obj, "InvoiceItems");
                if (jsonInvoiceItems != null){
                    for (int index = 0; index < jsonInvoiceItems.length();index++){
                        JSONObject lstItem = jsonInvoiceItems.getJSONObject(index);
                        entity.InvoiceItems.add(com.enrollandpay.serviceinterface.BusinessEntity.InvoiceItem.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Invoice)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Invoice() {
		InvoiceItems = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.InvoiceItem>();
			}
}
