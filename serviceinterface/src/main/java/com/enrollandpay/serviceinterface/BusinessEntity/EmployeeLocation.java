package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class EmployeeLocation extends BaseBusinessEntity {
	private Integer EmployeeId; 
	public Integer getEmployeeId() { return this.EmployeeId; }
	public void setEmployeeId(Integer EmployeeId) { this.EmployeeId = EmployeeId; }
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private Integer AccessType; 
	public Integer getAccessType() { return this.AccessType; }
	public void setAccessType(Integer AccessType) { this.AccessType = AccessType; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private String PosIdents; 
	public String getPosIdents() { return this.PosIdents; }
	public void setPosIdents(String PosIdents) { this.PosIdents = PosIdents; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Employee Employee;
	public Employee getEmployee() { return this.Employee; }
	public void setEmployee(Employee Employee) { this.Employee = Employee; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
	public Location getLocation() { return this.Location; }
	public void setLocation(Location Location) { this.Location = Location; }
    public static EmployeeLocation Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                EmployeeLocation entity = new EmployeeLocation();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.EmployeeId = Entities.ParseIntegerValue(obj, "EmployeeId");
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.AccessType = Entities.ParseIntegerValue(obj, "AccessType");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.PosIdents = Entities.ParseStringValue(obj, "PosIdents");
          if (obj.has("Employee")){  entity.Employee = com.enrollandpay.serviceinterface.BusinessEntity.Employee.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Employee"));}
          if (obj.has("Location")){  entity.Location = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Location"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (EmployeeLocation)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public EmployeeLocation() {
		}
}
