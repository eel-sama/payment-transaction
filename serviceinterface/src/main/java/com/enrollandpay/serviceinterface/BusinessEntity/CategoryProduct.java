package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class CategoryProduct implements Serializable {
	private Integer CategoryProductId; 
	public Integer getCategoryProductId() { return this.CategoryProductId; }
	public void setCategoryProductId(Integer CategoryProductId) { this.CategoryProductId = CategoryProductId; }
	private Integer CategoryId; 
	public Integer getCategoryId() { return this.CategoryId; }
	public void setCategoryId(Integer CategoryId) { this.CategoryId = CategoryId; }
	private Integer ProductId; 
	public Integer getProductId() { return this.ProductId; }
	public void setProductId(Integer ProductId) { this.ProductId = ProductId; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Category Category;
	public Category getCategory() { return this.Category; }
	public void setCategory(Category Category) { this.Category = Category; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Product Product;
	public Product getProduct() { return this.Product; }
	public void setProduct(Product Product) { this.Product = Product; }
    public static CategoryProduct Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                CategoryProduct entity = new CategoryProduct();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.CategoryProductId = Entities.ParseIntegerValue(obj, "CategoryProductId");
          entity.CategoryId = Entities.ParseIntegerValue(obj, "CategoryId");
          entity.ProductId = Entities.ParseIntegerValue(obj, "ProductId");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Category")){  entity.Category = com.enrollandpay.serviceinterface.BusinessEntity.Category.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Category"));}
          if (obj.has("Product")){  entity.Product = com.enrollandpay.serviceinterface.BusinessEntity.Product.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Product"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (CategoryProduct)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public CategoryProduct() {}
}
