package com.enrollandpay.serviceinterface.BusinessEntity;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Order extends BaseBusinessEntity implements Parcelable {
	private Integer OrderId; 
	public Integer getOrderId() { return this.OrderId; }
	public void setOrderId(Integer OrderId) { this.OrderId = OrderId; }
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private Integer SaleEmployeeId; 
	public Integer getSaleEmployeeId() { return this.SaleEmployeeId; }
	public void setSaleEmployeeId(Integer SaleEmployeeId) { this.SaleEmployeeId = SaleEmployeeId; }
	private Double AmountTotal; 
	public Double getAmountTotal() { return this.AmountTotal; }
	public void setAmountTotal(Double AmountTotal) { this.AmountTotal = AmountTotal; }
	private Double AmountTax; 
	public Double getAmountTax() { return this.AmountTax; }
	public void setAmountTax(Double AmountTax) { this.AmountTax = AmountTax; }
	private Double AmountGratuity; 
	public Double getAmountGratuity() { return this.AmountGratuity; }
	public void setAmountGratuity(Double AmountGratuity) { this.AmountGratuity = AmountGratuity; }
	private Double AmountTendered; 
	public Double getAmountTendered() { return this.AmountTendered; }
	public void setAmountTendered(Double AmountTendered) { this.AmountTendered = AmountTendered; }
	private Double AmountDiscount; 
	public Double getAmountDiscount() { return this.AmountDiscount; }
	public void setAmountDiscount(Double AmountDiscount) { this.AmountDiscount = AmountDiscount; }
    private Double AmountReturned;
    public Double getAmountReturned() { return this.AmountReturned; }
    public void setAmountReturned(Double AmountReturned) { this.AmountReturned = AmountReturned; }
	private Double AmountReward; 
	public Double getAmountReward() { return this.AmountReward; }
	public void setAmountReward(Double AmountReward) { this.AmountReward = AmountReward; }
	private String PosIdent; 
	public String getPosIdent() { return this.PosIdent; }
	public void setPosIdent(String PosIdent) { this.PosIdent = PosIdent; }
	private Date StartDateTime;
	public Date getStartDateTime() { return this.StartDateTime; }
	public void setStartDateTime(Date StartDateTime) { this.StartDateTime = StartDateTime; }
	private Date EndDateTime; 
	public Date getEndDateTime() { return this.EndDateTime; }
	public void setEndDateTime(Date EndDateTime) { this.EndDateTime = EndDateTime; }
	private Integer POSDeviceId; 
	public Integer getPOSDeviceId() { return this.POSDeviceId; }
	public void setPOSDeviceId(Integer POSDeviceId) { this.POSDeviceId = POSDeviceId; }
	private Integer PaymentDeviceId; 
	public Integer getPaymentDeviceId() { return this.PaymentDeviceId; }
	public void setPaymentDeviceId(Integer PaymentDeviceId) { this.PaymentDeviceId = PaymentDeviceId; }
	private Integer EnrollmentType; 
	public Integer getEnrollmentType() { return this.EnrollmentType; }
	public void setEnrollmentType(Integer EnrollmentType) { this.EnrollmentType = EnrollmentType; }
	private String AdditionalAttributes; 
	public String getAdditionalAttributes() { return this.AdditionalAttributes; }
	public void setAdditionalAttributes(String AdditionalAttributes) { this.AdditionalAttributes = AdditionalAttributes; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private String Commands; 
	public String getCommands() { return this.Commands; }
	public void setCommands(String Commands) { this.Commands = Commands; }
	private Double AmountLoyaltyUsed; 
	public Double getAmountLoyaltyUsed() { return this.AmountLoyaltyUsed; }
	public void setAmountLoyaltyUsed(Double AmountLoyaltyUsed) { this.AmountLoyaltyUsed = AmountLoyaltyUsed; }
	private Integer CurrencyType; 
	public Integer getCurrencyType() { return this.CurrencyType; }
	public void setCurrencyType(Integer CurrencyType) { this.CurrencyType = CurrencyType; }
	private Double AmountGratuity2; 
	public Double getAmountGratuity2() { return this.AmountGratuity2; }
	public void setAmountGratuity2(Double AmountGratuity2) { this.AmountGratuity2 = AmountGratuity2; }
	private Double AmountGratuity3; 
	public Double getAmountGratuity3() { return this.AmountGratuity3; }
	public void setAmountGratuity3(Double AmountGratuity3) { this.AmountGratuity3 = AmountGratuity3; }
	private Double AmountSurcharge; 
	public Double getAmountSurcharge() { return this.AmountSurcharge; }
	public void setAmountSurcharge(Double AmountSurcharge) { this.AmountSurcharge = AmountSurcharge; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Device PaymentDevice;
	public Device getPaymentDevice() { return this.PaymentDevice; }
	public void setPaymentDevice(Device PaymentDevice) { this.PaymentDevice = PaymentDevice; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Device POSDevice;
	public Device getPOSDevice() { return this.POSDevice; }
	public void setPOSDevice(Device POSDevice) { this.POSDevice = POSDevice; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Employee SaleEmployee;
	public Employee getSaleEmployee() { return this.SaleEmployee; }
	public void setSaleEmployee(Employee SaleEmployee) { this.SaleEmployee = SaleEmployee; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
	public Location getLocation() { return this.Location; }
	public void setLocation(Location Location) { this.Location = Location; }
	//private com.enrollandpay.serviceinterface.BusinessEntity.Tabletop;
    private Integer OriginalOrderId;
    public Integer getOriginalOrderId() { return this.OriginalOrderId; }
    public void setOriginalOrderId(Integer OriginalOrderId) { this.OriginalOrderId = OriginalOrderId; }
    private Integer RelayPOSDeviceId;
    public Integer getRelayPOSDeviceId() { return this.RelayPOSDeviceId; }
    public void setRelayPOSDeviceId(Integer RelayPOSDeviceId) { this.RelayPOSDeviceId = RelayPOSDeviceId; }
    private Integer RelayPaymentDeviceId;
    public Integer getRelayPaymentDeviceId() { return this.RelayPaymentDeviceId; }
    public void setRelayPaymentDeviceId(Integer RelayPaymentDeviceId) { this.RelayPaymentDeviceId = RelayPaymentDeviceId; }
    private Integer TabletopId;
    public Integer getTabletopId() { return this.TabletopId; }
    public void setTabletopId(Integer TabletopId) { this.TabletopId = TabletopId; }
    public List<OrderItem> getOrderItems() {
        return OrderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        OrderItems = orderItems;
    }

    public List<OrderPayment> getOrderPayments() {
        return OrderPayments;
    }

    public void setOrderPayments(List<OrderPayment> orderPayments) {
        OrderPayments = orderPayments;
    }

    public Double getOrderTip() {
	    if(getAmountGratuity() != null && getAmountGratuity() > 0.0) {
	        return getAmountGratuity();
        } else if(getOrderPayments() != null) {
            for (OrderPayment orderPayment : getOrderPayments()) {
                if (orderPayment.getAmountGratuity() != null && orderPayment.getAmountGratuity() > 0.0) {
                    return orderPayment.getAmountGratuity();
                }
            }
        }

	    return null;
    }

    public Double getTotalOrderItemsAmount() {
        if(getOrderItems() != null) {
            Double total = 0.0;
            for(OrderItem item : getOrderItems()) {
                if(item.getAmountTotal() != null && item.getQuantity() != null) {
                    if(item.getQuantity() > 0) {
                        if(!Double.isNaN(item.getAmountTotal()) || !Double.isInfinite(item.getAmountTotal())) {
                            total += item.getAmountTotal();
                        }
                    }
                }
            }
            return total;
        } else {
            return getAmountTotal() != null ? getAmountTotal() : 0.0;
        }
    }

    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderItem> OrderItems;
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment> OrderPayments; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction> ConsumerLoyaltyProgramTransactions; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderReview> OrderReviews;
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Message> Messages; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship> ConsumerRelationships; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.MessageThread> MessageThreads; 
    public static Order Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Order entity = new Order();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.OrderId = Entities.ParseIntegerValue(obj, "OrderId");
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.SaleEmployeeId = Entities.ParseIntegerValue(obj, "SaleEmployeeId");
          entity.AmountTotal = Entities.ParseDoubleValue(obj, "AmountTotal");
          entity.AmountTax = Entities.ParseDoubleValue(obj, "AmountTax");
          entity.AmountGratuity = Entities.ParseDoubleValue(obj, "AmountGratuity");
          entity.AmountTendered = Entities.ParseDoubleValue(obj, "AmountTendered");
          entity.AmountDiscount = Entities.ParseDoubleValue(obj, "AmountDiscount");
          entity.AmountReward = Entities.ParseDoubleValue(obj, "AmountReward");
          entity.AmountReturned = Entities.ParseDoubleValue(obj, "AmountReturned");
          entity.PosIdent = Entities.ParseStringValue(obj, "PosIdent");
          entity.StartDateTime = Entities.ParseDateValue(obj, "StartDateTime");
          entity.EndDateTime = Entities.ParseDateValue(obj, "EndDateTime");
          entity.POSDeviceId = Entities.ParseIntegerValue(obj, "POSDeviceId");
          entity.PaymentDeviceId = Entities.ParseIntegerValue(obj, "PaymentDeviceId");
          entity.EnrollmentType = Entities.ParseIntegerValue(obj, "EnrollmentType");
          entity.AdditionalAttributes = Entities.ParseStringValue(obj, "AdditionalAttributes");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.Commands = Entities.ParseStringValue(obj, "Commands");
          entity.AmountLoyaltyUsed = Entities.ParseDoubleValue(obj, "AmountLoyaltyUsed");
          entity.CurrencyType = Entities.ParseIntegerValue(obj, "CurrencyType");
          entity.AmountGratuity2 = Entities.ParseDoubleValue(obj, "AmountGratuity2");
          entity.AmountGratuity3 = Entities.ParseDoubleValue(obj, "AmountGratuity3");
          entity.AmountSurcharge = Entities.ParseDoubleValue(obj, "AmountSurcharge");
          entity.TabletopId = Entities.ParseIntegerValue(obj, "TabletopId");
          if (obj.has("PaymentDevice")){  entity.PaymentDevice = com.enrollandpay.serviceinterface.BusinessEntity.Device.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"PaymentDevice"));}
          if (obj.has("POSDevice")){  entity.POSDevice = com.enrollandpay.serviceinterface.BusinessEntity.Device.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"POSDevice"));}
          if (obj.has("SaleEmployee")){  entity.SaleEmployee = com.enrollandpay.serviceinterface.BusinessEntity.Employee.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"SaleEmployee"));}
          if (obj.has("Location")){  entity.Location = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Location"));}
          //if (obj.has("Tabletop")){  entity.Tabletop = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Tabletop"));}
          JSONArray jsonOrderItems = Entities.ParseJSONArrayValue(obj, "OrderItems");
                if (jsonOrderItems != null){
                    for (int index = 0; index < jsonOrderItems.length();index++){
                        JSONObject lstItem = jsonOrderItems.getJSONObject(index);
                        entity.OrderItems.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderItem.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOrderPayments = Entities.ParseJSONArrayValue(obj, "OrderPayments");
                if (jsonOrderPayments != null){
                    for (int index = 0; index < jsonOrderPayments.length();index++){
                        JSONObject lstItem = jsonOrderPayments.getJSONObject(index);
                        entity.OrderPayments.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerLoyaltyProgramTransactions = Entities.ParseJSONArrayValue(obj, "ConsumerLoyaltyProgramTransactions");
                if (jsonConsumerLoyaltyProgramTransactions != null){
                    for (int index = 0; index < jsonConsumerLoyaltyProgramTransactions.length();index++){
                        JSONObject lstItem = jsonConsumerLoyaltyProgramTransactions.getJSONObject(index);
                        entity.ConsumerLoyaltyProgramTransactions.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOrderReviews = Entities.ParseJSONArrayValue(obj, "OrderReviews");
                if (jsonOrderReviews != null){
                    for (int index = 0; index < jsonOrderReviews.length();index++){
                        JSONObject lstItem = jsonOrderReviews.getJSONObject(index);
                        entity.OrderReviews.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderReview.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMessages = Entities.ParseJSONArrayValue(obj, "Messages");
                if (jsonMessages != null){
                    for (int index = 0; index < jsonMessages.length();index++){
                        JSONObject lstItem = jsonMessages.getJSONObject(index);
                        entity.Messages.add(com.enrollandpay.serviceinterface.BusinessEntity.Message.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerRelationships = Entities.ParseJSONArrayValue(obj, "ConsumerRelationships");
                if (jsonConsumerRelationships != null){
                    for (int index = 0; index < jsonConsumerRelationships.length();index++){
                        JSONObject lstItem = jsonConsumerRelationships.getJSONObject(index);
                        entity.ConsumerRelationships.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMessageThreads = Entities.ParseJSONArrayValue(obj, "MessageThreads");
                if (jsonMessageThreads != null){
                    for (int index = 0; index < jsonMessageThreads.length();index++){
                        JSONObject lstItem = jsonMessageThreads.getJSONObject(index);
                        entity.MessageThreads.add(com.enrollandpay.serviceinterface.BusinessEntity.MessageThread.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Order)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Order() {
		OrderItems = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderItem>();
			OrderPayments = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment>();
			ConsumerLoyaltyProgramTransactions = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgramTransaction>();
			OrderReviews = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderReview>();
			Messages = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Message>();
			ConsumerRelationships = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship>();
			MessageThreads = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.MessageThread>();
			}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
