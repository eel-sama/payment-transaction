package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class BillingCycle extends BaseBusinessEntity {
	private Integer BillingCycleId; 
	public Integer getBillingCycleId() { return this.BillingCycleId; }
	public void setBillingCycleId(Integer BillingCycleId) { this.BillingCycleId = BillingCycleId; }
	private Integer CoalitionId; 
	public Integer getCoalitionId() { return this.CoalitionId; }
	public void setCoalitionId(Integer CoalitionId) { this.CoalitionId = CoalitionId; }
	private Date StartDate; 
	public Date getStartDate() { return this.StartDate; }
	public void setStartDate(Date StartDate) { this.StartDate = StartDate; }
	private Date EndDate; 
	public Date getEndDate() { return this.EndDate; }
	public void setEndDate(Date EndDate) { this.EndDate = EndDate; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Coalition Coalition;
	public Coalition getCoalition() { return this.Coalition; }
	public void setCoalition(Coalition Coalition) { this.Coalition = Coalition; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Invoice> Invoices; 
    public static BillingCycle Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                BillingCycle entity = new BillingCycle();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.BillingCycleId = Entities.ParseIntegerValue(obj, "BillingCycleId");
          entity.CoalitionId = Entities.ParseIntegerValue(obj, "CoalitionId");
          entity.StartDate = Entities.ParseDateValue(obj, "StartDate");
          entity.EndDate = Entities.ParseDateValue(obj, "EndDate");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          if (obj.has("Coalition")){  entity.Coalition = com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Coalition"));}
          JSONArray jsonInvoices = Entities.ParseJSONArrayValue(obj, "Invoices");
                if (jsonInvoices != null){
                    for (int index = 0; index < jsonInvoices.length();index++){
                        JSONObject lstItem = jsonInvoices.getJSONObject(index);
                        entity.Invoices.add(com.enrollandpay.serviceinterface.BusinessEntity.Invoice.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (BillingCycle)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public BillingCycle() {
		Invoices = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Invoice>();
			}
}
