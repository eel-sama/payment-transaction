package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class MessageThread extends BaseBusinessEntity {
	private String MessageThreadId; 
	public String getMessageThreadId() { return this.MessageThreadId; }
	public void setMessageThreadId(String MessageThreadId) { this.MessageThreadId = MessageThreadId; }
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private Integer LoyaltyProgramId; 
	public Integer getLoyaltyProgramId() { return this.LoyaltyProgramId; }
	public void setLoyaltyProgramId(Integer LoyaltyProgramId) { this.LoyaltyProgramId = LoyaltyProgramId; }
	private Integer OfferId; 
	public Integer getOfferId() { return this.OfferId; }
	public void setOfferId(Integer OfferId) { this.OfferId = OfferId; }
	private Integer OrderId; 
	public Integer getOrderId() { return this.OrderId; }
	public void setOrderId(Integer OrderId) { this.OrderId = OrderId; }
	private Integer UnreadCount; 
	public Integer getUnreadCount() { return this.UnreadCount; }
	public void setUnreadCount(Integer UnreadCount) { this.UnreadCount = UnreadCount; }
	private Integer MessageThreadType; 
	public Integer getMessageThreadType() { return this.MessageThreadType; }
	public void setMessageThreadType(Integer MessageThreadType) { this.MessageThreadType = MessageThreadType; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Date CreateDate; 
	public Date getCreateDate() { return this.CreateDate; }
	public void setCreateDate(Date CreateDate) { this.CreateDate = CreateDate; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private Integer MessageChannelType; 
	public Integer getMessageChannelType() { return this.MessageChannelType; }
	public void setMessageChannelType(Integer MessageChannelType) { this.MessageChannelType = MessageChannelType; }
	private Integer CoalitionId; 
	public Integer getCoalitionId() { return this.CoalitionId; }
	public void setCoalitionId(Integer CoalitionId) { this.CoalitionId = CoalitionId; }
	private String EmailAddress; 
	public String getEmailAddress() { return this.EmailAddress; }
	public void setEmailAddress(String EmailAddress) { this.EmailAddress = EmailAddress; }
	private String PhoneNumber; 
	public String getPhoneNumber() { return this.PhoneNumber; }
	public void setPhoneNumber(String PhoneNumber) { this.PhoneNumber = PhoneNumber; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    private com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram LoyaltyProgram;
	public LoyaltyProgram getLoyaltyProgram() { return this.LoyaltyProgram; }
	public void setLoyaltyProgram(LoyaltyProgram LoyaltyProgram) { this.LoyaltyProgram = LoyaltyProgram; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Offer Offer;
	public Offer getOffer() { return this.Offer; }
	public void setOffer(Offer Offer) { this.Offer = Offer; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Order Order;
	public Order getOrder() { return this.Order; }
	public void setOrder(Order Order) { this.Order = Order; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
	public Location getLocation() { return this.Location; }
	public void setLocation(Location Location) { this.Location = Location; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Coalition Coalition;
	public Coalition getCoalition() { return this.Coalition; }
	public void setCoalition(Coalition Coalition) { this.Coalition = Coalition; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Message> Messages; 
    public static MessageThread Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                MessageThread entity = new MessageThread();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.MessageThreadId = Entities.ParseStringValue(obj, "MessageThreadId");
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.LoyaltyProgramId = Entities.ParseIntegerValue(obj, "LoyaltyProgramId");
          entity.OfferId = Entities.ParseIntegerValue(obj, "OfferId");
          entity.OrderId = Entities.ParseIntegerValue(obj, "OrderId");
          entity.UnreadCount = Entities.ParseIntegerValue(obj, "UnreadCount");
          entity.MessageThreadType = Entities.ParseIntegerValue(obj, "MessageThreadType");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.CreateDate = Entities.ParseDateValue(obj, "CreateDate");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.MessageChannelType = Entities.ParseIntegerValue(obj, "MessageChannelType");
          entity.CoalitionId = Entities.ParseIntegerValue(obj, "CoalitionId");
          entity.EmailAddress = Entities.ParseStringValue(obj, "EmailAddress");
          entity.PhoneNumber = Entities.ParseStringValue(obj, "PhoneNumber");
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
          if (obj.has("LoyaltyProgram")){  entity.LoyaltyProgram = com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LoyaltyProgram"));}
          if (obj.has("Offer")){  entity.Offer = com.enrollandpay.serviceinterface.BusinessEntity.Offer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Offer"));}
          if (obj.has("Order")){  entity.Order = com.enrollandpay.serviceinterface.BusinessEntity.Order.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Order"));}
          if (obj.has("Location")){  entity.Location = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Location"));}
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
          if (obj.has("Coalition")){  entity.Coalition = com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Coalition"));}
          JSONArray jsonMessages = Entities.ParseJSONArrayValue(obj, "Messages");
                if (jsonMessages != null){
                    for (int index = 0; index < jsonMessages.length();index++){
                        JSONObject lstItem = jsonMessages.getJSONObject(index);
                        entity.Messages.add(com.enrollandpay.serviceinterface.BusinessEntity.Message.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (MessageThread)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public MessageThread() {
		Messages = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Message>();
			}
}
