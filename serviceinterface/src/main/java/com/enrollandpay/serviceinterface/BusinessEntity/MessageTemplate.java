package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class MessageTemplate extends BaseBusinessEntity {
	private Integer MessageTemplateId; 
	public Integer getMessageTemplateId() { return this.MessageTemplateId; }
	public void setMessageTemplateId(Integer MessageTemplateId) { this.MessageTemplateId = MessageTemplateId; }
	private Integer LanguageType; 
	public Integer getLanguageType() { return this.LanguageType; }
	public void setLanguageType(Integer LanguageType) { this.LanguageType = LanguageType; }
	private Integer MessageTemplateType; 
	public Integer getMessageTemplateType() { return this.MessageTemplateType; }
	public void setMessageTemplateType(Integer MessageTemplateType) { this.MessageTemplateType = MessageTemplateType; }
	private String EmailSubject; 
	public String getEmailSubject() { return this.EmailSubject; }
	public void setEmailSubject(String EmailSubject) { this.EmailSubject = EmailSubject; }
	private String EmailBody; 
	public String getEmailBody() { return this.EmailBody; }
	public void setEmailBody(String EmailBody) { this.EmailBody = EmailBody; }
	private String SMS; 
	public String getSMS() { return this.SMS; }
	public void setSMS(String SMS) { this.SMS = SMS; }
	private Integer CacheSequence; 
	public Integer getCacheSequence() { return this.CacheSequence; }
	public void setCacheSequence(Integer CacheSequence) { this.CacheSequence = CacheSequence; }
	private Date DateUpdate; 
	public Date getDateUpdate() { return this.DateUpdate; }
	public void setDateUpdate(Date DateUpdate) { this.DateUpdate = DateUpdate; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer MessageTemplateUseTypes; 
	public Integer getMessageTemplateUseTypes() { return this.MessageTemplateUseTypes; }
	public void setMessageTemplateUseTypes(Integer MessageTemplateUseTypes) { this.MessageTemplateUseTypes = MessageTemplateUseTypes; }
    public static MessageTemplate Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                MessageTemplate entity = new MessageTemplate();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.MessageTemplateId = Entities.ParseIntegerValue(obj, "MessageTemplateId");
          entity.LanguageType = Entities.ParseIntegerValue(obj, "LanguageType");
          entity.MessageTemplateType = Entities.ParseIntegerValue(obj, "MessageTemplateType");
          entity.EmailSubject = Entities.ParseStringValue(obj, "EmailSubject");
          entity.EmailBody = Entities.ParseStringValue(obj, "EmailBody");
          entity.SMS = Entities.ParseStringValue(obj, "SMS");
          entity.CacheSequence = Entities.ParseIntegerValue(obj, "CacheSequence");
          entity.DateUpdate = Entities.ParseDateValue(obj, "DateUpdate");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.MessageTemplateUseTypes = Entities.ParseIntegerValue(obj, "MessageTemplateUseTypes");
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (MessageTemplate)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public MessageTemplate() {
		}
}
