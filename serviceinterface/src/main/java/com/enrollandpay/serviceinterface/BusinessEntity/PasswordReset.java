package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class PasswordReset extends BaseBusinessEntity {
	private String PasswordResetId; 
	public String getPasswordResetId() { return this.PasswordResetId; }
	public void setPasswordResetId(String PasswordResetId) { this.PasswordResetId = PasswordResetId; }
	private String EmailAddress; 
	public String getEmailAddress() { return this.EmailAddress; }
	public void setEmailAddress(String EmailAddress) { this.EmailAddress = EmailAddress; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private Date RequestDateTime; 
	public Date getRequestDateTime() { return this.RequestDateTime; }
	public void setRequestDateTime(Date RequestDateTime) { this.RequestDateTime = RequestDateTime; }
	private Date CompleteDateTime; 
	public Date getCompleteDateTime() { return this.CompleteDateTime; }
	public void setCompleteDateTime(Date CompleteDateTime) { this.CompleteDateTime = CompleteDateTime; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    public static PasswordReset Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                PasswordReset entity = new PasswordReset();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.PasswordResetId = Entities.ParseStringValue(obj, "PasswordResetId");
          entity.EmailAddress = Entities.ParseStringValue(obj, "EmailAddress");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.RequestDateTime = Entities.ParseDateValue(obj, "RequestDateTime");
          entity.CompleteDateTime = Entities.ParseDateValue(obj, "CompleteDateTime");
          entity.TS = Entities.ParseStringValue(obj, "TS");
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (PasswordReset)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public PasswordReset() {
		}
}
