package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class DailySaleOverview extends BaseBusinessEntity {
	private Integer DailySaleOverviewId; 
	public Integer getDailySaleOverviewId() { return this.DailySaleOverviewId; }
	public void setDailySaleOverviewId(Integer DailySaleOverviewId) { this.DailySaleOverviewId = DailySaleOverviewId; }
	private Date DailyDate; 
	public Date getDailyDate() { return this.DailyDate; }
	public void setDailyDate(Date DailyDate) { this.DailyDate = DailyDate; }
	private Integer LoyaltyProgramId; 
	public Integer getLoyaltyProgramId() { return this.LoyaltyProgramId; }
	public void setLoyaltyProgramId(Integer LoyaltyProgramId) { this.LoyaltyProgramId = LoyaltyProgramId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private Integer TotalEnrollmentOrderCount; 
	public Integer getTotalEnrollmentOrderCount() { return this.TotalEnrollmentOrderCount; }
	public void setTotalEnrollmentOrderCount(Integer TotalEnrollmentOrderCount) { this.TotalEnrollmentOrderCount = TotalEnrollmentOrderCount; }
	private Integer TotalMemberOrderCount; 
	public Integer getTotalMemberOrderCount() { return this.TotalMemberOrderCount; }
	public void setTotalMemberOrderCount(Integer TotalMemberOrderCount) { this.TotalMemberOrderCount = TotalMemberOrderCount; }
	private Integer TotalNonMemberOrderCount; 
	public Integer getTotalNonMemberOrderCount() { return this.TotalNonMemberOrderCount; }
	public void setTotalNonMemberOrderCount(Integer TotalNonMemberOrderCount) { this.TotalNonMemberOrderCount = TotalNonMemberOrderCount; }
	private Double TotalEnrollmentSpendAmount; 
	public Double getTotalEnrollmentSpendAmount() { return this.TotalEnrollmentSpendAmount; }
	public void setTotalEnrollmentSpendAmount(Double TotalEnrollmentSpendAmount) { this.TotalEnrollmentSpendAmount = TotalEnrollmentSpendAmount; }
	private Double TotalMemberSpendAmount; 
	public Double getTotalMemberSpendAmount() { return this.TotalMemberSpendAmount; }
	public void setTotalMemberSpendAmount(Double TotalMemberSpendAmount) { this.TotalMemberSpendAmount = TotalMemberSpendAmount; }
	private Double TotalNonMemberSpendAmount; 
	public Double getTotalNonMemberSpendAmount() { return this.TotalNonMemberSpendAmount; }
	public void setTotalNonMemberSpendAmount(Double TotalNonMemberSpendAmount) { this.TotalNonMemberSpendAmount = TotalNonMemberSpendAmount; }
	private Integer EnrollmentCount; 
	public Integer getEnrollmentCount() { return this.EnrollmentCount; }
	public void setEnrollmentCount(Integer EnrollmentCount) { this.EnrollmentCount = EnrollmentCount; }
	private Integer DeclinedCount; 
	public Integer getDeclinedCount() { return this.DeclinedCount; }
	public void setDeclinedCount(Integer DeclinedCount) { this.DeclinedCount = DeclinedCount; }
	private Integer AssignedCount; 
	public Integer getAssignedCount() { return this.AssignedCount; }
	public void setAssignedCount(Integer AssignedCount) { this.AssignedCount = AssignedCount; }
	private Integer OptInToOfferCount; 
	public Integer getOptInToOfferCount() { return this.OptInToOfferCount; }
	public void setOptInToOfferCount(Integer OptInToOfferCount) { this.OptInToOfferCount = OptInToOfferCount; }
	private Integer TotalQuestionAnswerCount; 
	public Integer getTotalQuestionAnswerCount() { return this.TotalQuestionAnswerCount; }
	public void setTotalQuestionAnswerCount(Integer TotalQuestionAnswerCount) { this.TotalQuestionAnswerCount = TotalQuestionAnswerCount; }
	private Integer TotalQuestionAnswerScore; 
	public Integer getTotalQuestionAnswerScore() { return this.TotalQuestionAnswerScore; }
	public void setTotalQuestionAnswerScore(Integer TotalQuestionAnswerScore) { this.TotalQuestionAnswerScore = TotalQuestionAnswerScore; }
	private Integer TotalRewardEarnedOrderCount; 
	public Integer getTotalRewardEarnedOrderCount() { return this.TotalRewardEarnedOrderCount; }
	public void setTotalRewardEarnedOrderCount(Integer TotalRewardEarnedOrderCount) { this.TotalRewardEarnedOrderCount = TotalRewardEarnedOrderCount; }
	private Integer TotalRewardSpentOrderCount; 
	public Integer getTotalRewardSpentOrderCount() { return this.TotalRewardSpentOrderCount; }
	public void setTotalRewardSpentOrderCount(Integer TotalRewardSpentOrderCount) { this.TotalRewardSpentOrderCount = TotalRewardSpentOrderCount; }
	private Double TotalRewardEarnedAmount; 
	public Double getTotalRewardEarnedAmount() { return this.TotalRewardEarnedAmount; }
	public void setTotalRewardEarnedAmount(Double TotalRewardEarnedAmount) { this.TotalRewardEarnedAmount = TotalRewardEarnedAmount; }
	private Double TotalRewardSpentAmount; 
	public Double getTotalRewardSpentAmount() { return this.TotalRewardSpentAmount; }
	public void setTotalRewardSpentAmount(Double TotalRewardSpentAmount) { this.TotalRewardSpentAmount = TotalRewardSpentAmount; }
	private Double TotalRewardEarnedOrderAmount; 
	public Double getTotalRewardEarnedOrderAmount() { return this.TotalRewardEarnedOrderAmount; }
	public void setTotalRewardEarnedOrderAmount(Double TotalRewardEarnedOrderAmount) { this.TotalRewardEarnedOrderAmount = TotalRewardEarnedOrderAmount; }
	private Double TotalRewardSpentOrderAmount; 
	public Double getTotalRewardSpentOrderAmount() { return this.TotalRewardSpentOrderAmount; }
	public void setTotalRewardSpentOrderAmount(Double TotalRewardSpentOrderAmount) { this.TotalRewardSpentOrderAmount = TotalRewardSpentOrderAmount; }
	private Date TrendingDate; 
	public Date getTrendingDate() { return this.TrendingDate; }
	public void setTrendingDate(Date TrendingDate) { this.TrendingDate = TrendingDate; }
	private Integer TrendingType; 
	public Integer getTrendingType() { return this.TrendingType; }
	public void setTrendingType(Integer TrendingType) { this.TrendingType = TrendingType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer TotalDiscountOrderCount; 
	public Integer getTotalDiscountOrderCount() { return this.TotalDiscountOrderCount; }
	public void setTotalDiscountOrderCount(Integer TotalDiscountOrderCount) { this.TotalDiscountOrderCount = TotalDiscountOrderCount; }
	private Double TotalDiscountOrderAmount; 
	public Double getTotalDiscountOrderAmount() { return this.TotalDiscountOrderAmount; }
	public void setTotalDiscountOrderAmount(Double TotalDiscountOrderAmount) { this.TotalDiscountOrderAmount = TotalDiscountOrderAmount; }
	private Integer TotalDiscountCount; 
	public Integer getTotalDiscountCount() { return this.TotalDiscountCount; }
	public void setTotalDiscountCount(Integer TotalDiscountCount) { this.TotalDiscountCount = TotalDiscountCount; }
	private Double TotalDiscountAmount; 
	public Double getTotalDiscountAmount() { return this.TotalDiscountAmount; }
	public void setTotalDiscountAmount(Double TotalDiscountAmount) { this.TotalDiscountAmount = TotalDiscountAmount; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
	public Location getLocation() { return this.Location; }
	public void setLocation(Location Location) { this.Location = Location; }
    private com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram LoyaltyProgram;
	public LoyaltyProgram getLoyaltyProgram() { return this.LoyaltyProgram; }
	public void setLoyaltyProgram(LoyaltyProgram LoyaltyProgram) { this.LoyaltyProgram = LoyaltyProgram; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    public static DailySaleOverview Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                DailySaleOverview entity = new DailySaleOverview();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.DailySaleOverviewId = Entities.ParseIntegerValue(obj, "DailySaleOverviewId");
          entity.DailyDate = Entities.ParseDateValue(obj, "DailyDate");
          entity.LoyaltyProgramId = Entities.ParseIntegerValue(obj, "LoyaltyProgramId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.TotalEnrollmentOrderCount = Entities.ParseIntegerValue(obj, "TotalEnrollmentOrderCount");
          entity.TotalMemberOrderCount = Entities.ParseIntegerValue(obj, "TotalMemberOrderCount");
          entity.TotalNonMemberOrderCount = Entities.ParseIntegerValue(obj, "TotalNonMemberOrderCount");
          entity.TotalEnrollmentSpendAmount = Entities.ParseDoubleValue(obj, "TotalEnrollmentSpendAmount");
          entity.TotalMemberSpendAmount = Entities.ParseDoubleValue(obj, "TotalMemberSpendAmount");
          entity.TotalNonMemberSpendAmount = Entities.ParseDoubleValue(obj, "TotalNonMemberSpendAmount");
          entity.EnrollmentCount = Entities.ParseIntegerValue(obj, "EnrollmentCount");
          entity.DeclinedCount = Entities.ParseIntegerValue(obj, "DeclinedCount");
          entity.AssignedCount = Entities.ParseIntegerValue(obj, "AssignedCount");
          entity.OptInToOfferCount = Entities.ParseIntegerValue(obj, "OptInToOfferCount");
          entity.TotalQuestionAnswerCount = Entities.ParseIntegerValue(obj, "TotalQuestionAnswerCount");
          entity.TotalQuestionAnswerScore = Entities.ParseIntegerValue(obj, "TotalQuestionAnswerScore");
          entity.TotalRewardEarnedOrderCount = Entities.ParseIntegerValue(obj, "TotalRewardEarnedOrderCount");
          entity.TotalRewardSpentOrderCount = Entities.ParseIntegerValue(obj, "TotalRewardSpentOrderCount");
          entity.TotalRewardEarnedAmount = Entities.ParseDoubleValue(obj, "TotalRewardEarnedAmount");
          entity.TotalRewardSpentAmount = Entities.ParseDoubleValue(obj, "TotalRewardSpentAmount");
          entity.TotalRewardEarnedOrderAmount = Entities.ParseDoubleValue(obj, "TotalRewardEarnedOrderAmount");
          entity.TotalRewardSpentOrderAmount = Entities.ParseDoubleValue(obj, "TotalRewardSpentOrderAmount");
          entity.TrendingDate = Entities.ParseDateValue(obj, "TrendingDate");
          entity.TrendingType = Entities.ParseIntegerValue(obj, "TrendingType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.TotalDiscountOrderCount = Entities.ParseIntegerValue(obj, "TotalDiscountOrderCount");
          entity.TotalDiscountOrderAmount = Entities.ParseDoubleValue(obj, "TotalDiscountOrderAmount");
          entity.TotalDiscountCount = Entities.ParseIntegerValue(obj, "TotalDiscountCount");
          entity.TotalDiscountAmount = Entities.ParseDoubleValue(obj, "TotalDiscountAmount");
          if (obj.has("Location")){  entity.Location = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Location"));}
          if (obj.has("LoyaltyProgram")){  entity.LoyaltyProgram = com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LoyaltyProgram"));}
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (DailySaleOverview)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public DailySaleOverview() {
		}
}
