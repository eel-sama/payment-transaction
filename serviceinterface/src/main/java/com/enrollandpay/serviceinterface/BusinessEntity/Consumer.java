package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Consumer extends BaseBusinessEntity {
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private String NamePrefix; 
	public String getNamePrefix() { return this.NamePrefix; }
	public void setNamePrefix(String NamePrefix) { this.NamePrefix = NamePrefix; }
	private String NameFirst; 
	public String getNameFirst() { return this.NameFirst; }
	public void setNameFirst(String NameFirst) { this.NameFirst = NameFirst; }
	private String NameMiddle; 
	public String getNameMiddle() { return this.NameMiddle; }
	public void setNameMiddle(String NameMiddle) { this.NameMiddle = NameMiddle; }
	private String NameLast; 
	public String getNameLast() { return this.NameLast; }
	public void setNameLast(String NameLast) { this.NameLast = NameLast; }
	private String NameSuffix; 
	public String getNameSuffix() { return this.NameSuffix; }
	public void setNameSuffix(String NameSuffix) { this.NameSuffix = NameSuffix; }
	private Date BirthDate; 
	public Date getBirthDate() { return this.BirthDate; }
	public void setBirthDate(Date BirthDate) { this.BirthDate = BirthDate; }
	private Integer DefaultOptInType; 
	public Integer getDefaultOptInType() { return this.DefaultOptInType; }
	public void setDefaultOptInType(Integer DefaultOptInType) { this.DefaultOptInType = DefaultOptInType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer LanguageType; 
	public Integer getLanguageType() { return this.LanguageType; }
	public void setLanguageType(Integer LanguageType) { this.LanguageType = LanguageType; }
	private Integer SupersededByConsumerId; 
	public Integer getSupersededByConsumerId() { return this.SupersededByConsumerId; }
	public void setSupersededByConsumerId(Integer SupersededByConsumerId) { this.SupersededByConsumerId = SupersededByConsumerId; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer SupersededByConsumer;
	public Consumer getSupersededByConsumer() { return this.SupersededByConsumer; }
	public void setSupersededByConsumer(Consumer SupersededByConsumer) { this.SupersededByConsumer = SupersededByConsumer; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Card> Cards; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerContactInformation> ConsumerContactInformations; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram> ConsumerLoyaltyPrograms; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship> ConsumerRelationships; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment> OrderPayments; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentLoyaltyProgram> OrderPaymentLoyaltyPrograms; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentOffer> OrderPaymentOffers; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderReview> OrderReviews; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Message> Messages; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerOffer> ConsumerOffers; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareConsumer> LoyaltyProgramShareConsumers; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareRecipient> LoyaltyProgramShareRecipients; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.MessageThread> MessageThreads; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Consumer> ConsumersSuperseded; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess> ClientAccesses; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest> AuthorizationRequests; 
    public static Consumer Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Consumer entity = new Consumer();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.NamePrefix = Entities.ParseStringValue(obj, "NamePrefix");
          entity.NameFirst = Entities.ParseStringValue(obj, "NameFirst");
          entity.NameMiddle = Entities.ParseStringValue(obj, "NameMiddle");
          entity.NameLast = Entities.ParseStringValue(obj, "NameLast");
          entity.NameSuffix = Entities.ParseStringValue(obj, "NameSuffix");
          entity.BirthDate = Entities.ParseDateValue(obj, "BirthDate");
          entity.DefaultOptInType = Entities.ParseIntegerValue(obj, "DefaultOptInType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.LanguageType = Entities.ParseIntegerValue(obj, "LanguageType");
          entity.SupersededByConsumerId = Entities.ParseIntegerValue(obj, "SupersededByConsumerId");
          if (obj.has("SupersededByConsumer")){  entity.SupersededByConsumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"SupersededByConsumer"));}
          JSONArray jsonCards = Entities.ParseJSONArrayValue(obj, "Cards");
                if (jsonCards != null){
                    for (int index = 0; index < jsonCards.length();index++){
                        JSONObject lstItem = jsonCards.getJSONObject(index);
                        entity.Cards.add(com.enrollandpay.serviceinterface.BusinessEntity.Card.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerContactInformations = Entities.ParseJSONArrayValue(obj, "ConsumerContactInformations");
                if (jsonConsumerContactInformations != null){
                    for (int index = 0; index < jsonConsumerContactInformations.length();index++){
                        JSONObject lstItem = jsonConsumerContactInformations.getJSONObject(index);
                        entity.ConsumerContactInformations.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerContactInformation.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerLoyaltyPrograms = Entities.ParseJSONArrayValue(obj, "ConsumerLoyaltyPrograms");
                if (jsonConsumerLoyaltyPrograms != null){
                    for (int index = 0; index < jsonConsumerLoyaltyPrograms.length();index++){
                        JSONObject lstItem = jsonConsumerLoyaltyPrograms.getJSONObject(index);
                        entity.ConsumerLoyaltyPrograms.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerRelationships = Entities.ParseJSONArrayValue(obj, "ConsumerRelationships");
                if (jsonConsumerRelationships != null){
                    for (int index = 0; index < jsonConsumerRelationships.length();index++){
                        JSONObject lstItem = jsonConsumerRelationships.getJSONObject(index);
                        entity.ConsumerRelationships.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOrderPayments = Entities.ParseJSONArrayValue(obj, "OrderPayments");
                if (jsonOrderPayments != null){
                    for (int index = 0; index < jsonOrderPayments.length();index++){
                        JSONObject lstItem = jsonOrderPayments.getJSONObject(index);
                        entity.OrderPayments.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOrderPaymentLoyaltyPrograms = Entities.ParseJSONArrayValue(obj, "OrderPaymentLoyaltyPrograms");
                if (jsonOrderPaymentLoyaltyPrograms != null){
                    for (int index = 0; index < jsonOrderPaymentLoyaltyPrograms.length();index++){
                        JSONObject lstItem = jsonOrderPaymentLoyaltyPrograms.getJSONObject(index);
                        entity.OrderPaymentLoyaltyPrograms.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentLoyaltyProgram.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOrderPaymentOffers = Entities.ParseJSONArrayValue(obj, "OrderPaymentOffers");
                if (jsonOrderPaymentOffers != null){
                    for (int index = 0; index < jsonOrderPaymentOffers.length();index++){
                        JSONObject lstItem = jsonOrderPaymentOffers.getJSONObject(index);
                        entity.OrderPaymentOffers.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentOffer.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonOrderReviews = Entities.ParseJSONArrayValue(obj, "OrderReviews");
                if (jsonOrderReviews != null){
                    for (int index = 0; index < jsonOrderReviews.length();index++){
                        JSONObject lstItem = jsonOrderReviews.getJSONObject(index);
                        entity.OrderReviews.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderReview.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMessages = Entities.ParseJSONArrayValue(obj, "Messages");
                if (jsonMessages != null){
                    for (int index = 0; index < jsonMessages.length();index++){
                        JSONObject lstItem = jsonMessages.getJSONObject(index);
                        entity.Messages.add(com.enrollandpay.serviceinterface.BusinessEntity.Message.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumerOffers = Entities.ParseJSONArrayValue(obj, "ConsumerOffers");
                if (jsonConsumerOffers != null){
                    for (int index = 0; index < jsonConsumerOffers.length();index++){
                        JSONObject lstItem = jsonConsumerOffers.getJSONObject(index);
                        entity.ConsumerOffers.add(com.enrollandpay.serviceinterface.BusinessEntity.ConsumerOffer.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonLoyaltyProgramShareConsumers = Entities.ParseJSONArrayValue(obj, "LoyaltyProgramShareConsumers");
                if (jsonLoyaltyProgramShareConsumers != null){
                    for (int index = 0; index < jsonLoyaltyProgramShareConsumers.length();index++){
                        JSONObject lstItem = jsonLoyaltyProgramShareConsumers.getJSONObject(index);
                        entity.LoyaltyProgramShareConsumers.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareConsumer.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonLoyaltyProgramShareRecipients = Entities.ParseJSONArrayValue(obj, "LoyaltyProgramShareRecipients");
                if (jsonLoyaltyProgramShareRecipients != null){
                    for (int index = 0; index < jsonLoyaltyProgramShareRecipients.length();index++){
                        JSONObject lstItem = jsonLoyaltyProgramShareRecipients.getJSONObject(index);
                        entity.LoyaltyProgramShareRecipients.add(com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareRecipient.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMessageThreads = Entities.ParseJSONArrayValue(obj, "MessageThreads");
                if (jsonMessageThreads != null){
                    for (int index = 0; index < jsonMessageThreads.length();index++){
                        JSONObject lstItem = jsonMessageThreads.getJSONObject(index);
                        entity.MessageThreads.add(com.enrollandpay.serviceinterface.BusinessEntity.MessageThread.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonConsumersSuperseded = Entities.ParseJSONArrayValue(obj, "ConsumersSuperseded");
                if (jsonConsumersSuperseded != null){
                    for (int index = 0; index < jsonConsumersSuperseded.length();index++){
                        JSONObject lstItem = jsonConsumersSuperseded.getJSONObject(index);
                        entity.ConsumersSuperseded.add(com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonClientAccesses = Entities.ParseJSONArrayValue(obj, "ClientAccesses");
                if (jsonClientAccesses != null){
                    for (int index = 0; index < jsonClientAccesses.length();index++){
                        JSONObject lstItem = jsonClientAccesses.getJSONObject(index);
                        entity.ClientAccesses.add(com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonAuthorizationRequests = Entities.ParseJSONArrayValue(obj, "AuthorizationRequests");
                if (jsonAuthorizationRequests != null){
                    for (int index = 0; index < jsonAuthorizationRequests.length();index++){
                        JSONObject lstItem = jsonAuthorizationRequests.getJSONObject(index);
                        entity.AuthorizationRequests.add(com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Consumer)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public String NameFull() { 
		if(NameFirst != null && NameLast != null) { 
			return NameFirst + " " + NameLast; 
		} else { return null; } 
	}
	public String NameAbbr() { 
		if (NameFirst != null && NameLast != null) { 
			return NameLast + ", " + NameFirst.substring(0,1); 
		} else if (NameLast != null) {
			return NameLast;
		} else { return null; } 
	}
	public Consumer() {
		Cards = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Card>();
			ConsumerContactInformations = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerContactInformation>();
			ConsumerLoyaltyPrograms = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerLoyaltyProgram>();
			ConsumerRelationships = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerRelationship>();
			OrderPayments = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment>();
			OrderPaymentLoyaltyPrograms = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentLoyaltyProgram>();
			OrderPaymentOffers = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderPaymentOffer>();
			OrderReviews = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderReview>();
			Messages = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Message>();
			ConsumerOffers = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ConsumerOffer>();
			LoyaltyProgramShareConsumers = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareConsumer>();
			LoyaltyProgramShareRecipients = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgramShareRecipient>();
			MessageThreads = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.MessageThread>();
			ConsumersSuperseded = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Consumer>();
			ClientAccesses = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.ClientAccess>();
			AuthorizationRequests = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.AuthorizationRequest>();
			}
}
