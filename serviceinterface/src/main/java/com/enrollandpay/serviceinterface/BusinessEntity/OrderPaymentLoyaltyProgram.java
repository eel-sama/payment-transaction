package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class OrderPaymentLoyaltyProgram extends BaseBusinessEntity {
	private Integer OrderPaymentLoyaltyProgramId; 
	public Integer getOrderPaymentLoyaltyProgramId() { return this.OrderPaymentLoyaltyProgramId; }
	public void setOrderPaymentLoyaltyProgramId(Integer OrderPaymentLoyaltyProgramId) { this.OrderPaymentLoyaltyProgramId = OrderPaymentLoyaltyProgramId; }
	private Integer LoyaltyProgramId; 
	public Integer getLoyaltyProgramId() { return this.LoyaltyProgramId; }
	public void setLoyaltyProgramId(Integer LoyaltyProgramId) { this.LoyaltyProgramId = LoyaltyProgramId; }
	private Integer OrderPaymentId; 
	public Integer getOrderPaymentId() { return this.OrderPaymentId; }
	public void setOrderPaymentId(Integer OrderPaymentId) { this.OrderPaymentId = OrderPaymentId; }
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private Integer EnrollmentType; 
	public Integer getEnrollmentType() { return this.EnrollmentType; }
	public void setEnrollmentType(Integer EnrollmentType) { this.EnrollmentType = EnrollmentType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram LoyaltyProgram;
	public LoyaltyProgram getLoyaltyProgram() { return this.LoyaltyProgram; }
	public void setLoyaltyProgram(LoyaltyProgram LoyaltyProgram) { this.LoyaltyProgram = LoyaltyProgram; }
    private com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment OrderPayment;
	public OrderPayment getOrderPayment() { return this.OrderPayment; }
	public void setOrderPayment(OrderPayment OrderPayment) { this.OrderPayment = OrderPayment; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    public static OrderPaymentLoyaltyProgram Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                OrderPaymentLoyaltyProgram entity = new OrderPaymentLoyaltyProgram();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.OrderPaymentLoyaltyProgramId = Entities.ParseIntegerValue(obj, "OrderPaymentLoyaltyProgramId");
          entity.LoyaltyProgramId = Entities.ParseIntegerValue(obj, "LoyaltyProgramId");
          entity.OrderPaymentId = Entities.ParseIntegerValue(obj, "OrderPaymentId");
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.EnrollmentType = Entities.ParseIntegerValue(obj, "EnrollmentType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("LoyaltyProgram")){  entity.LoyaltyProgram = com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LoyaltyProgram"));}
          if (obj.has("OrderPayment")){  entity.OrderPayment = com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"OrderPayment"));}
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (OrderPaymentLoyaltyProgram)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public OrderPaymentLoyaltyProgram() {
		}
}
