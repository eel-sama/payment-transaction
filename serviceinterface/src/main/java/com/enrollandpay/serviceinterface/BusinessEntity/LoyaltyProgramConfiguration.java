package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class LoyaltyProgramConfiguration extends BaseBusinessEntity {
	private Integer LoyaltyProgramId; 
	public Integer getLoyaltyProgramId() { return this.LoyaltyProgramId; }
	public void setLoyaltyProgramId(Integer LoyaltyProgramId) { this.LoyaltyProgramId = LoyaltyProgramId; }
	private String EnrollmentSMSMessage; 
	public String getEnrollmentSMSMessage() { return this.EnrollmentSMSMessage; }
	public void setEnrollmentSMSMessage(String EnrollmentSMSMessage) { this.EnrollmentSMSMessage = EnrollmentSMSMessage; }
	private String EmailAddressDomain; 
	public String getEmailAddressDomain() { return this.EmailAddressDomain; }
	public void setEmailAddressDomain(String EmailAddressDomain) { this.EmailAddressDomain = EmailAddressDomain; }
	private Long PhoneId; 
	public Long getPhoneId() { return this.PhoneId; }
	public void setPhoneId(Long PhoneId) { this.PhoneId = PhoneId; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram LoyaltyProgram;
	public LoyaltyProgram getLoyaltyProgram() { return this.LoyaltyProgram; }
	public void setLoyaltyProgram(LoyaltyProgram LoyaltyProgram) { this.LoyaltyProgram = LoyaltyProgram; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Phone Phone;
	public Phone getPhone() { return this.Phone; }
	public void setPhone(Phone Phone) { this.Phone = Phone; }
    public static LoyaltyProgramConfiguration Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                LoyaltyProgramConfiguration entity = new LoyaltyProgramConfiguration();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.LoyaltyProgramId = Entities.ParseIntegerValue(obj, "LoyaltyProgramId");
          entity.EnrollmentSMSMessage = Entities.ParseStringValue(obj, "EnrollmentSMSMessage");
          entity.EmailAddressDomain = Entities.ParseStringValue(obj, "EmailAddressDomain");
          entity.PhoneId = Entities.ParseLongValue(obj, "PhoneId");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("LoyaltyProgram")){  entity.LoyaltyProgram = com.enrollandpay.serviceinterface.BusinessEntity.LoyaltyProgram.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"LoyaltyProgram"));}
          if (obj.has("Phone")){  entity.Phone = com.enrollandpay.serviceinterface.BusinessEntity.Phone.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Phone"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (LoyaltyProgramConfiguration)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public LoyaltyProgramConfiguration() {
		}
}
