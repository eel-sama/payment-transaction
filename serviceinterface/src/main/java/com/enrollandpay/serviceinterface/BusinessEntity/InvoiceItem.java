package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class InvoiceItem extends BaseBusinessEntity {
	private Integer InvoiceItemId; 
	public Integer getInvoiceItemId() { return this.InvoiceItemId; }
	public void setInvoiceItemId(Integer InvoiceItemId) { this.InvoiceItemId = InvoiceItemId; }
	private Integer InvoiceId; 
	public Integer getInvoiceId() { return this.InvoiceId; }
	public void setInvoiceId(Integer InvoiceId) { this.InvoiceId = InvoiceId; }
	private String Name; 
	public String getName() { return this.Name; }
	public void setName(String Name) { this.Name = Name; }
	private Double AmountDue; 
	public Double getAmountDue() { return this.AmountDue; }
	public void setAmountDue(Double AmountDue) { this.AmountDue = AmountDue; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private Integer ItemType; 
	public Integer getItemType() { return this.ItemType; }
	public void setItemType(Integer ItemType) { this.ItemType = ItemType; }
	private Date CreateDateTime; 
	public Date getCreateDateTime() { return this.CreateDateTime; }
	public void setCreateDateTime(Date CreateDateTime) { this.CreateDateTime = CreateDateTime; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Double Quantity; 
	public Double getQuantity() { return this.Quantity; }
	public void setQuantity(Double Quantity) { this.Quantity = Quantity; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Invoice Invoice;
	public Invoice getInvoice() { return this.Invoice; }
	public void setInvoice(Invoice Invoice) { this.Invoice = Invoice; }
    public static InvoiceItem Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                InvoiceItem entity = new InvoiceItem();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.InvoiceItemId = Entities.ParseIntegerValue(obj, "InvoiceItemId");
          entity.InvoiceId = Entities.ParseIntegerValue(obj, "InvoiceId");
          entity.Name = Entities.ParseStringValue(obj, "Name");
          entity.AmountDue = Entities.ParseDoubleValue(obj, "AmountDue");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.ItemType = Entities.ParseIntegerValue(obj, "ItemType");
          entity.CreateDateTime = Entities.ParseDateValue(obj, "CreateDateTime");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.Quantity = Entities.ParseDoubleValue(obj, "Quantity");
          if (obj.has("Invoice")){  entity.Invoice = com.enrollandpay.serviceinterface.BusinessEntity.Invoice.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Invoice"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (InvoiceItem)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public InvoiceItem() {
		}
}
