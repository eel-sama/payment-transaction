package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class OrderReview extends BaseBusinessEntity {
	private String OrderReviewId; 
	public String getOrderReviewId() { return this.OrderReviewId; }
	public void setOrderReviewId(String OrderReviewId) { this.OrderReviewId = OrderReviewId; }
	private Integer OrderId; 
	public Integer getOrderId() { return this.OrderId; }
	public void setOrderId(Integer OrderId) { this.OrderId = OrderId; }
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private Double Score; 
	public Double getScore() { return this.Score; }
	public void setScore(Double Score) { this.Score = Score; }
	private Integer TotalAsked; 
	public Integer getTotalAsked() { return this.TotalAsked; }
	public void setTotalAsked(Integer TotalAsked) { this.TotalAsked = TotalAsked; }
	private Integer TotalAnswered; 
	public Integer getTotalAnswered() { return this.TotalAnswered; }
	public void setTotalAnswered(Integer TotalAnswered) { this.TotalAnswered = TotalAnswered; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Date CreatedDateTime; 
	public Date getCreatedDateTime() { return this.CreatedDateTime; }
	public void setCreatedDateTime(Date CreatedDateTime) { this.CreatedDateTime = CreatedDateTime; }
	private Date UpdateDateTime; 
	public Date getUpdateDateTime() { return this.UpdateDateTime; }
	public void setUpdateDateTime(Date UpdateDateTime) { this.UpdateDateTime = UpdateDateTime; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Order Order;
	public Order getOrder() { return this.Order; }
	public void setOrder(Order Order) { this.Order = Order; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.OrderReviewQuestion> OrderReviewQuestions; 
    public static OrderReview Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                OrderReview entity = new OrderReview();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.OrderReviewId = Entities.ParseStringValue(obj, "OrderReviewId");
          entity.OrderId = Entities.ParseIntegerValue(obj, "OrderId");
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.Score = Entities.ParseDoubleValue(obj, "Score");
          entity.TotalAsked = Entities.ParseIntegerValue(obj, "TotalAsked");
          entity.TotalAnswered = Entities.ParseIntegerValue(obj, "TotalAnswered");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.CreatedDateTime = Entities.ParseDateValue(obj, "CreatedDateTime");
          entity.UpdateDateTime = Entities.ParseDateValue(obj, "UpdateDateTime");
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
          if (obj.has("Order")){  entity.Order = com.enrollandpay.serviceinterface.BusinessEntity.Order.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Order"));}
          JSONArray jsonOrderReviewQuestions = Entities.ParseJSONArrayValue(obj, "OrderReviewQuestions");
                if (jsonOrderReviewQuestions != null){
                    for (int index = 0; index < jsonOrderReviewQuestions.length();index++){
                        JSONObject lstItem = jsonOrderReviewQuestions.getJSONObject(index);
                        entity.OrderReviewQuestions.add(com.enrollandpay.serviceinterface.BusinessEntity.OrderReviewQuestion.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (OrderReview)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public OrderReview() {
		OrderReviewQuestions = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.OrderReviewQuestion>();
			}
}
