package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Sponsor extends BaseBusinessEntity {
	private Integer SponsorId; 
	public Integer getSponsorId() { return this.SponsorId; }
	public void setSponsorId(Integer SponsorId) { this.SponsorId = SponsorId; }
	private String Name; 
	public String getName() { return this.Name; }
	public void setName(String Name) { this.Name = Name; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Offer> Offers; 
    public static Sponsor Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Sponsor entity = new Sponsor();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.SponsorId = Entities.ParseIntegerValue(obj, "SponsorId");
          entity.Name = Entities.ParseStringValue(obj, "Name");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          JSONArray jsonOffers = Entities.ParseJSONArrayValue(obj, "Offers");
                if (jsonOffers != null){
                    for (int index = 0; index < jsonOffers.length();index++){
                        JSONObject lstItem = jsonOffers.getJSONObject(index);
                        entity.Offers.add(com.enrollandpay.serviceinterface.BusinessEntity.Offer.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Sponsor)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Sponsor() {
		Offers = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Offer>();
			}
}
