package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class TeamRepresentative extends BaseBusinessEntity {
	private Integer TeamId; 
	public Integer getTeamId() { return this.TeamId; }
	public void setTeamId(Integer TeamId) { this.TeamId = TeamId; }
	private Integer RepresentativeId; 
	public Integer getRepresentativeId() { return this.RepresentativeId; }
	public void setRepresentativeId(Integer RepresentativeId) { this.RepresentativeId = RepresentativeId; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer TeamRoleType; 
	public Integer getTeamRoleType() { return this.TeamRoleType; }
	public void setTeamRoleType(Integer TeamRoleType) { this.TeamRoleType = TeamRoleType; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Representative Representative;
	public Representative getRepresentative() { return this.Representative; }
	public void setRepresentative(Representative Representative) { this.Representative = Representative; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Team Team;
	public Team getTeam() { return this.Team; }
	public void setTeam(Team Team) { this.Team = Team; }
    public static TeamRepresentative Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                TeamRepresentative entity = new TeamRepresentative();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.TeamId = Entities.ParseIntegerValue(obj, "TeamId");
          entity.RepresentativeId = Entities.ParseIntegerValue(obj, "RepresentativeId");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.TeamRoleType = Entities.ParseIntegerValue(obj, "TeamRoleType");
          if (obj.has("Representative")){  entity.Representative = com.enrollandpay.serviceinterface.BusinessEntity.Representative.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Representative"));}
          if (obj.has("Team")){  entity.Team = com.enrollandpay.serviceinterface.BusinessEntity.Team.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Team"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (TeamRepresentative)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public TeamRepresentative() {
		}
}

