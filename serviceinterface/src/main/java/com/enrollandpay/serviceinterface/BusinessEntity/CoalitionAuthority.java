package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class CoalitionAuthority extends BaseBusinessEntity {
	private Integer CoalitionAuthorityId; 
	public Integer getCoalitionAuthorityId() { return this.CoalitionAuthorityId; }
	public void setCoalitionAuthorityId(Integer CoalitionAuthorityId) { this.CoalitionAuthorityId = CoalitionAuthorityId; }
	private Integer ParentId; 
	public Integer getParentId() { return this.ParentId; }
	public void setParentId(Integer ParentId) { this.ParentId = ParentId; }
	private Integer ChildId; 
	public Integer getChildId() { return this.ChildId; }
	public void setChildId(Integer ChildId) { this.ChildId = ChildId; }
	private Date StartDate; 
	public Date getStartDate() { return this.StartDate; }
	public void setStartDate(Date StartDate) { this.StartDate = StartDate; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Coalition Child;
	public Coalition getChild() { return this.Child; }
	public void setChild(Coalition Child) { this.Child = Child; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Coalition Parent;
	public Coalition getParent() { return this.Parent; }
	public void setParent(Coalition Parent) { this.Parent = Parent; }
    public static CoalitionAuthority Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                CoalitionAuthority entity = new CoalitionAuthority();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.CoalitionAuthorityId = Entities.ParseIntegerValue(obj, "CoalitionAuthorityId");
          entity.ParentId = Entities.ParseIntegerValue(obj, "ParentId");
          entity.ChildId = Entities.ParseIntegerValue(obj, "ChildId");
          entity.StartDate = Entities.ParseDateValue(obj, "StartDate");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Child")){  entity.Child = com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Child"));}
          if (obj.has("Parent")){  entity.Parent = com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Parent"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (CoalitionAuthority)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public CoalitionAuthority() {
		}
}
