package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class ConsumerContactInformation extends BaseBusinessEntity {
	private Integer ConsumerContactInformationId; 
	public Integer getConsumerContactInformationId() { return this.ConsumerContactInformationId; }
	public void setConsumerContactInformationId(Integer ConsumerContactInformationId) { this.ConsumerContactInformationId = ConsumerContactInformationId; }
	private Integer ConsumerId; 
	public Integer getConsumerId() { return this.ConsumerId; }
	public void setConsumerId(Integer ConsumerId) { this.ConsumerId = ConsumerId; }
	private String EmailAddress; 
	public String getEmailAddress() { return this.EmailAddress; }
	public void setEmailAddress(String EmailAddress) { this.EmailAddress = EmailAddress; }
	private String PhoneNumber; 
	public String getPhoneNumber() { return this.PhoneNumber; }
	public void setPhoneNumber(String PhoneNumber) { this.PhoneNumber = PhoneNumber; }
	private String VerificationIdent; 
	public String getVerificationIdent() { return this.VerificationIdent; }
	public void setVerificationIdent(String VerificationIdent) { this.VerificationIdent = VerificationIdent; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Consumer Consumer;
	public Consumer getConsumer() { return this.Consumer; }
	public void setConsumer(Consumer Consumer) { this.Consumer = Consumer; }
    public static ConsumerContactInformation Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                ConsumerContactInformation entity = new ConsumerContactInformation();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.ConsumerContactInformationId = Entities.ParseIntegerValue(obj, "ConsumerContactInformationId");
          entity.ConsumerId = Entities.ParseIntegerValue(obj, "ConsumerId");
          entity.EmailAddress = Entities.ParseStringValue(obj, "EmailAddress");
          entity.PhoneNumber = Entities.ParseStringValue(obj, "PhoneNumber");
          entity.VerificationIdent = Entities.ParseStringValue(obj, "VerificationIdent");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Consumer")){  entity.Consumer = com.enrollandpay.serviceinterface.BusinessEntity.Consumer.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Consumer"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (ConsumerContactInformation)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public ConsumerContactInformation() {
		}
}
