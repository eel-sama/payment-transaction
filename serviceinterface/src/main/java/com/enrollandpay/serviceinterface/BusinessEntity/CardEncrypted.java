package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class CardEncrypted extends BaseBusinessEntity {
	private Integer CardEncryptedId; 
	public Integer getCardEncryptedId() { return this.CardEncryptedId; }
	public void setCardEncryptedId(Integer CardEncryptedId) { this.CardEncryptedId = CardEncryptedId; }
	private Integer CardId; 
	public Integer getCardId() { return this.CardId; }
	public void setCardId(Integer CardId) { this.CardId = CardId; }
	private String EncryptedValue; 
	public String getEncryptedValue() { return this.EncryptedValue; }
	public void setEncryptedValue(String EncryptedValue) { this.EncryptedValue = EncryptedValue; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Card Card;
	public Card getCard() { return this.Card; }
	public void setCard(Card Card) { this.Card = Card; }
    public static CardEncrypted Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                CardEncrypted entity = new CardEncrypted();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.CardEncryptedId = Entities.ParseIntegerValue(obj, "CardEncryptedId");
          entity.CardId = Entities.ParseIntegerValue(obj, "CardId");
          entity.EncryptedValue = Entities.ParseStringValue(obj, "EncryptedValue");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Card")){  entity.Card = com.enrollandpay.serviceinterface.BusinessEntity.Card.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Card"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (CardEncrypted)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public CardEncrypted() {
		}
}
