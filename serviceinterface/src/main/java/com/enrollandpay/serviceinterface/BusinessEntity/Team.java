package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Team extends BaseBusinessEntity {
	private Integer TeamId; 
	public Integer getTeamId() { return this.TeamId; }
	public void setTeamId(Integer TeamId) { this.TeamId = TeamId; }
	private Integer CoalitionId; 
	public Integer getCoalitionId() { return this.CoalitionId; }
	public void setCoalitionId(Integer CoalitionId) { this.CoalitionId = CoalitionId; }
	private String Name; 
	public String getName() { return this.Name; }
	public void setName(String Name) { this.Name = Name; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Coalition Coalition;
	public Coalition getCoalition() { return this.Coalition; }
	public void setCoalition(Coalition Coalition) { this.Coalition = Coalition; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.TeamRepresentative> TeamRepresentatives; 
    public List<com.enrollandpay.serviceinterface.BusinessEntity.MerchantAccess> MerchantAccesses; 
    public static Team Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Team entity = new Team();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.TeamId = Entities.ParseIntegerValue(obj, "TeamId");
          entity.CoalitionId = Entities.ParseIntegerValue(obj, "CoalitionId");
          entity.Name = Entities.ParseStringValue(obj, "Name");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Coalition")){  entity.Coalition = com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Coalition"));}
          JSONArray jsonTeamRepresentatives = Entities.ParseJSONArrayValue(obj, "TeamRepresentatives");
                if (jsonTeamRepresentatives != null){
                    for (int index = 0; index < jsonTeamRepresentatives.length();index++){
                        JSONObject lstItem = jsonTeamRepresentatives.getJSONObject(index);
                        entity.TeamRepresentatives.add(com.enrollandpay.serviceinterface.BusinessEntity.TeamRepresentative.Deserialize(lst, lstItem));
                    }
                }
          JSONArray jsonMerchantAccesses = Entities.ParseJSONArrayValue(obj, "MerchantAccesses");
                if (jsonMerchantAccesses != null){
                    for (int index = 0; index < jsonMerchantAccesses.length();index++){
                        JSONObject lstItem = jsonMerchantAccesses.getJSONObject(index);
                        entity.MerchantAccesses.add(com.enrollandpay.serviceinterface.BusinessEntity.MerchantAccess.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Team)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Team() {
		TeamRepresentatives = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.TeamRepresentative>();
			MerchantAccesses = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.MerchantAccess>();
			}
}
