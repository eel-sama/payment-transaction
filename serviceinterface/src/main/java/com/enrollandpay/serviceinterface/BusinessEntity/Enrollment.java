package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Enrollment extends BaseBusinessEntity {
	private String EnrollmentId; 
	public String getEnrollmentId() { return this.EnrollmentId; }
	public void setEnrollmentId(String EnrollmentId) { this.EnrollmentId = EnrollmentId; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private String Data; 
	public String getData() { return this.Data; }
	public void setData(String Data) { this.Data = Data; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private Date UpdateDateTime; 
	public Date getUpdateDateTime() { return this.UpdateDateTime; }
	public void setUpdateDateTime(Date UpdateDateTime) { this.UpdateDateTime = UpdateDateTime; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    public static Enrollment Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Enrollment entity = new Enrollment();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.EnrollmentId = Entities.ParseStringValue(obj, "EnrollmentId");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.Data = Entities.ParseStringValue(obj, "Data");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.UpdateDateTime = Entities.ParseDateValue(obj, "UpdateDateTime");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Enrollment)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Enrollment() {
		}
}
