package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class Tabletop {
	private Integer TabletopId; 
	public Integer getTabletopId() { return this.TabletopId; }
	public void setTabletopId(Integer TabletopId) { this.TabletopId = TabletopId; }
	private String Name; 
	public String getName() { return this.Name; }
	public void setName(String Name) { this.Name = Name; }
	private String PosIdent; 
	public String getPosIdent() { return this.PosIdent; }
	public void setPosIdent(String PosIdent) { this.PosIdent = PosIdent; }
	private Integer StatusType;
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Integer LocationId; 
	public Integer getLocationId() { return this.LocationId; }
	public void setLocationId(Integer LocationId) { this.LocationId = LocationId; }
	private Integer PaymentDeviceId; 
	public Integer getPaymentDeviceId() { return this.PaymentDeviceId; }
	public void setPaymentDeviceId(Integer PaymentDeviceId) { this.PaymentDeviceId = PaymentDeviceId; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
	public Location getLocation() { return this.Location; }
	public void setLocation(Location Location) { this.Location = Location; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Device PaymentDevice;
	public Device getPaymentDevice() { return this.PaymentDevice; }
	public void setPaymentDevice(Device PaymentDevice) { this.PaymentDevice = PaymentDevice; }
    public List<com.enrollandpay.serviceinterface.BusinessEntity.Order> Orders; 
    public static Tabletop Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                Tabletop entity = new Tabletop();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.TabletopId = Entities.ParseIntegerValue(obj, "TabletopId");
          entity.Name = Entities.ParseStringValue(obj, "Name");
          entity.PosIdent = Entities.ParseStringValue(obj, "PosIdent");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.LocationId = Entities.ParseIntegerValue(obj, "LocationId");
          entity.PaymentDeviceId = Entities.ParseIntegerValue(obj, "PaymentDeviceId");
          if (obj.has("Location")){  entity.Location = com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Location"));}
          if (obj.has("PaymentDevice")){  entity.PaymentDevice = com.enrollandpay.serviceinterface.BusinessEntity.Device.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"PaymentDevice"));}
          JSONArray jsonOrders = Entities.ParseJSONArrayValue(obj, "Orders");
                if (jsonOrders != null){
                    for (int index = 0; index < jsonOrders.length();index++){
                        JSONObject lstItem = jsonOrders.getJSONObject(index);
                        entity.Orders.add(com.enrollandpay.serviceinterface.BusinessEntity.Order.Deserialize(lst, lstItem));
                    }
                }
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (Tabletop)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public Tabletop() {
		Orders = new ArrayList<com.enrollandpay.serviceinterface.BusinessEntity.Order>();
			}
}
