package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class LogError extends BaseBusinessEntity {
	private Integer LogErrorId; 
	public Integer getLogErrorId() { return this.LogErrorId; }
	public void setLogErrorId(Integer LogErrorId) { this.LogErrorId = LogErrorId; }
	private Date DateStamp; 
	public Date getDateStamp() { return this.DateStamp; }
	public void setDateStamp(Date DateStamp) { this.DateStamp = DateStamp; }
	private String DebugInfo; 
	public String getDebugInfo() { return this.DebugInfo; }
	public void setDebugInfo(String DebugInfo) { this.DebugInfo = DebugInfo; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
    public static LogError Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                LogError entity = new LogError();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.LogErrorId = Entities.ParseIntegerValue(obj, "LogErrorId");
          entity.DateStamp = Entities.ParseDateValue(obj, "DateStamp");
          entity.DebugInfo = Entities.ParseStringValue(obj, "DebugInfo");
          entity.TS = Entities.ParseStringValue(obj, "TS");
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (LogError)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public LogError() {
		}
}
