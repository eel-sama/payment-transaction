package com.enrollandpay.serviceinterface.BusinessEntity;
import android.util.Log;
import com.enrollandpay.serviceinterface.GlobalVariables;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

public class CoalitionMerchant extends BaseBusinessEntity {
	private Integer CoalitionMerchant1; 
	public Integer getCoalitionMerchant1() { return this.CoalitionMerchant1; }
	public void setCoalitionMerchant1(Integer CoalitionMerchant1) { this.CoalitionMerchant1 = CoalitionMerchant1; }
	private Integer MerchantId; 
	public Integer getMerchantId() { return this.MerchantId; }
	public void setMerchantId(Integer MerchantId) { this.MerchantId = MerchantId; }
	private Integer CoalitionId; 
	public Integer getCoalitionId() { return this.CoalitionId; }
	public void setCoalitionId(Integer CoalitionId) { this.CoalitionId = CoalitionId; }
	private Double BaseCost; 
	public Double getBaseCost() { return this.BaseCost; }
	public void setBaseCost(Double BaseCost) { this.BaseCost = BaseCost; }
	private Double BasePrice; 
	public Double getBasePrice() { return this.BasePrice; }
	public void setBasePrice(Double BasePrice) { this.BasePrice = BasePrice; }
	private Double PerLocationCost; 
	public Double getPerLocationCost() { return this.PerLocationCost; }
	public void setPerLocationCost(Double PerLocationCost) { this.PerLocationCost = PerLocationCost; }
	private Double PerLocationPrice; 
	public Double getPerLocationPrice() { return this.PerLocationPrice; }
	public void setPerLocationPrice(Double PerLocationPrice) { this.PerLocationPrice = PerLocationPrice; }
	private Double PerDeviceCost; 
	public Double getPerDeviceCost() { return this.PerDeviceCost; }
	public void setPerDeviceCost(Double PerDeviceCost) { this.PerDeviceCost = PerDeviceCost; }
	private Double PerDevicePrice; 
	public Double getPerDevicePrice() { return this.PerDevicePrice; }
	public void setPerDevicePrice(Double PerDevicePrice) { this.PerDevicePrice = PerDevicePrice; }
	private Double PerCampaignCost; 
	public Double getPerCampaignCost() { return this.PerCampaignCost; }
	public void setPerCampaignCost(Double PerCampaignCost) { this.PerCampaignCost = PerCampaignCost; }
	private Double PerCampaignPrice; 
	public Double getPerCampaignPrice() { return this.PerCampaignPrice; }
	public void setPerCampaignPrice(Double PerCampaignPrice) { this.PerCampaignPrice = PerCampaignPrice; }
	private Double PerSMSCost; 
	public Double getPerSMSCost() { return this.PerSMSCost; }
	public void setPerSMSCost(Double PerSMSCost) { this.PerSMSCost = PerSMSCost; }
	private Double PerSMSPrice; 
	public Double getPerSMSPrice() { return this.PerSMSPrice; }
	public void setPerSMSPrice(Double PerSMSPrice) { this.PerSMSPrice = PerSMSPrice; }
	private Date StartDate; 
	public Date getStartDate() { return this.StartDate; }
	public void setStartDate(Date StartDate) { this.StartDate = StartDate; }
	private Integer StatusType; 
	public Integer getStatusType() { return this.StatusType; }
	public void setStatusType(Integer StatusType) { this.StatusType = StatusType; }
	private String TS; 
	public String getTS() { return this.TS; }
	public void setTS(String TS) { this.TS = TS; }
	private Double PerCampaignPriceFreemium; 
	public Double getPerCampaignPriceFreemium() { return this.PerCampaignPriceFreemium; }
	public void setPerCampaignPriceFreemium(Double PerCampaignPriceFreemium) { this.PerCampaignPriceFreemium = PerCampaignPriceFreemium; }
	private Integer MaxFreeEnrollments; 
	public Integer getMaxFreeEnrollments() { return this.MaxFreeEnrollments; }
	public void setMaxFreeEnrollments(Integer MaxFreeEnrollments) { this.MaxFreeEnrollments = MaxFreeEnrollments; }
	private Integer PrepaidCampaigns; 
	public Integer getPrepaidCampaigns() { return this.PrepaidCampaigns; }
	public void setPrepaidCampaigns(Integer PrepaidCampaigns) { this.PrepaidCampaigns = PrepaidCampaigns; }
	private Integer SalesRepresentativeId; 
	public Integer getSalesRepresentativeId() { return this.SalesRepresentativeId; }
	public void setSalesRepresentativeId(Integer SalesRepresentativeId) { this.SalesRepresentativeId = SalesRepresentativeId; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Coalition Coalition;
	public Coalition getCoalition() { return this.Coalition; }
	public void setCoalition(Coalition Coalition) { this.Coalition = Coalition; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Merchant Merchant;
	public Merchant getMerchant() { return this.Merchant; }
	public void setMerchant(Merchant Merchant) { this.Merchant = Merchant; }
    private com.enrollandpay.serviceinterface.BusinessEntity.Representative SalesRepresentative;
	public Representative getSalesRepresentative() { return this.SalesRepresentative; }
	public void setSalesRepresentative(Representative SalesRepresentative) { this.SalesRepresentative = SalesRepresentative; }
    public static CoalitionMerchant Deserialize(HashMap<String,Object> lst, JSONObject obj){
        if (obj == null){return null;}
        try {
            if (obj.has("$id")) {
                CoalitionMerchant entity = new CoalitionMerchant();
                lst.put(obj.getString("$id"),entity);
                //Primary
          entity.CoalitionMerchant1 = Entities.ParseIntegerValue(obj, "CoalitionMerchant1");
          entity.MerchantId = Entities.ParseIntegerValue(obj, "MerchantId");
          entity.CoalitionId = Entities.ParseIntegerValue(obj, "CoalitionId");
          entity.BaseCost = Entities.ParseDoubleValue(obj, "BaseCost");
          entity.BasePrice = Entities.ParseDoubleValue(obj, "BasePrice");
          entity.PerLocationCost = Entities.ParseDoubleValue(obj, "PerLocationCost");
          entity.PerLocationPrice = Entities.ParseDoubleValue(obj, "PerLocationPrice");
          entity.PerDeviceCost = Entities.ParseDoubleValue(obj, "PerDeviceCost");
          entity.PerDevicePrice = Entities.ParseDoubleValue(obj, "PerDevicePrice");
          entity.PerCampaignCost = Entities.ParseDoubleValue(obj, "PerCampaignCost");
          entity.PerCampaignPrice = Entities.ParseDoubleValue(obj, "PerCampaignPrice");
          entity.PerSMSCost = Entities.ParseDoubleValue(obj, "PerSMSCost");
          entity.PerSMSPrice = Entities.ParseDoubleValue(obj, "PerSMSPrice");
          entity.StartDate = Entities.ParseDateValue(obj, "StartDate");
          entity.StatusType = Entities.ParseIntegerValue(obj, "StatusType");
          entity.TS = Entities.ParseStringValue(obj, "TS");
          entity.PerCampaignPriceFreemium = Entities.ParseDoubleValue(obj, "PerCampaignPriceFreemium");
          entity.MaxFreeEnrollments = Entities.ParseIntegerValue(obj, "MaxFreeEnrollments");
          entity.PrepaidCampaigns = Entities.ParseIntegerValue(obj, "PrepaidCampaigns");
          entity.SalesRepresentativeId = Entities.ParseIntegerValue(obj, "SalesRepresentativeId");
          if (obj.has("Coalition")){  entity.Coalition = com.enrollandpay.serviceinterface.BusinessEntity.Coalition.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Coalition"));}
          if (obj.has("Merchant")){  entity.Merchant = com.enrollandpay.serviceinterface.BusinessEntity.Merchant.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"Merchant"));}
          if (obj.has("SalesRepresentative")){  entity.SalesRepresentative = com.enrollandpay.serviceinterface.BusinessEntity.Representative.Deserialize(lst, Entities.ParseJSONObjectValue(obj,"SalesRepresentative"));}
            return entity;
        } else if (obj.has("$ref")) {
                //This is a ref, so look in the lst
                String refIdent = obj.getString("$ref");
                return (CoalitionMerchant)lst.get(refIdent);
            }
        }
        catch (Exception exception){
            Log.d(GlobalVariables.TAG, exception.toString());
        }
        return null;
    }
	public CoalitionMerchant() {
		}
}
