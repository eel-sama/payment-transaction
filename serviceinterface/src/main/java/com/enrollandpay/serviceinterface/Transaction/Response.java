package com.enrollandpay.serviceinterface.Transaction;

import com.enrollandpay.serviceinterface.Types;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Response {
    @SerializedName("POSIdent")
    @Expose
    private String POSIdent;
    @SerializedName("DecisionType")
    @Expose
    private Types.DecisionTypes DecisionType;
    @SerializedName("Reason")
    @Expose
    private String Reason;
    @SerializedName("ApprovedAmount")
    @Expose
    private Double ApprovedAmount;
    @SerializedName("ApprovedTipAmount")
    @Expose
    private Double ApprovedTipAmount;
    @SerializedName("BalanceAmount")
    @Expose
    private Double BalanceAmount;
    @SerializedName("ApprovalIdent")
    @Expose
    private String ApprovalIdent;
    @SerializedName("ReferenceIdent")
    @Expose
    private String ReferenceIdent;
    @SerializedName("VoidIdent")
    @Expose
    private String VoidIdent;
    @SerializedName("CardFirstSix")
    @Expose
    private String CardFirstSix;
    @SerializedName("CardLastFour")
    @Expose
    private String CardLastFour;
    @SerializedName("CardType")
    @Expose
    private Types.CardTypes CardType;
    @SerializedName("CardCustomerName")
    @Expose
    private String CardCustomerName;
    @SerializedName("CardToken")
    @Expose
    private String CardToken;
    @SerializedName("CardPAR")
    @Expose
    private String CardPAR;
    @SerializedName("OriginalRequest")
    @Expose
    private Request OriginalRequest;
    @SerializedName("BatchIdent")
    @Expose
    private String BatchIdent;
    @SerializedName("Attributes")
    @Expose
    private Map<String,String> Attributes = new HashMap<String,String>();
    /////////////////////////

    public Map<String, String> getAttributes() {        return Attributes;    }
    public void setAttributes(Map<String, String> attributes) {        Attributes = attributes;    }
    public String getBatchIdent() {  return BatchIdent;  }
    public void setBatchIdent(String batchIdent) { BatchIdent = batchIdent;   }
    public String getPOSIdent() {
        return POSIdent;
    }
    public void setPOSIdent(String POSIdent) {
        this.POSIdent = POSIdent;
    }
    public Types.DecisionTypes getDecisionType() {
        return DecisionType;
    }
    public void setDecisionType(Types.DecisionTypes decisionType) {
        DecisionType = decisionType;
    }
    public String getReason() {        return Reason;    }
    public void setReason(String reason) {        Reason = reason;    }
    public Double getApprovedAmount() {
        return ApprovedAmount;
    }

    public Double getApprovedTipAmount() {
        return ApprovedTipAmount;
    }

    public void setApprovedTipAmount(Double approvedTipAmount) {
        ApprovedTipAmount = approvedTipAmount;
    }

    public void setApprovedAmount(Double approvedAmount) {
        ApprovedAmount = approvedAmount;
    }
    public Double getBalanceAmount() {
        return BalanceAmount;
    }
    public void setBalanceAmount(Double balanceAmount) {
        BalanceAmount = balanceAmount;
    }
    public String getApprovalIdent() {
        return ApprovalIdent;
    }
    public void setApprovalIdent(String approvalIdent) {
        ApprovalIdent = approvalIdent;
    }
    public String getReferenceIdent() {
        return ReferenceIdent;
    }
    public void setReferenceIdent(String referenceIdent) {
        ReferenceIdent = referenceIdent;
    }
    public String getVoidIdent() {        return VoidIdent;    }
    public void setVoidIdent(String voidIdent) {        VoidIdent = voidIdent;    }
    public String getCardFirstSix() {
        return CardFirstSix;
    }
    public void setCardFirstSix(String cardFirstSix) {
        CardFirstSix = cardFirstSix;
    }
    public String getCardLastFour() {
        return CardLastFour;
    }
    public void setCardLastFour(String cardLastFour) {
        CardLastFour = cardLastFour;
    }
    public Types.CardTypes getCardType() {        return CardType;    }
    public void setCardType(Types.CardTypes cardType) {
        CardType = cardType;
    }
    public String getCardCustomerName() {
        return CardCustomerName;
    }
    public void setCardCustomerName(String cardCustomerName) {        CardCustomerName = cardCustomerName;    }
    public String getCardToken() {
        return CardToken;
    }
    public void setCardToken(String cardToken) {
        CardToken = cardToken;
    }
    public String getCardPAR() {
        return CardPAR;
    }
    public void setCardPAR(String cardPAR) {
        CardPAR = cardPAR;
    }
    public Request getOriginalRequest() {        return OriginalRequest;    }
    public void setOriginalRequest(Request originalRequest) {        OriginalRequest = originalRequest;    }
}
