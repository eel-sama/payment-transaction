package com.enrollandpay.serviceinterface.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppliedDiscount extends Transaction {
    @SerializedName("InternalIdent")
    @Expose
    private String InternalIdent;
    @SerializedName("AppliedCount")
    @Expose
    private Double AppliedCount;
    @SerializedName("AppliedAmount")
    @Expose
    private Double AppliedAmount;
    @SerializedName("Reason")
    @Expose
    private String Reason;
    /////////////////////
    public String getInternalIdent() {        return InternalIdent;    }
    public void setInternalIdent(String internalIdent) {        InternalIdent = internalIdent;    }
    public Double getAppliedCount() {        return AppliedCount;    }
    public void setAppliedCount(Double appliedCount) {        AppliedCount = appliedCount;    }
    public Double getAppliedAmount() {        return AppliedAmount;    }
    public void setAppliedAmount(Double appliedAmount) {        AppliedAmount = appliedAmount;    }
    public String getReason() {        return Reason;    }
    public void setReason(String reason) {        Reason = reason; }
}
