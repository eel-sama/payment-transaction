package com.enrollandpay.serviceinterface.Transaction;

import android.content.Intent;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApplyDiscount {
    @SerializedName("TransactionIdent")
    @Expose
    private String TransactionIdent;
    @SerializedName("OrderId")
    @Expose
    private Integer OrderId;
    @SerializedName("OrderPaymentId")
    @Expose
    private Integer OrderPaymentId;
    @SerializedName("InternalIdent")
    @Expose
    private String InternalIdent;
    @SerializedName("POSDiscountIdent")
    @Expose
    private String POSDiscountIdent;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Amount")
    @Expose
    private Double Amount;
    @SerializedName("Percentage")
    @Expose
    private Double Percentage;
    @SerializedName("ApplyRewardOfferType")
    @Expose
    private Integer ApplyRewardOfferType;
    @SerializedName("LoyaltyProgramId")
    @Expose
    private Integer LoyaltyProgramId;
    ///////////////////////////////////////////
    public String getLookupId(){
        return TextUtils.isEmpty(getTransactionIdent()) ? "ORDERID_" + getOrderId() : getTransactionIdent();
    }
    public String getTransactionIdent() {        return TransactionIdent;    }
    public void setTransactionIdent(String transactionIdent) {        TransactionIdent = transactionIdent;    }
    public String getInternalIdent() {
        return InternalIdent;
    }
    public void setInternalIdent(String internalIdent) {
        InternalIdent = internalIdent;
    }
    public String getPOSDiscountIdent() {
        return POSDiscountIdent;
    }
    public void setPOSDiscountIdent(String POSDiscountIdent) {
        this.POSDiscountIdent = POSDiscountIdent;
    }
    public String getName() {
        return Name;
    }
    public void setName(String name) {
        Name = name;
    }
    public Double getAmount() {
        return Amount;
    }
    public void setAmount(Double amount) {
        Amount = amount;
    }
    public Double getPercentage() {
        return Percentage;
    }
    public void setPercentage(Double percentage) {
        Percentage = percentage;
    }
    public Integer getApplyRewardOfferType() {
        return ApplyRewardOfferType;
    }
    public void setApplyRewardOfferType(Integer applyRewardOfferType) {        ApplyRewardOfferType = applyRewardOfferType;    }
    public Integer getLoyaltyProgramId() {
        return LoyaltyProgramId;
    }
    public void setLoyaltyProgramId(Integer loyaltyProgramId) {        LoyaltyProgramId = loyaltyProgramId;    }
    public Integer getOrderId() {        return OrderId;    }
    public void setOrderId(Integer orderId) {        OrderId = orderId;    }
    public Integer getOrderPaymentId() {        return OrderPaymentId;    }
    public void setOrderPaymentId(Integer orderPaymentId) {        OrderPaymentId = orderPaymentId;    }
}