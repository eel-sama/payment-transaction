package com.enrollandpay.serviceinterface.Transaction;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Transaction {
    @SerializedName("EmployeeName")
    @Expose
    private String EmployeeName;
    @SerializedName("EmployeeId")
    @Expose
    private Integer EmployeeId;
    @SerializedName("EmployeeIdent")
    @Expose
    private String EmployeeIdent;
    @SerializedName("TableIdent")
    @Expose
    private String TableIdent;
    @SerializedName("TabletopId")
    @Expose
    private Integer TabletopId;
    @SerializedName("RegisterIdent")
    @Expose
    private String RegisterIdent;
    @SerializedName("POSIdent")
    @Expose
    private String POSIdent;
    //////OPTIONAL
    @SerializedName("TransactionAmount")
    @Expose
    private Double TransactionAmount;
    @SerializedName("TransactionDueAmount")
    @Expose
    private Double TransactionDueAmount;
    @SerializedName("TransactionDiscountAmount")
    @Expose
    private Double TransactionDiscountAmount;
    @SerializedName("TransactionTaxAmount")
    @Expose
    private Double TransactionTaxAmount;
    @SerializedName("Items")
    @Expose
    private List<Item> items = null;
    @SerializedName("OrderId")
    @Expose
    private Integer OrderId;
    @SerializedName("OrderPaymentId")
    @Expose
    private Integer OrderPaymentId;
    @SerializedName("TransactionTenderedAmount")
    @Expose
    private Double TransactionTenderedAmount = 0.0;
    @SerializedName("TransactionCurrentApprovedAmount")
    @Expose
    private Double TransactionCurrentApprovedAmount = null;
    ///////////////////////////////////////////
    public Double getTransactionTenderedAmount() {        return TransactionTenderedAmount;    }
    public void setTransactionTenderedAmount(Double transactionTenderedAmount) {        TransactionTenderedAmount = transactionTenderedAmount;    }
    public Integer getEmployeeId() {        return EmployeeId;    }
    public void setEmployeeId(Integer employeeId) {        EmployeeId = employeeId;    }
    public String getEmployeeName() {
        return EmployeeName;
    }
    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }
    public String getEmployeeIdent() {
        return EmployeeIdent;
    }
    public void setEmployeeIdent(String employeeIdent) {
        EmployeeIdent = employeeIdent;
    }
    public String getTableIdent() {
        return TableIdent;
    }
    public void setTableIdent(String tableIdent) {
        TableIdent = tableIdent;
    }
    public Integer getTabletopId() {        return TabletopId;    }
    public void setTabletopId(Integer tabletopId) {        TabletopId = tabletopId;    }
    public String getRegisterIdent() {
        return RegisterIdent;
    }
    public void setRegisterIdent(String registerIdent) {
        RegisterIdent = registerIdent;
    }
    public String getLookupId(){
       return TextUtils.isEmpty(getPOSIdent()) ? "ORDERID_" + getOrderId() : getPOSIdent();
    }
    public String getPOSIdent() {
        return POSIdent;
    }
    public void setPOSIdent(String POSIdent) {
        this.POSIdent = POSIdent;
    }
    public Double getTransactionAmount() {
        return TransactionAmount;
    }
    public void setTransactionAmount(Double transactionAmount) {        TransactionAmount = transactionAmount;    }
    public Double getTransactionDiscountAmount() {
        return TransactionDiscountAmount;
    }
    public Double getTransactionDueAmount() {        return TransactionDueAmount;  }
    public void setTransactionDueAmount(Double transactionDueAmount) {
        TransactionDueAmount = transactionDueAmount;
    }
    public Double getTransactionCurrentApprovedAmount() {        return TransactionCurrentApprovedAmount;    }
    public void setTransactionCurrentApprovedAmount(Double transactionCurrentApprovedAmount) {        TransactionCurrentApprovedAmount = transactionCurrentApprovedAmount;    }
    public void setTransactionDiscountAmount(Double transactionDiscountAmount) {        TransactionDiscountAmount = transactionDiscountAmount;    }
    public Double getTransactionTaxAmount() {
        return TransactionTaxAmount;
    }
    public void setTransactionTaxAmount(Double transactionTaxAmount) {        TransactionTaxAmount = transactionTaxAmount;    }
    public boolean hasItems() { return (items != null && items.size() > 0); }
    public List<Item> getItems() {        return items;    }
    public void setItems(List<Item> items) {
        this.items = items;
    }
    public Integer getOrderId() {        return OrderId;    }
    public void setOrderId(Integer orderId) {        OrderId = orderId;    }
    public Integer getOrderPaymentId() {        return OrderPaymentId;    }
    public void setOrderPaymentId(Integer orderPaymentId) {        OrderPaymentId = orderPaymentId;    }
}
