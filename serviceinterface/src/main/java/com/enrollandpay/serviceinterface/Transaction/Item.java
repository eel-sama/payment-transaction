package com.enrollandpay.serviceinterface.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {
    @SerializedName("OrderItemId")
    @Expose
    private Integer OrderItemId;
    @SerializedName("POSItemIdent")
    @Expose
    private String POSItemIdent;
    @SerializedName("ProductId")
    @Expose
    private Integer ProductId;
    @SerializedName("ParentTransactionItemIdent")
    @Expose
    private String ParentTransactionItemIdent;
    @SerializedName("Quantity")
    @Expose
    private Double Quantity;
    @SerializedName("QuantitySplit")
    @Expose
    private Double QuantitySplit;
    @SerializedName("POSProductIdent")
    @Expose
    private String POSProductIdent;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Description")
    @Expose
    private String Description;
    @SerializedName("TotalAmount")
    @Expose
    private Double TotalAmount;
    @SerializedName("TotalTaxAmount")
    @Expose
    private Double TaxAmount;
    /////////////////////

    public Double getQuantitySplit() {        return QuantitySplit;    }
    public void setQuantitySplit(Double quantitySplit) {        QuantitySplit = quantitySplit;    }
    public String getParentTransactionItemIdent() {        return ParentTransactionItemIdent;    }
    public void setParentTransactionItemIdent(String parentTransactionItemIdent) {        this.ParentTransactionItemIdent = parentTransactionItemIdent;    }
    public String getPOSItemIdent() {        return POSItemIdent;    }
    public void setPOSItemIdent(String POSItemIdent) {        this.POSItemIdent = POSItemIdent;    }
    public Double getQuantity() {        return Quantity;    }
    public void setQuantity(Double quantity) {        Quantity = quantity;    }
    public String getName() {        return Name;    }
    public void setName(String name) {        Name = name;    }
    public String getDescription() {        return Description;    }
    public void setDescription(String description) {        Description = description;    }
    public Double getTotalAmount() {        return TotalAmount;    }
    public void setTotalAmount(Double totalAmount) {        TotalAmount = totalAmount;    }
    public Double getTaxAmount() {        return TaxAmount;    }
    public void setTaxAmount(Double taxAmount) {        TaxAmount = taxAmount;    }
    public String getPOSProductIdent() {        return POSProductIdent;    }
    public void setPOSProductIdent(String POSProductIdent) {        this.POSProductIdent = POSProductIdent;    }
    public Integer getOrderItemId() {        return OrderItemId;    }
    public void setOrderItemId(Integer orderItemId) {        OrderItemId = orderItemId;    }

    public Integer getProductId() {        return ProductId;    }
    public void setProductId(Integer productId) {        ProductId = productId;    }
}
