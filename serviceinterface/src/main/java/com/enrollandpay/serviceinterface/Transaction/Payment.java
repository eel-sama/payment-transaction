package com.enrollandpay.serviceinterface.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payment {
    @SerializedName("AmountTendered")
    @Expose
    private Double AmountTendered;
    @SerializedName("ApprovalIdent")
    @Expose
    private String ApprovalIdent;
    @SerializedName("CardType")
    @Expose
    private Integer CardType;
    @SerializedName("NameOnCard")
    @Expose
    private String NameOnCard;
    @SerializedName("OrderPaymentId")
    @Expose
    private Integer OrderPaymentId;
    @SerializedName("PanMasked")
    @Expose
    private String PanMasked;
    @SerializedName("ReferenceIdent")
    @Expose
    private String ReferenceIdent;
    @SerializedName("StatusType")
    @Expose
    private Integer StatusType;

    public Double getAmountTendered() {
        return AmountTendered;
    }

    public void setAmountTendered(Double amountTendered) {
        AmountTendered = amountTendered;
    }

    public String getApprovalIdent() {
        return ApprovalIdent;
    }

    public void setApprovalIdent(String approvalIdent) {
        ApprovalIdent = approvalIdent;
    }

    public Integer getCardType() {
        return CardType;
    }

    public void setCardType(Integer cardType) {
        CardType = cardType;
    }

    public String getNameOnCard() {
        return NameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        NameOnCard = nameOnCard;
    }

    public Integer getOrderPaymentId() {
        return OrderPaymentId;
    }

    public void setOrderPaymentId(Integer orderPaymentId) {
        OrderPaymentId = orderPaymentId;
    }

    public String getPanMasked() {
        return PanMasked;
    }

    public void setPanMasked(String panMasked) {
        PanMasked = panMasked;
    }

    public String getReferenceIdent() {
        return ReferenceIdent;
    }

    public void setReferenceIdent(String referenceIdent) {
        ReferenceIdent = referenceIdent;
    }

    public Integer getStatusType() {
        return StatusType;
    }

    public void setStatusType(Integer statusType) {
        StatusType = statusType;
    }
    public Payment(){}
    public Payment(com.enrollandpay.serviceinterface.Commands.Payment payment){
        this.AmountTendered = payment.getAmountTendered();
        this.ApprovalIdent = payment.getApprovalIdent();
        this.CardType = payment.getCardType();
        this.NameOnCard = payment.getNameOnCard();
        this.OrderPaymentId = payment.getOrderPaymentId();
        this.PanMasked = payment.getPanMasked();
        this.ReferenceIdent = payment.getReferenceIdent();
        this.StatusType = payment.getStatusType();
    }
    public Payment(com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment orderPayment){
        this.AmountTendered = orderPayment.getAmountTendered();
        this.ApprovalIdent = orderPayment.getApprovalIdent();
        if (orderPayment.getCard() != null) {
            this.CardType = orderPayment.getCard().getCardType();
            this.NameOnCard = orderPayment.getCard().getNameOnCard();
            this.PanMasked = orderPayment.getCard().getPanMasked();
        }
        this.OrderPaymentId = orderPayment.getOrderPaymentId();
        this.ReferenceIdent = orderPayment.getReferenceIdent();
        this.StatusType = orderPayment.getStatusType();
    }
}
