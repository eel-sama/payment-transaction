package com.enrollandpay.serviceinterface.Transaction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.enrollandpay.serviceinterface.Types;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Request extends Transaction {
    public enum TransactionRequestTypes{
        None, Sale, Void, Return, Print, Open, Close
    }
    public enum SearchMode {
        InvoiceNo,
        ConsumerCard,
        PhoneNo,
        Email,
        Date
    }
    public enum OperationType {
        NONE,
        RETURN
    }
    public enum RequestTypes
    {
        CreditSale(1), CreditAuth(2), CreditPostAuth(3), CreditForced(4), CreditReturn(5), CreditVerify(6), CreditVoidSale(7), CreditVoidAuth(8), CreditVoidPostAuth(9), CreditVoidForced(10), CreditVoidReturn(11),CreditBalance(12),
        DebitSale(13), DebitReturn(14), DebitVoidSale(15), DebitVoidReturn(16),DebitBalance(17),
        EBTSale(18), EBTReturn(19), EBTVoidSale(20), EBTVoidReturn(21), EBTWithdrawal(22), EBTBalance(23), CashSale(24), CashVoid(25), CashReturn(26), CreditInternalAuth(27), TokenPar(99), OpenTab(28), GenericReturn(29), GenericSale(30), LineItemSale(31);
        private final Integer  RequestTypeId;
        RequestTypes(int requestTypeId) {
            this.RequestTypeId = requestTypeId;
        }
        public Integer getValue(){
            return RequestTypeId;
        }
        public static RequestTypes fromInteger(Integer x) {
            if (x == null){return  null;}
            switch(x) {
                case 1:
                    return CreditSale;
                case 2:
                    return CreditAuth;
                case 3:
                    return CreditPostAuth;
                case 4:
                    return CreditForced;
                case 5:
                    return CreditReturn;
                case 6:
                    return CreditVerify;
                case 7:
                    return CreditVoidSale;
                case 8:
                    return CreditVoidAuth;
                case 9:
                    return CreditVoidPostAuth;
                case 10:
                    return CreditVoidForced;
                case 11:
                    return CreditVoidReturn;
                case 12:
                    return CreditBalance;
                case 13:
                    return DebitSale;
                case 14:
                    return DebitReturn;
                case 15:
                    return DebitVoidSale;
                case 16:
                    return DebitVoidReturn;
                case 17:
                    return DebitBalance;
                case 18:
                    return EBTSale;
                case 19:
                    return EBTReturn;
                case 20:
                    return EBTVoidSale;
                case 21:
                    return  EBTVoidReturn;
                case 22:
                    return EBTWithdrawal;
                case 23:
                    return EBTBalance;
                case 24:
                    return CashSale;
                case 25:
                    return CashVoid;
                case 26:
                    return CashReturn;
                case 27:
                    return CreditInternalAuth;
                case 28:
                    return OpenTab;
                case 99:
                    return TokenPar;
                case 29:
                    return GenericReturn;
                case 30:
                    return GenericSale;
                case 31:
                    return LineItemSale;
            }
            return  null;
        }
    };
    private transient List<Payment> payments = new ArrayList<>();
    //private transient List<Integer> paymentTypes = new ArrayList<>();
    private transient Double totalAmountTendered;

    private transient Set<String> paidItems = new HashSet<>();
    private transient List<Double> tips = new ArrayList<>();
    private transient List<Double> discounts = new ArrayList<>();
    private transient  boolean splitPayment = false;

    private transient boolean matchDevicePaymentId = true;
    private transient boolean isOpenTransaction = false;
    private transient boolean isDirty = false;

    // this is used primarily on Main->OnManageTransaction
    private transient OperationType operationType = OperationType.NONE;

    public boolean isPaIsProcessedRefund() {
        return paIsProcessedRefund;
    }

    public void setPaIsProcessedRefund(boolean paIsProcessedRefund) {
        this.paIsProcessedRefund = paIsProcessedRefund;
    }

    private transient boolean paIsProcessedRefund = false;

    @SerializedName("RequestType")
    @Expose
    private RequestTypes RequestType;
    @SerializedName("IsPaymentAdjustment")
    @Expose
    private Boolean IsPaymentAdjustment = false;
    @SerializedName("IsPostAuth")
    @Expose
    private Boolean IsPostAuth = false;
    @SerializedName("RequestAmount")
    @Expose
    private Double RequestAmount;
    @SerializedName("RequestTipAmount")
    @Expose
    private Double RequestTipAmount;
    @SerializedName("RequestTipAmount2")
    @Expose
    private Double RequestTipAmount2;
    @SerializedName("RequestTipAmount3")
    @Expose
    private Double RequestTipAmount3;
    @SerializedName("RequestSurchargeAmount")
    @Expose
    private Double RequestSurchargeAmount;
    @SerializedName("IsExternal")
    @Expose
    private Boolean IsExternal = false;
    @SerializedName("NumberOfRetries")
    @Expose
    private Integer NumberOfRetries = 0;
    @SerializedName("VoidPOSIdent")
    @Expose
    private String VoidPOSIdent;
    @SerializedName("ReturnTransactionIdent")
    @Expose
    private String ReturnTransactionIdent;
    @SerializedName("ReturnOrderId")
    @Expose
    private Integer ReturnOrderId;
    @SerializedName("IsReturnRequest")
    @Expose
    private Boolean IsReturnRequest = false;
    //////////////

    public List<Payment> getPayments() {
        return payments;
    }
    public Double getTotalPayments () {
        Double total = 0.0;
        for (Payment payment : getPayments()) {
            if ((payment.getStatusType() & Types.PaymentStatusTypes.Complete.getValue()) == Types.PaymentStatusTypes.Complete.getValue()) {
                total += payment.getAmountTendered();
            }
        }
        return total;
    }
    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    /**
     * This is only used for OpenTab transactions
     * @return
     */
    public Double getOpenTabAmount() {
        if(payments != null) {
            for(Payment payment : getPayments()) {
                if(payment.getStatusType() >= Types.PaymentStatusTypes.OpenTab.getValue()) {
                    return payment.getAmountTendered() != null ? payment.getAmountTendered() : 0.0;
                }
            }
        }

        return 0.0;
    }
    public boolean hasOpenTabInPayments() {
        if(payments != null) {
            for(Payment payment : getPayments()) {
                if(payment.getStatusType() >= Types.PaymentStatusTypes.OpenTab.getValue()) {
                    return payment.getAmountTendered() != null && payment.getAmountTendered() > 0.0 ? true : false;
                }
            }
        }
        return false;
    }
    public Double getTotalAmountTendered() {
        return totalAmountTendered;
    }
    public void setTotalAmountTendered(Double totalAmountTendered) {
        this.totalAmountTendered = totalAmountTendered;
    }
    public boolean hasTenderedAmount() {
        return (getTransactionTenderedAmount() != null && getTransactionTenderedAmount() > 0)
                || ((getTotalAmountTendered() != null && getTotalAmountTendered() > 0));
    }
    public boolean isMatchDevicePaymentId() {
        return matchDevicePaymentId;
    }
    public void setMatchDevicePaymentId(boolean matchDevicePaymentId) {
        this.matchDevicePaymentId = matchDevicePaymentId;
    }
    public boolean isOpenTransaction() {
        return isOpenTransaction;
    }
    public void setOpenTransaction(boolean openTransaction) {
        isOpenTransaction = openTransaction;
    }
    public boolean isDirty() {
        return isDirty;
    }
    public void setDirty(boolean dirty) {
        isDirty = dirty;
    }
    public Set<String> getPaidItems() {
        return paidItems;
    }
    public void setPaidItems(Set<String> paidItems) {
        this.paidItems = paidItems;
    }
    public List<Double> getTips() {
        return tips;
    }
    public void setTips(List<Double> tips) {
        this.tips = tips;
    }
    public Double getTipsTotal () {
        Double total = 0.0;
        if(tips != null) {
            for (Double amount : tips) {
                total += amount;
            }
        }
        return total;
    }
    public List<Double> getDiscounts() {
        return discounts;
    }
    public Double getDiscountTotal () {
        Double total = 0.0;
        if(discounts != null) {
            for (Double amount : discounts) {
                total += amount;
            }
        }
        return total;
    }
    public void setDiscounts(List<Double> discounts) {
        this.discounts = discounts;
    }
    public boolean isSplitPayment() {
        return splitPayment;
    }
    public void setSplitPayment(boolean splitPayment) {
        this.splitPayment = splitPayment;
    }
    public Boolean getIsReturnRequest() {        return IsReturnRequest;    }
    public void setIsReturnRequest(Boolean returnRequest) {        IsReturnRequest = returnRequest;    }

    public Integer getReturnOrderId() {
        return ReturnOrderId;
    }

    public void setReturnOrderId(Integer returnOrderId) {
        ReturnOrderId = returnOrderId;
        if (returnOrderId != null){
            IsReturnRequest = true;
        }
    }

    public String getReturnTransactionIdent() {        return ReturnTransactionIdent;    }
    public void setReturnTransactionIdent(String returnTransactionIdent) {
        ReturnTransactionIdent = returnTransactionIdent;
        if (returnTransactionIdent != null){
            IsReturnRequest = true;
        }
    }
    public String getVoidPOSIdent() {        return VoidPOSIdent;    }
    public void setVoidPOSIdent(String voidPOSIdent) {        VoidPOSIdent = voidPOSIdent;    }
    public Integer getNumberOfRetries() {        return NumberOfRetries;    }
    public void setNumberOfRetries(Integer numberOfRetries) {        NumberOfRetries = numberOfRetries;   }
    public Boolean getIsPaymentAdjustment() {        return IsPaymentAdjustment;    }
    public void setIsPaymentAdjustment(Boolean paymentAdjustment) {        IsPaymentAdjustment = paymentAdjustment;    }
    public Boolean getIsPostAuth() {        return IsPostAuth;    }
    public void setIsPostAuth(Boolean postAuth) {        IsPostAuth = postAuth;    }
    public Boolean getExternal() {        return IsExternal;    }
    public void setExternal(Boolean external) {        IsExternal = external;    }
    public String getApprovalIdent() {
        return ApprovalIdent;
    }
    public void setApprovalIdent(String approvalIdent) {
        ApprovalIdent = approvalIdent;
    }
    public String getReferenceIdent() {
        return ReferenceIdent;
    }
    public void setReferenceIdent(String referenceIdent) {
        ReferenceIdent = referenceIdent;
    }
    public String getVoidIdent() {        return VoidIdent;    }
    public void setVoidIdent(String voidIdent) {        VoidIdent = voidIdent;    }
    public String getCardToken() {
        return CardToken;
    }
    public void setCardToken(String cardToken) {
        CardToken = cardToken;
    }
    public String getCardPAR() {
        return CardPAR;
    }
    public void setCardPAR(String cardPAR) {
        CardPAR = cardPAR;
    }
    /////////////Additional Parameters
    @SerializedName("ApprovalIdent")
    @Expose
    private String ApprovalIdent;
    @SerializedName("ReferenceIdent")
    @Expose
    private String ReferenceIdent;
    @SerializedName("VoidIdent")
    @Expose
    private String VoidIdent;
    @SerializedName("CardToken")
    @Expose
    private String CardToken;
    @SerializedName("CardPAR")
    @Expose
    private String CardPAR;
    @SerializedName("CapturedSignature")
    @Expose
    private String CapturedSignature = null;
    @SerializedName("OpenTabName")
    @Expose
    private String OpenTabName = null;
    //////////////////////////////////////////////////////////

    public String getOpenTabName() {        return OpenTabName;    }
    public void setOpenTabName(String openTabName) {        OpenTabName = openTabName;    }
    public String getCapturedSignature() {        return CapturedSignature;    }
    public void setCapturedSignature(String capturedSignature) {        CapturedSignature = capturedSignature;    }
    public void setRequestType(RequestTypes requestType) {
        RequestType = requestType;
    }
    public RequestTypes getRequestType() {
        return RequestType;
    }
    public Double getRequestAmount() {
        return RequestAmount;
    }
    public void setRequestAmount(Double requestAmount) {
        RequestAmount = requestAmount;
    }
    public Double getRequestTipAmount() {
        return RequestTipAmount;
    }
    public void setRequestTipAmount(Double requestTipAmount) {        RequestTipAmount = requestTipAmount;    }
    public Double getRequestTipAmount2() {        return RequestTipAmount2;    }
    public void setRequestTipAmount2(Double requestTipAmount2) {        RequestTipAmount2 = requestTipAmount2;    }
    public Double getRequestTipAmount3() {        return RequestTipAmount3;    }
    public void setRequestTipAmount3(Double requestTipAmount3) {        RequestTipAmount3 = requestTipAmount3;    }
    public Double getRequestSurchargeAmount() {        return RequestSurchargeAmount;    }
    public void setRequestSurchargeAmount(Double requestSurchargeAmount) {        RequestSurchargeAmount = requestSurchargeAmount;    }

    public OperationType getOperationType() {
        return operationType;
    }
    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    @SerializedName("AdditionalAttributes")
    @Expose
    private HashMap<String, String> additionalAttributes = new HashMap<>();

    public HashMap<String, String> getAdditionalAttributes() {
        return additionalAttributes;
    }

    public void setAdditionalAttributes(HashMap<String, String> additionalAttributes) {
        this.additionalAttributes = additionalAttributes;
    }

    public void Merge(Request request){
        if (request.getItems() != null) {
            this.setItems(request.getItems());
        }
        this.setPayments(request.getPayments());
        this.setTotalAmountTendered(getTotalAmountTendered());
        this.setMatchDevicePaymentId(request.isMatchDevicePaymentId());
        this.setOpenTransaction(request.isOpenTransaction());
        this.setDirty(request.isDirty());
        this.setPaidItems(request.getPaidItems());
        this.setTips(request.getTips());
        this.setDiscounts(request.getDiscounts());
        this.setSplitPayment(request.isSplitPayment());
        this.setTransactionAmount(request.getTransactionAmount());
        this.setRequestAmount(request.getRequestAmount());
        this.setTransactionTaxAmount(request.getTransactionTaxAmount());
        this.setTransactionDueAmount(request.getTransactionDueAmount());
        this.setRequestSurchargeAmount(request.getRequestSurchargeAmount());
        this.setRequestTipAmount(request.getRequestTipAmount());
        this.setRequestTipAmount2(request.getRequestTipAmount2());
        this.setRequestTipAmount3(request.getRequestTipAmount3());
        this.setTransactionDiscountAmount(request.getTransactionDiscountAmount());
        this.setPOSIdent(request.getPOSIdent());
        this.setReferenceIdent(request.getReferenceIdent());
        this.setApprovalIdent(request.getApprovalIdent());
        this.setVoidIdent(request.getVoidIdent());
        this.setOrderId(request.getOrderId());
        this.setOrderPaymentId(request.getOrderPaymentId());
        this.setRequestType(request.getRequestType());
        this.setCapturedSignature(request.getCapturedSignature());
        this.setCardToken(request.getCardToken());
        this.setCardPAR(request.getCardPAR());
        this.setIsPostAuth(request.getIsPostAuth());
        this.setIsPaymentAdjustment(request.getIsPaymentAdjustment());
        this.setExternal(request.getExternal());
        this.setEmployeeName(request.getEmployeeName());
        this.setEmployeeId(request.getEmployeeId());
        this.setEmployeeIdent(request.getEmployeeIdent());
        this.setTableIdent(request.getTableIdent());
        this.setTabletopId(request.getTabletopId());
        this.setAdditionalAttributes(request.getAdditionalAttributes());
        this.setOperationType(request.getOperationType());
    }
}