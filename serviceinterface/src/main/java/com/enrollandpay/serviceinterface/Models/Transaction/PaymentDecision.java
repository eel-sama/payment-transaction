package com.enrollandpay.serviceinterface.Models.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class PaymentDecision extends Transaction{
    @SerializedName("AmountApproved")
    @Expose
    private Double AmountApproved;
    @SerializedName("AmountTotal")
    @Expose
    private Double AmountTotal;
    @SerializedName("AmountDue")
    @Expose
    private Double AmountDue;
    @SerializedName("AmountTax")
    @Expose
    private Double AmountTax;
//    @SerializedName("AmountGratuity")
//    @Expose
//    private Double AmountGratuity;
//    @SerializedName("AmountGratuity2")
//    @Expose
//    private Double AmountGratuity2;
//    @SerializedName("AmountGratuity3")
//    @Expose
//    private Double AmountGratuity3;
//    @SerializedName("AmountSurcharge")
//    @Expose
//    private Double AmountSurcharge;
    @SerializedName("ApprovalIdent")
    @Expose
    private String ApprovalIdent;
    @SerializedName("ReferenceIdent")
    @Expose
    private String ReferenceIdent;
    @SerializedName("VoidIdent")
    @Expose
    private String VoidIdent;
    @SerializedName("Reason")
    @Expose
    private String Reason;
    @SerializedName("CardInformation")
    @Expose
    private CardInformation CardInformation;
    @SerializedName("BatchIdent")
    @Expose
    private String BatchIdent;
    @SerializedName("PaymentType")
    @Expose
    private Integer PaymentType;
    @SerializedName("RequestType")
    @Expose
    private Integer RequestType;
    @SerializedName("AdditionalAttributes")
    @Expose
    private Map<String,String> Attributes = new HashMap<String,String>();
    @SerializedName("OpenTabName")
    @Expose
    private String OpenTabName;
    ///////////////////////////////

    public String getOpenTabName() {        return OpenTabName;    }
    public void setOpenTabName(String openTabName) {        OpenTabName = openTabName;    }

    public Double getAmountDue() {        return AmountDue;    }
    public void setAmountDue(Double amountDue) {        AmountDue = amountDue;    }
    public Map<String, String> getAttributes() {        return Attributes;    }
    public void setAttributes(Map<String, String> attributes) {        Attributes = attributes;    }
    public Integer getPaymentType() {        return PaymentType;    }
    public void setPaymentType(Integer paymentType) {        PaymentType = paymentType;    }
    public String getBatchIdent() {        return BatchIdent;    }
    public void setBatchIdent(String batchIdent) {        BatchIdent = batchIdent;    }
    public Double getAmountApproved() {
        return AmountApproved;
    }
    public void setAmountApproved(Double amountApproved) {
        AmountApproved = amountApproved;
    }
    public Double getAmountTotal() {
        return AmountTotal;
    }
    public void setAmountTotal(Double amountTotal) {
        AmountTotal = amountTotal;
    }
    public Double getAmountTax() {
        return AmountTax;
    }
    public void setAmountTax(Double amountTax) {
        AmountTax = amountTax;
    }
//    public Double getAmountGratuity() {
//        return AmountGratuity;
//    }
//    public void setAmountGratuity(Double amountGratuity) {
//        AmountGratuity = amountGratuity;
//    }
//    public Double getAmountGratuity2() {
//        return AmountGratuity2;
//    }
//    public void setAmountGratuity2(Double amountGratuity2) {
//        AmountGratuity2 = amountGratuity2;
//    }
//    public Double getAmountGratuity3() {
//        return AmountGratuity3;
//    }
//    public void setAmountGratuity3(Double amountGratuity3) {
//        AmountGratuity3 = amountGratuity3;
//    }
//    public Double getAmountSurcharge() {
//        return AmountSurcharge;
//    }
//    public void setAmountSurcharge(Double amountSurcharge) {
//        AmountSurcharge = amountSurcharge;
//    }
    public String getApprovalIdent() {
        return ApprovalIdent;
    }
    public void setApprovalIdent(String approvalIdent) {
        ApprovalIdent = approvalIdent;
    }
    public String getReferenceIdent() {
        return ReferenceIdent;
    }
    public void setReferenceIdent(String referenceIdent) {
        ReferenceIdent = referenceIdent;
    }
    public String getVoidIdent() {
        return VoidIdent;
    }
    public void setVoidIdent(String voidIdent) {
        VoidIdent = voidIdent;
    }
    public String getReason() {
        return Reason;
    }
    public void setReason(String reason) {
        Reason = reason;
    }
    public com.enrollandpay.serviceinterface.Models.Transaction.CardInformation getCardInformation() { return CardInformation;    }
    public void setCardInformation(com.enrollandpay.serviceinterface.Models.Transaction.CardInformation cardInformation) {        CardInformation = cardInformation;    }
    public Integer getRequestType() {        return RequestType;    }
    public void setRequestType(Integer requestType) {        RequestType = requestType;    }
}
