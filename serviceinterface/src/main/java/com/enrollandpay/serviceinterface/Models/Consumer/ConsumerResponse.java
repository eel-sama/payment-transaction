package com.enrollandpay.serviceinterface.Models.Consumer;

import com.enrollandpay.serviceinterface.BusinessEntity.Consumer;
import com.enrollandpay.serviceinterface.BusinessEntity.Order;
import com.enrollandpay.serviceinterface.Models.ResponseBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ConsumerResponse extends ResponseBase implements Serializable {
    @SerializedName("Consumer")
    @Expose
    private Consumer consumer;

    public Consumer getConsumer() {        return consumer;    }
    public void setConsumer(Consumer consumer) {        this.consumer = consumer;    }
}
