package com.enrollandpay.serviceinterface.Models.Location;

import com.enrollandpay.serviceinterface.BusinessEntity.Entities;
import com.enrollandpay.serviceinterface.BusinessEntity.Location;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class LocationOffer {
    @Expose
    @SerializedName("Location")
    private com.enrollandpay.serviceinterface.BusinessEntity.Location Location;
    @Expose
    @SerializedName("Offers")
    private ArrayList<OfferDetails> Offers;
    @Expose
    @SerializedName("Range")
    private Double Range;
    @Expose
    @SerializedName("IsFavorite")
    private Boolean IsFavorite;

    public com.enrollandpay.serviceinterface.BusinessEntity.Location getLocation() {        return Location;    }
    public void setLocation(com.enrollandpay.serviceinterface.BusinessEntity.Location location) {        Location = location;    }
    public ArrayList<OfferDetails> getOffers() {        return Offers;    }
    public void setOffers(ArrayList<OfferDetails> offers) {        Offers = offers;    }
    public Double getRange() {        return Range;    }
    public void setRange(Double range) {        Range = range;    }
    public Boolean getIsFavorite() {        return IsFavorite;    }
    public void setIsFavorite(Boolean favorite) {        IsFavorite = favorite;    }
    public static ArrayList<LocationOffer> GetLocationOffers(JSONArray list){
        ArrayList<LocationOffer> returnList = new ArrayList<>();
        HashMap<String,Object> lookup = new HashMap<>();
        for (int iter=0;iter<list.length();iter++){
            try {
                JSONObject item = list.getJSONObject(iter);
                LocationOffer locationOffer = new LocationOffer();
                locationOffer.setLocation(com.enrollandpay.serviceinterface.BusinessEntity.Location.Deserialize(lookup, item.getJSONObject("Location") ));
                if (item.has("Range")){locationOffer.setRange(item.getDouble("Range"));}
                if (item.has("IsFavorite")){locationOffer.setIsFavorite(item.getBoolean("IsFavorite"));}
                if (item.has("Offers")){
                    JSONArray offers = item.getJSONArray("Offers");
                    locationOffer.setOffers(OfferDetails.GetOfferDetails(offers));
                }
                returnList.add(locationOffer);
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return returnList;
    }
}
