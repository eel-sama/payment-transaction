package com.enrollandpay.serviceinterface.Models.Security;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Credentials implements Serializable
{

    @SerializedName("DeviceId")
    @Expose
    private Integer DeviceId;
    @SerializedName("DeviceName")
    @Expose
    private String DeviceName;
    @SerializedName("LocationId")
    @Expose
    private Integer LocationId;
    @SerializedName("MerchantId")
    @Expose
    private Integer MerchantId;

    public Integer getDeviceId() {        return DeviceId;    }
    public void setDeviceId(Integer deviceId) {        DeviceId = deviceId;    }
    public String getDeviceName() {        return DeviceName;    }
    public void setDeviceName(String deviceName) {        DeviceName = deviceName;    }
    public Integer getLocationId() {        return LocationId;    }
    public void setLocationId(Integer locationId) {        LocationId = locationId;    }
    public Integer getMerchantId() {        return MerchantId;    }
    public void setMerchantId(Integer merchantId) {        MerchantId = merchantId;    }
}