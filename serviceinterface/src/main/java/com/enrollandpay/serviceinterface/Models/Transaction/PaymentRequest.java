package com.enrollandpay.serviceinterface.Models.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentRequest extends Transaction {
    @SerializedName("PosTerminalIdent")
    @Expose
    private String PosTerminalIdent;
    @SerializedName("PaymentDeviceId")
    @Expose
    private Integer PaymentDeviceId;
    @SerializedName("AmountTotal")
    @Expose
    private Double AmountTotal;
    @SerializedName("AmountDue")
    @Expose
    private Double AmountDue;
    @SerializedName("AmountTax")
    @Expose
    private Double AmountTax;
//    @SerializedName("AmountGratuity")
//    @Expose
//    private Double AmountGratuity;
//    @SerializedName("AmountGratuity2")
//    @Expose
//    private Double AmountGratuity2;
//    @SerializedName("AmountGratuity3")
//    @Expose
//    private Double AmountGratuity3;
//    @SerializedName("AmountSurcharge")
//    @Expose
//    private Double AmountSurcharge;
    @SerializedName("PaymentType")
    @Expose
    private Integer PaymentType;
    @SerializedName("CardInformation")
    @Expose
    private CardInformation CardInformation;
    @SerializedName("RequestType")
    @Expose
    private Integer RequestType;
    @SerializedName("OpenTabName")
    @Expose
    private String OpenTabName;
    /////////////////////////////////

    public String getOpenTabName() {        return OpenTabName;    }
    public void setOpenTabName(String openTabName) {        OpenTabName = openTabName;    }
    public Double getAmountDue() {        return AmountDue;    }
    public void setAmountDue(Double amountDue) {        AmountDue = amountDue;    }
    public String getPosTerminalIdent() {        return PosTerminalIdent;    }
    public void setPosTerminalIdent(String posTerminalIdent) {        PosTerminalIdent = posTerminalIdent;    }
    public Integer getPaymentDeviceId() {        return PaymentDeviceId;    }
    public void setPaymentDeviceId(Integer paymentDeviceId) {        PaymentDeviceId = paymentDeviceId;    }
    public Double getAmountTotal() {        return AmountTotal;    }
    public void setAmountTotal(Double amountTotal) {        AmountTotal = amountTotal;    }
    public Double getAmountTax() {        return AmountTax;    }
    public void setAmountTax(Double amountTax) {        AmountTax = amountTax;    }
    public Integer getPaymentType() {        return PaymentType;    }
    public void setPaymentType(Integer paymentType) {        PaymentType = paymentType;    }
    public com.enrollandpay.serviceinterface.Models.Transaction.CardInformation getCardInformation() {        return CardInformation;    }
    public void setCardInformation(com.enrollandpay.serviceinterface.Models.Transaction.CardInformation cardInformation) {        CardInformation = cardInformation;    }
//    public Double getAmountGratuity() {        return AmountGratuity;    }
//    public void setAmountGratuity(Double amountGratuity) {        AmountGratuity = amountGratuity;    }
//    public Double getAmountGratuity2() {        return AmountGratuity2;    }
//    public void setAmountGratuity2(Double amountGratuity2) {        AmountGratuity2 = amountGratuity2;    }
//    public Double getAmountGratuity3() {        return AmountGratuity3;    }
//    public void setAmountGratuity3(Double amountGratuity3) {        AmountGratuity3 = amountGratuity3;    }
//    public Double getAmountSurcharge() {        return AmountSurcharge;    }
//    public void setAmountSurcharge(Double amountSurcharge) {        AmountSurcharge = amountSurcharge;    }
    public Integer getRequestType() {        return RequestType;    }
    public void setRequestType(Integer requestType) {        RequestType = requestType;    }
}
