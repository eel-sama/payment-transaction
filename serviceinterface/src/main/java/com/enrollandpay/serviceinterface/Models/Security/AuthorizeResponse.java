package com.enrollandpay.serviceinterface.Models.Security;

import java.io.Serializable;
import com.enrollandpay.serviceinterface.Models.ResponseBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthorizeResponse extends ResponseBase implements Serializable {
    @SerializedName("DisplayCode")
    @Expose
    private String displayCode;
    @SerializedName("Secret")
    @Expose
    private String secret;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("SentEmail")
    @Expose
    private Boolean sentEmail;

    public String getDisplayCode() {
        return displayCode;
    }

    public void setDisplayCode(String displayCode) {
        this.displayCode = displayCode;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSentEmail() {
        return sentEmail;
    }

    public void setSentEmail(Boolean sentEmail) {
        this.sentEmail = sentEmail;
    }
}