package com.enrollandpay.serviceinterface.Models.Location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class OfferDetails {
    @Expose
    @SerializedName("Description")
    private String Description;
    @Expose
    @SerializedName("OfferId")
    private Integer OfferId;
    @Expose
    @SerializedName("IsFavorite")
    private Boolean IsFavorite;

    public String getDescription() {        return Description;    }
    public void setDescription(String description) {        Description = description;    }
    public Integer getOfferId() {        return OfferId;    }
    public void setOfferId(Integer offerId) {        OfferId = offerId;    }
    public Boolean getIsFavorite() {        return IsFavorite;    }
    public void setIsFavorite(Boolean favorite) {        IsFavorite = favorite;    }
    public static ArrayList<OfferDetails> GetOfferDetails(JSONArray list) {
        ArrayList<OfferDetails> returnList = new ArrayList<>();
        for (int iterO =0;iterO < list.length(); iterO++ ){
            try {
                JSONObject item = list.getJSONObject(iterO);
                OfferDetails offerDetails = new OfferDetails();
                if (item.has("Description")){offerDetails.setDescription(item.getString("Description"));}
                if (item.has("OfferId")){offerDetails.setOfferId(item.getInt("OfferId"));}
                if (item.has("IsFavorite")){offerDetails.setIsFavorite(item.getBoolean("IsFavorite"));}
                returnList.add(offerDetails);
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
        return returnList;
    }
}