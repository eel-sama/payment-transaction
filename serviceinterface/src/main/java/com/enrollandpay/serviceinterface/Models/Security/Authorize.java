package com.enrollandpay.serviceinterface.Models.Security;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Authorize  implements Serializable
{

    @SerializedName("TerminalIdent")
    @Expose
    private String terminalIdent;
    @SerializedName("AccountIdent")
    @Expose
    private String accountIdent;
    @SerializedName("ClientIdent")
    @Expose
    private String clientIdent;

    public String getTerminalIdent() {
        return terminalIdent;
    }

    public void setTerminalIdent(String terminalIdent) {
        this.terminalIdent = terminalIdent;
    }

    public String getAccountIdent() {
        return accountIdent;
    }

    public void setAccountIdent(String accountIdent) {
        this.accountIdent = accountIdent;
    }

    public String getClientIdent() {
        return clientIdent;
    }

    public void setClientIdent(String clientIdent) {
        this.clientIdent = clientIdent;
    }

}