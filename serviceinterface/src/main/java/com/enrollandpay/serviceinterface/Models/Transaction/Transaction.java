package com.enrollandpay.serviceinterface.Models.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transaction {
    @SerializedName("TransactionPaymentIdent")
    @Expose
    private String TransactionPaymentIdent;
    @SerializedName("OrderId")
    @Expose
    private Integer OrderId;
    @SerializedName("OrderPaymentId")
    @Expose
    private Integer OrderPaymentId;
    @SerializedName("LocationId")
    @Expose
    private Integer LocationId;
    @SerializedName("LocationIdent")
    @Expose
    private String LocationIdent;

    @SerializedName("TransactionIdent")
    @Expose
    private String TransactionIdent;

    @SerializedName("Information")
    @Expose
    private TransactionInformation Information;
    @SerializedName("ReturnTransactionIdent")
    @Expose
    private String ReturnTransactionIdent;
    @SerializedName("OriginalOrderId")
    @Expose
    private Integer ReturnOrderId;
    @SerializedName("OverridePaymentDeviceId")
    @Expose
    private Integer OverridePaymentDeviceId;
    @SerializedName("OverridePosDeviceId")
    @Expose
    private Integer OverridePosDeviceId;
    @SerializedName("AmountGratuity")
    @Expose
    private Double AmountGratuity;
    @SerializedName("AmountGratuity2")
    @Expose
    private Double AmountGratuity2;
    @SerializedName("AmountGratuity3")
    @Expose
    private Double AmountGratuity3;
    @SerializedName("AmountSurcharge")
    @Expose
    private Double AmountSurcharge;
    /////////////////////////

    public Integer getReturnOrderId() {        return ReturnOrderId;    }
    public void setReturnOrderId(Integer returnOrderId) {        ReturnOrderId = returnOrderId;    }

    public Double getAmountGratuity() {
        return AmountGratuity;
    }

    public void setAmountGratuity(Double amountGratuity) {
        AmountGratuity = amountGratuity;
    }

    public Double getAmountGratuity2() {
        return AmountGratuity2;
    }

    public void setAmountGratuity2(Double amountGratuity2) {
        AmountGratuity2 = amountGratuity2;
    }

    public Double getAmountGratuity3() {
        return AmountGratuity3;
    }

    public void setAmountGratuity3(Double amountGratuity3) {
        AmountGratuity3 = amountGratuity3;
    }

    public Double getAmountSurcharge() {
        return AmountSurcharge;
    }

    public void setAmountSurcharge(Double amountSurcharge) {
        AmountSurcharge = amountSurcharge;
    }

    public Integer getOverridePaymentDeviceId() {
        return OverridePaymentDeviceId;
    }

    public void setOverridePaymentDeviceId(Integer overridePaymentDeviceId) {
        OverridePaymentDeviceId = overridePaymentDeviceId;
    }

    public Integer getOverridePosDeviceId() {
        return OverridePosDeviceId;
    }

    public void setOverridePosDeviceId(Integer overridePosDeviceId) {
        OverridePosDeviceId = overridePosDeviceId;
    }

    public String getReturnTransactionIdent() {        return ReturnTransactionIdent;    }
    public void setReturnTransactionIdent(String returnTransactionIdent) {        ReturnTransactionIdent = returnTransactionIdent;    }

    public String getTransactionPaymentIdent() {
        return TransactionPaymentIdent;
    }

    public void setTransactionPaymentIdent(String transactionPaymentIdent) {
        this.TransactionPaymentIdent = transactionPaymentIdent;
    }

    public Integer getOrderId() {
        return OrderId;
    }

    public void setOrderId(Integer orderId) {
        this.OrderId = orderId;
    }

    public Integer getOrderPaymentId() {
        return OrderPaymentId;
    }

    public void setOrderPaymentId(Integer orderPaymentId) {
        this.OrderPaymentId = orderPaymentId;
    }

    public Integer getLocationId() {
        return LocationId;
    }

    public void setLocationId(Integer locationId) {
        this.LocationId = locationId;
    }

    public String getLocationIdent() {
        return LocationIdent;
    }

    public void setLocationIdent(String locationIdent) {
        this.LocationIdent = locationIdent;
    }

    public String getTransactionIdent() {
        return TransactionIdent;
    }

    public void setTransactionIdent(String transactionIdent) {
        this.TransactionIdent = transactionIdent;
    }

    public TransactionInformation getInformation() {
        return Information;
    }

    public void setInformation(TransactionInformation information) {
        this.Information = information;
    }
}
