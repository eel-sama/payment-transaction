package com.enrollandpay.serviceinterface.Models.Security;
import java.io.Serializable;
import com.enrollandpay.serviceinterface.Models.ResponseBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyResponse extends ResponseBase implements Serializable
{

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("JWT")
    @Expose
    private String jWT;
    @SerializedName("AuthorizationRequestStatusType")
    @Expose
    private Integer authorizationRequestStatusType;
    @SerializedName("DisplayCode")
    @Expose
    private String displayCode;
    private final static long serialVersionUID = 7446949657798039532L;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getJWT() {
        return jWT;
    }

    public void setJWT(String jWT) {
        this.jWT = jWT;
    }

    public Integer getAuthorizationRequestStatusType() {
        return authorizationRequestStatusType;
    }

    public void setAuthorizationRequestStatusType(Integer authorizationRequestStatusType) {
        this.authorizationRequestStatusType = authorizationRequestStatusType;
    }

    public String getDisplayCode() {
        return displayCode;
    }

    public void setDisplayCode(String displayCode) {
        this.displayCode = displayCode;
    }

}