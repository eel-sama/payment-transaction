package com.enrollandpay.serviceinterface.Models.Security;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Verify  implements Serializable
{

    @SerializedName("Secret")
    @Expose
    private String secret;
    private final static long serialVersionUID = -6973395045516405374L;

    public String getSecret() {
        return secret;
    }
    public void setSecret(String secret) {
        this.secret = secret;
    }
}