package com.enrollandpay.serviceinterface.Models.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class CardInformation {
    @SerializedName("NameOnCard")
    @Expose
    private String NameOnCard;
    @SerializedName("PANFirstSix")
    @Expose
    private Integer PANFirstSix;
    @SerializedName("PANLastFour")
    @Expose
    private Integer PANLastFour;
    @SerializedName("UniqueCardIdent")
    @Expose
    private String UniqueCardIdent;
    @SerializedName("UniqueCardPar")
    @Expose
    private String UniqueCardPar;
    @SerializedName("CardType")
    @Expose
    private Integer CardType;
    @SerializedName("PaymentType")
    @Expose
    private Integer PaymentType;
    @SerializedName("Attributes")
    @Expose
    private Map<String,String> Attributes = new HashMap<String,String>();
    /////////////////////////
    public Map<String, String> getAttributes() {        return Attributes;    }
    public void setAttributes(Map<String, String> attributes) {        Attributes = attributes;    }
    public String getNameOnCard() {        return NameOnCard;    }
    public void setNameOnCard(String nameOnCard) {        NameOnCard = nameOnCard;    }
    public Integer getPANFirstSix() {        return PANFirstSix;    }
    public void setPANFirstSix(Integer PANFirstSix) {        this.PANFirstSix = PANFirstSix;    }
    public Integer getPANLastFour() {        return PANLastFour;    }
    public void setPANLastFour(Integer PANLastFour) {        this.PANLastFour = PANLastFour;    }
    public String getUniqueCardIdent() {        return UniqueCardIdent;    }
    public void setUniqueCardIdent(String uniqueCardIdent) {        UniqueCardIdent = uniqueCardIdent;    }
    public String getUniqueCardPar() {        return UniqueCardPar;    }
    public void setUniqueCardPar(String uniqueCardPar) {        UniqueCardPar = uniqueCardPar;    }
    public Integer getCardType() {        return CardType;    }
    public void setCardType(Integer cardType) {        CardType = cardType;    }
    public Integer getPaymentType() {        return PaymentType;    }
    public void setPaymentType(Integer paymentType) {        PaymentType = paymentType;    }
}
