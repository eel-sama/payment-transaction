package com.enrollandpay.serviceinterface.Models.Employee;

import com.enrollandpay.serviceinterface.Models.ResponseBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class OverviewResponse extends ResponseBase implements Serializable
{

    @SerializedName("Employees")
    @Expose
    private ArrayList<OverviewEmployee> Employees;

    public ArrayList<OverviewEmployee> getEmployees() {        return Employees;    }
    public void setEmployees(ArrayList<OverviewEmployee> employees) {        Employees = employees;    }

    public class OverviewEmployee implements Serializable
    {
        @SerializedName("AmountGratuity")
        @Expose
        private Double AmountGratuity;
        @SerializedName("AmountGratuity2")
        @Expose
        private Double AmountGratuity2;
        @SerializedName("AmountGratuity3")
        @Expose
        private Double AmountGratuity3;
        @SerializedName("AmountTotal")
        @Expose
        private Double AmountTotal;
        @SerializedName("EmployeeId")
        @Expose
        private Integer EmployeeId;
        @SerializedName("NameFirst")
        @Expose
        private String NameFirst;
        @SerializedName("NameLast")
        @Expose
        private String NameLast;
        @SerializedName("TransactionCount")
        @Expose
        private Integer TransactionCount;

        public Double getAmountGratuity() {
            return AmountGratuity;
        }

        public void setAmountGratuity(Double amountGratuity) {
            AmountGratuity = amountGratuity;
        }

        public Double getAmountGratuity2() {
            return AmountGratuity2;
        }

        public void setAmountGratuity2(Double amountGratuity2) {
            AmountGratuity2 = amountGratuity2;
        }

        public Double getAmountGratuity3() {
            return AmountGratuity3;
        }

        public void setAmountGratuity3(Double amountGratuity3) {
            AmountGratuity3 = amountGratuity3;
        }

        public Double getAmountTotal() {
            return AmountTotal;
        }

        public void setAmountTotal(Double amountTotal) {
            AmountTotal = amountTotal;
        }

        public Integer getEmployeeId() {
            return EmployeeId;
        }

        public void setEmployeeId(Integer employeeId) {
            EmployeeId = employeeId;
        }

        public String getNameFirst() {
            return NameFirst;
        }

        public void setNameFirst(String nameFirst) {
            NameFirst = nameFirst;
        }

        public String getNameLast() {
            return NameLast;
        }

        public void setNameLast(String nameLast) {
            NameLast = nameLast;
        }

        public Integer getTransactionCount() {
            return TransactionCount;
        }

        public void setTransactionCount(Integer transactionCount) {
            TransactionCount = transactionCount;
        }
    }

}