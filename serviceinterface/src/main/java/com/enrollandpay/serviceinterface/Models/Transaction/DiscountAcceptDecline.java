package com.enrollandpay.serviceinterface.Models.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DiscountAcceptDecline {
    @SerializedName("TransactionPaymentIdent")
    @Expose
    private String TransactionPaymentIdent;
    @SerializedName("OrderId")
    @Expose
    private Integer OrderId;
    @SerializedName("OrderPaymentId")
    @Expose
    private Integer OrderPaymentId;
    @SerializedName("LocationId")
    @Expose
    private Integer LocationId;
    @SerializedName("LocationIdent")
    @Expose
    private String LocationIdent;

    @SerializedName("TransactionIdent")
    @Expose
    private String TransactionIdent;
    ///////////////////////////////////////////////////////////
    @SerializedName("CommandId")
    @Expose
    private String CommandId;
    @SerializedName("InternalIdent")
    @Expose
    private String InternalIdent;
    @SerializedName("POSIdent")
    @Expose
    private String POSIdent;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Amount")
    @Expose
    private Double Amount;
    @SerializedName("ApplyCount")
    @Expose
    private Integer ApplyCount;
    @SerializedName("Percentage")
    @Expose
    private Double Percentage;
    @SerializedName("ApplyDiscountType")
    @Expose
    private Integer ApplyDiscountType;

    public String getTransactionPaymentIdent() {
        return TransactionPaymentIdent;
    }

    public void setTransactionPaymentIdent(String transactionPaymentIdent) {
        TransactionPaymentIdent = transactionPaymentIdent;
    }

    public Integer getOrderId() {
        return OrderId;
    }

    public void setOrderId(Integer orderId) {
        OrderId = orderId;
    }

    public Integer getOrderPaymentId() {
        return OrderPaymentId;
    }

    public void setOrderPaymentId(Integer orderPaymentId) {
        OrderPaymentId = orderPaymentId;
    }

    public Integer getLocationId() {
        return LocationId;
    }

    public void setLocationId(Integer locationId) {
        LocationId = locationId;
    }

    public String getLocationIdent() {
        return LocationIdent;
    }

    public void setLocationIdent(String locationIdent) {
        LocationIdent = locationIdent;
    }

    public String getTransactionIdent() {
        return TransactionIdent;
    }

    public void setTransactionIdent(String transactionIdent) {
        TransactionIdent = transactionIdent;
    }

    public String getCommandId() {
        return CommandId;
    }

    public void setCommandId(String commandId) {
        CommandId = commandId;
    }

    public String getInternalIdent() {
        return InternalIdent;
    }

    public void setInternalIdent(String internalIdent) {
        InternalIdent = internalIdent;
    }

    public String getPOSIdent() {
        return POSIdent;
    }

    public void setPOSIdent(String POSIdent) {
        this.POSIdent = POSIdent;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Double getAmount() {
        return Amount;
    }

    public void setAmount(Double amount) {
        Amount = amount;
    }

    public Integer getApplyCount() {
        return ApplyCount;
    }

    public void setApplyCount(Integer applyCount) {
        ApplyCount = applyCount;
    }

    public Double getPercentage() {
        return Percentage;
    }

    public void setPercentage(Double percentage) {
        Percentage = percentage;
    }

    public Integer getApplyDiscountType() {
        return ApplyDiscountType;
    }

    public void setApplyDiscountType(Integer applyDiscountType) {
        ApplyDiscountType = applyDiscountType;
    }
}
