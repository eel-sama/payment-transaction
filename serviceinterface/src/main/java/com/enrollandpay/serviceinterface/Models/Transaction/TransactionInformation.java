package com.enrollandpay.serviceinterface.Models.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class TransactionInformation {
    @SerializedName("EmployeeIdent")
    @Expose
    private String EmployeeIdent;
    @SerializedName("PosTerminalIdent")
    @Expose
    private String POSTerminalIdent;
    @SerializedName("EmployeeName")
    @Expose
    private String EmployeeName;
    @SerializedName("EmployeeId")
    @Expose
    private Integer EmployeeId;
    @SerializedName("PaymentDeviceId")
    @Expose
    private Integer PaymentDeviceId;
    @SerializedName("AmountTotal")
    @Expose
    private Double AmountTotal;
    @SerializedName("AmountDue")
    @Expose
    private Double AmountDue;
    @SerializedName("AmountTax")
    @Expose
    private Double AmountTax;
    @SerializedName("AmountGratuity")
    @Expose
    private Double AmountGratuity;
    @SerializedName("AmountTendered")
    @Expose
    private Double AmountTendered;
    @SerializedName("AmountReward")
    @Expose
    private Double AmountReward;
    @SerializedName("AmountDiscount")
    @Expose
    private Double AmountDiscount;
    @SerializedName("TabletopId")
    @Expose
    private Integer TabletopId;
    @SerializedName("AdditionalAttributes")
    @Expose
    private java.util.Map<String, String> AdditionalAttributes;
    @SerializedName("Items")
    @Expose
    private List<TransactionItem> Items = null;
    /////////

    public Integer getEmployeeId() {        return EmployeeId;    }
    public void setEmployeeId(Integer employeeId) {        EmployeeId = employeeId;    }

    public String getEmployeeIdent() {
        return EmployeeIdent;
    }

    public void setEmployeeIdent(String employeeIdent) {
        this.EmployeeIdent = employeeIdent;
    }

    public String getPOSTerminalIdent() {
        return POSTerminalIdent;
    }

    public void setPOSTerminalIdent(String POSTerminalIdent) {
        this.POSTerminalIdent = POSTerminalIdent;
    }

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.EmployeeName = employeeName;
    }

    public Integer getPaymentDeviceId() {
        return PaymentDeviceId;
    }

    public void setPaymentDeviceId(Integer paymentDeviceId) {
        this.PaymentDeviceId = paymentDeviceId;
    }

    public Double getAmountTotal() {
        return AmountTotal;
    }

    public void setAmountTotal(Double amountTotal) {
        this.AmountTotal = amountTotal;
    }

    public Double getAmountDue() {
        return AmountDue;
    }

    public void setAmountDue(Double amountDue) {
        this.AmountDue = amountDue;
    }

    public Double getAmountTax() {
        return AmountTax;
    }

    public void setAmountTax(Double amountTax) {
        this.AmountTax = amountTax;
    }

    public Double getAmountGratuity() {
        return AmountGratuity;
    }

    public void setAmountGratuity(Double amountGratuity) {
        this.AmountGratuity = amountGratuity;
    }

    public Double getAmountTendered() {
        return AmountTendered;
    }

    public void setAmountTendered(Double amountTendered) {
        this.AmountTendered = amountTendered;
    }

    public Double getAmountReward() {
        return AmountReward;
    }

    public void setAmountReward(Double amountReward) {
        this.AmountReward = amountReward;
    }

    public Double getAmountDiscount() {
        return AmountDiscount;
    }

    public void setAmountDiscount(Double amountDiscount) {
        this.AmountDiscount = amountDiscount;
    }

    public Map<String, String> getAdditionalAttributes() {
        return AdditionalAttributes;
    }

    public void setAdditionalAttributes(Map<String, String> additionalAttributes) {
        this.AdditionalAttributes = additionalAttributes;
    }

    public List<TransactionItem> getItems() {
        return Items;
    }

    public void setItems(List<TransactionItem> items) {
        this.Items = items;
    }

    public Integer getTabletopId() {
        return TabletopId;
    }

    public void setTabletopId(Integer tabletopId) {
        TabletopId = tabletopId;
    }
}
