package com.enrollandpay.serviceinterface.Models.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuestionAnswer {
    @SerializedName("OrderReviewQuestionId")
    @Expose
    private Integer OrderReviewQuestionId;
    @SerializedName("QuestionId")
    @Expose
    private Integer QuestionId;
    @SerializedName("ConsumerId")
    @Expose
    private Integer ConsumerId;
    @SerializedName("OrderId")
    @Expose
    private Integer OrderId;
    @SerializedName("Score")
    @Expose
    private Integer Score;
    //////////////////////////

    public Integer getOrderReviewQuestionId() {  return OrderReviewQuestionId;    }
    public void setOrderReviewQuestionId(Integer orderReviewQuestionId) {  OrderReviewQuestionId = orderReviewQuestionId;    }
    public Integer getQuestionId() {  return QuestionId;   }
    public void setQuestionId(Integer questionId) {  QuestionId = questionId;    }
    public Integer getConsumerId() {  return ConsumerId;  }
    public void setConsumerId(Integer consumerId) {  ConsumerId = consumerId;  }
    public Integer getOrderId() {  return OrderId;   }
    public void setOrderId(Integer orderId) { OrderId = orderId;  }
    public Integer getScore() { return Score;  }
    public void setScore(Integer score) { Score = score;  }
}
