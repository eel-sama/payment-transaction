package com.enrollandpay.serviceinterface.Models.Order;

import com.enrollandpay.serviceinterface.BusinessEntity.Order;
import com.enrollandpay.serviceinterface.Models.ResponseBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrderResponse extends ResponseBase implements Serializable {
    @SerializedName("Order")
    @Expose
    private Order order;
    //////
    public Order getOrder() {        return order;    }
    public void setOrder(Order order) {        this.order = order;    }
}
