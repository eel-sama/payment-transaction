package com.enrollandpay.serviceinterface.Models;

import java.io.Serializable;
import com.enrollandpay.serviceinterface.Models.ResponseBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ErrorResponse  extends ResponseBase implements Serializable {
    @SerializedName("ErrorCode")
    @Expose
    private Integer errorCode;
    @SerializedName("ErrorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("ErrorMessageCodes")
    @Expose
    private List<Object> errorMessageCodes = null;
    @SerializedName("InvalidDatas")
    @Expose
    private List<Object> invalidDatas = null;

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<Object> getErrorMessageCodes() {
        return errorMessageCodes;
    }

    public void setErrorMessageCodes(List<Object> errorMessageCodes) {
        this.errorMessageCodes = errorMessageCodes;
    }

    public List<Object> getInvalidDatas() {
        return invalidDatas;
    }

    public void setInvalidDatas(List<Object> invalidDatas) {
        this.invalidDatas = invalidDatas;
    }
}