package com.enrollandpay.serviceinterface.Models.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Enrollment {
    @SerializedName("LoyaltyProgramId")
    @Expose
    private Integer LoyaltyProgramId;
    @SerializedName("TransactionIdent")
    @Expose
    private String TransactionIdent;
    @SerializedName("TransactionPaymentIdent")
    @Expose
    private String TransactionPaymentIdent;
    @SerializedName("OrderId")
    @Expose
    private Integer OrderId;
    @SerializedName("OrderPaymentId")
    @Expose
    private Integer OrderPaymentId;
    @SerializedName("LocationId")
    @Expose
    private Integer LocationId;
    @SerializedName("LocationIdent")
    @Expose
    private String LocationIdent;
    ///////////////////////////////
    public Integer getLoyaltyProgramId() {        return LoyaltyProgramId;    }
    public void setLoyaltyProgramId(Integer loyaltyProgramId) {        LoyaltyProgramId = loyaltyProgramId;    }
    public String getTransactionIdent() {        return TransactionIdent;    }
    public void setTransactionIdent(String transactionIdent) {        TransactionIdent = transactionIdent;    }
    public String getTransactionPaymentIdent() {        return TransactionPaymentIdent;    }
    public void setTransactionPaymentIdent(String transactionPaymentIdent) {        TransactionPaymentIdent = transactionPaymentIdent;    }
    public Integer getOrderId() {        return OrderId;    }
    public void setOrderId(Integer orderId) {        OrderId = orderId;    }
    public Integer getOrderPaymentId() {        return OrderPaymentId;    }
    public void setOrderPaymentId(Integer orderPaymentId) {        OrderPaymentId = orderPaymentId;    }
    public Integer getLocationId() {        return LocationId;    }
    public void setLocationId(Integer locationId) {        LocationId = locationId;    }
    public String getLocationIdent() {        return LocationIdent;    }
    public void setLocationIdent(String locationIdent) {        LocationIdent = locationIdent;    }
}
