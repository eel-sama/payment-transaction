package com.enrollandpay.serviceinterface.Models.Order;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BatchStats {
    @SerializedName("BatchIdent")
    @Expose
    private String BatchIdent;
    @SerializedName("AutoComplete")
    @Expose
    private Boolean AutoComplete;

    public String getBatchIdent() {
        return BatchIdent;
    }

    public void setBatchIdent(String batchIdent) {
        BatchIdent = batchIdent;
    }

    public Boolean getAutoComplete() {
        return AutoComplete;
    }

    public void setAutoComplete(Boolean autoComplete) {
        AutoComplete = autoComplete;
    }
}