package com.enrollandpay.serviceinterface.Models.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentAdjusted  extends Transaction {
    @Expose
    @SerializedName("ReferenceIdent")
    private String ReferenceIdent;
    @Expose
    @SerializedName("ApprovalIdent")
    private String ApprovalIdent;
    @Expose
    @SerializedName("VoidIdent")
    private String VoidIdent;
    @Expose
    @SerializedName("Reason")
    private String Reason;
    @Expose
    @SerializedName("Signature")
    private String Signature;
    ////////////
    public String getSignature() {        return Signature;    }
    public void setSignature(String signature) {        Signature = signature;    }
    public String getReferenceIdent() {        return ReferenceIdent;    }
    public void setReferenceIdent(String referenceIdent) {        ReferenceIdent = referenceIdent;    }
    public String getApprovalIdent() {        return ApprovalIdent;    }
    public void setApprovalIdent(String approvalIdent) {        ApprovalIdent = approvalIdent;    }
    public String getVoidIdent() {        return VoidIdent;    }
    public void setVoidIdent(String voidIdent) {        VoidIdent = voidIdent;    }
    public String getReason() {        return Reason;    }
    public void setReason(String reason) {        Reason = reason;    }
}