package com.enrollandpay.serviceinterface.Models.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EnrollmentAccept extends Enrollment{
    @SerializedName("MemberName")
    @Expose
    private String MemberName;
    @SerializedName("PhoneNumber")
    @Expose
    private String PhoneNumber;
    @SerializedName("EmailAddress")
    @Expose
    private String EmailAddress;
    ///////////////////////////////
    public String getMemberName() {        return MemberName;    }
    public void setMemberName(String memberName) {        MemberName = memberName;    }
    public String getPhoneNumber() {        return PhoneNumber;    }
    public void setPhoneNumber(String phoneNumber) {        PhoneNumber = phoneNumber;    }
    public String getEmailAddress() {        return EmailAddress;    }
    public void setEmailAddress(String emailAddress) {        EmailAddress = emailAddress;    }
}
