package com.enrollandpay.serviceinterface.Models.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Error {
    @SerializedName("debugInfo")
    @Expose
    private String DebugInfo;
    public String getDebugInfo() {        return DebugInfo;    }
    public void setDebugInfo(String debugInfo) {        DebugInfo = debugInfo;    }
}
