package com.enrollandpay.serviceinterface.Models.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class TransactionItem {
    @SerializedName("TransactionItemIdent")
    @Expose
    private String TransactionItemIdent;
    @SerializedName("ParentTransactionItemIdent")
    @Expose
    private String ParentTransactionItemIdent;
    @SerializedName("OrderItemId")
    @Expose
    private Integer OrderItemId;
    @SerializedName("ProductId")
    @Expose
    private Integer ProductId;
    @SerializedName("ProductIdent")
    @Expose
    private String ProductIdent;
    @SerializedName("ProductName")
    @Expose
    private String ProductName;
    @SerializedName("Quantity")
    @Expose
    private Double Quantity;
    @SerializedName("AmountTotal")
    @Expose
    private Double AmountTotal;
    @SerializedName("AmountTax")
    @Expose
    private Double AmountTax;
    ///////////////////////////
    @SerializedName("AdditionalAttributes")
    @Expose
    private java.util.Map<String, String> AdditionalAttributes;

    public String getParentTransactionItemIdent() {        return ParentTransactionItemIdent;    }
    public void setParentTransactionItemIdent(String parentTransactionItemIdent) {        ParentTransactionItemIdent = parentTransactionItemIdent;    }
    public Integer getProductId() {        return ProductId;    }
    public void setProductId(Integer productId) {        ProductId = productId;    }
    public String getTransactionItemIdent() {
        return TransactionItemIdent;
    }
    public void setTransactionItemIdent(String transactionItemIdent) {        this.TransactionItemIdent = transactionItemIdent;    }
    public Integer getOrderItemId() {
        return OrderItemId;
    }
    public void setOrderItemId(Integer orderItemId) {
        this.OrderItemId = orderItemId;
    }
    public String getProductIdent() {
        return ProductIdent;
    }
    public void setProductIdent(String productIdent) {
        this.ProductIdent = productIdent;
    }
    public String getProductName() {
        return ProductName;
    }
    public void setProductName(String productName) {
        this.ProductName = productName;
    }
    public Double getQuantity() {
        return Quantity;
    }
    public void setQuantity(Double quantity) {
        this.Quantity = quantity;
    }
    public Double getAmountTotal() {
        return AmountTotal;
    }
    public void setAmountTotal(Double amountTotal) {
        this.AmountTotal = amountTotal;
    }
    public Double getAmountTax() {
        return AmountTax;
    }
    public void setAmountTax(Double amountTax) {                  this.AmountTax = amountTax;    }
    public Map<String, String> getAdditionalAttributes() {        return AdditionalAttributes;    }
    public void setAdditionalAttributes(Map<String, String> additionalAttributes) {        AdditionalAttributes = additionalAttributes;    }
}
