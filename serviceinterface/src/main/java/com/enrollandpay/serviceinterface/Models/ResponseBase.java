package com.enrollandpay.serviceinterface.Models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  abstract class ResponseBase implements Serializable
{
    @SerializedName("ResponseType")
    @Expose
    private Integer responseType;
    @SerializedName("ResponseMessages")
    @Expose
    private List<String> responseMessages = null;
    @SerializedName("Errors")
    @Expose
    private java.util.Map<String, String> errors;
    public Integer getResponseType() {
        return responseType;
    }

    public void setResponseType(Integer responseType) {
        this.responseType = responseType;
    }

    public List<String> getResponseMessages() {
        return responseMessages;
    }

    public void setResponseMessages(List<String> responseMessages) {
        this.responseMessages = responseMessages;
    }

    public java.util.Map<String, String> getErrors() {
        return errors;
    }

    public void setErrors(java.util.Map<String, String> errors) {
        this.errors = errors;
    }
}