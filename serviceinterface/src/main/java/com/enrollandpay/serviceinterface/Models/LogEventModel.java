package com.enrollandpay.serviceinterface.Models;

public class LogEventModel {

    String requestType;
    String requestUrl;
    String requestBody;
    String responseBody;

    public LogEventModel() {
    }

    public LogEventModel(String requestType, String requestUrl, String requestBody, String responseBody) {
        this.requestType = requestType;
        this.requestUrl = requestUrl;
        this.requestBody = requestBody;
        this.responseBody = responseBody;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }
}
