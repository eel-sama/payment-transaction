package com.enrollandpay.serviceinterface.Models.Security;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AuthorizationRequest  implements Serializable {
    @SerializedName("Secret")
    @Expose
    private String secret;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
