package com.enrollandpay.serviceinterface.Models.Transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardPresented extends Transaction {
    @SerializedName("NameOnCard")
    @Expose
    private String NameOnCard;
    @SerializedName("PANFirstSix")
    @Expose
    private Integer PANFirstSix;
    @SerializedName("PANLastFour")
    @Expose
    private Integer PANLastFour;
    @SerializedName("UniqueCardIdent")
    @Expose
    private String UniqueCardIdent;
    @SerializedName("PaymentToken")
    @Expose
    private String PaymentToken;
    @SerializedName("CardType")
    @Expose
    private Integer CardType;
    @SerializedName("BatchIdent")
    @Expose
    private String BatchIdent;
    @SerializedName("RequestType")
    @Expose
    private Integer RequestType;
    @SerializedName("OpenTabName")
    @Expose
    private String OpenTabName;
    ////////////////

    public String getBatchIdent() {        return BatchIdent;    }
    public void setBatchIdent(String batchIdent) {        BatchIdent = batchIdent;    }
    public String getNameOnCard() {
        return NameOnCard;
    }
    public void setNameOnCard(String nameOnCard) {
        NameOnCard = nameOnCard;
    }
    public Integer getPANFirstSix() {
        return PANFirstSix;
    }
    public void setPANFirstSix(Integer PANFirstSix) {
        this.PANFirstSix = PANFirstSix;
    }
    public Integer getPANLastFour() {
        return PANLastFour;
    }
    public void setPANLastFour(Integer PANLastFour) {
        this.PANLastFour = PANLastFour;
    }
    public String getUniqueCardIdent() {
        return UniqueCardIdent;
    }
    public void setUniqueCardIdent(String uniqueCardIdent) {
        UniqueCardIdent = uniqueCardIdent;
    }
    public String getPaymentToken() {
        return PaymentToken;
    }
    public void setPaymentToken(String paymentToken) {
        PaymentToken = paymentToken;
    }
    public Integer getCardType() {
        return CardType;
    }
    public void setCardType(Integer cardType) {
        CardType = cardType;
    }
    public Integer getRequestType() {        return RequestType;    }
    public void setRequestType(Integer requestType) {        RequestType = requestType;    }
    public String getOpenTabName() {        return OpenTabName;    }
    public void setOpenTabName(String openTabName) {        OpenTabName = openTabName;    }
}