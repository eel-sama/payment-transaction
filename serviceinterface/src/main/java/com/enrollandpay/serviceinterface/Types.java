package com.enrollandpay.serviceinterface;

public class Types {
    public enum CardTypes
    {
        Amex(1),Visa(2),Mastercard(3),Discover(4),
        Electron(5),
        Maestro(6),
        Dankort(7),
        Interpayment(8),
        Unionpay(9),
        Diners(10),
        JCB(11),
        Gift(12),
        Unknown(13);
        private final Integer  CardTypeId;
        CardTypes(int cardTypeId) {
            this.CardTypeId = cardTypeId;
        }
        public static CardTypes fromInteger(Integer x) {
            if (x == null){return  null;}
            switch(x) {
                case 1:
                    return Amex;
                case 2:
                    return Visa;
                case 3:
                    return Mastercard;
                case 4:
                    return Discover;
                case 5:
                    return Electron;
                case 6:
                    return Maestro;
                case 7:
                    return Dankort;
                case 8:
                    return Interpayment;
                case 9:
                    return Unionpay;
                case 10:
                    return Diners;
                case 11:
                    return JCB;
                case 12:
                    return Gift;
                default:
                    return Unknown;
            }
        }
        public Integer getValue(){
            return CardTypeId;
        }
    }
    public enum DecisionTypes
    {
        Approved(1),Declined(2),Partial(3),Auth(4),Token(5);
        private final Integer  DecisionTypeId;
        DecisionTypes(Integer decisionTypeId) {
            this.DecisionTypeId = decisionTypeId;
        }
        public Integer getValue(){
            return  DecisionTypeId;
        }
        public static DecisionTypes fromInteger(Integer x) {
            if (x == null){return  null;}
            switch(x) {
                case 1:
                    return Approved;
                case 2:
                    return Declined;
                case 3:
                    return Partial;
                case 5:
                    return Token;
                case 4:
                    return Auth;
            }
            return null;
        }
    }
    public enum EnrollmentMethodTypes
    {
        EmailAddress(1), PhoneNumber(2);
        private final Integer  EnrollmentMethodTypeId;
        EnrollmentMethodTypes(Integer enrollmentMethodTypeId) {
            this.EnrollmentMethodTypeId = enrollmentMethodTypeId;
        }
        public Integer getValue(){
            return  EnrollmentMethodTypeId;
        }
        public static EnrollmentMethodTypes fromInteger(Integer x) {
            if (x == null){return  null;}
            switch(x) {
                case 1:
                    return EmailAddress;
                case 2:
                    return PhoneNumber;
            }
            return null;
        }
    }
    public enum TransactionCommandRecipientTypes
    {
        POS(1), PaymentTerminal(2), Middleware(4);
        private final Integer  TransactionCommandRecipientTypeId;
        TransactionCommandRecipientTypes(Integer transactionCommandRecipientTypeId) {
            this.TransactionCommandRecipientTypeId = transactionCommandRecipientTypeId;
        }
        public Integer getValue(){
            return  TransactionCommandRecipientTypeId;
        }
        public static TransactionCommandRecipientTypes fromInteger(Integer x) {
            if (x == null){return  null;}
            switch(x) {
                case 1:
                    return POS;
                case 2:
                    return PaymentTerminal;
                case 4:
                    return Middleware;
            }
            return null;
        }
    }
    public enum PaymentTypes
    {
        Credit(1), Cash(2), Chip(4), Swipe(8), Contactless(16), ChargeAccount(32), Debit(64);
        private final Integer  PaymentTypeId;
        PaymentTypes(Integer paymentTypeId) {
            this.PaymentTypeId = paymentTypeId;
        }
        public Integer getValue(){
            return  PaymentTypeId;
        }
    }
    public enum QuestionTypes{
        Mandatory(1),
        Important(2),
        FiveStar(4),
        Thumb(8);
        private final Integer QuestionType;
        QuestionTypes(Integer questionType){this.QuestionType = questionType;}
        public Integer getValue(){return QuestionType;}
    }
    public enum OrderStatuses{
        Open(1),
        Canceled(2),
        Completed(4),
        Voided(8);
        private final Integer OrderStatus;
        OrderStatuses(Integer orderStatus){this.OrderStatus = orderStatus;}
        public Integer getValue(){return OrderStatus;}
    }
    public enum PaymentStatusTypes
    {
        Canceled(1),
        CardPresented(2),
        PaymentRequested(4),
        PresentedEnrollment(8),
        WaitingEnrollmentDecision(16),
        AcceptedEnrollment(32),
        DeclinedEnrollment(64),
        AlreadyMember(128),
        PresentedSelectOffer(256),
        CompletedSelectOffer(512),
        PresentedRecap(1024),
        RequestedAuthorization(2048),
        CompletedAuthorization(4096),
        Complete(8192),
        ApplyFailedToPOS(16384),
        TokenInPaymentDecision(32768),
        PaymentAdjustmentRequested(65536),
        ApplyingDiscount(131072),
        Batched(262144),
        VoidPending(524288),
        CancelPending(1048576),
        Voided(2097152),
        OpenTab(4194304);

        private final Integer  paymentStatusTypeId;

        PaymentStatusTypes(int paymentStatusType) {
            paymentStatusTypeId = paymentStatusType;
        }

        public Integer getValue(){
            return  paymentStatusTypeId;
        }

        public static PaymentStatusTypes fromInteger(Integer value) {
            if(value == null) return null;

            switch (value) {
                case 1:
                    return Canceled;
                case 2:
                    return CardPresented;
                case 4:
                    return PaymentRequested;
                case 8:
                    return PresentedEnrollment;
                case 16:
                    return WaitingEnrollmentDecision;
                case 32:
                    return AcceptedEnrollment;
                case 64:
                    return DeclinedEnrollment;
                case 128:
                    return AlreadyMember;
                case 256:
                    return PresentedSelectOffer;
                case 512:
                    return CompletedSelectOffer;
                case 1024:
                    return PresentedRecap;
                case 2048:
                    return RequestedAuthorization;
                case 4096:
                    return CompletedAuthorization;
                case 8192:
                    return Complete;
                case 16384:
                    return ApplyFailedToPOS;
                case 32768:
                    return TokenInPaymentDecision;
                case 65536:
                    return PaymentAdjustmentRequested;
                case 131072:
                    return ApplyingDiscount;
                case 262144:
                    return Batched;
                case 524288:
                    return VoidPending;
                case 1048576:
                    return CancelPending;
                case 2097152:
                    return Voided;
                case 4194304:
                    return OpenTab;
            }
            return null;
        }
    }

    public enum RequestTypeEnum {

        Error(1),
        Authorize(2),
        Verify(3),
        TransactionResponse(4),
        DeviceManage(5),
        Enrollment(6),
        EnrolledMerchant(7),
        Login(8),
        ApplicationProfile(9),
        Offers(10),
        DailyOverviews(11),
        Consumers(12),
        Consumer(13),
        Devices(14),
        DeviceInformation(15),
        TransactionRewardOffers(16),
        TransactionOverview(17),
        OfferInformation(18),
        OrderInformation(19),
        Orders(20),
        Products(21),
        ProductInformation(22),
        ProductRelationships(23),
        ProductStatistics(24),
        ProductManage(25),
        OrderReviewInformation(26),
        OrderReviews(27),
        OfferManage(28),
        DailyOfferOverviews(29),
        Locations(30),
        LocationInformation(31),
        LocationManage(32),
        Employees(33),
        EmployeeInformation(34),
        EmployeeManage(35),
        LoyaltyPrograms(36),
        LoyaltyProgramInformation(37),
        ChangePassword(38),
        LoyaltyProgramManage(39),
        QuestionManage(40),
        QuestionInformation(41),
        Questions(42),
        MerchantManage(43),
        EnrollmentCreated(44),
        ForgotPassword(45),
        ConfirmPasswordReset(46),
        Representatives(47),
        RepresentativeManage(48),
        RepresentativeInformation(49),
        MerchantInformation(50),
        Merchants(51),
        Teams(52),
        TeamManage(53),
        TeamInformation(54),
        MerchantAccesses(55),
        MerchantAccessesManage(56),
        Coalitions(57),
        CoalitionManage(58),
        CoalitionInformation(59),
        BillingCycleInformation(60),
        BillingCycles(61),
        InvoiceInformation(62),
        Invoices(63),
        InvoiceItems(64),
        OfferDetails(65),
        MessageThreads(66),
        MessageThreadInformation(67),
        Messages(68),
        PortalConfiguration(69),
        MessageManage(70),
        PhoneNumbers(71),
        ConsumerManage(72),
        MessageTemplates(73),
        MessageTemplateGenerate(74),
        AuthorizeConsumer(75),
        ConsumerLoyaltyPrograms(76),
        LocationOffers(77),
        OrderPayments(78);

        private int  requestType;
        RequestTypeEnum(int value) {
            this.requestType = requestType;
        }

        public static RequestTypeEnum fromInteger(int x) {
            switch(x) {
                case 1:
                    return Error;
                case 2:
                    return Authorize;
                case 3:
                    return Verify;
                case 4:
                    return TransactionResponse;
                case 5:
                    return DeviceManage;
                case 6:
                    return Enrollment;
                case 7:
                    return EnrolledMerchant;
                case 8:
                    return Login;
                case 19:
                    return OrderInformation;
            }
            return null;
        }
    }
}