package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PromotedOffer {
    @SerializedName("OfferId")
    @Expose
    private Integer OfferId;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Display")
    @Expose
    private String Display;
}