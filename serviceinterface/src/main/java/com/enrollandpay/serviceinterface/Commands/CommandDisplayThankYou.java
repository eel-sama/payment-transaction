package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CommandDisplayThankYou extends Command {
    @SerializedName("AmountTotal")
    @Expose
    private Double AmountTotal;
    @SerializedName("AmountGratuity")
    @Expose
    private Double AmountGratuity;
    @SerializedName("AmountReward")
    @Expose
    private Double AmountReward;
    @SerializedName("AmountDiscount")
    @Expose
    private Double AmountDiscount;
    @SerializedName("Items")
    @Expose
    private List<DisplayThankYouItem> Items = null;
    @SerializedName("Location")
    @Expose
    private TransactionOverviewLocation Location;
    @SerializedName("OrderDate")
    @Expose
    private String OrderDate;
    @SerializedName("OrderStatus")
    @Expose
    private Integer OrderStatus;
    @SerializedName("Recipients")
    @Expose
    private List<TransactionOverviewRecipient> Recipients = new ArrayList<>();
    ////////////////////////////////////////////////////


    public Double getAmountTotal() {        return AmountTotal;    }
    public void setAmountTotal(Double amountTotal) {        AmountTotal = amountTotal;    }
    public Double getAmountReward() {        return AmountReward;    }
    public void setAmountReward(Double amountReward) {        AmountReward = amountReward;    }
    public Double getAmountGratuity() {
        return AmountGratuity;
    }
    public void setAmountGratuity(Double amountGratuity) {
        AmountGratuity = amountGratuity;
    }
    public Double getAmountDiscount() {
        return AmountDiscount;
    }
    public void setAmountDiscount(Double amountDiscount) {
        AmountDiscount = amountDiscount;
    }
    public List<DisplayThankYouItem> getItems() {
        return Items;
    }
    public void setItems(List<DisplayThankYouItem> items) {
        Items = items;
    }
    public TransactionOverviewLocation getLocation() {
        return Location;
    }
    public void setLocation(TransactionOverviewLocation location) {
        Location = location;
    }
    public String getOrderDate() {
        return OrderDate;
    }
    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }
    public Integer getOrderStatus() {        return OrderStatus;    }
    public void setOrderStatus(Integer orderStatus) {        OrderStatus = orderStatus;    }
    public List<TransactionOverviewRecipient> getRecipients() {
        return Recipients;
    }
    public void setRecipients(List<TransactionOverviewRecipient> recipients) { Recipients = recipients;    }

    private transient Boolean hasAddedTip = false;
}
