package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommandPaymentCancel extends CommandDisplayTransaction {
    @SerializedName("MemberName")
    @Expose
    private String MemberName;
    @SerializedName("ConsumerId")
    @Expose
    private Integer ConsumerId;
    @SerializedName("AmountTendered")
    @Expose
    private Double AmountTendered;
//    @SerializedName("AmountTotal")
//    @Expose
//    private Double AmountTotal;
//    @SerializedName("AmountTax")
//    @Expose
//    private Double AmountTax;
    @SerializedName("AmountReward")
    @Expose
    private Double AmountReward;
    @SerializedName("AmountDiscount")
    @Expose
    private Double AmountDiscount;
    @SerializedName("AmountSaved")
    @Expose
    private Double AmountSaved;
    @SerializedName("ReferenceIdent")
    @Expose
    private String ReferenceIdent;
    @SerializedName("ApprovalIdent")
    @Expose
    private String ApprovalIdent;
    @SerializedName("VoidIdent")
    @Expose
    private String VoidIdent;
    @SerializedName("Reason")
    @Expose
    private String Reason;
    @SerializedName("CardEncryptedIdent")
    @Expose
    private String CardEncryptedIdent;
    @SerializedName("ApprovalType")
    @Expose
    private Integer ApprovalType;
//    @SerializedName("AmountGratuity")
//    @Expose
//    private Double AmountGratuity;
//    @SerializedName("AmountGratuity2")
//    @Expose
//    private Double AmountGratuity2;
//    @SerializedName("AmountGratuity3")
//    @Expose
//    private Double AmountGratuity3;
//    @SerializedName("AmountSurcharge")
//    @Expose
//    private Double AmountSurcharge;
    /////////////////////////////////////////////
    public String getMemberName() {        return MemberName;    }
    public void setMemberName(String memberName) {        MemberName = memberName;    }
    public Integer getConsumerId() {        return ConsumerId;    }
    public void setConsumerId(Integer consumerId) {        ConsumerId = consumerId;    }
    public Double getAmountTendered() {        return AmountTendered;    }
    public void setAmountTendered(Double amountTendered) {        AmountTendered = amountTendered;    }
//    public Double getAmountTotal() {        return AmountTotal;    }
//    public void setAmountTotal(Double amountTotal) {        AmountTotal = amountTotal;    }
    public Double getAmountReward() {        return AmountReward;    }
    public void setAmountReward(Double amountReward) {        AmountReward = amountReward;    }
    public Double getAmountDiscount() {        return AmountDiscount;    }
    public void setAmountDiscount(Double amountDiscount) {        AmountDiscount = amountDiscount;    }
    public Double getAmountSaved() {        return AmountSaved;    }
    public void setAmountSaved(Double amountSaved) {        AmountSaved = amountSaved;    }
    public String getReferenceIdent() {        return ReferenceIdent;    }
    public void setReferenceIdent(String referenceIdent) {        ReferenceIdent = referenceIdent;    }
    public String getApprovalIdent() {        return ApprovalIdent;    }
    public void setApprovalIdent(String approvalIdent) {        ApprovalIdent = approvalIdent;    }
    public String getVoidIdent() {        return VoidIdent;    }
    public void setVoidIdent(String voidIdent) {        VoidIdent = voidIdent;    }
    public String getReason() {        return Reason;    }
    public void setReason(String reason) {        Reason = reason;    }
    public String getCardEncryptedIdent() {        return CardEncryptedIdent;    }
    public void setCardEncryptedIdent(String cardEncryptedIdent) {        CardEncryptedIdent = cardEncryptedIdent;    }
    public Integer getApprovalType() {        return ApprovalType;    }
    public void setApprovalType(Integer approvalType) {        ApprovalType = approvalType;    }
//    public Double getAmountGratuity() {        return AmountGratuity;    }
//    public void setAmountGratuity(Double amountGratuity) {        AmountGratuity = amountGratuity;    }
//    public Double getAmountGratuity2() {        return AmountGratuity2;    }
//    public void setAmountGratuity2(Double amountGratuity2) {        AmountGratuity2 = amountGratuity2;    }
//    public Double getAmountGratuity3() {        return AmountGratuity3;    }
//    public void setAmountGratuity3(Double amountGratuity3) {        AmountGratuity3 = amountGratuity3;    }
//    public Double getAmountSurcharge() {        return AmountSurcharge;    }
//    public void setAmountSurcharge(Double amountSurcharge) {        AmountSurcharge = amountSurcharge;    }
}
