package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class TransactionOverviewPayment {
    @SerializedName("AmountDiscount")    @Expose
    private Double AmountDiscount ;
    @SerializedName("AmountGratuity")    @Expose
    private Double AmountGratuity ;
    @SerializedName("AmountGratuity2")    @Expose
    private Double AmountGratuity2 ;
    @SerializedName("AmountGratuity3")    @Expose
    private Double AmountGratuity3 ;
    @SerializedName("AmountReward")    @Expose
    private Double AmountReward ;
    @SerializedName("AmountSurcharge")    @Expose
    private Double AmountSurcharge ;
    @SerializedName("AmountTax")    @Expose
    private Double AmountTax ;
    @SerializedName("AmountTendered")    @Expose
    private Double AmountTendered ;
    ///<summary></summary>
    @SerializedName("ReferenceIdent")    @Expose
    private String ReferenceIdent ;
    ///<summary></summary>
    @SerializedName("ApprovalIdent")    @Expose
    private String ApprovalIdent ;
    ///<summary></summary>
    @SerializedName("VoidIdent")    @Expose
    private String VoidIdent ;
    ///<summary></summary>
    @SerializedName("CardType")    @Expose
    private Integer CardType ;
    @SerializedName("CardMasked")    @Expose
    private String CardMasked ;
    @SerializedName("CardConsumerApprovalIdent")    @Expose
    private String CardConsumerApprovalIdent ;
    @SerializedName("NameOnCard")    @Expose
    private String NameOnCard ;
    @SerializedName("AdditionalAttributes")    @Expose
    private Map<String, Object> AdditionalAttributes ;
    @SerializedName("Signature")    @Expose
    private String Signature;
    @SerializedName("PaymentType")    @Expose
    private Integer PaymentType;

    public Integer getPaymentType() {        return PaymentType;    }
    public void setPaymentType(Integer paymentType) {        PaymentType = paymentType;    }

    public String getSignature() {        return Signature;    }

    public void setSignature(String signature) {        Signature = signature;    }

    public Double getAmountDiscount() {
        return AmountDiscount;
    }

    public void setAmountDiscount(Double amountDiscount) {
        AmountDiscount = amountDiscount;
    }

    public Double getAmountGratuity() {
        return AmountGratuity;
    }

    public void setAmountGratuity(Double amountGratuity) {
        AmountGratuity = amountGratuity;
    }

    public Double getAmountGratuity2() {
        return AmountGratuity2 != null ? AmountGratuity2 : 0.0;
    }

    public void setAmountGratuity2(Double amountGratuity2) {
        AmountGratuity2 = amountGratuity2;
    }

    public Double getAmountGratuity3() {
        return AmountGratuity3 != null ? AmountGratuity3 : 0.0;
    }

    public void setAmountGratuity3(Double amountGratuity3) {
        AmountGratuity3 = amountGratuity3;
    }

    public Double getAmountReward() {
        return AmountReward;
    }

    public void setAmountReward(Double amountReward) {
        AmountReward = amountReward;
    }

    public Double getAmountSurcharge() {
        return AmountSurcharge;
    }

    public void setAmountSurcharge(Double amountSurcharge) {
        AmountSurcharge = amountSurcharge;
    }

    public Double getAmountTax() {
        return AmountTax;
    }

    public void setAmountTax(Double amountTax) {
        AmountTax = amountTax;
    }

    public Double getAmountTendered() {
        return AmountTendered;
    }

    public void setAmountTendered(Double amountTendered) {
        AmountTendered = amountTendered;
    }

    public String getReferenceIdent() {
        return ReferenceIdent;
    }

    public void setReferenceIdent(String referenceIdent) {
        ReferenceIdent = referenceIdent;
    }

    public String getApprovalIdent() {
        return ApprovalIdent;
    }

    public void setApprovalIdent(String approvalIdent) {
        ApprovalIdent = approvalIdent;
    }

    public String getVoidIdent() {
        return VoidIdent;
    }

    public void setVoidIdent(String voidIdent) {
        VoidIdent = voidIdent;
    }

    public Integer getCardType() {
        return CardType;
    }

    public void setCardType(Integer cardType) {
        CardType = cardType;
    }

    public String getCardMasked() {
        return CardMasked;
    }

    public void setCardMasked(String cardMasked) {
        CardMasked = cardMasked;
    }

    public String getCardConsumerApprovalIdent() {
        return CardConsumerApprovalIdent;
    }

    public void setCardConsumerApprovalIdent(String cardConsumerApprovalIdent) {
        CardConsumerApprovalIdent = cardConsumerApprovalIdent;
    }

    public String getNameOnCard() {
        return NameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        NameOnCard = nameOnCard;
    }

    public Map<String, Object> getAdditionalAttributes() {
        return AdditionalAttributes;
    }

    public void setAdditionalAttributes(Map<String, Object> additionalAttributes) {
        AdditionalAttributes = additionalAttributes;
    }
}
