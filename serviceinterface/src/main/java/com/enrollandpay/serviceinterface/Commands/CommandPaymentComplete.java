package com.enrollandpay.serviceinterface.Commands;

import com.enrollandpay.serviceinterface.Models.Transaction.TransactionItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommandPaymentComplete extends Command {
    @SerializedName("MemberName")
    @Expose
    private String MemberName;
    @SerializedName("ConsumerId")
    @Expose
    private Integer ConsumerId ;
    @SerializedName("AmountTendered")
    @Expose
    private Double AmountTendered ;
    @SerializedName("AmountTotal")
    @Expose
    private Double AmountTotal ;
    @SerializedName("AmountDue")
    @Expose
    private Double AmountDue ;
    @SerializedName("AmountTax")
    @Expose
    private Double AmountTax ;
    @SerializedName("AmountReward")
    @Expose
    private Double AmountReward ;
    @SerializedName("AmountDiscount")
    @Expose
    private Double AmountDiscount ;
    @SerializedName("AmountGratuity")
    @Expose
    private Double AmountGratuity ;
    @SerializedName("AmountGratuity2")
    @Expose
    private Double AmountGratuity2 ;
    @SerializedName("AmountGratuity3")
    @Expose
    private Double AmountGratuity3 ;
    @SerializedName("AmountSaved")
    @Expose
    private Double AmountSaved ;
    @SerializedName("AmountSurcharge")
    @Expose
    private Double AmountSurcharge ;
    @SerializedName("ReferenceIdent")
    @Expose
    private String ReferenceIdent ;
    @SerializedName("ApprovalIdent")
    @Expose
    private String ApprovalIdent ;
    @SerializedName("VoidIdent")
    @Expose
    private String VoidIdent ;
    @SerializedName("BatchIdent")
    @Expose
    private String BatchIdent;
    @SerializedName("Reason")
    @Expose
    private String Reason ;
    @SerializedName("ApprovalType")
    @Expose
    private Integer ApprovalType ;
    @SerializedName("CardEncryptedIdent")
    @Expose
    private String CardEncryptedIdent ;
    @SerializedName("NameOnCard")
    @Expose
    private String NameOnCard ;
    @SerializedName("PANFirstSix")
    @Expose
    private Integer PANFirstSix ;
    @SerializedName("PANLastFour")
    @Expose
    private Integer PANLastFour ;
    @SerializedName("CardPAR")
    @Expose
    private String CardPAR ;
    @SerializedName("CardType")
    @Expose
    private Integer CardType ;
    @SerializedName("PaymentStatusType")
    @Expose
    private Integer PaymentStatusType ;
    @SerializedName("Signature")
    @Expose
    private String Signature;
    @SerializedName("Items")
    @Expose
    private List<TransactionItem> Items = null;
    ///////////////////////////////////////////////////////

    public List<TransactionItem> getItems() {        return Items;    }
    public void setItems(List<TransactionItem> items) {        Items = items;    }

    public String getSignature() {        return Signature;    }
    public void setSignature(String signature) {        Signature = signature;    }
    public String getMemberName() {        return MemberName;    }
    public void setMemberName(String memberName) {        MemberName = memberName;    }
    public Integer getConsumerId() {        return ConsumerId;    }
    public void setConsumerId(Integer consumerId) {        ConsumerId = consumerId;    }
    public Double getAmountTendered() {        return AmountTendered;    }
    public void setAmountTendered(Double amountTendered) {        AmountTendered = amountTendered;    }
    public Double getAmountTotal() {        return AmountTotal;    }
    public void setAmountTotal(Double amountTotal) {        AmountTotal = amountTotal;    }
    public Double getAmountDue() {        return AmountDue;    }
    public void setAmountDue(Double amountDue) {        AmountDue = amountDue;    }
    public Double getAmountTax() {        return AmountTax;    }
    public void setAmountTax(Double amountTax) {        AmountTax = amountTax;    }
    public Double getAmountReward() {        return AmountReward;    }
    public void setAmountReward(Double amountReward) {        AmountReward = amountReward;    }
    public Double getAmountDiscount() {        return AmountDiscount;    }
    public void setAmountDiscount(Double amountDiscount) {        AmountDiscount = amountDiscount;    }
    public Double getAmountGratuity() {        return AmountGratuity;    }
    public void setAmountGratuity(Double amountGratuity) {        AmountGratuity = amountGratuity;    }
    public Double getAmountGratuity2() {        return AmountGratuity2;    }
    public void setAmountGratuity2(Double amountGratuity2) {        AmountGratuity2 = amountGratuity2;    }
    public Double getAmountGratuity3() {        return AmountGratuity3;    }
    public void setAmountGratuity3(Double amountGratuity3) {        AmountGratuity3 = amountGratuity3;    }
    public Double getAmountSaved() {        return AmountSaved;    }
    public void setAmountSaved(Double amountSaved) {        AmountSaved = amountSaved;    }
    public Double getAmountSurcharge() {        return AmountSurcharge;    }
    public void setAmountSurcharge(Double amountSurcharge) {        AmountSurcharge = amountSurcharge;    }
    public String getReferenceIdent() {        return ReferenceIdent;    }
    public void setReferenceIdent(String referenceIdent) {        ReferenceIdent = referenceIdent;    }
    public String getApprovalIdent() {        return ApprovalIdent;    }
    public void setApprovalIdent(String approvalIdent) {        ApprovalIdent = approvalIdent;    }
    public String getVoidIdent() {        return VoidIdent;    }
    public void setVoidIdent(String voidIdent) {        VoidIdent = voidIdent;    }
    public String getBatchIdent() {        return BatchIdent;    }
    public void setBatchIdent(String batchIdent) {        BatchIdent = batchIdent;    }
    public String getReason() {        return Reason;    }
    public void setReason(String reason) {        Reason = reason;    }
    public Integer getApprovalType() {        return ApprovalType;    }
    public void setApprovalType(Integer approvalType) {        ApprovalType = approvalType;    }
    public String getCardEncryptedIdent() {        return CardEncryptedIdent;    }
    public void setCardEncryptedIdent(String cardEncryptedIdent) {        CardEncryptedIdent = cardEncryptedIdent;    }
    public String getNameOnCard() {        return NameOnCard;    }
    public void setNameOnCard(String nameOnCard) {        NameOnCard = nameOnCard;    }
    public Integer getPANFirstSix() {        return PANFirstSix;    }
    public void setPANFirstSix(Integer PANFirstSix) {        this.PANFirstSix = PANFirstSix;    }
    public Integer getPANLastFour() {        return PANLastFour;    }
    public void setPANLastFour(Integer PANLastFour) {        this.PANLastFour = PANLastFour;    }
    public String getCardPAR() {        return CardPAR;    }
    public void setCardPAR(String cardPAR) {        CardPAR = cardPAR;    }
    public Integer getCardType() {        return CardType;    }
    public void setCardType(Integer cardType) {        CardType = cardType;    }
    public Integer getPaymentStatusType() {        return PaymentStatusType;    }
    public void setPaymentStatusType(Integer paymentStatusType) {        PaymentStatusType = paymentStatusType;    }
}
