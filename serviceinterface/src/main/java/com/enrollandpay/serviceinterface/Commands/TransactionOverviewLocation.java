package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionOverviewLocation {
    @SerializedName("LocationId")
    @Expose
    private Integer LocationId;
    @SerializedName("CurrencyType")
    @Expose
    private Integer CurrencyType;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Address")
    @Expose
    private String Address;
}
