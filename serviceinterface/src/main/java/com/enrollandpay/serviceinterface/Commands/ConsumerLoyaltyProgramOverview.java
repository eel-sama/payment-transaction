package com.enrollandpay.serviceinterface.Commands;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConsumerLoyaltyProgramOverview {
    @SerializedName("ProgramName")
    @Expose
    private String ProgramName;
    @SerializedName("ProgramType")
    @Expose
    private Integer ProgramType;
    @SerializedName("FreeProductCurrent")
    @Expose
    private Integer FreeProductCurrent;
    @SerializedName("PointBalanceCurrent")
    @Expose
    private Double PointBalanceCurrent;
    @SerializedName("PointBalanceLifetime")
    @Expose
    private Double PointBalanceLifetime;
    @SerializedName("RewardBalanceCurrent")
    @Expose
    private Double RewardBalanceCurrent;
    @SerializedName("RewardBalanceLifetime")
    @Expose
    private Double RewardBalanceLifetime;
    @SerializedName("PrimarySpendRequiredPoints")
    @Expose
    private Double PrimarySpendRequiredPoints;
    @SerializedName("PrimarySpendReceiveRewardAmount")
    @Expose
    private Double PrimarySpendReceiveRewardAmount;
    @SerializedName("PrimarySpendReceiveProductName")
    @Expose
    private String PrimarySpendReceiveProductName;
    @SerializedName("PrimarySpendReceiveOther")
    @Expose
    private String PrimarySpendReceiveOther;
    @SerializedName("PrimarySpendReceiveProductValuedUpTo")
    @Expose
    private Double PrimarySpendReceiveProductValuedUpTo;
    @SerializedName("LatestOrderProductValuedUpToEarned")
    @Expose
    private List<Double> LatestOrderProductValuedUpToEarned = null;
    @SerializedName("LatestOrderDiscount")
    @Expose
    private Double LatestOrderDiscount;
    @SerializedName("LatestOrderRewardEarned")
    @Expose
    private Double LatestOrderRewardEarned;
    @SerializedName("LatestOrderRewardSpent")
    @Expose
    private Double LatestOrderRewardSpent;
    @SerializedName("LatestOrderPointsEarned")
    @Expose
    private Double LatestOrderPointsEarned;
    @SerializedName("LatestOrderPointsSpent")
    @Expose
    private Double LatestOrderPointsSpent;
    /////////////////////////////////

    public String getProgramName() {
        return ProgramName;
    }

    public void setProgramName(String programName) {
        ProgramName = programName;
    }

    public Integer getProgramType() {
        return ProgramType;
    }

    public void setProgramType(Integer programType) {
        ProgramType = programType;
    }

    public Integer getFreeProductCurrent() {
        return FreeProductCurrent;
    }

    public void setFreeProductCurrent(Integer freeProductCurrent) {
        FreeProductCurrent = freeProductCurrent;
    }

    public Double getPointBalanceCurrent() {
        return PointBalanceCurrent;
    }

    public void setPointBalanceCurrent(Double pointBalanceCurrent) {
        PointBalanceCurrent = pointBalanceCurrent;
    }

    public Double getPointBalanceLifetime() {
        return PointBalanceLifetime;
    }

    public void setPointBalanceLifetime(Double pointBalanceLifetime) {
        PointBalanceLifetime = pointBalanceLifetime;
    }

    public Double getRewardBalanceCurrent() {
        return RewardBalanceCurrent;
    }

    public void setRewardBalanceCurrent(Double rewardBalanceCurrent) {
        RewardBalanceCurrent = rewardBalanceCurrent;
    }

    public Double getRewardBalanceLifetime() {
        return RewardBalanceLifetime;
    }

    public void setRewardBalanceLifetime(Double rewardBalanceLifetime) {
        RewardBalanceLifetime = rewardBalanceLifetime;
    }

    public Double getPrimarySpendRequiredPoints() {
        return PrimarySpendRequiredPoints;
    }

    public void setPrimarySpendRequiredPoints(Double primarySpendRequiredPoints) {
        PrimarySpendRequiredPoints = primarySpendRequiredPoints;
    }

    public Double getPrimarySpendReceiveRewardAmount() {
        return PrimarySpendReceiveRewardAmount;
    }

    public void setPrimarySpendReceiveRewardAmount(Double primarySpendReceiveRewardAmount) {
        PrimarySpendReceiveRewardAmount = primarySpendReceiveRewardAmount;
    }

    public String getPrimarySpendReceiveProductName() {
        return PrimarySpendReceiveProductName;
    }

    public void setPrimarySpendReceiveProductName(String primarySpendReceiveProductName) {
        PrimarySpendReceiveProductName = primarySpendReceiveProductName;
    }

    public String getPrimarySpendReceiveOther() {
        return PrimarySpendReceiveOther;
    }

    public void setPrimarySpendReceiveOther(String primarySpendReceiveOther) {
        PrimarySpendReceiveOther = primarySpendReceiveOther;
    }

    public Double getPrimarySpendReceiveProductValuedUpTo() {
        return PrimarySpendReceiveProductValuedUpTo;
    }

    public void setPrimarySpendReceiveProductValuedUpTo(Double primarySpendReceiveProductValuedUpTo) {
        PrimarySpendReceiveProductValuedUpTo = primarySpendReceiveProductValuedUpTo;
    }

    public List<Double> getLatestOrderProductValuedUpToEarned() {
        return LatestOrderProductValuedUpToEarned;
    }

    public void setLatestOrderProductValuedUpToEarned(List<Double> latestOrderProductValuedUpToEarned) {
        LatestOrderProductValuedUpToEarned = latestOrderProductValuedUpToEarned;
    }

    public Double getLatestOrderDiscount() {
        return LatestOrderDiscount;
    }

    public void setLatestOrderDiscount(Double latestOrderDiscount) {
        LatestOrderDiscount = latestOrderDiscount;
    }

    public Double getLatestOrderRewardEarned() {
        return LatestOrderRewardEarned;
    }

    public void setLatestOrderRewardEarned(Double latestOrderRewardEarned) {
        LatestOrderRewardEarned = latestOrderRewardEarned;
    }

    public Double getLatestOrderRewardSpent() {
        return LatestOrderRewardSpent;
    }

    public void setLatestOrderRewardSpent(Double latestOrderRewardSpent) {
        LatestOrderRewardSpent = latestOrderRewardSpent;
    }

    public Double getLatestOrderPointsEarned() {
        return LatestOrderPointsEarned;
    }

    public void setLatestOrderPointsEarned(Double latestOrderPointsEarned) {
        LatestOrderPointsEarned = latestOrderPointsEarned;
    }

    public Double getLatestOrderPointsSpent() {
        return LatestOrderPointsSpent;
    }

    public void setLatestOrderPointsSpent(Double latestOrderPointsSpent) {
        LatestOrderPointsSpent = latestOrderPointsSpent;
    }
}
