package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommandTransactionItem {
    @SerializedName("TransactionItemIdent")
    @Expose
    private String TransactionItemIdent;
    @SerializedName("OrderItemId")
    @Expose
    private Integer OrderItemId;
    @SerializedName("ProductId")
    @Expose
    private Integer ProductId;
    @SerializedName("ProductIdent")
    @Expose
    private String ProductIdent;
    @SerializedName("ProductName")
    @Expose
    private String ProductName;
    @SerializedName("Quantity")
    @Expose
    private Integer Quantity;
    @SerializedName("QuantitySplit")
    @Expose
    private Integer QuantitySplit;
    @SerializedName("AmountTotal")
    @Expose
    private Double AmountTotal;
    @SerializedName("AmountTax")
    @Expose
    private Double AmountTax;
    @SerializedName("ParentTransactionItemIdent")
    @Expose
    private String ParentTransactionItemIdent;
    ///////////////////////////

    public String getTransactionItemIdent() {
        return TransactionItemIdent;
    }

    public void setTransactionItemIdent(String transactionItemIdent) {
        TransactionItemIdent = transactionItemIdent;
    }

    public Integer getOrderItemId() {
        return OrderItemId;
    }

    public void setOrderItemId(Integer orderItemId) {
        OrderItemId = orderItemId;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public Integer getQuantity() {
        return Quantity;
    }

    public void setQuantity(Integer quantity) {
        Quantity = quantity;
    }

    public Double getAmountTotal() {
        return AmountTotal;
    }

    public void setAmountTotal(Double amountTotal) {
        AmountTotal = amountTotal;
    }

    public Double getAmountTax() {
        return AmountTax;
    }

    public void setAmountTax(Double amountTax) {
        AmountTax = amountTax;
    }

    public Integer getProductId() {        return ProductId;    }

    public void setProductId(Integer productId) {        ProductId = productId;    }

    public Integer getQuantitySplit() {        return QuantitySplit;    }

    public void setQuantitySplit(Integer quantitySplit) {        QuantitySplit = quantitySplit;    }

    public String getParentTransactionItemIdent() {        return ParentTransactionItemIdent;    }

    public void setParentTransactionItemIdent(String parentTransactionItemIdent) {        ParentTransactionItemIdent = parentTransactionItemIdent;    }

    public String getProductIdent() {
        return ProductIdent;
    }

    public void setProductIdent(String productIdent) {
        ProductIdent = productIdent;
    }
}
