package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payment {
    @SerializedName("AmountTendered")
    @Expose
    private Double AmountTendered;
    @SerializedName("ApprovalIdent")
    @Expose
    private String ApprovalIdent;
    @SerializedName("CardType")
    @Expose
    private Integer CardType;
    @SerializedName("NameOnCard")
    @Expose
    private String NameOnCard;
    @SerializedName("OrderPaymentId")
    @Expose
    private Integer OrderPaymentId;
    @SerializedName("PanMasked")
    @Expose
    private String PanMasked;
    @SerializedName("ReferenceIdent")
    @Expose
    private String ReferenceIdent;
    @SerializedName("StatusType")
    @Expose
    private Integer StatusType;

    public Double getAmountTendered() {
        return AmountTendered;
    }

    public void setAmountTendered(Double amountTendered) {
        AmountTendered = amountTendered;
    }

    public String getApprovalIdent() {
        return ApprovalIdent;
    }

    public void setApprovalIdent(String approvalIdent) {
        ApprovalIdent = approvalIdent;
    }

    public Integer getCardType() {
        return CardType;
    }

    public void setCardType(Integer cardType) {
        CardType = cardType;
    }

    public String getNameOnCard() {
        return NameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        NameOnCard = nameOnCard;
    }

    public Integer getOrderPaymentId() {
        return OrderPaymentId;
    }

    public void setOrderPaymentId(Integer orderPaymentId) {
        OrderPaymentId = orderPaymentId;
    }

    public String getPanMasked() {
        return PanMasked;
    }

    public void setPanMasked(String panMasked) {
        PanMasked = panMasked;
    }

    public String getReferenceIdent() {
        return ReferenceIdent;
    }

    public void setReferenceIdent(String referenceIdent) {
        ReferenceIdent = referenceIdent;
    }

    public Integer getStatusType() {
        return StatusType;
    }

    public void setStatusType(Integer statusType) {
        StatusType = statusType;
    }
}
