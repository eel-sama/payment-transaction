package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class CommandDisplayQuestionModel extends Command {
    @SerializedName("OrderReviewQuestionId")
    @Expose
    private Integer OrderReviewQuestionId;
    @SerializedName("QuestionType")
    @Expose
    private Integer QuestionType;
    @SerializedName("QuestionDisplayLanguages")
    @Expose
    private Map<String,String> QuestionDisplayLanguages;
    /////////////////////////////////////////

    public Integer getOrderReviewQuestionId() {
        return OrderReviewQuestionId;
    }

    public void setOrderReviewQuestionId(Integer orderReviewQuestionId) {
        OrderReviewQuestionId = orderReviewQuestionId;
    }

    public Integer getQuestionType() {
        return QuestionType;
    }

    public void setQuestionType(Integer questionType) {
        QuestionType = questionType;
    }

    public Map<String,String> getQuestionDisplayLanguages() {
        return QuestionDisplayLanguages;
    }

    public void setQuestionDisplayLanguages(Map<String,String> questionDisplayLanguages) {
        QuestionDisplayLanguages = questionDisplayLanguages;
    }
}
