package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CommandDisplayDiscountSelect extends Command {
    @SerializedName("Discounts")
    @Expose
    private List<CommandDiscountOption> Discounts = new ArrayList<>();

    public List<CommandDiscountOption> getDiscounts() {       return Discounts;    }
    public void setDiscounts(List<CommandDiscountOption> discounts) {        Discounts = discounts;    }
}