package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommandRequestAuthorization extends Command {
    @SerializedName("MemberName")
    @Expose
    private String MemberName;
    @SerializedName("AmountTotal")
    @Expose
    private Double AmountTotal;
    @SerializedName("AmountDue")
    @Expose
    private Double AmountDue;
    @SerializedName("AmountTax")
    @Expose
    private Double AmountTax;
    @SerializedName("AmountGratuity")
    @Expose
    private Double AmountGratuity;
    @SerializedName("AmountGratuity2")
    @Expose
    private Double AmountGratuity2;
    @SerializedName("AmountGratuity3")
    @Expose
    private Double AmountGratuity3;
    @SerializedName("AmountSurcharge")
    @Expose
    private Double AmountSurcharge;
    @SerializedName("AmountReward")
    @Expose
    private Double AmountReward;
    @SerializedName("AmountDiscount")
    @Expose
    private Double AmountDiscount;
    @SerializedName("AmountSaved")
    @Expose
    private Double AmountSaved;
    @SerializedName("CardAdded")
    @Expose
    private Boolean CardAdded;
    @SerializedName("CardPaymentToken")
    @Expose
    private String CardPaymentToken;
    /////////////////////
    public String getMemberName() {        return MemberName;    }
    public void setMemberName(String memberName) {        MemberName = memberName;    }
    public Double getAmountTotal() {        return AmountTotal;    }
    public void setAmountTotal(Double amountTotal) {        AmountTotal = amountTotal;    }
    public Double getAmountTax() {        return AmountTax;    }
    public void setAmountTax(Double amountTax) {        AmountTax = amountTax;    }
    public Double getAmountDiscount() {        return AmountDiscount;    }
    public void setAmountDiscount(Double amountDiscount) {        AmountDiscount = amountDiscount;    }
    public Double getAmountSaved() {        return AmountSaved;    }
    public void setAmountSaved(Double amountSaved) {        AmountSaved = amountSaved;    }
    public Double getAmountDue() {        return AmountDue;    }
    public void setAmountDue(Double amountDue) {        AmountDue = amountDue;    }
    public Double getAmountReward() {        return AmountReward;    }
    public void setAmountReward(Double amountReward) {        AmountReward = amountReward;    }
    public Boolean getCardAdded() {        return CardAdded;    }
    public void setCardAdded(Boolean cardAdded) {        CardAdded = cardAdded;    }
    public Double getAmountGratuity() {        return AmountGratuity;    }
    public void setAmountGratuity(Double amountGratuity) {        AmountGratuity = amountGratuity;    }
    public Double getAmountGratuity2() {        return AmountGratuity2;    }
    public void setAmountGratuity2(Double amountGratuity2) {        AmountGratuity2 = amountGratuity2;    }
    public Double getAmountGratuity3() {        return AmountGratuity3;    }
    public void setAmountGratuity3(Double amountGratuity3) {        AmountGratuity3 = amountGratuity3;    }
    public Double getAmountSurcharge() {        return AmountSurcharge;    }
    public void setAmountSurcharge(Double amountSurcharge) {        AmountSurcharge = amountSurcharge;    }
    public String getCardPaymentToken() {        return CardPaymentToken;    }
    public void setCardPaymentToken(String cardPaymentToken) {        CardPaymentToken = cardPaymentToken;    }

    private transient Boolean hasAddedTip = false;
    public Boolean getHasAddedTip() {
        return hasAddedTip;
    }
    public void setHasAddedTip(Boolean hasAddedTip) {
        this.hasAddedTip = hasAddedTip;
    }
}
