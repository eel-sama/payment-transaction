package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommandRequestPaymentAdjustment extends Command {
    @SerializedName("ConsumerId")
    @Expose
    private Integer ConsumerId;
    @SerializedName("AmountTendered")
    @Expose
    private Double AmountTendered;
    @SerializedName("AmountTotal")
    @Expose
    private Double AmountTotal;
    @SerializedName("AmountDue")
    @Expose
    private Double AmountDue;
    @SerializedName("AmountTax")
    @Expose
    private Double AmountTax;
    @SerializedName("AmountGratuity")
    @Expose
    private Double AmountGratuity;
    @SerializedName("AmountGratuity2")
    @Expose
    private Double AmountGratuity2;
    @SerializedName("AmountGratuity3")
    @Expose
    private Double AmountGratuity3;
    @SerializedName("AmountSurcharge")
    @Expose
    private Double AmountSurcharge;
    @SerializedName("AmountReward")
    @Expose
    private Double AmountReward;
    @SerializedName("AmountDiscount")
    @Expose
    private Double AmountDiscount;
    @SerializedName("AmountSaved")
    @Expose
    private Double AmountSaved;
    @SerializedName("ReferenceIdent")
    @Expose
    private String ReferenceIdent;
    @SerializedName("ApprovalIdent")
    @Expose
    private String ApprovalIdent;
    @SerializedName("VoidIdent")
    @Expose
    private String VoidIdent;
    @SerializedName("CardEncryptedIdent")
    @Expose
    private String CardEncryptedIdent;
    @SerializedName("ApprovalType")
    @Expose
    private Integer ApprovalType;
    @SerializedName("CapturedSignature")
    @Expose
    private String CapturedSignature = null;
    @SerializedName("BypassSignature")
    @Expose
    private Boolean BypassSignature = false;

    public Boolean getHasAddedTip() {
        return hasAddedTip;
    }

    public void setHasAddedTip(Boolean hasAddedTip) {
        this.hasAddedTip = hasAddedTip;
    }

    private transient Boolean hasAddedTip = false;

    ////////////////////////
    public Boolean getBypassSignature() {        return BypassSignature;    }
    public void setBypassSignature(Boolean bypassSignature) {        BypassSignature = bypassSignature;    }
    public String getCapturedSignature() {        return CapturedSignature;    }
    public void setCapturedSignature(String capturedSignature) {        CapturedSignature = capturedSignature;    }
    public Integer getConsumerId() {        return ConsumerId;    }
    public void setConsumerId(Integer consumerId) {        ConsumerId = consumerId;    }
    public Double getAmountTendered() {        return AmountTendered;    }
    public void setAmountTendered(Double amountTendered) {        AmountTendered = amountTendered;    }
    public Double getAmountTotal() {        return AmountTotal;    }
    public void setAmountTotal(Double amountTotal) {        AmountTotal = amountTotal;    }
    public Double getAmountDue() {        return AmountDue;   }
    public void setAmountDue(Double amountDue) {        AmountDue = amountDue;    }
    public Double getAmountTax() {        return AmountTax;    }
    public void setAmountTax(Double amountTax) {        AmountTax = amountTax;    }
    public Double getAmountReward() {        return AmountReward;    }
    public void setAmountReward(Double amountReward) {        AmountReward = amountReward;    }
    public Double getAmountDiscount() {        return AmountDiscount;    }
    public void setAmountDiscount(Double amountDiscount) {        AmountDiscount = amountDiscount;    }
    public Double getAmountSaved() {        return AmountSaved;    }
    public void setAmountSaved(Double amountSaved) {        AmountSaved = amountSaved;    }
    public String getReferenceIdent() {        return ReferenceIdent;   }
    public void setReferenceIdent(String referenceIdent) {        ReferenceIdent = referenceIdent;    }
    public String getApprovalIdent() {        return ApprovalIdent;    }
    public void setApprovalIdent(String approvalIdent) {        ApprovalIdent = approvalIdent;    }
    public String getVoidIdent() {        return VoidIdent;    }
    public void setVoidIdent(String voidIdent) {        VoidIdent = voidIdent;    }
    public Integer getApprovalType() {        return ApprovalType;    }
    public void setApprovalType(Integer approvalType) {        ApprovalType = approvalType;    }
    public Double getAmountGratuity() {        return AmountGratuity;    }
    public void setAmountGratuity(Double amountGratuity) {        AmountGratuity = amountGratuity;    }
    public Double getAmountGratuity2() {        return AmountGratuity2;    }
    public void setAmountGratuity2(Double amountGratuity2) {        AmountGratuity2 = amountGratuity2;    }
    public Double getAmountGratuity3() {        return AmountGratuity3;    }
    public void setAmountGratuity3(Double amountGratuity3) {        AmountGratuity3 = amountGratuity3;    }
    public Double getAmountSurcharge() {        return AmountSurcharge;    }
    public void setAmountSurcharge(Double amountSurcharge) {        AmountSurcharge = amountSurcharge;    }
    public String getCardEncryptedIdent() {        return CardEncryptedIdent;    }
    public void setCardEncryptedIdent(String cardEncryptedIdent) {        CardEncryptedIdent = cardEncryptedIdent;    }
}