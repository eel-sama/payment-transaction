package com.enrollandpay.serviceinterface.Commands;

import com.enrollandpay.serviceinterface.Models.Transaction.TransactionItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommandTabOpened extends CommandDisplayTransaction {
    @SerializedName("MemberName")
    @Expose
    private String MemberName;
    @SerializedName("ConsumerId")
    @Expose
    private Integer ConsumerId ;
    @SerializedName("AmountTendered")
    @Expose
    private Double AmountTendered ;
    @SerializedName("AmountDue")
    @Expose
    private Double AmountDue ;
    @SerializedName("AmountReward")
    @Expose
    private Double AmountReward ;
    @SerializedName("AmountDiscount")
    @Expose
    private Double AmountDiscount ;
    @SerializedName("AmountSaved")
    @Expose
    private Double AmountSaved ;
    @SerializedName("ReferenceIdent")
    @Expose
    private String ReferenceIdent ;
    @SerializedName("ApprovalIdent")
    @Expose
    private String ApprovalIdent ;
    @SerializedName("VoidIdent")
    @Expose
    private String VoidIdent ;
    @SerializedName("BatchIdent")
    @Expose
    private String BatchIdent;
    @SerializedName("Reason")
    @Expose
    private String Reason ;
    @SerializedName("CardEncryptedIdent")
    @Expose
    private String CardEncryptedIdent ;
    @SerializedName("NameOnCard")
    @Expose
    private String NameOnCard ;
    @SerializedName("PANFirstSix")
    @Expose
    private Integer PANFirstSix ;
    @SerializedName("PANLastFour")
    @Expose
    private Integer PANLastFour ;
    @SerializedName("CardPAR")
    @Expose
    private String CardPAR ;
    @SerializedName("CardType")
    @Expose
    private Integer CardType ;
    @SerializedName("RequestType")
    @Expose
    private Integer RequestType ;
    ///////////////////////////////////////////////////////

    public String getMemberName() {        return MemberName;    }
    public void setMemberName(String memberName) {        MemberName = memberName;    }
    public Integer getConsumerId() {        return ConsumerId;    }
    public void setConsumerId(Integer consumerId) {        ConsumerId = consumerId;    }
    public Double getAmountTendered() {        return AmountTendered;    }
    public void setAmountTendered(Double amountTendered) {        AmountTendered = amountTendered;    }
    public Double getAmountDue() {        return AmountDue;    }
    public void setAmountDue(Double amountDue) {        AmountDue = amountDue;    }
    public Double getAmountReward() {        return AmountReward;    }
    public void setAmountReward(Double amountReward) {        AmountReward = amountReward;    }
    public Double getAmountDiscount() {        return AmountDiscount;    }
    public void setAmountDiscount(Double amountDiscount) {        AmountDiscount = amountDiscount;    }
    public Double getAmountSaved() {        return AmountSaved;    }
    public void setAmountSaved(Double amountSaved) {        AmountSaved = amountSaved;    }
    public String getReferenceIdent() {        return ReferenceIdent;    }
    public void setReferenceIdent(String referenceIdent) {        ReferenceIdent = referenceIdent;    }
    public String getApprovalIdent() {        return ApprovalIdent;    }
    public void setApprovalIdent(String approvalIdent) {        ApprovalIdent = approvalIdent;    }
    public String getVoidIdent() {        return VoidIdent;    }
    public void setVoidIdent(String voidIdent) {        VoidIdent = voidIdent;    }
    public String getBatchIdent() {        return BatchIdent;    }
    public void setBatchIdent(String batchIdent) {        BatchIdent = batchIdent;    }
    public String getReason() {        return Reason;    }
    public void setReason(String reason) {        Reason = reason;    }
    public String getCardEncryptedIdent() {        return CardEncryptedIdent;    }
    public void setCardEncryptedIdent(String cardEncryptedIdent) {        CardEncryptedIdent = cardEncryptedIdent;    }
    public String getNameOnCard() {        return NameOnCard;    }
    public void setNameOnCard(String nameOnCard) {        NameOnCard = nameOnCard;    }
    public Integer getPANFirstSix() {        return PANFirstSix;    }
    public void setPANFirstSix(Integer PANFirstSix) {        this.PANFirstSix = PANFirstSix;    }
    public Integer getPANLastFour() {        return PANLastFour;    }
    public void setPANLastFour(Integer PANLastFour) {        this.PANLastFour = PANLastFour;    }
    public String getCardPAR() {        return CardPAR;    }
    public void setCardPAR(String cardPAR) {        CardPAR = cardPAR;    }
    public Integer getCardType() {        return CardType;    }
    public void setCardType(Integer cardType) {        CardType = cardType;    }
    public Integer getRequestType() {        return RequestType;    }
    public void setRequestType(Integer requestType) {        RequestType = requestType;    }
}
