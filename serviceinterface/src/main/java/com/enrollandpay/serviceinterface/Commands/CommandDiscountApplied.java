package com.enrollandpay.serviceinterface.Commands;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommandDiscountApplied extends Command{
    @SerializedName("AppliedCount")
    @Expose
    private Double appliedCount;
    @SerializedName("AppliedAmount")
    @Expose
    private Double appliedAmount;
    @SerializedName("Reason")
    @Expose
    private String reason;
    @SerializedName("InternalIdent")
    @Expose
    private String internalIdent;
}
