package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class Command {
    @SerializedName("CommandId")
    @Expose
    private String CommandId;
    @SerializedName("TransactionIdent")
    @Expose
    private String TransactionIdent;
    @SerializedName("OrderId")
    @Expose
    private Integer OrderId;
    @SerializedName("OrderPaymentId")
    @Expose
    private Integer OrderPaymentId;
    @SerializedName("PaymentType")
    @Expose
    private Integer PaymentType;
    @SerializedName("PaymentRequestType")
    @Expose
    private Integer PaymentRequestType;
    @SerializedName("CommandType")
    @Expose
    private Integer CommandType;
    @SerializedName("StatusType")
    @Expose
    private Integer StatusType;
    @SerializedName("RecipientType")
    @Expose
    private Integer RecipientType;
    @SerializedName("StatusInformation")
    @Expose
    private String StatusInformation;
    @SerializedName("CreateDateTime")
    @Expose
    private String CreateDateTime;
    @SerializedName("ResponseDateTime")
    @Expose
    private String ResponseDateTime;
    @SerializedName("POSDeviceId")
    @Expose
    private Integer POSDeviceId;
    @SerializedName("PaymentDeviceId")
    @Expose
    private Integer PaymentDeviceId;
    @SerializedName("DelayCommand")
    @Expose
    private Integer DelayCommand;
    @SerializedName("PrerequisiteCommandId")
    @Expose
    private Integer PrerequisiteCommandId;
    @SerializedName("AdditionalAttributes")
    @Expose
    private java.util.Map<String, String> AdditionalAttributes;
    ///////////////////////////////////////////
    public String getTransactionIdent() {        return TransactionIdent;    }
    public void setTransactionIdent(String transactionIdent) {        TransactionIdent = transactionIdent;    }
    public String getCommandId() {        return CommandId;    }
    public void setCommandId(String commandId) {        CommandId = commandId;    }
    public Integer getOrderId() {        return OrderId;    }
    public void setOrderId(Integer orderId) {        OrderId = orderId;    }
    public Integer getOrderPaymentId() {        return OrderPaymentId;    }
    public void setOrderPaymentId(Integer orderPaymentId) {        OrderPaymentId = orderPaymentId;    }
    public Integer getPaymentType() {        return PaymentType;    }
    public void setPaymentType(Integer paymentType) {        PaymentType = paymentType;    }
    public Integer getCommandType() {        return CommandType;    }
    public void setCommandType(Integer commandType) {        CommandType = commandType;    }
    public Integer getStatusType() {        return StatusType;    }
    public void setStatusType(Integer statusType) {        StatusType = statusType;    }
    public Integer getRecipientType() {        return RecipientType;    }
    public void setRecipientType(Integer recipientType) {        RecipientType = recipientType;    }
    public String getStatusInformation() {        return StatusInformation;    }
    public void setStatusInformation(String statusInformation) {        StatusInformation = statusInformation;    }
    public String getCreateDateTime() {        return CreateDateTime;    }
    public void setCreateDateTime(String createDateTime) {        CreateDateTime = createDateTime;    }
    public String getResponseDateTime() {        return ResponseDateTime;    }
    public void setResponseDateTime(String responseDateTime) {        ResponseDateTime = responseDateTime;    }
    public Integer getPOSDeviceId() {        return POSDeviceId;    }
    public void setPOSDeviceId(Integer POSDeviceId) {        this.POSDeviceId = POSDeviceId;    }
    public Integer getPaymentDeviceId() {        return PaymentDeviceId;    }
    public void setPaymentDeviceId(Integer paymentDeviceId) {        PaymentDeviceId = paymentDeviceId;    }
    public Integer getDelayCommand() {        return DelayCommand;    }
    public void setDelayCommand(Integer delayCommand) {        DelayCommand = delayCommand;    }
    public Integer getPrerequisiteCommandId() {        return PrerequisiteCommandId;    }
    public void setPrerequisiteCommandId(Integer prerequisiteCommandId) {        PrerequisiteCommandId = prerequisiteCommandId;    }
    public Integer getPaymentRequestType() {        return PaymentRequestType;    }
    public void setPaymentRequestType(Integer paymentRequestType) {        PaymentRequestType = paymentRequestType;    }
    public Map<String, String> getAdditionalAttributes() {        return AdditionalAttributes;    }
    public void setAdditionalAttributes(Map<String, String> additionalAttributes) {        AdditionalAttributes = additionalAttributes;    }
}