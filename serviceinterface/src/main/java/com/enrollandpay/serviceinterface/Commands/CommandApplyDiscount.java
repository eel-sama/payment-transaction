package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommandApplyDiscount extends Command{
    @SerializedName("InternalIdent")
    @Expose
    private String InternalIdent;
    @SerializedName("POSIdent")
    @Expose
    private String POSIdent;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Amount")
    @Expose
    private Double Amount;
    @SerializedName("Percentage")
    @Expose
    private Double Percentage;
    @SerializedName("ApplyRewardOfferType")
    @Expose
    private Integer ApplyRewardOfferType;
    @SerializedName("LoyaltyProgramId")
    @Expose
    private Integer LoyaltyProgramId;
    @Expose
    @SerializedName("QualifyingProducts")
    private List<QualifyingProduct> qualifyingProducts;
    @Expose
    @SerializedName("QualifyingProductDiscounts")
    private List<QualifyingProductDiscount> qualifyingProductDiscounts;
    ///////////////////////////////////////////
    public String getInternalIdent() {
        return InternalIdent;
    }
    public void setInternalIdent(String internalIdent) {
        InternalIdent = internalIdent;
    }
    public String getPOSIdent() {
        return POSIdent;
    }
    public void
    setPOSIdent(String POSIdent) {
        this.POSIdent = POSIdent;
    }
    public String getName() {
        return Name;
    }
    public void setName(String name) {
        Name = name;
    }
    public Double getAmount() {
        return Amount;
    }
    public void setAmount(Double amount) {
        Amount = amount;
    }
    public Double getPercentage() {
        return Percentage;
    }
    public void setPercentage(Double percentage) {
        Percentage = percentage;
    }
    public Integer getApplyRewardOfferType() {
        return ApplyRewardOfferType;
    }
    public void setApplyRewardOfferType(Integer applyRewardOfferType) {        ApplyRewardOfferType = applyRewardOfferType;    }
    public Integer getLoyaltyProgramId() {
        return LoyaltyProgramId;
    }
    public void setLoyaltyProgramId(Integer loyaltyProgramId) {        LoyaltyProgramId = loyaltyProgramId;    }
    public List<QualifyingProduct> getQualifyingProducts() {        return qualifyingProducts;    }
    public List<QualifyingProductDiscount> getQualifyingProductDiscounts() {        return qualifyingProductDiscounts;    }
    public void setQualifyingProductDiscounts(List<QualifyingProductDiscount> qualifyingProductDiscounts) {        this.qualifyingProductDiscounts = qualifyingProductDiscounts;    }
    public void setQualifyingProducts(List<QualifyingProduct> qualifyingProducts) {        this.qualifyingProducts = qualifyingProducts;    }
    ///////////////////
    public class QualifyingProduct
    {
        @Expose
        @SerializedName("Matches")
        private List<QualifyingProductMatch> matches;
        @Expose
        @SerializedName("PosIdents")
        private String posIdents;
        @Expose
        @SerializedName("ProductId")
        private Integer productId;
        @Expose
        @SerializedName("Quantity")
        private Double quantity;
        public List<QualifyingProductMatch> getMatches() {            return matches;        }
        public void setMatches(List<QualifyingProductMatch> matches) {            this.matches = matches;        }
        public String getPosIdents() {            return posIdents;        }
        public void setPosIdents(String posIdents) {            this.posIdents = posIdents;        }
        public Integer getProductId() {            return productId;        }
        public void setProductId(Integer productId) {            this.productId = productId;        }
        public Double getQuantity() {            return quantity;        }
        public void setQuantity(Double quantity) {            this.quantity = quantity;        }
    }
    /// <summary>
    /// Contains general information about a product which will receive a discount
    /// </summary>
    public class QualifyingProductDiscount
    {
        @Expose
        @SerializedName("DiscountAmount")
		private Double discountAmount;
        @Expose
        @SerializedName("Quantity")
        private Double quantity;
        @Expose
        @SerializedName("Matches")
        private List<QualifyingProductMatch> Matches;
        @Expose
        @SerializedName("PosIdents")
        private String posIdents;
        @Expose
        @SerializedName("ProductId")
        private Integer productId;
        ////////////////////////////////
        public Double getDiscountAmount() {            return discountAmount;        }
        public void setDiscountAmount(Double discountAmount) {            this.discountAmount = discountAmount;        }
        public List<QualifyingProductMatch> getMatches() {            return Matches;        }
        public void setMatches(List<QualifyingProductMatch> matches) {            Matches = matches;        }
        public String getPosIdents() {            return posIdents;        }
        public void setPosIdents(String posIdents) {            this.posIdents = posIdents;        }
        public Integer getProductId() {            return productId;        }
        public void setProductId(Integer productId) {            this.productId = productId;        }
        public Double getQuantity() {            return quantity;        }
        public void setQuantity(Double quantity) {            this.quantity = quantity;        }
    }
    public class QualifyingProductMatch
    {
        @Expose
        @SerializedName("OrderItemId")
		private Integer orderItemId;
        @Expose
        @SerializedName("Quantity")
        private Double quantity;
        @Expose
        @SerializedName("TransactionItemIdent")
        private String transactionItemIdent;
        /////////////////////////////////////
        public Integer getOrderItemId() {            return orderItemId;        }
        public void setOrderItemId(Integer orderItemId) {            this.orderItemId = orderItemId;        }
        public Double getQuantity() {            return quantity;        }
        public void setQuantity(Double quantity) {            this.quantity = quantity;        }
        public String getTransactionItemIdent() {            return transactionItemIdent;        }
        public void setTransactionItemIdent(String transactionItemIdent) {            this.transactionItemIdent = transactionItemIdent;        }
    }
}
