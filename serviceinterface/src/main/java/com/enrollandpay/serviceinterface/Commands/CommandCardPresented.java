package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommandCardPresented extends CommandDisplayTransaction {
    @SerializedName("AmountDue")
    @Expose
    private Double AmountDue;
    @SerializedName("AmountReward")
    @Expose
    private Double AmountReward;
    @SerializedName("AmountDiscount")
    @Expose
    private Double AmountDiscount;
    @SerializedName("AmountSaved")
    @Expose
    private Double AmountSaved;
    /////////////////////CARD INFORMATION//////////////////////
    /// <summary>Card holder name</summary>
    @SerializedName("NameOnCard")
    @Expose
    private String NameOnCard;
    /// <summary>First 6</summary>
    @SerializedName("PANFirstSix")
    @Expose
    private Integer PANFirstSix;
    /// <summary>Last 4</summary>
    @SerializedName("PANLastFour")
    @Expose
    private Integer PANLastFour;
    /// <summary>Unique id associated with card</summary>
    @SerializedName("CardPAR")
    @Expose
    private String CardPAR;
    /// <summary>Unique id associated with card</summary>
    @SerializedName("CardEncryptedIdent")
    @Expose
    private String CardEncryptedIdent;
    /// <summary>Card Type</summary>
    @SerializedName("CardType")
    @Expose
    private Integer CardType;
    /////////////////////////

    public Double getAmountDue() {
        return AmountDue;
    }

    public void setAmountDue(Double amountDue) {
        AmountDue = amountDue;
    }

    public Double getAmountReward() {
        return AmountReward;
    }

    public void setAmountReward(Double amountReward) {
        AmountReward = amountReward;
    }

    public Double getAmountDiscount() {
        return AmountDiscount;
    }

    public void setAmountDiscount(Double amountDiscount) {
        AmountDiscount = amountDiscount;
    }

    public Double getAmountSaved() {
        return AmountSaved;
    }

    public void setAmountSaved(Double amountSaved) {
        AmountSaved = amountSaved;
    }
    /////////////////////////////////////////////CARD INFORMATION///////////////////////////////
    public String getNameOnCard() {        return NameOnCard;    }
    public void setNameOnCard(String nameOnCard) {        NameOnCard = nameOnCard;    }
    public Integer getPANFirstSix() {        return PANFirstSix;    }
    public void setPANFirstSix(Integer PANFirstSix) {        this.PANFirstSix = PANFirstSix;    }
    public Integer getPANLastFour() {        return PANLastFour;    }
    public void setPANLastFour(Integer PANLastFour) {        this.PANLastFour = PANLastFour;    }
    public String getCardPAR() {        return CardPAR;    }
    public void setCardPAR(String cardPAR) {        CardPAR = cardPAR;    }
    public String getCardEncryptedIdent() {        return CardEncryptedIdent;    }
    public void setCardEncryptedIdent(String cardEncryptedIdent) {        CardEncryptedIdent = cardEncryptedIdent;    }
    public Integer getCardType() {        return CardType;    }
    public void setCardType(Integer cardType) {        CardType = cardType;    }
}
