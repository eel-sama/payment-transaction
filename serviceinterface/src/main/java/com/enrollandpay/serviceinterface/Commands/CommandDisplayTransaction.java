package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommandDisplayTransaction extends Command {
    @SerializedName("AmountTotalDue")
    @Expose
    private Double AmountTotalDue;
    @SerializedName("AmountTotal")
    @Expose
    private Double AmountTotal;
    @SerializedName("AmountTotalTendered")
    @Expose
    private Double AmountTotalTendered;
    @SerializedName("AmountTotalReward")
    @Expose
    private Double AmountTotalReward;
    @SerializedName("AmountTotalDiscount")
    @Expose
    private Double AmountTotalDiscount;
    @SerializedName("AmountTotalGratuity")
    @Expose
    private Double AmountTotalGratuity;
    @SerializedName("AmountTotalSaved")
    @Expose
    private Double AmountTotalSaved;
    @SerializedName("AmountTax")
    @Expose
    private Double AmountTax;
    @SerializedName("AmountGratuity")
    @Expose
    private Double AmountGratuity;
    @SerializedName("AmountGratuity2")
    @Expose
    private Double AmountGratuity2;
    @SerializedName("AmountGratuity3")
    @Expose
    private Double AmountGratuity3;
    @SerializedName("AmountSurcharge")
    @Expose
    private Double AmountSurcharge;
    @SerializedName("Items")
    @Expose
    private List<CommandTransactionItem> Items = null;
    @SerializedName("Payments")
    @Expose
    private List<Payment> Payments = null;

    public Double getAmountTotalGratuity() {
        return AmountTotalGratuity;
    }

    public void setAmountTotalGratuity(Double amountTotalGratuity) {
        AmountTotalGratuity = amountTotalGratuity;
    }

    public Double getAmountTotal() {        return AmountTotal;    }
    public void setAmountTotal(Double amountTotal) {        AmountTotal = amountTotal;    }
    public Double getAmountTotalTendered() {        return AmountTotalTendered;    }
    public void setAmountTotalTendered(Double amountTotalTendered) {        AmountTotalTendered = amountTotalTendered;    }
    public Double getAmountTotalReward() {        return AmountTotalReward;    }
    public void setAmountTotalReward(Double amountTotalReward) {        AmountTotalReward = amountTotalReward;    }
    public Double getAmountTotalDiscount() {        return AmountTotalDiscount;    }
    public void setAmountTotalDiscount(Double amountTotalDiscount) {        AmountTotalDiscount = amountTotalDiscount;    }
    public Double getAmountTotalSaved() {        return AmountTotalSaved;    }
    public void setAmountTotalSaved(Double amountTotalSaved) {        AmountTotalSaved = amountTotalSaved;    }
    public Double getAmountTotalDue() {        return AmountTotalDue;    }
    public void setAmountTotalDue(Double amountDue) {        AmountTotalDue = amountDue;    }
    public Double getAmountTax() {        return AmountTax;   }
    public void setAmountTax(Double amountTax) {        AmountTax = amountTax;    }
    public Double getAmountGratuity() {        return AmountGratuity;    }
    public void setAmountGratuity(Double amountGratuity) {        AmountGratuity = amountGratuity;    }
    public Double getAmountGratuity2() {        return AmountGratuity2;    }
    public void setAmountGratuity2(Double amountGratuity2) {        AmountGratuity2 = amountGratuity2;    }
    public Double getAmountGratuity3() {        return AmountGratuity3;    }
    public void setAmountGratuity3(Double amountGratuity3) {        AmountGratuity3 = amountGratuity3;    }
    public Double getAmountSurcharge() {        return AmountSurcharge;    }
    public void setAmountSurcharge(Double amountSurcharge) {        AmountSurcharge = amountSurcharge;    }
    public List<CommandTransactionItem> getItems() {        return Items;    }
    public void setItems(List<CommandTransactionItem> items) {        Items = items;    }

    public List<Payment> getPayments() { return Payments; }
    public void setPayments(List<Payment> payments) { Payments = payments; }
}
