package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class TransactionOverviewQuestion {
    @SerializedName("OrderReviewQuestionId")
    @Expose
    private Integer OrderReviewQuestionId;
    @SerializedName("QuestionDisplayLanguages")
    @Expose
    private java.util.Map<String, String> QuestionDisplayLanguages;
    @SerializedName("QuestionType")
    @Expose
    private Integer QuestionType;
    @SerializedName("Score")
    @Expose
    private Double Score;
    ///////////////////////////////
    public Integer getOrderReviewQuestionId() {        return OrderReviewQuestionId;    }
    public void setOrderReviewQuestionId(Integer orderReviewQuestionId) {        OrderReviewQuestionId = orderReviewQuestionId;    }
    public Map<String, String> getQuestionDisplayLanguages() {        return QuestionDisplayLanguages;    }
    public void setQuestionDisplayLanguages(Map<String, String> questionDisplayLanguages) {        QuestionDisplayLanguages = questionDisplayLanguages;    }
    public Integer getQuestionType() {        return QuestionType;    }
    public void setQuestionType(Integer questionType) {        QuestionType = questionType;    }
    public Double getScore() {        return Score;    }
    public void setScore(Double score) {        Score = score;    }
}
