package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reward {
    @SerializedName("ConsumerLoyaltyProgramId")
    @Expose
    private Integer ConsumerLoyaltyProgramId;
    @SerializedName("ProgramName")
    @Expose
    private String ProgramName;
    @SerializedName("RewardAmount")
    @Expose
    private Double RewardAmount;
    //////////////////////////////////////////////////////////////////////////

    public Integer getConsumerLoyaltyProgramId() {
        return ConsumerLoyaltyProgramId;
    }

    public void setConsumerLoyaltyProgramId(Integer consumerLoyaltyProgramId) {
        ConsumerLoyaltyProgramId = consumerLoyaltyProgramId;
    }

    public String getProgramName() {
        return ProgramName;
    }

    public void setProgramName(String programName) {
        ProgramName = programName;
    }

    public Double getRewardAmount() {
        return RewardAmount;
    }

    public void setRewardAmount(Double rewardAmount) {
        RewardAmount = rewardAmount;
    }
}
