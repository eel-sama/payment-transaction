package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommandDiscountOption {
    @SerializedName("ApplyCount")
    @Expose
    private Integer ApplyCount;
    @SerializedName("InternalIdent")
    @Expose
    private String InternalIdent;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Amount")
    @Expose
    private Double Amount;
    @SerializedName("ProgramName")
    @Expose
    private String ProgramName;

    public Integer getApplyCount() {        return ApplyCount;    }
    public void setApplyCount(Integer applyCount) {        ApplyCount = applyCount;    }
    public String getInternalIdent() {        return InternalIdent;    }
    public void setInternalIdent(String internalIdent) {        InternalIdent = internalIdent;    }
    public String getName() {        return Name;    }
    public void setName(String name) {        Name = name;    }
    public Double getAmount() {        return Amount;    }
    public void setAmount(Double amount) {        Amount = amount;    }
    public String getProgramName() {        return ProgramName;    }
    public void setProgramName(String programName) {        ProgramName = programName;    }
}