package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class TransactionOverviewRecipient {
    @SerializedName("ConsumerId")
    @Expose
    private Integer ConsumerId;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("NameFirst")
    @Expose
    private String NameFirst;
    @SerializedName("NameLast")
    @Expose
    private String NameLast;
    @SerializedName("AmountTendered")
    @Expose
    private Double AmountTendered;
    @SerializedName("AmountGratuity")
    @Expose
    private Double AmountGratuity;
    @SerializedName("AmountGratuity2")
    @Expose
    private Double AmountGratuity2;
    @SerializedName("AmountGratuity3")
    @Expose
    private Double AmountGratuity3;
    @SerializedName("AmountSurcharge")
    @Expose
    private Double AmountSurcharge;
    @SerializedName("AmountReward")
    @Expose
    private Double AmountReward;
    @SerializedName("OrderReviewId")
    @Expose
    private String OrderReviewId;
    @SerializedName("SendToEmailAddress")
    @Expose
    private String SendToEmailAddress;
    @SerializedName("SendToPhoneNumber")
    @Expose
    private String SendToPhoneNumber;
    @SerializedName("Questions")
    @Expose
    private List<TransactionOverviewQuestion> Questions = null;
    @SerializedName("ConsumerLoyaltyProgramOverviews")
    @Expose
    private List<ConsumerLoyaltyProgramOverview> ConsumerLoyaltyProgramOverviews = null;
    @SerializedName("OptInType")
    @Expose
    private Integer OptInType;
    @SerializedName("Payments")
    @Expose
    private List<TransactionOverviewPayment> Payments = null;
    @SerializedName("RecapLanguages")
    @Expose
    private java.util.Map<String, String> RecapLanguages;
    ///////////////////////////////////

    public Integer getConsumerId() {        return ConsumerId;    }
    public void setConsumerId(Integer consumerId) {        ConsumerId = consumerId;    }
    public String getName() {        return Name;    }
    public void setName(String name) {        Name = name;    }
    public String getNameFirst() {        return NameFirst;    }
    public void setNameFirst(String nameFirst) {        NameFirst = nameFirst;    }
    public String getNameLast() {        return NameLast;    }
    public void setNameLast(String nameLast) {        NameLast = nameLast;    }
    public Double getAmountTendered() {        return AmountTendered;    }
    public void setAmountTendered(Double amountTendered) {        AmountTendered = amountTendered;    }
    public Double getAmountGratuity() {        return AmountGratuity;    }
    public void setAmountGratuity(Double amountGratuity) {        AmountGratuity = amountGratuity;    }
    public Double getAmountGratuity2() {        return AmountGratuity2;    }
    public void setAmountGratuity2(Double amountGratuity2) {        AmountGratuity2 = amountGratuity2;    }
    public Double getAmountGratuity3() {        return AmountGratuity3;    }
    public void setAmountGratuity3(Double amountGratuity3) {        AmountGratuity3 = amountGratuity3;    }
    public Double getAmountSurcharge() {        return AmountSurcharge;    }
    public void setAmountSurcharge(Double amountSurcharge) {        AmountSurcharge = amountSurcharge;    }
    public Double getAmountReward() {        return AmountReward;    }
    public void setAmountReward(Double amountReward) {        AmountReward = amountReward;    }
    public String getOrderReviewId() {        return OrderReviewId;    }
    public void setOrderReviewId(String orderReviewId) {        OrderReviewId = orderReviewId;    }
    public String getSendToEmailAddress() {        return SendToEmailAddress;    }
    public void setSendToEmailAddress(String sendToEmailAddress) {        SendToEmailAddress = sendToEmailAddress;    }
    public String getSendToPhoneNumber() {        return SendToPhoneNumber;    }
    public void setSendToPhoneNumber(String sendToPhoneNumber) {        SendToPhoneNumber = sendToPhoneNumber;    }
    public List<TransactionOverviewQuestion> getQuestions() {        return Questions;    }
    public void setQuestions(List<TransactionOverviewQuestion> questions) {        Questions = questions;    }
    public List<ConsumerLoyaltyProgramOverview> getConsumerLoyaltyProgramOverviews() {        return ConsumerLoyaltyProgramOverviews;    }
    public void setConsumerLoyaltyProgramOverviews(List<ConsumerLoyaltyProgramOverview> consumerLoyaltyProgramOverviews) {        ConsumerLoyaltyProgramOverviews = consumerLoyaltyProgramOverviews;    }
    public Integer getOptInType() {        return OptInType;    }
    public void setOptInType(Integer optInType) {        OptInType = optInType;    }
    public List<TransactionOverviewPayment> getPayments() {        return Payments;    }
    public void setPayments(List<TransactionOverviewPayment> payments) {        Payments = payments;    }
    public Map<String, String> getRecapLanguages() {        return RecapLanguages;    }
    public void setRecapLanguages(Map<String, String> recapLanguages) {        RecapLanguages = recapLanguages;    }
}
