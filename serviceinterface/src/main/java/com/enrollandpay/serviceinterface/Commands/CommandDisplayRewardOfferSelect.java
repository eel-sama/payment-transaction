package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommandDisplayRewardOfferSelect extends Command {
    @SerializedName("Rewards")
    @Expose
    private List<Reward> rewards = null;
    @SerializedName("Offers")
    @Expose
    private List<Offer> offers = null;

    public List<Reward> getRewards() {
        return rewards;
    }

    public void setRewards(List<Reward> rewards) {
        this.rewards = rewards;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }
}
