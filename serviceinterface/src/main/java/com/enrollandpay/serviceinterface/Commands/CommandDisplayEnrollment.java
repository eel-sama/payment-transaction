package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommandDisplayEnrollment extends Command {
    @SerializedName("LoyaltyProgramId")
    @Expose
    private Integer LoyaltyProgramId;
    @SerializedName("JoinProgram")
    @Expose
    private String JoinProgram;
    @SerializedName("OfferMessage")
    @Expose
    private String OfferMessage;
    @SerializedName("MethodType")
    @Expose
    private Integer MethodType;
    ///////////////////
    public Integer getLoyaltyProgramId() {
        return LoyaltyProgramId;
    }

    public void setLoyaltyProgramId(Integer loyaltyProgramId) {
        LoyaltyProgramId = loyaltyProgramId;
    }

    public String getJoinProgram() {
        return JoinProgram;
    }

    public void setJoinProgram(String joinProgram) {
        JoinProgram = joinProgram;
    }

    public String getOfferMessage() {
        return OfferMessage;
    }

    public void setOfferMessage(String offerMessage) {
        OfferMessage = offerMessage;
    }

    public Integer getMethodType() {
        return MethodType;
    }

    public void setMethodType(Integer methodType) {
        MethodType = methodType;
    }
}
