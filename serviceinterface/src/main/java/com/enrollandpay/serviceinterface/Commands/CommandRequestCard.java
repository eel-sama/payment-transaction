package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommandRequestCard extends Command {
    @SerializedName("AmountTotal")
    @Expose
    private Double AmountTotal;
    @SerializedName("AmountDue")
    @Expose
    private Double AmountDue;
    @SerializedName("AmountTax")
    @Expose
    private Double AmountTax;
    @SerializedName("AmountGratuity")
    @Expose
    private Double AmountGratuity;
    @SerializedName("AmountGratuity2")
    @Expose
    private Double AmountGratuity2;
    @SerializedName("AmountGratuity3")
    @Expose
    private Double AmountGratuity3;
    @SerializedName("AmountSurcharge")
    @Expose
    private Double AmountSurcharge;
    @SerializedName("AmountReward")
    @Expose
    private Double AmountReward;
    @SerializedName("AmountDiscount")
    @Expose
    private Double AmountDiscount;
    @SerializedName("AmountSaved")
    @Expose
    private Double AmountSaved;
    @SerializedName("PromotedOffers")
    @Expose
    private List<PromotedOffer> PromotedOffers = null;
    ////////////////////////////////////
    public Double getAmountTotal() {        return AmountTotal;    }
    public void setAmountTotal(Double amountTotal) {        AmountTotal = amountTotal;    }
    public Double getAmountDue() {        return AmountDue;    }
    public void setAmountDue(Double amountDue) {        AmountDue = amountDue;    }
    public Double getAmountTax() {        return AmountTax;    }
    public void setAmountTax(Double amountTax) {        AmountTax = amountTax;    }
    public Double getAmountGratuity() {        return AmountGratuity;    }
    public void setAmountGratuity(Double amountGratuity) {        AmountGratuity = amountGratuity;    }
    public Double getAmountGratuity2() {        return AmountGratuity2;    }
    public void setAmountGratuity2(Double amountGratuity2) {        AmountGratuity2 = amountGratuity2;    }
    public Double getAmountGratuity3() {        return AmountGratuity3;    }
    public void setAmountGratuity3(Double amountGratuity3) {        AmountGratuity3 = amountGratuity3;    }
    public Double getAmountSurcharge() {        return AmountSurcharge;    }
    public void setAmountSurcharge(Double amountSurcharge) {        AmountSurcharge = amountSurcharge;    }
    public Double getAmountReward() {        return AmountReward;    }
    public void setAmountReward(Double amountReward) {        AmountReward = amountReward;    }
    public Double getAmountDiscount() {        return AmountDiscount;    }
    public void setAmountDiscount(Double amountDiscount) {        AmountDiscount = amountDiscount;    }
    public Double getAmountSaved() {        return AmountSaved;    }
    public void setAmountSaved(Double amountSaved) {        AmountSaved = amountSaved;    }
    public List<PromotedOffer> getPromotedOffers() {        return PromotedOffers;    }
    public void setPromotedOffers(List<PromotedOffer> promotedOffers) {        PromotedOffers = promotedOffers;    }
}