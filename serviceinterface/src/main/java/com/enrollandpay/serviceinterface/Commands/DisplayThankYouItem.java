package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DisplayThankYouItem {
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Quantity")
    @Expose
    private Integer Quantity;
    @SerializedName("ProductId")
    @Expose
    private Integer ProductId;
    @SerializedName("AmountTotal")
    @Expose
    private Double AmountTotal;
    @SerializedName("PosIdent")
    @Expose
    private String PosIdent;
    @SerializedName("ParentTransactionItemIdent")
    @Expose
    private String ParentTransactionItemIdent;
    ///////////////////////////////////////////

    public String getName() {        return Name;    }
    public void setName(String name) {        Name = name;    }
    public Integer getQuantity() {        return Quantity;    }
    public void setQuantity(Integer quantity) {        Quantity = quantity;    }
    public Double getAmountTotal() {        return AmountTotal;    }
    public void setAmountTotal(Double amountTotal) {        AmountTotal = amountTotal;    }
    public Integer getProductId() {        return ProductId;    }
    public void setProductId(Integer productId) {        ProductId = productId;    }
    public String getPosIdent() {        return PosIdent;    }
    public void setPosIdent(String posIdent) {        PosIdent = posIdent;    }
    public String getParentTransactionItemIdent() {        return ParentTransactionItemIdent;    }
    public void setParentTransactionItemIdent(String parentTransactionItemIdent) {        ParentTransactionItemIdent = parentTransactionItemIdent;    }
}
