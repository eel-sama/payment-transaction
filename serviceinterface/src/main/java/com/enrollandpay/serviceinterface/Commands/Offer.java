package com.enrollandpay.serviceinterface.Commands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Offer {
    @SerializedName("OrderOfferId")
    @Expose
    /// <summary>Internal Offer Id </summary>
    private Integer OrderOfferId;
    @SerializedName("Name")
    @Expose
    /// <summary>Display Name of Offer</summary>
    private String Name;
    @SerializedName("Description")
    @Expose
    /// <summary>Description</summary>
    private String Description;
    @SerializedName("TimesUsed")
    @Expose
    private Integer TimesUsed;
    @SerializedName("MaxTimes")
    @Expose
    private Integer MaxTimes;
    @SerializedName("WasDeclined")
    @Expose
    private Boolean WasDeclined;
    @SerializedName("AutoAccepted")
    @Expose
    private Boolean AutoAccepted;
    //////////////////////////////////////////////

    public Integer getOrderOfferId() {
        return OrderOfferId;
    }

    public void setOrderOfferId(Integer orderOfferId) {
        OrderOfferId = orderOfferId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Integer getTimesUsed() {
        return TimesUsed;
    }

    public void setTimesUsed(Integer timesUsed) {
        TimesUsed = timesUsed;
    }

    public Integer getMaxTimes() {
        return MaxTimes;
    }

    public void setMaxTimes(Integer maxTimes) {
        MaxTimes = maxTimes;
    }

    public Boolean getWasDeclined() {
        return WasDeclined;
    }

    public void setWasDeclined(Boolean wasDeclined) {
        WasDeclined = wasDeclined;
    }

    public Boolean getAutoAccepted() {
        return AutoAccepted;
    }

    public void setAutoAccepted(Boolean autoAccepted) {
        AutoAccepted = autoAccepted;
    }
}
