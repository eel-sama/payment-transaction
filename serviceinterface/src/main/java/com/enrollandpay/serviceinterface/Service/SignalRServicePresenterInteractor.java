package com.enrollandpay.serviceinterface.Service;

import android.content.Context;

/**
 * Created by shelendrak on 2/28/2018.
 */

public interface SignalRServicePresenterInteractor {
    /**
     * @param :context
     * @desc : This function is used to start Signal R from Service.
     */
    void startSignalR(Context context, String jwt);
}
