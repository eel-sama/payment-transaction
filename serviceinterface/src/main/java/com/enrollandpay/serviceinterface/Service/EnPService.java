package com.enrollandpay.serviceinterface.Service;

import android.app.Application;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.Nullable;

import com.auth0.android.jwt.JWT;
import com.enrollandpay.serviceinterface.BusinessEntity.Category;
import com.enrollandpay.serviceinterface.BusinessEntity.Employee;
import com.enrollandpay.serviceinterface.BusinessEntity.Extended.OrderPaymentBatch;
import com.enrollandpay.serviceinterface.BusinessEntity.Location;
import com.enrollandpay.serviceinterface.BusinessEntity.Order;
import com.enrollandpay.serviceinterface.BusinessEntity.Product;
import com.enrollandpay.serviceinterface.BusinessEntity.Tabletop;
import com.enrollandpay.serviceinterface.Commands.Command;
import com.enrollandpay.serviceinterface.GlobalVariables;
import com.enrollandpay.serviceinterface.Models.Consumer.ConsumerResponse;
import com.enrollandpay.serviceinterface.Models.Location.LocationOffer;
import com.enrollandpay.serviceinterface.Models.Order.BatchStats;
import com.enrollandpay.serviceinterface.Models.Security.Credentials;
import com.enrollandpay.serviceinterface.Models.Transaction.AppliedDiscount;
import com.enrollandpay.serviceinterface.Models.Transaction.CardPresented;
import com.enrollandpay.serviceinterface.Models.Transaction.DiscountAcceptDecline;
import com.enrollandpay.serviceinterface.Models.Transaction.Enrollment;
import com.enrollandpay.serviceinterface.Models.Transaction.EnrollmentAccept;
import com.enrollandpay.serviceinterface.Models.Transaction.Error;
import com.enrollandpay.serviceinterface.Models.Transaction.PaymentAdjusted;
import com.enrollandpay.serviceinterface.Models.Transaction.PaymentDecision;
import com.enrollandpay.serviceinterface.Models.Transaction.PaymentRequest;
import com.enrollandpay.serviceinterface.Models.Transaction.QuestionAnswer;
import com.enrollandpay.serviceinterface.Models.Transaction.SendReceipt;
import com.enrollandpay.serviceinterface.Models.Transaction.Transaction;
import com.enrollandpay.serviceinterface.Transaction.Request;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.OkHttpClient;

public class EnPService extends Service implements SignalRServiceInteractor {
    String newresponse = null;
    private SignalRServicePresenter mSignalRServicePresenter;
    private IBinder Binder = new EnPServiceBinder();

    public EnPService() {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return Binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mSignalRServicePresenter = new SignalRServicePresenter(this);
        mSignalRServicePresenter.startSignalR(EnPService.this, Utility.getJWT(EnPService.this));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        Log.e(GlobalVariables.TAG, "onDestroy");
        super.onDestroy();
        Intent broadcastIntent = new Intent(GlobalVariables.SERVICEINTENT);
        sendBroadcast(broadcastIntent);
    }


    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return true;
    }

    /*
    method for filtering the signal R command message
     */
    @Override
    public void setSignalRSuccess(String response) {
        if (newresponse != null && newresponse.equalsIgnoreCase(response)) {
            return;
        }
        newresponse = response;
        Log.v(GlobalVariables.TAG, response);
        String newresponse = response;
        Intent intent = new Intent();
        intent.setAction(GlobalVariables.Service_Intent_Message);
        intent.putExtra(GlobalVariables.MESSAGE_DATA, response);
        try {
            JSONObject obj;
            obj = new JSONObject(response);
            TransactionEnum transactionEnum = TransactionEnum.values()[obj.getInt(GlobalVariables.COMMAND_TYPE) - 1];
            switch (transactionEnum) {
                case DISPLAYENROLLMENT:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.DISPLAYENROLLMENT);
                    break;
                case DISPLAYREVISEDBILL:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.DISPLAYREVISEDBILL);
                    break;
                case DISPLAYTRANSACTION:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.DISPLAYTRANSACTION);
                    break;
                case REQUESTCARD:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.REQUESTCARD);
                    break;
                case DISPLAYDISCOUNTSELECT:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.DISPLAYDISCOUNTSELECT);
                    break;
                case DISPLAYQUESTION:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.DISPLAYQUESTION);
                    break;
                case DISPLAYCARDADDED:
                case DISPLAYTHANKYOU:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.DISPLAYTHANKYOU);
                    break;
                case REQUESTAUTHORIZATION:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.REQUESTAUTHORIZATION);
                    break;
                case PAYMENTCOMPLETE:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.PAYMENTCOMPLETE);
                    break;
                case APPLYOFFER:
                case APPLYREWARD:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.APPLYDISCOUNT);
                    return;
                case ApplyDiscount:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.APPLYDISCOUNT);
                    break;
                case CardPresented:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.CARDPRESENTED);
                    break;
                case PaymentRequested:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.PAYMENTREQUESTED);
                    break;
                case DiscountApplied:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.DISCOUNTAPPLIED);
                    break;
                case OrderCompleted:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.ORDERCOMPLETED);
                    break;
                case RequestPaymentAdjustment:
                    intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.PAYMENTADJUST);
                    break;
            }
            sendBroadcast(intent);
            return;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * This function is used to return Error, get from Webservice / Signal R.
     *
     * @param failError
     */
    @Override
    public void setSignalRSFailureResponse(String failError) {

    }
    /**
     * method for getting jwt value
     *
     * @param jwt
     */
    @Override
    public void setJwtUpdate(String jwt) {
        Utility.saveJWT(EnPService.this, jwt);
        if (_isAuthorizing){
            authorized();
            _isAuthorizing = false;
        }
        jwtChanged();
        //broadcastJwtUpdate(jwt);
    }

    @Override
    public void setConnectionIdUpdate(String connectionId) {
        Utility.saveConnectionId(EnPService.this, connectionId);
    }

    @Override
    public void setSignalRAuthorizationResponse(com.enrollandpay.serviceinterface.Models.Security.AuthorizeResponse rAuthorizationResponse) {
        broadcastAuthorization(rAuthorizationResponse);
    }
    private static Credentials CurrentCredentials;
    public Credentials getCurrentCredentials() {        return CurrentCredentials;    }
    @Override
    public void onSignalRAuthenticated(Integer deviceId, String deviceName, Integer merchantId, Integer locationId){
        Credentials credentials= new Credentials();
        credentials.setDeviceId(deviceId);
        credentials.setDeviceName(deviceName);
        credentials.setLocationId(locationId);
        credentials.setMerchantId(merchantId);
        CurrentCredentials = credentials;
        try {
            Intent intent = new Intent();
            intent.setAction(GlobalVariables.Service_Intent_Message);
            intent.putExtra(GlobalVariables.MESSAGE_DATA, new Gson().toJson(credentials));
            intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.AUTHORIZATION_ISAUTHORIZED);
            sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void CardPresented(CardPresented model, ResponseCallback callback){
        if (model != null && model.getRequestType() == Request.RequestTypes.OpenTab.getValue() && model.getInformation() != null && model.getInformation().getAdditionalAttributes() != null && !model.getInformation().getAdditionalAttributes().containsKey("OPENTAB")){
            model.getInformation().getAdditionalAttributes().put("OPENTAB","true");
        }
        if (Utility.isPOSDevice(getApplicationContext())){
            model.setOverridePosDeviceId(Utility.DeviceId(getApplicationContext()));
        }
        model.setOverridePaymentDeviceId(Utility.DeviceId(getApplicationContext()));
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_TRANSACTION_CARD_PRESENTED, model, callback);
    }
    /*public void ReturnRequested(Transaction model, ResponseCallback callback){
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_TRANSACTION_RETURN_REQUESTED, model, callback);
    }*/
    public void PaymentCancel(Transaction model, ResponseCallback callback) {
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_PAYMENT_CANCEL_REQUEST, model, callback);
    }
    public void PaymentCancelled(Transaction model, ResponseCallback callback) {
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_PAYMENT_CANCEL, model, callback);
    }
    public void PaymentVoid(Transaction model, ResponseCallback callback){
        if (Utility.isPOSDevice(getApplicationContext())){
            model.setOverridePosDeviceId(Utility.DeviceId(getApplicationContext()));
        }
        model.setOverridePaymentDeviceId(Utility.DeviceId(getApplicationContext()));
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_TRANSACTION_VOID, model, callback);
    }
    public void PaymentVoided(Transaction model, ResponseCallback callback){
        if (Utility.isPOSDevice(getApplicationContext())){
            model.setOverridePosDeviceId(Utility.DeviceId(getApplicationContext()));
        }
        model.setOverridePaymentDeviceId(Utility.DeviceId(getApplicationContext()));
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_TRANSACTION_PAYMENT_VOIDED, model, callback);
    }
    public void AcceptEnroll(EnrollmentAccept model, ResponseCallback callback){
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_ACCEPT_ENROLLMENT, model, callback);
    }
    public void DeclineEnroll(Enrollment model, ResponseCallback callback){
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_DECLINE_ENROLLMENT, model, callback);
    }
    public void PaymentRequested(PaymentRequest model, ResponseCallback callback){
        if (model != null && model.getRequestType() == Request.RequestTypes.OpenTab.getValue() && model.getInformation() != null && model.getInformation().getAdditionalAttributes() != null && !model.getInformation().getAdditionalAttributes().containsKey("OPENTAB")){
            model.getInformation().getAdditionalAttributes().put("OPENTAB","true");
        }
        if (Utility.isPOSDevice(getApplicationContext())){
            model.setOverridePosDeviceId(Utility.DeviceId(getApplicationContext()));
        }
        model.setOverridePaymentDeviceId(Utility.DeviceId(getApplicationContext()));
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_PAYMENT_REQUEST, model, callback);
    }
    public void DiscountApplied(AppliedDiscount model, ResponseCallback callback){
        if (Utility.isPOSDevice(getApplicationContext())){
            model.setOverridePosDeviceId(Utility.DeviceId(getApplicationContext()));
        }
        model.setOverridePaymentDeviceId(Utility.DeviceId(getApplicationContext()));
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_DISCOUNT_APPLIED, model, callback);
    }
    public void PaymentDecision(PaymentDecision model, ResponseCallback callback){
        if (model != null && model.getRequestType() == Request.RequestTypes.OpenTab.getValue() && model.getInformation() != null && model.getInformation().getAdditionalAttributes() != null && !model.getInformation().getAdditionalAttributes().containsKey("OPENTAB")){
            model.getInformation().getAdditionalAttributes().put("OPENTAB","true");
        }
        if (Utility.isPOSDevice(getApplicationContext())){
            model.setOverridePosDeviceId(Utility.DeviceId(getApplicationContext()));
        }
        model.setOverridePaymentDeviceId(Utility.DeviceId(getApplicationContext()));
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_PAYMENT_DECISION, model, callback);
    }
    public void PaymentAdjusted(PaymentAdjusted model, ResponseCallback callback){
        if (Utility.isPOSDevice(getApplicationContext())){
            model.setOverridePosDeviceId(Utility.DeviceId(getApplicationContext()));
        }
        model.setOverridePaymentDeviceId(Utility.DeviceId(getApplicationContext()));
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_PAYMENT_ADJUSTED, model, callback);
    }
    public void TransactionComplete(Transaction model, ResponseCallback callback){
        if (Utility.isPOSDevice(getApplicationContext())){
            model.setOverridePosDeviceId(Utility.DeviceId(getApplicationContext()));
        }
        model.setOverridePaymentDeviceId(Utility.DeviceId(getApplicationContext()));
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_TRANSACTION_COMPLETE, model, callback);
    }
    public void TransactionCancel(Transaction model, ResponseCallback callback){
        if (Utility.isPOSDevice(getApplicationContext())){
            model.setOverridePosDeviceId(Utility.DeviceId(getApplicationContext()));
        }
        model.setOverridePaymentDeviceId(Utility.DeviceId(getApplicationContext()));
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_TRANSACTION_VOID, model, callback);
    }
    public OkHttpClient GetHttpClient() {
        return mSignalRServicePresenter.GetHttpClient();
    }
    public void TransactionManage(Transaction model, ResponseCallback callback){
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_TRANSACTION_MANAGE, model, callback);
    }
    public void OrderDetail(Integer orderId, ResponseCallback callback){
        mSignalRServicePresenter.GetRequest(GlobalVariables.WEB_SERVICE_FOR_ORDER_DETAIL.replace(GlobalVariables.WEB_SERVICE_PARAM_ORDERID, orderId.toString()), Order.class, callback);
    }
    public void AnswerQuestion(QuestionAnswer model, ResponseCallback callback){
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_QUESTION_ANSWER, model, callback);
    }
    public void DiscountAccepted(DiscountAcceptDecline model, ResponseCallback callback){
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_DISCOUNT_ACCEPT, model, callback);
    }
    public void DiscountDeclineAll(DiscountAcceptDecline model, ResponseCallback callback){
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_DISCOUNT_DECLINEALL, model, callback);
    }
    public void SendReceipt(SendReceipt model, ResponseCallback callback){
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_SEND_RECEIPT, model, callback);
    }
    public void SendVoid(Transaction model, ResponseCallback callback){ 
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_SEND_VOID, model, callback);
    }
    public void Batch(BatchStats model, ResponseCallback callback){
        mSignalRServicePresenter.SubmitRequestGeneric(GlobalVariables.WEB_SERVICE_FOR_BATCH, model, OrderPaymentBatch.class, callback);
    }
    public void OverviewByLookup(String emailAddress, String phoneNumber, String encryptedCardValue, String par, Integer locationId, ResponseCallback callback){
        String url = GlobalVariables.WEB_SERVICE_FOR_CONSUMER_OVERVIEWBYLOOKUP;
        Boolean isFirst = true;
        try {
            if (locationId != null) {
                url += (isFirst ? "?" : "&") + GlobalVariables.WEB_SERVICE_PARAM_SET_LOCATIONID.replace(GlobalVariables.WEB_SERVICE_PARAM_LOCATIONID, locationId.toString());
                isFirst = false;
            }
            if (emailAddress != null) {
                url += (isFirst ? "?" : "&") + GlobalVariables.WEB_SERVICE_PARAM_SET_EMAIL_ADDRESS.replace(GlobalVariables.WEB_SERVICE_PARAM_EMAIL_ADDRESS, URLEncoder.encode(emailAddress, "utf-8"));
                isFirst = false;
            }
            if (phoneNumber != null) {
                url += (isFirst ? "?" : "&") + GlobalVariables.WEB_SERVICE_PARAM_SET_PHONE_NUMBER.replace(GlobalVariables.WEB_SERVICE_PARAM_PHONE_NUMBER, URLEncoder.encode(phoneNumber, "utf-8"));
                isFirst = false;
            }
            if (encryptedCardValue != null) {
                url += (isFirst ? "?" : "&") + GlobalVariables.WEB_SERVICE_PARAM_SET_ENCRYPTED_CARD_VALUE.replace(GlobalVariables.WEB_SERVICE_PARAM_ENCRYPTED_CARD_VALUE, URLEncoder.encode(encryptedCardValue, "utf-8"));
                isFirst = false;
            }
            if (par != null) {
                url += (isFirst ? "?" : "&") + GlobalVariables.WEB_SERVICE_PARAM_SET_PAR.replace(GlobalVariables.WEB_SERVICE_PARAM_PAR, URLEncoder.encode(par, "utf-8"));
            }
        }
        catch (Exception ex){

        }
        mSignalRServicePresenter.GetRequest(url, ConsumerResponse.class, callback);
    }
    public void LogError(String error, ResponseCallback callback){
        Error errorObj = new Error();
        errorObj.setDebugInfo(error);
        mSignalRServicePresenter.SubmitRequestGeneric(GlobalVariables.WEB_SERVICE_FOR_TRANSACTION_ERROR, errorObj, String.class, callback);
    }
    public void EmployeeOverviews(Integer locationId, Date reportDate, ResponseCallback callback){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        mSignalRServicePresenter.GetRequest(GlobalVariables.WEB_SERVICE_FOR_EMPLOYEEOVERVIEWS.replace(GlobalVariables.WEB_SERVICE_PARAM_SALES_DATE, format.format(reportDate)).replace(GlobalVariables.WEB_SERVICE_PARAM_LOCATIONID, locationId.toString()), com.enrollandpay.serviceinterface.Models.Employee.OverviewResponse.class, callback);
    }
    public void LocationOffers(Integer locationId, Integer consumerId, ResponseCallback callback){
        mSignalRServicePresenter.GetRequestList(GlobalVariables.WEB_SERVICE_FOR_CONSUMER_LOCATIONOFFERS.replace(GlobalVariables.WEB_SERVICE_PARAM_CONSUMERID, consumerId.toString()).replace(GlobalVariables.WEB_SERVICE_PARAM_LOCATIONID, locationId.toString()), LocationOffer.class, callback);
    }
    public void LocationDetail(Integer locationId, ResponseCallback callback){
        mSignalRServicePresenter.GetRequest(GlobalVariables.WEB_SERVICE_FOR_LOCATION_DETAIL.replace(GlobalVariables.WEB_SERVICE_PARAM_LOCATIONID, locationId.toString()), Location.class, callback);
    }
    public void LocationEmployees(Integer locationId, ResponseCallback callback){
        mSignalRServicePresenter.GetRequestList(GlobalVariables.WEB_SERVICE_FOR_LOCATION_EMPLOYEES.replace(GlobalVariables.WEB_SERVICE_PARAM_LOCATIONID, locationId.toString()), Employee.class, callback);
    }
    public void LocationProducts(Integer locationId, ResponseCallback callback){
        mSignalRServicePresenter.GetRequestList(GlobalVariables.WEB_SERVICE_FOR_LOCATION_PRODUCTS.replace(GlobalVariables.WEB_SERVICE_PARAM_LOCATIONID, locationId.toString()), Product.class, callback);
    }
    public void LocationCategories(Integer locationId, ResponseCallback callback){
        mSignalRServicePresenter.GetRequestList(GlobalVariables.WEB_SERVICE_FOR_LOCATION_CATEGORIES.replace(GlobalVariables.WEB_SERVICE_PARAM_LOCATIONID, locationId.toString()), Category.class, callback);
    }
    public void LocationTabletops(Integer locationId, ResponseCallback callback){
        mSignalRServicePresenter.GetRequestList(GlobalVariables.WEB_SERVICE_FOR_LOCATION_TABLETOPS.replace(GlobalVariables.WEB_SERVICE_PARAM_LOCATIONID, locationId.toString()), Tabletop.class, callback);
    }
    public void SearchOrderPayments(String token, String par, String transactionIdent, String phoneNumber, String emailAddress, Date searchDate, String batchIdent,
                                    Integer pageNumber, Integer displayCount, String tableTopID, String employeeID, Integer status, String openTabName, ResponseCallback callback){
        String url = GlobalVariables.WEB_SERVICE_FOR_ORDERPAYMENT_SEARCH + "?";
        String prefixUrl = "";
        try{
            if (token != null) {
                url = url + prefixUrl + "uniqueCardIdent=" + URLEncoder.encode(token, "utf-8");
                prefixUrl = "&";
            }
            if (par != null){
                url = url + prefixUrl + "uniqueCardPar=" + URLEncoder.encode(par, "utf-8");
                prefixUrl = "&";
            }
            if (transactionIdent != null){
                url = url + prefixUrl + "posIdent=" + URLEncoder.encode(transactionIdent, "utf-8");
                prefixUrl = "&";
            }
            if (phoneNumber != null){
                url = url + prefixUrl + "phoneNumber=" + URLEncoder.encode(phoneNumber, "utf-8");
                prefixUrl = "&";
            }
            if (emailAddress != null){
                url = url + prefixUrl + "emailAddress=" + URLEncoder.encode(emailAddress, "utf-8");
                prefixUrl = "&";
            }
            if (status != null) {
                // Status
                // Open = 1,
                // Canceled = 2,
                // Completed = 4,
                // Voided = 8
                // OpenTab = 4194304
                url = url + prefixUrl + "statusType=" + URLEncoder.encode(status.toString(), "utf-8");
                prefixUrl = "&";
            }
            if (searchDate != null){
                url = url + prefixUrl + "startDate=" + URLEncoder.encode(Utility.ExportDate(searchDate), "utf-8");
                prefixUrl = "&";
                url = url + prefixUrl + "endDate=" + URLEncoder.encode(Utility.ExportDate(searchDate), "utf-8");
            }
            if (batchIdent != null){
                url = url + prefixUrl + "batchIdent=" + URLEncoder.encode(batchIdent, "utf-8");
                prefixUrl = "&";
            }
            if (pageNumber != null){
                url = url + prefixUrl + "pageNumber=" + pageNumber.toString();
                prefixUrl = "&";
            }
            if (openTabName != null){
                url = url + prefixUrl + "openTabName=" + URLEncoder.encode(openTabName, "utf-8");
                prefixUrl = "&";
            }
            // Add tabletopId, employeeID
            if(tableTopID != null) {
                url = url + prefixUrl + "tabletopId=" + URLEncoder.encode(tableTopID, "utf-8");
                prefixUrl = "&";
            }
            if(employeeID != null) {
                url = url + prefixUrl + "employeeId=" + URLEncoder.encode(employeeID, "utf-8");
                prefixUrl = "&";
            }
            // Add tabletopId, employeeID

            if (displayCount != null){
                url = url + prefixUrl + "displayCount=" + displayCount.toString();
            }
        }
        catch (Exception ex){

        }
        mSignalRServicePresenter.GetRequest(url, callback);
    }
    public void SearchOrders(String token, String par, String transactionIdent, String phoneNumber, String emailAddress,
                             Date searchDate, String batchIdent, Integer status, Integer pageNumber, Integer displayCount, String tableTopID, String employeeID, String openTabName, ResponseCallback callback){
//        String url = GlobalVariables.WEB_SERVICE_FOR_ORDERPAYMENT_SEARCH + "?";
        String url = GlobalVariables.WEB_SERVICE_FOR_ORDER_SEARCH + "?";
        String prefixUrl = "";
        try{
            if (token != null) {
                url = url + prefixUrl + "uniqueCardIdent=" + URLEncoder.encode(token, "utf-8");
                prefixUrl = "&";
            }
            if (par != null){
                url = url + prefixUrl + "uniqueCardPar=" + URLEncoder.encode(par, "utf-8");
                prefixUrl = "&";
            }
            if (transactionIdent != null){
                url = url + prefixUrl + "posIdent=" + URLEncoder.encode(transactionIdent, "utf-8");
                prefixUrl = "&";
            }
            if (phoneNumber != null){
                url = url + prefixUrl + "phoneNumber=" + URLEncoder.encode(phoneNumber, "utf-8");
                prefixUrl = "&";
            }
            if (emailAddress != null){
                url = url + prefixUrl + "emailAddress=" + URLEncoder.encode(emailAddress, "utf-8");
                prefixUrl = "&";
            }
            if (status != null) {
                // Status
                // Open = 1,
                // Canceled = 2,
                // Completed = 4,
                // Voided = 8
                // OpenTab = 4194304
                url = url + prefixUrl + "statusType=" + URLEncoder.encode(status.toString(), "utf-8");
                prefixUrl = "&";
            }
            if (searchDate != null){
                url = url + prefixUrl + "startDate=" + URLEncoder.encode( Utility.ExportDate(searchDate), "utf-8");
                prefixUrl = "&";
                url = url + prefixUrl + "endDate=" + URLEncoder.encode(Utility.ExportDate(searchDate), "utf-8");
            }
            if (batchIdent != null){
                url = url + prefixUrl + "batchIdent=" + URLEncoder.encode(batchIdent, "utf-8");
                prefixUrl = "&";
            }
            if (openTabName != null){
                url = url + prefixUrl + "openTabName=" + URLEncoder.encode(openTabName, "utf-8");
                prefixUrl = "&";
            }
            if (pageNumber != null){
                url = url + prefixUrl + "pageNumber=" + pageNumber.toString();
                prefixUrl = "&";
            }

            // Add tabletopId, employeeID
            if(tableTopID != null) {
                url = url + prefixUrl + "tabletopId=" + URLEncoder.encode(tableTopID, "utf-8");
                prefixUrl = "&";
            }
            if(employeeID != null) {
                url = url + prefixUrl + "saleEmployeeId=" + URLEncoder.encode(employeeID, "utf-8");
                prefixUrl = "&";
            }
            // Add tabletopId, employeeID

            if (displayCount != null){
                url = url + prefixUrl + "displayCount=" + displayCount.toString();
            }
        }
        catch (Exception ex){

        }
        mSignalRServicePresenter.GetRequest(url, callback);
    }
    public void GetCurrentCommand(ResponseCallback callback){
        mSignalRServicePresenter.GetRequest(GlobalVariables.WEB_SERVICE_FOR_COMMAND_CURRENT, Command.class, callback);
    }
    public void GetCurrentCommand(Integer OrderID, ResponseCallback callback){
        Transaction transaction = new Transaction();
        transaction.setOrderId(OrderID);
        mSignalRServicePresenter.SubmitRequest(GlobalVariables.WEB_SERVICE_FOR_COMMAND_CURRENT, transaction, callback);
    }
    /**
     * it will brodacst the updated jwt value
     *
     * @param jwt
     */
    private void broadcastJwtUpdate(String jwt) {
        try {
            Intent intent = new Intent();
            intent.setAction(GlobalVariables.Service_Intent_Message);
            intent.putExtra(GlobalVariables.MESSAGE_DATA, Base64.encodeToString(Utility.encryptJwt(jwt), Base64.DEFAULT));
            //intent.putExtra(GlobalVariables.MESSAGE_DATA, jwt);
            intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.JWT_CHANGE);
            sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void authorized() {
        try {
            Intent intent = new Intent();
            intent.setAction(GlobalVariables.Service_Intent_Message);
            intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.AUTHORIZATION_REQUEST_APPROVED);
            sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void jwtChanged() {
        try {
            Intent intent = new Intent();
            intent.setAction(GlobalVariables.Service_Intent_Message);
            intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.JWT_CHANGED);
            sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    Boolean _isAuthorizing = false;
    /**
     * method to open the authirisation screen
     *
     * @param authorizeResponse
     */
    private void broadcastAuthorization(com.enrollandpay.serviceinterface.Models.Security.AuthorizeResponse authorizeResponse) {
        _isAuthorizing = true;
        Intent intent = new Intent();
        intent.setAction(GlobalVariables.Service_Intent_Message);
        intent.putExtra(GlobalVariables.MESSAGE_DATA, new Gson().toJson(authorizeResponse));
        intent.putExtra(GlobalVariables.MESSAGE_TYPE, GlobalVariables.AUTHORIZATION_REQUEST);
        sendBroadcast(intent);
    }
    public class EnPServiceBinder extends Binder{
        public EnPService getService(){
            return EnPService.this;
        }
    }
}