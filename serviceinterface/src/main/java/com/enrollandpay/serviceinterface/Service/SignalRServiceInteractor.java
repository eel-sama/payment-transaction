package com.enrollandpay.serviceinterface.Service;

import com.enrollandpay.serviceinterface.Models.Security.AuthorizeResponse;
import com.enrollandpay.serviceinterface.Models.Security.Credentials;

public interface SignalRServiceInteractor {
    /**
     * used to return success  response  returned by Signal R .
     */
    void setSignalRSuccess(String response);
    /**
     * used to show Signal R failure error.
     */
    void setSignalRSFailureResponse(String fail);
    /**
     * used to update JWT Token.
     */
    void setJwtUpdate(String jwt);
    /**
     * Used to update ConnectionId.
     */
    void setConnectionIdUpdate(String connectionId);
    /**
     * used to show Signal R Authorization Response.
     */
    void setSignalRAuthorizationResponse(AuthorizeResponse rAuthorizationResponse);

    void onSignalRAuthenticated(Integer deviceId, String deviceName, Integer merchantId, Integer locationId);

    Credentials getCurrentCredentials();
}
