package com.enrollandpay.serviceinterface.Service;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.auth0.android.jwt.Claim;
import com.auth0.android.jwt.JWT;
import com.enrollandpay.serviceinterface.GlobalVariables;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;


public class Utility {
    public static String password = "pay@enroll#:!@%$";
    public static String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    /**
     * This is a function used to save JWT in SharedPreference.
     *
     * @param context
     * @param jwt
     */
    public static void saveJWT(Context context,
                               String jwt) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                GlobalVariables.APP_PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedPrefEditor = sharedPreferences.edit();
        sharedPrefEditor.putString(GlobalVariables.APP_PREFS_JWT,
                jwt == null ? null : jwt.trim());

        sharedPrefEditor.apply();
    }

    /**
     * This is a function used to get JWT from SharedPreference.
     *
     * @param context
     * @return
     */
    public static String getJWT(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                GlobalVariables.APP_PREFS_NAME, Context.MODE_PRIVATE);
        //saveJWT(context,null);
        return sharedPreferences.getString(GlobalVariables.APP_PREFS_JWT, null);
    }
    /**
     * This is a function used to save JWT in SharedPreference.
     *
     * @param context
     * @param connectionId
     */
    public static void saveConnectionId(Context context,
                                        String connectionId) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                GlobalVariables.APP_PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedPrefEditor = sharedPreferences.edit();
        sharedPrefEditor.putString(GlobalVariables.APP_PREFS_CONNECTION_ID,
                connectionId.trim());
        sharedPrefEditor.apply();
    }

    /**
     * This is a function used to get JWT from SharedPreference.
     *
     * @param context
     * @return
     */
    public static String getConnectionId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                GlobalVariables.APP_PREFS_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(GlobalVariables.APP_PREFS_CONNECTION_ID, null);
    }
    public static String ExportDate(Date date){
        if (date == null){return null;}
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        return format.format(date);
    }
    public static String DateFormat(Date date) {
        return DateFormat(date, "yyyy-MM-dd'T'HH:mm:ss");
    }
    public static String DateFormat(Date date, String formatPattern) {
        if (date == null){return null;}
        SimpleDateFormat sdf = new SimpleDateFormat(formatPattern);
        sdf.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        return sdf.format(date);
    }
    public static SecretKey generateKey()
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        return new SecretKeySpec(password.getBytes(), "AES");
    }

    public static byte[] encryptJwt(String message)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidKeySpecException {
        /* Encrypt the message. */
        Cipher cipher = null;
        cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, generateKey());
        byte[] cipherText = cipher.doFinal(message.getBytes("UTF-8"));

        return cipherText;
    }
    public static Boolean isPOSDevice(Context context)
    {
        JWT jwt = new JWT(getJWT(context));
        Claim claim = jwt.getClaim("DeviceType");
        if (claim != null){
            String deviceType = claim.asString();
            return (deviceType != null && deviceType.indexOf("POS") > -1);
        }
        return false;
    }
    public static Integer DeviceId(Context context)
    {
        JWT jwt = new JWT(getJWT(context));
        Claim claim = jwt.getClaim("DeviceId");
        if (claim != null){
            Integer deviceId = claim.asInt();
            return deviceId;
        }
        return null;
    }
    public static boolean isNetworkAvailable(Context context) {
        boolean _isNetAvailable = false;
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm
                .getActiveNetworkInfo();
        if (wifiNetwork != null) {
            _isNetAvailable = wifiNetwork.isConnectedOrConnecting();
        }

        NetworkInfo mobileNetwork = cm
                .getActiveNetworkInfo();
        if (mobileNetwork != null) {
            _isNetAvailable = mobileNetwork.isConnectedOrConnecting();
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            _isNetAvailable = activeNetwork.isConnectedOrConnecting();
        }
        return _isNetAvailable;
    }
}