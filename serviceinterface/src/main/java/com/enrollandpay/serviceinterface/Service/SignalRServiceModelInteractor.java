package com.enrollandpay.serviceinterface.Service;

import android.content.Context;

import com.enrollandpay.serviceinterface.Models.Security.AuthorizeResponse;

public interface SignalRServiceModelInteractor {

    /**
     * this interface used for Server response to Signal R.
     */
    interface OnSignalRFinishedListener {

        void onSignalRSuccess(String response);

        void onSignalRFailureResponse(String failer);

        void onSignalRJwtUpdate(String jwt);

        void onSignalRConnectionIdUpdate(String connectionId);

        void onSignalRAuthorizationResponse(AuthorizeResponse rAuthorizationResponse);

        void onSignalRAuthenticated(Integer deviceId, String deviceName, Integer merchantId, Integer locationId);
    }

    /**
     * This function is used for Pass the Service Parametr to Model class.
     *
     * @param context
     */
    void startSignal(Context context, String jwt, SignalRServiceModelInteractor.OnSignalRFinishedListener listener);
}