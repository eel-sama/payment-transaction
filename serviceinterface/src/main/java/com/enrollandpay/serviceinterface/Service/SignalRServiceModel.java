package com.enrollandpay.serviceinterface.Service;

import android.content.Context;
import android.util.Log;

import com.auth0.android.jwt.Claim;
import com.auth0.android.jwt.JWT;
import com.enrollandpay.serviceinterface.BusinessEntity.BaseBusinessEntity;
import com.enrollandpay.serviceinterface.BusinessEntity.Category;
import com.enrollandpay.serviceinterface.BusinessEntity.Consumer;
import com.enrollandpay.serviceinterface.BusinessEntity.Employee;
import com.enrollandpay.serviceinterface.BusinessEntity.EmployeeLocation;
import com.enrollandpay.serviceinterface.BusinessEntity.Entities;
import com.enrollandpay.serviceinterface.BusinessEntity.Location;
import com.enrollandpay.serviceinterface.BusinessEntity.Order;
import com.enrollandpay.serviceinterface.BusinessEntity.Product;
import com.enrollandpay.serviceinterface.BusinessEntity.Tabletop;
import com.enrollandpay.serviceinterface.Commands.Command;
import com.enrollandpay.serviceinterface.Commands.*;
import com.enrollandpay.serviceinterface.Models.Consumer.ConsumerResponse;
import com.enrollandpay.serviceinterface.Models.Location.LocationOffer;
import com.enrollandpay.serviceinterface.Models.LogEventModel;
import com.enrollandpay.serviceinterface.Models.Order.OrderResponse;
import com.enrollandpay.serviceinterface.Models.Security.AuthorizationRequest;
import com.enrollandpay.serviceinterface.Models.Security.Authorize;
import com.enrollandpay.serviceinterface.Models.Security.AuthorizeResponse;
import com.enrollandpay.serviceinterface.Models.ErrorResponse;
import com.enrollandpay.serviceinterface.Models.Security.Verify;
import com.enrollandpay.serviceinterface.Models.Security.VerifyResponse;
import com.enrollandpay.serviceinterface.Types;
import com.google.gson.Gson;


import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;
import com.enrollandpay.serviceinterface.GlobalVariables;
import com.enrollandpay.serviceinterface.R;

public class SignalRServiceModel implements SignalRServiceModelInteractor {
    private Boolean _isRunning = false;
    private String Domain;
    private Boolean UseSSL = true;
    private String ConnectionId = null;
    private Boolean ShouldReconnect = false;
    private Boolean IsAuthenticated = false;
    private Boolean IsConnected = false;
    private String JWT = "";
    private String ClientIdent = null;
    private String TerminalIdent = null;
    private String AccountIdent = null;
    private String LastMessageId = null;
    private String GroupsToken = null;
    private ArrayList<SignalRHub> Hubs = new ArrayList<>();
    private JSONObject NegotiatedInfo, obj;
    private WebSocket websocket = null;
    private OnSignalRFinishedListener listener;
    private Context context;

    public OkHttpClient getHttpClient() {
        return httpClient;
    }

    public void setHttpClient(OkHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    private  OkHttpClient httpClient = new OkHttpClient.Builder().connectTimeout(15,TimeUnit.SECONDS).writeTimeout(15,TimeUnit.SECONDS).readTimeout(30,TimeUnit.SECONDS).build();
    private Gson gson = new Gson();

    @Override
    public void startSignal(Context context, String jwt, OnSignalRFinishedListener listener) {
        this.listener = listener;
        this.context = context;
        ArrayList<SignalRHub> hubs = new ArrayList<>();
        hubs.add(new SignalRHub(GlobalVariables.Connection_hub));
        startToconnect(GlobalVariables.BASE_URL, false, hubs, jwt, GlobalVariables.ACCESS_TOKEN_CREDENTIALS, null, null);
    }

    public void startToconnect(String domain, Boolean useSSL, ArrayList<SignalRHub> hubs, String jwt, String clientIdent, String terminalIdent, String accountIdent) {
        if (this._isRunning) {
            return;
        }
        this.Domain = domain;
        this.UseSSL = useSSL;
        this.JWT = jwt == null ? null : jwt.replace(GlobalVariables.BEARER, "");
        this.Hubs = hubs;
        this._isRunning = true;
        this.ClientIdent = clientIdent;
        this.TerminalIdent = terminalIdent;
        this.AccountIdent = accountIdent;
        IsAuthenticated = !(jwt == null || jwt == "");

        Runnable r = new Runnable() {
            @Override
            public void run() {
                Negotiate();
            }
        };
        Thread connect = new Thread(r);
        connect.start();

    }
    public void GetRequest(String url,final ResponseCallback callback) {
        if (!(url.toLowerCase().startsWith("https://") || url.toLowerCase().startsWith("https://"))) {
            url = GetHttpUrl() + url;
        }
        Log.v(GlobalVariables.TAG, url);
        final String gotoUrl = url;
        Thread runnable = new Thread() {
            @Override
            public void run() {
                try {
                    Request request = GetRequest(gotoUrl);
                    Call call = httpClient.newCall(request);
                    Response response = call.execute();
                    String res = response.body().string();
                    String auth = response.headers().get(GlobalVariables.Authorization);
                    if (auth != null) {
                        auth = auth.replace(GlobalVariables.BEARER, "");
                        SetJWT(auth);
                    }
                    JSONObject jsonObject = new JSONObject(res);
                    callback.onSuccess(jsonObject);
                } catch (Exception ex) {
                    Log.v(GlobalVariables.TAG, ex.toString());
                    callback.onFailed(ex.toString());
                }
            }
        };
        runnable.start();
    }
    public <X> void GetRequestList(String url, final Class<X> classOfX,final ResponseCallback callback) {
        if (!(url.toLowerCase().startsWith("https://") || url.toLowerCase().startsWith("https://"))) {
            url = GetHttpUrl() + url;
        }
        Log.v(GlobalVariables.TAG, url);
        final String gotoUrl = url;
        Thread runnable = new Thread() {
            @Override
            public void run() {
                try {
                    Request request = GetRequest(gotoUrl);
                    Call call = httpClient.newCall(request);
                    Response response = call.execute();
                    String res = response.body().string();
                    String auth = response.headers().get(GlobalVariables.Authorization);
                    if (auth != null) {
                        auth = auth.replace(GlobalVariables.BEARER, "");
                        SetJWT(auth);
                    }
                    if (!response.isSuccessful()){
                        callback.onFailed(res);
                        return;
                    }
                    Log.v(GlobalVariables.TAG, res);
                    JSONObject jsonObject = new JSONObject(res);
                    if (classOfX.isAssignableFrom(LocationOffer.class)){
                        callback.onSuccess(LocationOffer.GetLocationOffers (jsonObject.getJSONArray("Collection")));
                    }
                    else if (classOfX.isAssignableFrom(Employee.class)){
                        callback.onSuccess(Entities.GetEmployees(jsonObject.getJSONArray("Collection")));
                    }
                    else if (classOfX.isAssignableFrom(Product.class)){
                        callback.onSuccess(Entities.GetProducts(jsonObject.getJSONArray("Collection")));
                    }
                    else if (classOfX.isAssignableFrom(Tabletop.class)){
                        callback.onSuccess(Entities.GetTabletops(jsonObject.getJSONArray("Collection")));
                    }
                    else if (classOfX.isAssignableFrom(Category.class)){
                        callback.onSuccess(Entities.GetCategories(jsonObject.getJSONArray("Collection")));
                    }
                    else {
                        callback.onSuccess(new Gson().fromJson(res, classOfX));
                    }
                } catch (Exception ex) {
                    Log.v(GlobalVariables.TAG, ex.toString());
                    callback.onFailed(ex.toString());
                }
            }
        };
        runnable.start();
    }
    public <X> void GetRequest(String url, final Class<X> classOfX,final ResponseCallback callback) {
        if (!(url.toLowerCase().startsWith("https://") || url.toLowerCase().startsWith("https://"))) {
            url = GetHttpUrl() + url;
        }
        Log.v(GlobalVariables.TAG, url);
        final String gotoUrl = url;
        Thread runnable = new Thread() {
            @Override
            public void run() {
                try {
                    Request request = GetRequest(gotoUrl);
                    Call call = httpClient.newCall(request);
                    Response response = call.execute();
                    String res = response.body().string();

                    EventBus.getDefault().post(new LogEventModel("GET", gotoUrl, "no data", res));

                    String auth = response.headers().get(GlobalVariables.Authorization);
                    if (auth != null) {
                        auth = auth.replace(GlobalVariables.BEARER, "");
                        SetJWT(auth);
                    }
                    if (!response.isSuccessful()){
                        callback.onFailed(res);
                        return;
                    }
                    Log.v(GlobalVariables.TAG, res);
                    if (res == null || res.compareTo("null")==0){
                        //If null is returned then we consider it a successful call and treat it that way.
                        callback.onSuccess(null);
                        return;
                    }
                    JSONObject jsonObject = new JSONObject(res);
                    if (classOfX.isAssignableFrom(Location.class)){
                        callback.onSuccess(Location.Deserialize(new HashMap<String,Object>(), jsonObject.getJSONObject("Location")));
                    }
                    else if (classOfX.isAssignableFrom(ConsumerResponse.class)){
                        ConsumerResponse consumerResponse = new ConsumerResponse();
                        if (jsonObject.has("Consumer")) {
                            consumerResponse.setConsumer(Consumer.Deserialize(new HashMap<String, Object>(), jsonObject.getJSONObject("Consumer")));
                        }
                        callback.onSuccess(consumerResponse);
                    }
                    else if (classOfX.isAssignableFrom(Order.class)){
                        Order order = null;
                        if (jsonObject.has("Order")) {
                            order = Order.Deserialize(new HashMap<String, Object>(), jsonObject.getJSONObject("Order"));
                        }
                        callback.onSuccess(order);
                    }
                    else if (classOfX == (new ArrayList<LocationOffer>()).getClass()){
                        callback.onSuccess(LocationOffer.GetLocationOffers (jsonObject.getJSONArray("Collection")));
                    }
                    else if (classOfX == (new ArrayList<Employee>()).getClass()){
                        callback.onSuccess(Entities.GetEmployees(jsonObject.getJSONArray("Collection")));
                    }
                    else if (classOfX.isAssignableFrom(Command.class)){
                        if (jsonObject != null && jsonObject.has(GlobalVariables.COMMAND_TYPE)){
                            Integer commandType = jsonObject.getInt(GlobalVariables.COMMAND_TYPE);
                            TransactionEnum transactionEnum = TransactionEnum.fromInteger(commandType);
                            switch (transactionEnum){
                                case Idle:
                                    callback.onSuccess(null);
                                    break;
                                case DISPLAYENROLLMENT:
                                    callback.onSuccess(new Gson().fromJson(res, CommandDisplayEnrollment.class));
                                    break;
                                case DISPLAYTRANSACTION:
                                    callback.onSuccess(new Gson().fromJson(res, CommandDisplayTransaction.class));
                                    break;
                                case REQUESTCARD:
                                    callback.onSuccess(new Gson().fromJson(res, CommandRequestCard.class));
                                    break;
                                case DISPLAYQUESTION:
                                    callback.onSuccess(new Gson().fromJson(res, CommandDisplayQuestionModel.class));
                                    break;
                                case DISPLAYTHANKYOU:
                                    callback.onSuccess(new Gson().fromJson(res, CommandDisplayThankYou.class));
                                    break;
                                case DISPLAYDISCOUNTSELECT:
                                    callback.onSuccess(new Gson().fromJson(res, CommandDisplayDiscountSelect.class));
                                    break;
                                case REQUESTAUTHORIZATION:
                                    callback.onSuccess(new Gson().fromJson(res, CommandRequestAuthorization.class));
                                    break;
                                case PAYMENTCOMPLETE:
                                    callback.onSuccess(new Gson().fromJson(res, CommandPaymentComplete.class));
                                    break;
                                case ApplyDiscount:
                                    callback.onSuccess(new Gson().fromJson(res, CommandApplyDiscount.class));
                                    break;
                                case CardPresented:
                                    callback.onSuccess(new Gson().fromJson(res, CommandCardPresented.class));
                                    break;
                                case PaymentRequested:
                                    callback.onSuccess(new Gson().fromJson(res, CommandPaymentRequested.class));
                                    break;
                                case DiscountApplied:
                                    callback.onSuccess(new Gson().fromJson(res, CommandDiscountApplied.class));
                                    break;
                                case OrderCompleted:
                                    callback.onSuccess(new Gson().fromJson(res, CommandOrderCompleted.class));
                                    break;
                                case RequestPaymentAdjustment:
                                    callback.onSuccess(new Gson().fromJson(res, CommandRequestPaymentAdjustment.class));
                                    break;
                                case PaymentVoid:
                                    callback.onSuccess(new Gson().fromJson(res, CommandPaymentVoid.class));
                                    break;
                                case PaymentCancel:
                                    callback.onSuccess(new Gson().fromJson(res, CommandPaymentCancel.class));
                                    break;
                                case TabOpened:
                                    callback.onSuccess(new Gson().fromJson(res, CommandTabOpened.class));
                                    break;
                            }
                        }
                    }
                    else {
                        callback.onSuccess(new Gson().fromJson(res, classOfX));
                    }
                } catch (Exception ex) {
                    Log.v(GlobalVariables.TAG, ex.toString());
                    callback.onFailed(ex.toString());
                }
            }
        };
        runnable.start();
    }
    public <T> void SubmitRequest(String url, final T model,final ResponseCallback callback)
    {
        if (!(url.toLowerCase().startsWith("https://") || url.toLowerCase().startsWith("https://"))){
            url = GetHttpUrl() + url;
        }
        final String gotoUrl = url;
//        Thread runnable = new Thread() {
//            @Override
//            public void run() {
                try {
                    String dataToSend =gson.toJson(model);
                    Log.v(GlobalVariables.TAG, gotoUrl);
                    Log.v(GlobalVariables.TAG, dataToSend);
                    Request request = JsonRequest(gotoUrl, dataToSend);
                    Call call = httpClient.newCall(request);
                    final String _dataToSend = dataToSend;
                    // .execute is thread blocking. enqueue is async
                    call.enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            String err = e.toString();
                            Log.e(GlobalVariables.ErrorMessage, e.getLocalizedMessage());
                            Log.e(GlobalVariables.ErrorMessage, err);
                            callback.onFailed(err);
                            return;
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {

//                            Response response = call.execute();
                            String res = response.body().string();
                            // SO -> https://stackoverflow.com/questions/60671465/retrofit-java-lang-illegalstateexception-closed
//                    String res = response.peekBody(2048).string();
                            // TODO - broadcast here eventbus so our mainactivity can listen to it!
                             EventBus.getDefault().post(new LogEventModel("POST", gotoUrl, _dataToSend, res));

                            String auth = response.headers().get(GlobalVariables.Authorization);
                            if (auth != null) {
                                auth = auth.replace(GlobalVariables.BEARER, "");
                                SetJWT(auth);
                            }
                            if (!response.isSuccessful()){
                                callback.onFailed(res);
                                return;
                            }
                            if (res == "" || res == null)
                            {
                                callback.onSuccess(null);
                                return;
                            }
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(res);

                                Log.v(GlobalVariables.TAG, res);
                                if (jsonObject != null && jsonObject.has(GlobalVariables.COMMAND_TYPE)){
                                    Integer commandType = jsonObject.getInt(GlobalVariables.COMMAND_TYPE);
                                    TransactionEnum transactionEnum = TransactionEnum.fromInteger(commandType);
                                    switch (transactionEnum){
                                        case DISPLAYENROLLMENT:
                                            callback.onSuccess(new Gson().fromJson(res, CommandDisplayEnrollment.class));
                                            return;
                                        case DISPLAYTRANSACTION:
                                            callback.onSuccess(new Gson().fromJson(res, CommandDisplayTransaction.class));
                                            return;
                                        case REQUESTCARD:
                                            callback.onSuccess(new Gson().fromJson(res, CommandRequestCard.class));
                                            return;
                                        case DISPLAYQUESTION:
                                            callback.onSuccess(new Gson().fromJson(res, CommandDisplayQuestionModel.class));
                                            return;
                                        case DISPLAYTHANKYOU:
                                            callback.onSuccess(new Gson().fromJson(res, CommandDisplayThankYou.class));
                                            return;
                                        case DISPLAYDISCOUNTSELECT:
                                            callback.onSuccess(new Gson().fromJson(res, CommandDisplayDiscountSelect.class));
                                            return;
                                        case REQUESTAUTHORIZATION:
                                            callback.onSuccess(new Gson().fromJson(res, CommandRequestAuthorization.class));
                                            return;
                                        case PAYMENTCOMPLETE:
                                            callback.onSuccess(new Gson().fromJson(res, CommandPaymentComplete.class));
                                            return;
                                        case ApplyDiscount:
                                            callback.onSuccess(new Gson().fromJson(res, CommandApplyDiscount.class));
                                            return;
                                        case CardPresented:
                                            callback.onSuccess(new Gson().fromJson(res, CommandCardPresented.class));
                                            return;
                                        case PaymentRequested:
                                            callback.onSuccess(new Gson().fromJson(res, CommandPaymentRequested.class));
                                            return;
                                        case DiscountApplied:
                                            callback.onSuccess(new Gson().fromJson(res, CommandDiscountApplied.class));
                                            return;
                                        case OrderCompleted:
                                            callback.onSuccess(new Gson().fromJson(res, CommandOrderCompleted.class));
                                            return;
                                        case RequestPaymentAdjustment:
                                            callback.onSuccess(new Gson().fromJson(res, CommandRequestPaymentAdjustment.class));
                                            return;
                                        case PaymentVoid:
                                            callback.onSuccess(new Gson().fromJson(res, CommandPaymentVoid.class));
                                            return;
                                        case PaymentCancel:
                                            callback.onSuccess(new Gson().fromJson(res, CommandPaymentCancel.class));
                                            return;
                                        case TabOpened:
                                            callback.onSuccess(new Gson().fromJson(res, CommandTabOpened.class));
                                            return;
                                        default:
                                            callback.onSuccess(null);
                                    }
                                }
                                else if (jsonObject != null && jsonObject.has(GlobalVariables.Response_Type)){
                                    Integer responseType = jsonObject.getInt(GlobalVariables.Response_Type);
                                    Types.RequestTypeEnum requestTypeEnum = Types.RequestTypeEnum.fromInteger(responseType);
                                    switch (requestTypeEnum){
                                        case OrderInformation:
                                            OrderResponse orderResponse = new OrderResponse();
                                            orderResponse.setOrder(Entities.GetOrder(jsonObject.getJSONObject("Order")));
                                            callback.onSuccess(orderResponse);
                                            return;
                                        case Error:
                                            callback.onFailed(new Gson().fromJson(res, ErrorResponse.class).getErrorMessage());
                                            return;
                                    }

                                }
                                else {
                                    callback.onFailed(res);
                                }
                            } catch (JSONException e) {
                                String err = e.toString();
                                Log.e(GlobalVariables.ErrorMessage, e.getLocalizedMessage());
                                Log.e(GlobalVariables.ErrorMessage, err);
                                callback.onFailed(err);
                                return;
                            }
                        }
                    });
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e(GlobalVariables.ErrorMessage, e.getLocalizedMessage());
                    Log.e(GlobalVariables.ErrorMessage, err);
                    callback.onFailed(err);
                    return;
                }
//            }
//        };
//        runnable.start();
    }
    public <T> void SubmitRequestGeneric(String url, final T model,final ResponseCallback callback)
    {
        if (!(url.toLowerCase().startsWith("https://") || url.toLowerCase().startsWith("https://"))){
            url = GetHttpUrl() + url;
        }
        final String gotoUrl = url;
        Thread runnable = new Thread() {
            @Override
            public void run() {
                try {
                    String dataToSend =gson.toJson(model);
                    Log.v(GlobalVariables.TAG, gotoUrl);
                    Log.v(GlobalVariables.TAG, dataToSend);
                    Request request = JsonRequest(gotoUrl, dataToSend);
                    Call call = httpClient.newCall(request);
                    Response response = call.execute();
                    String res = response.body().string();
                    String auth = response.headers().get(GlobalVariables.Authorization);
                    if (auth != null) {
                        auth = auth.replace(GlobalVariables.BEARER, "");
                        SetJWT(auth);
                    }
                    if (!response.isSuccessful()){
                        callback.onFailed(res);
                        return;
                    }
                    if (res == "" || res == null)
                    {
                        callback.onSuccess(null);
                        return;
                    }
                    JSONObject jsonObject = new JSONObject(res);
                    Log.v(GlobalVariables.TAG, res);
                    callback.onSuccess(res);
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e(GlobalVariables.ErrorMessage, err.toString());
                    callback.onFailed(err);
                    return;
                }

            }
        };
        runnable.start();
    }
    public <T,X> void SubmitRequestGeneric(String url, final T model, final Class<X> classOfX, final ResponseCallback callback)
    {
        if (!(url.toLowerCase().startsWith("https://") || url.toLowerCase().startsWith("https://"))){
            url = GetHttpUrl() + url;
        }
        final String gotoUrl = url;
        Thread runnable = new Thread() {
            @Override
            public void run() {
                try {
                    String dataToSend =gson.toJson(model);
                    Log.v(GlobalVariables.TAG, gotoUrl);
                    Log.v(GlobalVariables.TAG, dataToSend);
                    Request request = JsonRequest(gotoUrl, dataToSend);
                    Call call = httpClient.newCall(request);
                    Response response = call.execute();
                    String res = response.body().string();
                    String auth = response.headers().get(GlobalVariables.Authorization);
                    if (auth != null) {
                        auth = auth.replace(GlobalVariables.BEARER, "");
                        SetJWT(auth);
                    }
                    if (!response.isSuccessful()){
                        callback.onFailed(res);
                        return;
                    }
                    if (res == "" || res == null)
                    {
                        callback.onSuccess(null);
                        return;
                    }
                    Log.v(GlobalVariables.TAG, res);
                    if (classOfX.isAssignableFrom(String.class)){
                        callback.onSuccess(res.toString());
                    }
                    else {
                        callback.onSuccess(new Gson().fromJson(res, classOfX));
                    }
                } catch (Exception e) {
                    String err = e.toString();
                    Log.e(GlobalVariables.ErrorMessage, err.toString());
                    callback.onFailed(err);
                    return;
                }
            }
        };
        runnable.start();
    }
    private void Negotiate() {
        String url = GetSignalRHttpUrl() + GlobalVariables.NEGOTIATE_WEB_SOCKET;
        BufferedReader reader = null;
        StringBuilder stringBuilder;
        if (JWT != null) {
            url += GlobalVariables.Authorization_Bearer + JWT;
        }
        if (Hubs != null && !Hubs.isEmpty()) {
            url += GlobalVariables.ConnectionData + EncodingUtil.encodeURI(gson.toJson(Hubs));
        }
        try {
            URL urlAddress = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlAddress.openConnection();
            connection.setRequestMethod("GET");
            connection.setReadTimeout(GlobalVariables.CONSTANT_TIME_OUT);

            OkHttpClient httpClient = new OkHttpClient.Builder().connectTimeout(GlobalVariables.CONSTANT_TIME_OUT, TimeUnit.SECONDS)
                    .writeTimeout(GlobalVariables.CONSTANT_TIME_OUT, TimeUnit.SECONDS)
                    .readTimeout(GlobalVariables.CONSTANT_TIME_OUT, TimeUnit.SECONDS)
                    .build();
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Call call = httpClient.newCall(request);
            Response response = call.execute();
            String res = response.body().string();
            obj = new JSONObject(res);
            if (obj != null) {
                SetConnectionId(obj.getString(GlobalVariables.Connection_Id));
            }
            NegotiatedInfo = obj;
            Connect();
        } catch (Exception e) {
            String err = e.toString();
            _isRunning = false;
            Log.e(GlobalVariables.ErrorMessage, err.toString());
        }
    }
    private void SetConnectionId(String val) {
        if (ConnectionId == val) {
            return;
        }
        ConnectionId = val;
        /**
         * used to send updated Connection Id to Broadcast message.
         */
        listener.onSignalRConnectionIdUpdate(ConnectionId);
    }
    private void Connect() {
        ShouldReconnect = true;
        String url = GetSignalRWSUrl() + GlobalVariables.CONNECT_WEB_SOCKET;
        try {

            if (JWT != null) {
                url += GlobalVariables.Authorization_Bearer + JWT;
            }
            if (NegotiatedInfo != null) {
                url += GlobalVariables.ConstantConnectionToken + EncodingUtil.encodeURIComponent(NegotiatedInfo.getString(GlobalVariables.ConnectionToken));
            }
            if (Hubs != null && !Hubs.isEmpty()) {

                url += GlobalVariables.ConnectionData + EncodingUtil.encodeURI(gson.toJson(Hubs));
            }
        } catch (Exception ex) {
            String error = ex.getMessage();
        }
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        websocket = client.newWebSocket(request, webSocketListener);
        client.dispatcher().executorService().shutdown();
    }
    private  WebSocketListener webSocketListener = new WebSocketListener() {
        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            super.onOpen(webSocket, response);
            IsConnected = true;
            if (!IsAuthenticated) {
                Authenticate(ClientIdent, TerminalIdent, AccountIdent);
            }
            else if (JWT != null){
                broadcastAuthenticated();
            }
            StartServer();
        }

        @Override
        public void onClosed(WebSocket webSocket, int code, String reason) {
            super.onClosed(webSocket, code, reason);
        }

        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) {
            super.onMessage(webSocket, bytes);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            super.onFailure(webSocket, t, response);
            IsConnected = false;
            Reconnect();
        }

        @Override
        public void onMessage(WebSocket webSocket, final String text) {
            super.onMessage(webSocket, text);
            // log to test the service is running or not.
            Log.e("success", "onMessage");
            String response = text;
            SignalRMessage msg = gson.fromJson(response, SignalRMessage.class);
            if (msg != null) {
                if (msg.C != null) {
                    LastMessageId = msg.C;
                }
                if (msg.G != null) {
                    GroupsToken = msg.G;
                }
                if (msg.M != null) {
                    for (SignalRMessageMethod method : msg.M) {
                        switch (method.M) {
                            case GlobalVariables.AUTHORIZATION_REQUEST:
                            case GlobalVariables.AUTHORIZATION_REQUEST_APPROVED:
                                ProcessAuthorizationRequestJson(gson.toJsonTree((method.A.get(0))).getAsJsonObject().toString());
                                break;
                            case GlobalVariables.TRANSACTION_UPDATE:
                                ProcessJson(gson.toJsonTree((method.A.get(0))).getAsJsonObject().toString());
                                break;
                            case GlobalVariables.TRANSACTION_COMMAND:
                                ProcessJsonForOpenScreen(gson.toJsonTree((method.A.get(0))).getAsJsonObject().toString());
                                break;
                        }
                    }
                }

            }

        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            IsConnected = false;
            Reconnect();
        }
    };
    private void ProcessJsonForOpenScreen(String json) {
        Log.v("sucesss", "command recived");
        listener.onSignalRSuccess(json);
    }
    private void ProcessAuthorizationRequestJson(String json) {
        try {

            obj = new JSONObject(json);
            AuthorizationRequest auth = gson.fromJson(json, AuthorizationRequest.class);
            if (auth != null) {
                Verify(auth.getSecret());
            } else {
                //TODO
                listener.onSignalRFailureResponse(context.getString(R.string.authorization_error));
            }
        } catch (Exception e) {
            String err = e.toString();
            Log.e(GlobalVariables.ErrorMessage, err.toString());
            return;
        }
    }
    public void Verify(String secret) {
        Verify model = new Verify();
        model.setSecret(secret);
        String url = GetHttpUrl() + GlobalVariables.VERIFY;
        try {

            Request request = JsonRequest(url, gson.toJson(model));
            Call call = httpClient.newCall(request);
            Response response = call.execute();
            ProcessResponse(response);
        } catch (Exception e) {
            String err = e.toString();
            Log.e(GlobalVariables.ErrorMessage, err.toString());
            return;
        }
    }
    private Request GetRequest(String url) {
        Request.Builder builder = new Request.Builder()
                .url(url);
        if (ConnectionId != null) {
            builder.addHeader(GlobalVariables.ConnectionHubId, ConnectionId);
        }
        if (JWT != null && JWT.length() > 0) {
            builder.addHeader(GlobalVariables.Authorization, GlobalVariables.BEARER + JWT);
        }
        return builder.get().build();
    }
    private Request JsonRequest(String url, String json) {
        RequestBody body = RequestBody.create(MediaType.parse(GlobalVariables.APPLICATION_JSON_CHARSET_UTF_8), json);
        Request.Builder builder = new Request.Builder()
                .url(url);
        if (ConnectionId != null) {
            builder.addHeader(GlobalVariables.ConnectionHubId, ConnectionId);
        }
        if (JWT != null && JWT.length() > 0) {
            builder.addHeader(GlobalVariables.Authorization, GlobalVariables.BEARER + JWT);
        }
        return builder.post(body).build();
    }
    private void StartServer() {
        String url = GetSignalRHttpUrl() + GlobalVariables.START_WEB_SOCKET;
        try {
            if (JWT != null) {
                url += GlobalVariables.Authorization_Bearer + JWT;
            }
            if (NegotiatedInfo != null) {
                url += GlobalVariables.ConstantConnectionToken + EncodingUtil.encodeURIComponent(NegotiatedInfo.getString(GlobalVariables.ConnectionToken));
            }
            if (Hubs != null && !Hubs.isEmpty()) {

                url += GlobalVariables.ConnectionData + EncodingUtil.encodeURI(gson.toJson(Hubs));
            }
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Call call = httpClient.newCall(request);
            Response response = call.execute();
            String res = response.body().string();
            obj = new JSONObject(res);
            if (obj != null) {

            }
        } catch (Exception e) {
            String err = e.toString();
            Log.e(GlobalVariables.ErrorMessage, err.toString());
            this._isRunning = false;
            return;
        }
    }
    private void broadcastAuthenticated(){
        JWT jwt = new JWT(this.JWT);
        Integer merchantId = null, locationId = null, deviceId = null;
        Claim claim;
        String deviceName = null;
        claim = jwt.getClaim("MerchantId");
        if (claim != null){
            merchantId = claim.asInt();
        }
        claim = jwt.getClaim("LocationId");
        if (claim != null){
            locationId = claim.asInt();
        }
        claim = jwt.getClaim("DeviceName");
        if (claim != null){
            deviceName = claim.asString();
        }
        claim = jwt.getClaim("DeviceId");
        if (claim != null){
            deviceId = claim.asInt();
        }
        listener.onSignalRAuthenticated(deviceId, deviceName, merchantId, locationId);
    }
    /***
    This function is used to reconnect the Websocket.
     ***/
    private void Reconnect() {
        if (!ShouldReconnect) {
            return;
        }
        String url = GetSignalRWSUrl() + GlobalVariables.RECONNECT_WEB_SOCKET;
        try {

            if (JWT != null) {
                url += GlobalVariables.Authorization_Bearer + JWT;
            }
            if (NegotiatedInfo != null) {
                url += GlobalVariables.ConstantConnectionToken + EncodingUtil.encodeURIComponent(NegotiatedInfo.getString(GlobalVariables.ConnectionToken));
            }
            if (Hubs != null && !Hubs.isEmpty()) {
                url += GlobalVariables.ConnectionData + EncodingUtil.encodeURI(gson.toJson(Hubs));
            }
            if (LastMessageId != null) {
                url += GlobalVariables.CONSTANT_MESSAGE_ID + EncodingUtil.encodeURI(LastMessageId);
            }
            if (GroupsToken != null) {
                url += GlobalVariables.CONSTANT_GROUP_TOKEN + EncodingUtil.encodeURI(GroupsToken);
            }
        } catch (Exception ex) {
            String err = ex.toString();
            Log.e(GlobalVariables.ErrorMessage, err.toString());
        }
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        websocket = client.newWebSocket(request, webSocketListener);
        client.dispatcher().executorService().shutdown();
    }
    private String GetSignalRHttpUrl() {
        if (UseSSL) {
            return Domain + GlobalVariables.SIGNAL_R;
        } else {
            return Domain + GlobalVariables.SIGNAL_R;
        }
    }
    private String GetHttpUrl() {
        if (UseSSL) {
            return Domain;
        } else {
            return Domain;
        }
    }
    private String GetSignalRWSUrl() {
        String socketDomain = Domain;
        if (socketDomain.contains(GlobalVariables.HTTPS)) {
            socketDomain = socketDomain.replace(GlobalVariables.HTTPS, "");

        } else if (socketDomain.contains(GlobalVariables.HTTP)) {
            socketDomain = socketDomain.replace(GlobalVariables.HTTP, "");
        }
        if (UseSSL) {
            return GlobalVariables.WSS + socketDomain + GlobalVariables.SIGNAL_R;
        } else {
            return GlobalVariables.WSS + socketDomain + GlobalVariables.SIGNAL_R;
        }
    }
    /**
     * This functiopn is used to Authorize, so hit API of Authorization.
     *
     * @param clientIdent
     * @param terminalIdent
     * @param accountIdent
     */
    public void Authenticate(String clientIdent, String terminalIdent, String accountIdent) {
        Authorize model = new Authorize();
        model.setClientIdent(clientIdent);
        model.setTerminalIdent(terminalIdent);
        model.setAccountIdent(accountIdent);
        String url = GetHttpUrl() + GlobalVariables.Authorize;
        try {
            Request request = JsonRequest(url, gson.toJson(model));
            Call call = httpClient.newCall(request);
            Response response = call.execute();
            ProcessResponse(response);
        } catch (Exception e) {
            String err = e.toString();
            Log.e(GlobalVariables.ErrorMessage, err.toString());
            return;
        }
    }
    private void ProcessResponse(Response response) {
        try {
            String res = response.body().string();
            String auth = response.headers().get(GlobalVariables.Authorization);
            if (auth != null) {
                auth = auth.replace(GlobalVariables.BEARER, "");
                SetJWT(auth);
            } else {
                //TODO
                listener.onSignalRFailureResponse(context.getString(R.string.authorization_error));

            }
            ProcessJson(res);
        } catch (Exception e) {
            String err = e.toString();
            Log.e(GlobalVariables.ErrorMessage, err.toString());
            return;
        }
    }
    private void SetJWT(String auth) {
        if (auth != null) {
            auth = auth.replace(GlobalVariables.BEARER, "");
        }
        if (JWT != auth) {
            JWT = auth;
            IsAuthenticated = true;
            /*Pass JWT , to update the client*/
            listener.onSignalRJwtUpdate(auth);
            broadcastAuthenticated();
            if (IsConnected) {
                Stop();
                ShouldReconnect = true;
                Negotiate();
            }
        }
    }

    public void Stop() {
        ShouldReconnect = false;
        websocket.cancel();
        String url = GetSignalRHttpUrl() + GlobalVariables.ABORT_WEB_SOCKET;
        try {
            if (JWT != null) {
                url += GlobalVariables.Authorization_Bearer + JWT;
            }
            if (NegotiatedInfo != null) {
                url += GlobalVariables.ConstantConnectionToken + EncodingUtil.encodeURIComponent(NegotiatedInfo.getString(GlobalVariables.ConnectionToken));
            }
            if (Hubs != null && !Hubs.isEmpty()) {
                url += GlobalVariables.ConnectionData + EncodingUtil.encodeURI(gson.toJson(Hubs));
            }
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Call call = httpClient.newCall(request);
            Response response = call.execute();
            String res = response.body().string();
            obj = new JSONObject(res);
            if (obj != null) {

            }
        } catch (Exception e) {
            String err = e.toString();
            this._isRunning = false;
            return;
        }
        _isRunning = false;
    }
    private void ProcessJson(String json) {
        try {

            obj = new JSONObject(json);
            if (obj != null) {
                switch (obj.getInt(GlobalVariables.Response_Type)) {
                    case GlobalVariables.RESPONSE_TYPES_AUTHORIZE:
                        AuthorizeResponse auth = gson.fromJson(json, AuthorizeResponse.class);
                        listener.onSignalRAuthorizationResponse(auth);

                        break;
                    case GlobalVariables.RESPONSE_TYPES_ERROR:
                        ErrorResponse err = gson.fromJson(json, ErrorResponse.class);
                        listener.onSignalRFailureResponse(err.getErrorMessage());
                        break;
                    case GlobalVariables.RESPONSE_TYPES_VERIFY:
                        VerifyResponse verifyResponse = gson.fromJson(json, VerifyResponse.class);
                        if (verifyResponse != null) {
                            /*Pass JWT , to update the client*/
                            listener.onSignalRJwtUpdate(verifyResponse.getJWT());
                        }
                        break;
                    case GlobalVariables.RESPONSE_TYPES_TRANSACTION_RESPONSE:
                      /*
                      Need to quesry regarding to Transaction Response.
                       */
                        break;
                }
            }
        } catch (Exception e) {
            String err = e.toString();
            Log.e(GlobalVariables.ErrorMessage, err.toString());
            return;
        }
    }
}
