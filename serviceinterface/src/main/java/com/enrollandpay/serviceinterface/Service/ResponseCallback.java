package com.enrollandpay.serviceinterface.Service;

public interface ResponseCallback<T> {
    void onSuccess(T t);

    void onFailed(String msg);

}