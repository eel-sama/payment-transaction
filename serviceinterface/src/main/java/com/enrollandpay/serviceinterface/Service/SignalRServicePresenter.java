package com.enrollandpay.serviceinterface.Service;

import android.content.Context;
import android.util.Log;

import com.enrollandpay.serviceinterface.Models.Security.AuthorizeResponse;

import okhttp3.OkHttpClient;

public class SignalRServicePresenter implements SignalRServicePresenterInteractor, SignalRServiceModelInteractor.OnSignalRFinishedListener {

    private SignalRServiceInteractor mSignalRServiceInteractor;
    private SignalRServiceModel mSignalRServiceModelInteractor;

    public SignalRServicePresenter(SignalRServiceInteractor mSignalRServiceInteractor) {
        this.mSignalRServiceInteractor = mSignalRServiceInteractor;
        this.mSignalRServiceModelInteractor = new SignalRServiceModel();
    }
    public OkHttpClient GetHttpClient() {
        return this.mSignalRServiceModelInteractor.getHttpClient();
    }
    public <T> void SubmitRequest(String url, T model, ResponseCallback callback)
    {
        this.mSignalRServiceModelInteractor.SubmitRequest(url, model, callback);
    }
    public <T> void SubmitRequestGeneric(String url, T model, ResponseCallback callback)
    {
        this.mSignalRServiceModelInteractor.SubmitRequestGeneric(url, model, callback);
    }
    public <T,X> void SubmitRequestGeneric(String url, T model, Class<X> classOfX, ResponseCallback callback)
    {
        this.mSignalRServiceModelInteractor.SubmitRequestGeneric(url, model, classOfX, callback);
    }
    public void GetRequest(String url, ResponseCallback callback){
        this.mSignalRServiceModelInteractor.GetRequest(url, callback);
    }
    public <X> void GetRequest(String url, Class<X> classOfX, ResponseCallback callback){
        this.mSignalRServiceModelInteractor.GetRequest(url, classOfX, callback);
    }
    public <X> void GetRequestList(String url, Class<X> classOfX, ResponseCallback callback){
        this.mSignalRServiceModelInteractor.GetRequestList(url, classOfX, callback);
    }
    @Override
    public void startSignalR(Context context, String jwt) {
        mSignalRServiceModelInteractor.startSignal(context, jwt,this);
    }

    @Override
    public void onSignalRSuccess(String response) {
        mSignalRServiceInteractor.setSignalRSuccess(response);
    }

    @Override
    public void onSignalRFailureResponse(String failer) {
        mSignalRServiceInteractor.setSignalRSFailureResponse(failer);
    }

    @Override
    public void onSignalRJwtUpdate(String jwt) {
        mSignalRServiceInteractor.setJwtUpdate(jwt);
    }

    @Override
    public void onSignalRConnectionIdUpdate(String connectionId) {
        mSignalRServiceInteractor.setConnectionIdUpdate(connectionId);
    }
    @Override
    public void onSignalRAuthenticated(Integer deviceId, String deviceName, Integer merchantId, Integer locationId){
        mSignalRServiceInteractor.onSignalRAuthenticated(deviceId, deviceName, merchantId, locationId);
    }

    @Override
    public void onSignalRAuthorizationResponse(AuthorizeResponse rAuthorizationResponse) {
        mSignalRServiceInteractor.setSignalRAuthorizationResponse(rAuthorizationResponse);
    }


}
