package com.enrollandpay.serviceinterface.Service;

public enum TransactionEnum {

    DISPLAYTRANSACTION(1), REQUESTCARD(2), DISPLAYENROLLMENT(3), DISPLAYDISCOUNTSELECT(4), DISPLAYREVISEDBILL(5), DISPLAYCARDADDED(6), REQUESTAUTHORIZATION(7), PAYMENTCOMPLETE(8),
    DISPLAYQUESTION(9), DISPLAYTHANKYOU(10), APPLYREWARD(11), APPLYOFFER(12), ApplyDiscount(13),CardPresented(14),PaymentRequested(15),OrderCompleted(16),OrderCanceled(17),DiscountApplied(18), DiscountSelectList(19), RequestPaymentAdjustment(20), Idle(21), PaymentVoid(22), PaymentCancel(23), TabOpened(24);

    private int  screenId;
    TransactionEnum(int screenId) {
        this.screenId = screenId;
    }

    public static TransactionEnum fromInteger(int x) {
        switch(x) {
            case 1:
                return DISPLAYTRANSACTION;
            case 2:
                return REQUESTCARD;
            case 3:
                return DISPLAYENROLLMENT;
            case 4:
                return DISPLAYDISCOUNTSELECT;
            case 5:
                return DISPLAYREVISEDBILL;
            case 6:
                return DISPLAYCARDADDED;
            case 7:
                return REQUESTAUTHORIZATION;
            case 8:
                return PAYMENTCOMPLETE;
            case 9:
                return DISPLAYQUESTION;
            case 10:
                return DISPLAYTHANKYOU;
            case 13:
                return ApplyDiscount;
            case 14:
                return CardPresented;
            case 15:
                return PaymentRequested;
            case 16:
                return OrderCompleted;
            case 17:
                return OrderCompleted;
            case 18:
                return DiscountApplied;
            case 19:
                return  DiscountSelectList;
            case 20:
                return RequestPaymentAdjustment;
            case 21:
                return Idle;
            case 22:
                return PaymentVoid;
            case 23:
                return PaymentCancel;
            case 24:
                return TabOpened;
        }
        return null;
    }
}