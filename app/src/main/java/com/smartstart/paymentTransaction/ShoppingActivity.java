package com.smartstart.paymentTransaction;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.enrollandpay.serviceinterface.BusinessEntity.Order;
import com.enrollandpay.serviceinterface.BusinessEntity.OrderItem;
import com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment;
import com.enrollandpay.serviceinterface.Service.EnPService;
import com.enrollandpay.serviceinterface.Service.ResponseCallback;
import com.enrollandpay.serviceinterface.Service.Utility;
import com.smartstart.paymentTransaction.adapter.ItemsAdapter;
import com.smartstart.paymentTransaction.constants.AppConfig;
import com.smartstart.paymentTransaction.databinding.ShoppingMainBinding;
import com.smartstart.paymentTransaction.listeners.OnRefreshSummaryListener;
import com.smartstart.paymentTransaction.model.Item;
import com.smartstart.paymentTransaction.utils.App;
import com.smartstart.paymentTransaction.utils.Session;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

public class ShoppingActivity extends AppCompatActivity implements View.OnClickListener, OnRefreshSummaryListener {

    private static final String TAG = "ShoppingActivity";

    private EnPService enPService;
    private Boolean mServiceIsRunning = false;
    private ShoppingMainBinding binding;
    Session session;
    List<Item> items;
    ItemsAdapter itemsAdapter;
    Bundle args;
    Integer orderId;
    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            final int position = viewHolder.getAdapterPosition();
            switch (direction){
                case ItemTouchHelper.LEFT:
                    items.remove(position);
                    itemsAdapter.notifyItemRemoved(position);
                    refreshSummary(items);
                    session.setCart(ShoppingActivity.this.getString(R.string.cart_key), items);
                    break;
            }
        }

        @Override
        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                    .addSwipeLeftBackgroundColor(ContextCompat.getColor(ShoppingActivity.this, R.color.red))
                    .addSwipeLeftActionIcon(R.drawable.ic_baseline_delete_24)
                    .create()
                    .decorate();
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    };

    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            enPService = null;
            App.SetEnPService(null);
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            EnPService.EnPServiceBinder myBinder = (EnPService.EnPServiceBinder) service;
            enPService = myBinder.getService();
            App.SetEnPService(enPService);

            if (enPService.getCurrentCredentials() != null) {
                AppConfig.setDeviceCredentials(enPService.getCurrentCredentials());
                if (getIntent().getBundleExtra("container") != null) {
                    fetchDetail();
                }
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ShoppingMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("By Item");
//        binding.extendedFabProduct.setOnClickListener(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerItems.setLayoutManager(layoutManager);

        session = new Session(this);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(binding.recyclerItems);
        binding.btnSplit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(ShoppingActivity.this, binding.btnSplit);
                popupMenu.getMenuInflater().inflate(R.menu.menu_checkout, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        //switch here for menu's id
                        return true;
                    }
                });
                popupMenu.show();
            }
        });

        Utility.saveJWT(this, "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA5LzA5L2lkZW50aXR5L2NsYWltcy9hY3RvciI6IkRldmljZSIsIkRldmljZUlkIjoiMTE1IiwiRGV2aWNlTmFtZSI6IlJ5YW4gRGV2aWNlIiwiQ2xpZW50SWQiOiIxIiwiQ2xpZW50QWNjZXNzSWQiOiJlOTE1YTI4MmVjNTc0MTliODVhM2FmYzBhNjBkYjhhNmM2NWIwNTA4ZmNhNDQzYzc5OTVlZmI0Y2Y3ZWM3ZTU2IiwiQ2xpZW50SWRlbnQiOiIxNDdlMzEzOTc2NTQ0MDAzODdmODBjOWRjN2NmZTdmMiIsIkNvYWxpdGlvbklkIjpbIiIsIiJdLCJMb2NhdGlvbklkIjoiMTMiLCJMb2NhdGlvbnMiOiJbXSIsIkxvY2F0aW9uVGltZVpvbmVUeXBlIjoiR01UTUlOVVMwODAwUEFDSUZJQ1RJTUUiLCJNZXJjaGFudElkIjoiMTkiLCJNZXJjaGFudHMiOiJbXSIsIkRldmljZVR5cGUiOiJQT1MsIFBheW1lbnRUZXJtaW5hbCIsIlBheW1lbnREZXZpY2VJZCI6IiIsIlJlbGF5RGV2aWNlSWQiOiIiLCJBUElWZXJzaW9uIjoiIiwibmJmIjoxNjEzMzczMDk0LCJleHAiOjE2MTM0NTk0OTQsImlzcyI6Imh0dHBzOi8vc2VydmljZS5lbnJvbGxhbmRwYXkuY29tIiwiYXVkIjoiMTQ3ZTMxMzk3NjU0NDAwMzg3ZjgwYzlkYzdjZmU3ZjIifQ.tXgzFAwfhl1_ncEMAEUoNsAiqe67mo0aUsRt5ZnCkLk");
        StartService();
        if (savedInstanceState != null) {
//            SetMessage(savedInstanceState.getString(GlobalVariables.MESSAGE_TYPE), savedInstanceState.getString(GlobalVariables.MESSAGE_DATA));
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = this.getIntent();
        if(intent.getBundleExtra("container") != null) {
            args = intent.getBundleExtra("container");
            orderId = args.getInt("orderId");
        }
        if (orderId == null) refreshSummary(session.getProducts(getString(R.string.cart_key)));
        else {
            items = new ArrayList<>();
            session.clearSession(getString(R.string.cart_key));
        }
//      session.setCart(getString(R.string.cart_key), new ArrayList<>());
        itemsAdapter = new ItemsAdapter(items, ShoppingActivity.this);
        binding.recyclerItems.setAdapter(itemsAdapter);
        itemsAdapter.setOnRefreshSummaryListener(this);
    }

    private void refreshSummary(List<Item> tempItems) {
        if (tempItems != null) {
            items = tempItems;
            DecimalFormat df2 = new DecimalFormat("#.##");
            double subtotal = 0, vat = 0,total = 0, discount = 0, amount_tendered = 0;
            for (Item item: tempItems) {
                subtotal = subtotal + item.getTotal();
                if (item.getTax() != null) {
                    vat = vat + (item.getTax().getPercentage() * item.getSaleAmount());
                }
                if (item.getAmountTax() != 0) {
                    vat = item.getAmountTax();
                }
                if (item.getDiscount() != 0) {
                    discount = item.getDiscount();
                }
                if (item.getOrderPayments().size() > 0) {
                    for(OrderPayment orderPayment : item.getOrderPayments()) {
                        binding.payments.setText(orderPayment.getPaymentType().toString());
//                        binding.payments.append(orderPayment.getPaymentType());
                    }
                }
                if (item.getAmountTotal() != 0) {
                    subtotal = item.getAmountTotal();
                }
                amount_tendered = item.getAmountTendered();
            }
            total = total + subtotal + vat;

            binding.subtotal.setText("$"+df2.format(subtotal));
            binding.vat.setText("$"+df2.format(vat));
            binding.discount.setText("$"+df2.format(discount));
            binding.amountTendered.setText("$"+df2.format(amount_tendered));
            binding.total.setText("$"+df2.format(total));
        } else {
            items = new ArrayList<>();
            binding.subtotal.setText("$0");
            binding.vat.setText("$0");
            binding.total.setText("$0");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_shop, menu);
        return true;
    }

    @Override
    public void onClick(View view) {

    }

    public void onAddProduct(MenuItem m) {
        startActivity(new Intent(this, CategoryActivity.class));
    }

    @Override
    public void refreshSummary(View view) {
        List<Item> items = session.getProducts(getString(R.string.cart_key));
        refreshSummary(items);
    }

    private void fetchDetail() {
        enPService.OrderDetail(orderId, new ResponseCallback() {
            @Override
            public void onSuccess(Object o) {
                Order ord = (Order) o;
                Item i = new Item();
                i.setDiscount(ord.getAmountDiscount());
                i.setOrderPayments(ord.getOrderPayments());
                i.setAmountTax(ord.getAmountTax());
                i.setAmountTotal(ord.getAmountTotal());
                i.setAmountTendered(ord.getAmountTendered());
                if (ord.getOrderItems().size() > 0) {
                    for (OrderItem item : ord.getOrderItems()) {
                        i.setName(item.getName());
                        i.setTotal(item.getAmountTotal());
                        i.setQuantity(item.getQuantity().intValue());
//                        i.setAmountTax(i.getAmountTax()+item.getAmountTax());
                        items.add(i);
                    }
                } else {
                    i.setTotal(ord.getAmountTotal());
                    items.add(i);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshSummary(items);
                        itemsAdapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onFailed(String msg) {

            }
        });
    }

    private void StartService() {
        if (!mServiceIsRunning) {
            mServiceIsRunning = true;
            try {
                Intent intent = new Intent(this, EnPService.class);
                bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
                startService(intent);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
