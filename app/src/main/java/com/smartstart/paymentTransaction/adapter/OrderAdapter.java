package com.smartstart.paymentTransaction.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.enrollandpay.serviceinterface.BusinessEntity.Order;
import com.smartstart.paymentTransaction.ShoppingActivity;
import com.smartstart.paymentTransaction.databinding.OrderItemBinding;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewHolder> {

    private final List<Order> orderList;

    public OrderAdapter(List<Order> orderList) {
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        @NonNull OrderItemBinding itemView = OrderItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new OrderViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int position) {
        Order order = orderList.get(position);
        holder.binding.orderId.setText("Order Transaction No. " + order.getOrderId());
        holder.binding.itemsTotal.setText("$" + order.getAmountTotal() + " Total");
        holder.order = order;
//        holder.orderItems.addAll(order.getOrderItems());
//        holder.binding.noItemsOrdered.setText(collection.getOrderItems().size()+" items ordered");
    }

    public void setData(List<Order> orders) {
        this.orderList.clear();
        this.orderList.addAll(orders);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public OrderItemBinding binding;
        public Order order;

        public OrderViewHolder(OrderItemBinding b) {
            super(b.getRoot());
            binding = b;
            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(), ShoppingActivity.class);
            Bundle args = new Bundle();
            args.putInt("orderId", order.getOrderId());
            intent.putExtra("container", args);
            view.getContext().startActivity(intent);
        }
    }
}
