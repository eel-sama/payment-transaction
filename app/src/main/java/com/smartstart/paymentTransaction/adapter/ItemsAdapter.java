package com.smartstart.paymentTransaction.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.smartstart.paymentTransaction.R;
import com.smartstart.paymentTransaction.databinding.DialogAddProductBinding;
import com.smartstart.paymentTransaction.databinding.ItemItemBinding;
import com.smartstart.paymentTransaction.listeners.OnRefreshSummaryListener;
import com.smartstart.paymentTransaction.model.Item;
import com.smartstart.paymentTransaction.utils.Session;

import java.text.DecimalFormat;
import java.util.List;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemsViewHolder> {

    OnRefreshSummaryListener onRefreshSummaryListener;
    private final List<Item> itemList;
    Session session;

    public ItemsAdapter(List<Item> itemList, Context context) {
        this.itemList = itemList;
        session = new Session(context);
    }

    public void setOnRefreshSummaryListener(OnRefreshSummaryListener onRefreshSummaryListener) {
        this.onRefreshSummaryListener = onRefreshSummaryListener;
    }

    @NonNull
    @Override
    public ItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemItemBinding binding = ItemItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ItemsViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsViewHolder holder, int position) {
        Item item = itemList.get(position);
        holder.binding.employeeName.setText("$" + item.getSaleAmount() + "x" + item.getQuantity() + " = $" + item.getTotal());
        holder.binding.itemProduct.setText(item.getName());
        holder.item = item;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }


    public class ItemsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ItemItemBinding binding;
        public Item item;
        private DialogAddProductBinding dialogAddProductBinding;

        public ItemsViewHolder(@NonNull ItemItemBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            dialogAddProductBinding = DialogAddProductBinding.inflate(LayoutInflater.from(view.getContext()));
            final Dialog dialog = new Dialog(view.getContext());
            final int[] quantity = {item.getQuantity()};
            final double[] total = {item.getTotal()};
            DecimalFormat df2 = new DecimalFormat("#.##");
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(dialogAddProductBinding.getRoot());
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialogAddProductBinding.productQuantity.setText(String.valueOf(quantity[0]));
            dialogAddProductBinding.productName.setText(item.getName());
            dialogAddProductBinding.productPrize.setText("$ " + total[0]);
            final Item[] tempItem = {new Item()};
            dialogAddProductBinding.btnMinusQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    quantity[0] = quantity[0] - 1;
                    total[0] = item.getSaleAmount() * quantity[0];
                    dialogAddProductBinding.productPrize.setText("$ " + df2.format(total[0]));
                    dialogAddProductBinding.productQuantity.setText(String.valueOf(quantity[0]));
                }
            });
            dialogAddProductBinding.btnAddQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    quantity[0] = quantity[0] + 1;
                    total[0] = item.getSaleAmount() * quantity[0];
                    dialogAddProductBinding.productPrize.setText("$ " + df2.format(total[0]));
                    dialogAddProductBinding.productQuantity.setText(String.valueOf(quantity[0]));
                }
            });
            dialogAddProductBinding.btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    item.setQuantity(quantity[0]);
                    if (quantity[0] == 1)
                        item.setTotal(item.getSaleAmount());
                    else
                        item.setTotal(total[0]);
                    session.setCart(view.getContext().getString(R.string.cart_key), itemList);
                    onRefreshSummaryListener.refreshSummary(view);
                    notifyDataSetChanged();
                    dialog.dismiss();
                }
            });
            dialogAddProductBinding.btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }
}
