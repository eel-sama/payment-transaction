package com.smartstart.paymentTransaction.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.enrollandpay.serviceinterface.BusinessEntity.Category;
import com.enrollandpay.serviceinterface.BusinessEntity.Product;
import com.enrollandpay.serviceinterface.BusinessEntity.Tax;
import com.smartstart.paymentTransaction.ProductActivity;
import com.smartstart.paymentTransaction.R;
import com.smartstart.paymentTransaction.databinding.DialogAddProductBinding;
import com.smartstart.paymentTransaction.databinding.ItemCategoryBinding;
import com.smartstart.paymentTransaction.databinding.ItemEmptyBinding;
import com.smartstart.paymentTransaction.databinding.ItemProductBinding;
import com.smartstart.paymentTransaction.listeners.OnRefreshSummaryListener;
import com.smartstart.paymentTransaction.model.Item;
import com.smartstart.paymentTransaction.utils.Session;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    OnRefreshSummaryListener onRefreshSummaryListener;
    List<Item> items;
    Session session;
    private final List<Object> categoryList;

    public ProductAdapter(List<Object> categoryList, Context context) {
        this.categoryList = categoryList;
        session = new Session(context);
        items = new ArrayList<>();
    }

    public void setOnRefreshSummaryListener(OnRefreshSummaryListener onRefreshSummaryListener) {
        this.onRefreshSummaryListener = onRefreshSummaryListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;

        switch (viewType) {
            case 0:
                ItemCategoryBinding itemCategoryBinding = ItemCategoryBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
                viewHolder = new ProductAdapter.CategoryViewHolder(itemCategoryBinding);
                break;
            case 1:
                ItemProductBinding itemProductBinding = ItemProductBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
                viewHolder = new ProductAdapter.ProductViewHolder(itemProductBinding);
                break;
            case 2:
                ItemEmptyBinding binding = ItemEmptyBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
                viewHolder = new ProductAdapter.EmptyViewHolder(binding);
            default:
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                Category category = (Category) categoryList.get(position);
                ProductAdapter.CategoryViewHolder categoryViewHolder = (ProductAdapter.CategoryViewHolder) holder;
                categoryViewHolder.itemCategoryBinding.tvName.setText(category.getName());
                categoryViewHolder.category = category;
                break;
            case 1:
                Product product = (Product) categoryList.get(position);
                ProductAdapter.ProductViewHolder productViewHolder = (ProductAdapter.ProductViewHolder) holder;
                productViewHolder.itemProductBinding.tvName.setText(product.getName());
                productViewHolder.product = product;
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (categoryList.get(position) instanceof Category) {
            return 0;
        } else if (categoryList.get(position) instanceof Product) {
            return 1;
        }
        return 2;
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public void setData(List<Object> categories) {
        this.categoryList.clear();
        this.categoryList.addAll(categories);
        notifyDataSetChanged();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ItemCategoryBinding itemCategoryBinding;
        public Category category;

        public CategoryViewHolder(@NonNull ItemCategoryBinding b) {
            super(b.getRoot());
            itemCategoryBinding = b;
            itemCategoryBinding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(), ProductActivity.class);
            Bundle args = new Bundle();
            args.putSerializable("category", category);
            intent.putExtra("container", args);
            view.getContext().startActivity(intent);
        }
    }

    private class EmptyViewHolder extends RecyclerView.ViewHolder {
        public EmptyViewHolder(ItemEmptyBinding itemEmptyBinding) {
            super(itemEmptyBinding.getRoot());
        }
    }

    private class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ItemProductBinding itemProductBinding;
        public Product product;
        private DialogAddProductBinding dialogAddProductBinding;

        public ProductViewHolder(ItemProductBinding binding) {
            super(binding.getRoot());
            itemProductBinding = binding;
            itemProductBinding.getRoot().setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            dialogAddProductBinding = DialogAddProductBinding.inflate(LayoutInflater.from(view.getContext()));
            final Dialog dialog = new Dialog(view.getContext());
            final int[] quantity = {1};
            final double[] total = {0};
            DecimalFormat df2 = new DecimalFormat("#.##");
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(dialogAddProductBinding.getRoot());
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialogAddProductBinding.productName.setText(product.getName());
            dialogAddProductBinding.productPrize.setText("$ " + product.getSaleAmount());
            final Item[] item = {new Item()};
            dialogAddProductBinding.btnMinusQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    quantity[0] = quantity[0] - 1;
                    total[0] = product.getSaleAmount() * quantity[0];
                    dialogAddProductBinding.productPrize.setText("$ " + df2.format(total[0]));
                    dialogAddProductBinding.productQuantity.setText(String.valueOf(quantity[0]));
                }
            });
            dialogAddProductBinding.btnAddQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    quantity[0] = quantity[0] + 1;
                    total[0] = product.getSaleAmount() * quantity[0];
                    dialogAddProductBinding.productPrize.setText("$ " + df2.format(total[0]));
                    dialogAddProductBinding.productQuantity.setText(String.valueOf(quantity[0]));
                }
            });
            dialogAddProductBinding.btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    item[0].setTax(new Tax());
                    item[0].getTax().setPercentage(product.getTax().getPercentage());
                    item[0].setName(product.getName());
                    item[0].setQuantity(quantity[0]);
                    if (quantity[0] == 1)
                        item[0].setTotal(product.getSaleAmount());
                    else
                        item[0].setTotal(total[0]);
                    item[0].setSaleAmount(product.getSaleAmount());
                    items.add(item[0]);
                    session.setCart(view.getContext().getString(R.string.cart_key), items);
                    onRefreshSummaryListener.refreshSummary(view);
                    dialog.dismiss();
                }
            });
            dialogAddProductBinding.btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }
}
