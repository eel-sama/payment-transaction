package com.smartstart.paymentTransaction.model.Employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Collection {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("EmployeeId")
    @Expose
    private Integer employeeId;
    @SerializedName("MerchantId")
    @Expose
    private Integer merchantId;
    @SerializedName("NamePrefix")
    @Expose
    private Object namePrefix;
    @SerializedName("NameFirst")
    @Expose
    private String nameFirst;
    @SerializedName("NameMiddle")
    @Expose
    private Object nameMiddle;
    @SerializedName("NameLast")
    @Expose
    private String nameLast;
    @SerializedName("NameSuffix")
    @Expose
    private Object nameSuffix;
    @SerializedName("EmailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("PhoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("AccessType")
    @Expose
    private Integer accessType;
    @SerializedName("StatusType")
    @Expose
    private Integer statusType;
    @SerializedName("TS")
    @Expose
    private String ts;
    @SerializedName("LanguageType")
    @Expose
    private Integer languageType;
    @SerializedName("ClientAccesses")
    @Expose
    private List<Object> clientAccesses = null;
    @SerializedName("ConsumerLoyaltyPrograms")
    @Expose
    private List<Object> consumerLoyaltyPrograms = null;
    @SerializedName("EmployeeAuthentication")
    @Expose
    private Object employeeAuthentication;
    @SerializedName("EmployeeLocations")
    @Expose
    private List<EmployeeLocation> employeeLocations = null;
    @SerializedName("Merchant")
    @Expose
    private Object merchant;
    @SerializedName("SaleOrders")
    @Expose
    private List<Object> saleOrders = null;
    @SerializedName("EmployeeImpersonations")
    @Expose
    private List<Object> employeeImpersonations = null;
    @SerializedName("Messages")
    @Expose
    private List<Object> messages = null;
    @SerializedName("EntityState")
    @Expose
    private Integer entityState;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public Object getNamePrefix() {
        return namePrefix;
    }

    public void setNamePrefix(Object namePrefix) {
        this.namePrefix = namePrefix;
    }

    public String getNameFirst() {
        return nameFirst;
    }

    public void setNameFirst(String nameFirst) {
        this.nameFirst = nameFirst;
    }

    public Object getNameMiddle() {
        return nameMiddle;
    }

    public void setNameMiddle(Object nameMiddle) {
        this.nameMiddle = nameMiddle;
    }

    public String getNameLast() {
        return nameLast;
    }

    public void setNameLast(String nameLast) {
        this.nameLast = nameLast;
    }

    public Object getNameSuffix() {
        return nameSuffix;
    }

    public void setNameSuffix(Object nameSuffix) {
        this.nameSuffix = nameSuffix;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getAccessType() {
        return accessType;
    }

    public void setAccessType(Integer accessType) {
        this.accessType = accessType;
    }

    public Integer getStatusType() {
        return statusType;
    }

    public void setStatusType(Integer statusType) {
        this.statusType = statusType;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Integer getLanguageType() {
        return languageType;
    }

    public void setLanguageType(Integer languageType) {
        this.languageType = languageType;
    }

    public List<Object> getClientAccesses() {
        return clientAccesses;
    }

    public void setClientAccesses(List<Object> clientAccesses) {
        this.clientAccesses = clientAccesses;
    }

    public List<Object> getConsumerLoyaltyPrograms() {
        return consumerLoyaltyPrograms;
    }

    public void setConsumerLoyaltyPrograms(List<Object> consumerLoyaltyPrograms) {
        this.consumerLoyaltyPrograms = consumerLoyaltyPrograms;
    }

    public Object getEmployeeAuthentication() {
        return employeeAuthentication;
    }

    public void setEmployeeAuthentication(Object employeeAuthentication) {
        this.employeeAuthentication = employeeAuthentication;
    }

    public List<EmployeeLocation> getEmployeeLocations() {
        return employeeLocations;
    }

    public void setEmployeeLocations(List<EmployeeLocation> employeeLocations) {
        this.employeeLocations = employeeLocations;
    }

    public Object getMerchant() {
        return merchant;
    }

    public void setMerchant(Object merchant) {
        this.merchant = merchant;
    }

    public List<Object> getSaleOrders() {
        return saleOrders;
    }

    public void setSaleOrders(List<Object> saleOrders) {
        this.saleOrders = saleOrders;
    }

    public List<Object> getEmployeeImpersonations() {
        return employeeImpersonations;
    }

    public void setEmployeeImpersonations(List<Object> employeeImpersonations) {
        this.employeeImpersonations = employeeImpersonations;
    }

    public List<Object> getMessages() {
        return messages;
    }

    public void setMessages(List<Object> messages) {
        this.messages = messages;
    }

    public Integer getEntityState() {
        return entityState;
    }

    public void setEntityState(Integer entityState) {
        this.entityState = entityState;
    }

    @Override
    public String toString() {
        return this.nameFirst + ' ' + this.nameLast;
    }
}
