package com.smartstart.paymentTransaction.model.Product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Collection {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("ProductId")
    @Expose
    private Integer productId;
    @SerializedName("MerchantId")
    @Expose
    private Integer merchantId;
    @SerializedName("LocationId")
    @Expose
    private Object locationId;
    @SerializedName("ParentProductId")
    @Expose
    private Object parentProductId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Description")
    @Expose
    private Object description;
    @SerializedName("PosIdents")
    @Expose
    private String posIdents;
    @SerializedName("SaleAmount")
    @Expose
    private Double saleAmount;
    @SerializedName("StatusType")
    @Expose
    private Integer statusType;
    @SerializedName("TS")
    @Expose
    private String ts;
    @SerializedName("CreatedDateTime")
    @Expose
    private String createdDateTime;
    @SerializedName("TaxId")
    @Expose
    private Object taxId;
    @SerializedName("Location")
    @Expose
    private Object location;
    @SerializedName("Merchant")
    @Expose
    private Object merchant;
    @SerializedName("OfferAssociations")
    @Expose
    private List<Object> offerAssociations = new ArrayList<Object>();
    @SerializedName("OrderItems")
    @Expose
    private List<Object> orderItems = new ArrayList<Object>();
    @SerializedName("ProductChildren")
    @Expose
    private List<Object> productChildren = new ArrayList<Object>();
    @SerializedName("ProductParent")
    @Expose
    private Object productParent;
    @SerializedName("ProductStatistics")
    @Expose
    private List<Object> productStatistics = new ArrayList<Object>();
    @SerializedName("ProductRelationshipsA")
    @Expose
    private List<Object> productRelationshipsA = new ArrayList<Object>();
    @SerializedName("ProductRelationshipsB")
    @Expose
    private List<Object> productRelationshipsB = new ArrayList<Object>();
    @SerializedName("Tax")
    @Expose
    private Object tax;
    @SerializedName("CategoryProducts")
    @Expose
    private List<Object> categoryProducts = new ArrayList<Object>();
    @SerializedName("ProductAddOnsAsAddOn")
    @Expose
    private List<Object> productAddOnsAsAddOn = new ArrayList<Object>();
    @SerializedName("ProductAddOnsAsProduct")
    @Expose
    private List<Object> productAddOnsAsProduct = new ArrayList<Object>();
    @SerializedName("EntityState")
    @Expose
    private Integer entityState;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public Object getLocationId() {
        return locationId;
    }

    public void setLocationId(Object locationId) {
        this.locationId = locationId;
    }

    public Object getParentProductId() {
        return parentProductId;
    }

    public void setParentProductId(Object parentProductId) {
        this.parentProductId = parentProductId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getPosIdents() {
        return posIdents;
    }

    public void setPosIdents(String posIdents) {
        this.posIdents = posIdents;
    }

    public Double getSaleAmount() {
        return saleAmount;
    }

    public void setSaleAmount(Double saleAmount) {
        this.saleAmount = saleAmount;
    }

    public Integer getStatusType() {
        return statusType;
    }

    public void setStatusType(Integer statusType) {
        this.statusType = statusType;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Object getTaxId() {
        return taxId;
    }

    public void setTaxId(Object taxId) {
        this.taxId = taxId;
    }

    public Object getLocation() {
        return location;
    }

    public void setLocation(Object location) {
        this.location = location;
    }

    public Object getMerchant() {
        return merchant;
    }

    public void setMerchant(Object merchant) {
        this.merchant = merchant;
    }

    public List<Object> getOfferAssociations() {
        return offerAssociations;
    }

    public void setOfferAssociations(List<Object> offerAssociations) {
        this.offerAssociations = offerAssociations;
    }

    public List<Object> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<Object> orderItems) {
        this.orderItems = orderItems;
    }

    public List<Object> getProductChildren() {
        return productChildren;
    }

    public void setProductChildren(List<Object> productChildren) {
        this.productChildren = productChildren;
    }

    public Object getProductParent() {
        return productParent;
    }

    public void setProductParent(Object productParent) {
        this.productParent = productParent;
    }

    public List<Object> getProductStatistics() {
        return productStatistics;
    }

    public void setProductStatistics(List<Object> productStatistics) {
        this.productStatistics = productStatistics;
    }

    public List<Object> getProductRelationshipsA() {
        return productRelationshipsA;
    }

    public void setProductRelationshipsA(List<Object> productRelationshipsA) {
        this.productRelationshipsA = productRelationshipsA;
    }

    public List<Object> getProductRelationshipsB() {
        return productRelationshipsB;
    }

    public void setProductRelationshipsB(List<Object> productRelationshipsB) {
        this.productRelationshipsB = productRelationshipsB;
    }

    public Object getTax() {
        return tax;
    }

    public void setTax(Object tax) {
        this.tax = tax;
    }

    public List<Object> getCategoryProducts() {
        return categoryProducts;
    }

    public void setCategoryProducts(List<Object> categoryProducts) {
        this.categoryProducts = categoryProducts;
    }

    public List<Object> getProductAddOnsAsAddOn() {
        return productAddOnsAsAddOn;
    }

    public void setProductAddOnsAsAddOn(List<Object> productAddOnsAsAddOn) {
        this.productAddOnsAsAddOn = productAddOnsAsAddOn;
    }

    public List<Object> getProductAddOnsAsProduct() {
        return productAddOnsAsProduct;
    }

    public void setProductAddOnsAsProduct(List<Object> productAddOnsAsProduct) {
        this.productAddOnsAsProduct = productAddOnsAsProduct;
    }

    public Integer getEntityState() {
        return entityState;
    }

    public void setEntityState(Integer entityState) {
        this.entityState = entityState;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
