package com.smartstart.paymentTransaction.model.Employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmployeeLocation {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("EmployeeId")
    @Expose
    private Integer employeeId;
    @SerializedName("LocationId")
    @Expose
    private Integer locationId;
    @SerializedName("AccessType")
    @Expose
    private Integer accessType;
    @SerializedName("StatusType")
    @Expose
    private Integer statusType;
    @SerializedName("TS")
    @Expose
    private String ts;
    @SerializedName("PosIdents")
    @Expose
    private String posIdents;
    @SerializedName("Employee")
    @Expose
    private Employee__1 employee;
    @SerializedName("Location")
    @Expose
    private Location location;
    @SerializedName("EntityState")
    @Expose
    private Integer entityState;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getAccessType() {
        return accessType;
    }

    public void setAccessType(Integer accessType) {
        this.accessType = accessType;
    }

    public Integer getStatusType() {
        return statusType;
    }

    public void setStatusType(Integer statusType) {
        this.statusType = statusType;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getPosIdents() {
        return posIdents;
    }

    public void setPosIdents(String posIdents) {
        this.posIdents = posIdents;
    }

    public Employee__1 getEmployee() {
        return employee;
    }

    public void setEmployee(Employee__1 employee) {
        this.employee = employee;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Integer getEntityState() {
        return entityState;
    }

    public void setEntityState(Integer entityState) {
        this.entityState = entityState;
    }

}
