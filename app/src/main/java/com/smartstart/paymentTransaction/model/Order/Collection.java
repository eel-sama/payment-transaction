package com.smartstart.paymentTransaction.model.Order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Collection {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("OrderId")
    @Expose
    private Integer orderId;
    @SerializedName("LocationId")
    @Expose
    private Integer locationId;
    @SerializedName("SaleEmployeeId")
    @Expose
    private Object saleEmployeeId;
    @SerializedName("AmountTotal")
    @Expose
    private Double amountTotal;
    @SerializedName("AmountTax")
    @Expose
    private Double amountTax;
    @SerializedName("AmountGratuity")
    @Expose
    private Double amountGratuity;
    @SerializedName("AmountTendered")
    @Expose
    private Double amountTendered;
    @SerializedName("AmountDiscount")
    @Expose
    private Double amountDiscount;
    @SerializedName("AmountReward")
    @Expose
    private Double amountReward;
    @SerializedName("PosIdent")
    @Expose
    private String posIdent;
    @SerializedName("StartDateTime")
    @Expose
    private String startDateTime;
    @SerializedName("EndDateTime")
    @Expose
    private Object endDateTime;
    @SerializedName("POSDeviceId")
    @Expose
    private Integer pOSDeviceId;
    @SerializedName("PaymentDeviceId")
    @Expose
    private Integer paymentDeviceId;
    @SerializedName("EnrollmentType")
    @Expose
    private Object enrollmentType;
    @SerializedName("AdditionalAttributes")
    @Expose
    private String additionalAttributes;
    @SerializedName("StatusType")
    @Expose
    private Integer statusType;
    @SerializedName("TS")
    @Expose
    private String ts;
    @SerializedName("Commands")
    @Expose
    private Object commands;
    @SerializedName("AmountLoyaltyUsed")
    @Expose
    private Double amountLoyaltyUsed;
    @SerializedName("CurrencyType")
    @Expose
    private Integer currencyType;
    @SerializedName("AmountGratuity2")
    @Expose
    private Double amountGratuity2;
    @SerializedName("AmountGratuity3")
    @Expose
    private Double amountGratuity3;
    @SerializedName("AmountSurcharge")
    @Expose
    private Double amountSurcharge;
    @SerializedName("AmountReturned")
    @Expose
    private Object amountReturned;
    @SerializedName("OriginalOrderId")
    @Expose
    private Object originalOrderId;
    @SerializedName("RelayPOSDeviceId")
    @Expose
    private Object relayPOSDeviceId;
    @SerializedName("RelayPaymentDeviceId")
    @Expose
    private Object relayPaymentDeviceId;
    @SerializedName("TabletopId")
    @Expose
    private Object tabletopId;
    @SerializedName("PaymentDevice")
    @Expose
    private Object paymentDevice;
    @SerializedName("POSDevice")
    @Expose
    private Object pOSDevice;
    @SerializedName("SaleEmployee")
    @Expose
    private Object saleEmployee;
    @SerializedName("Location")
    @Expose
    private Location location;
    @SerializedName("OrderItems")
    @Expose
    private List<Object> orderItems = null;
    @SerializedName("OrderPayments")
    @Expose
    private List<Object> orderPayments = null;
    @SerializedName("ConsumerLoyaltyProgramTransactions")
    @Expose
    private List<Object> consumerLoyaltyProgramTransactions = null;
    @SerializedName("OrderReviews")
    @Expose
    private List<Object> orderReviews = null;
    @SerializedName("Messages")
    @Expose
    private List<Object> messages = null;
    @SerializedName("ConsumerRelationships")
    @Expose
    private List<Object> consumerRelationships = null;
    @SerializedName("MessageThreads")
    @Expose
    private List<Object> messageThreads = null;
    @SerializedName("Orders")
    @Expose
    private List<Object> orders = null;
    @SerializedName("OriginalOrder")
    @Expose
    private Object originalOrder;
    @SerializedName("Device2")
    @Expose
    private Object device2;
    @SerializedName("Device3")
    @Expose
    private Object device3;
    @SerializedName("Tabletop")
    @Expose
    private Object tabletop;
    @SerializedName("EntityState")
    @Expose
    private Integer entityState;
    @SerializedName("$ref")
    @Expose
    private String $ref;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Object getSaleEmployeeId() {
        return saleEmployeeId;
    }

    public void setSaleEmployeeId(Object saleEmployeeId) {
        this.saleEmployeeId = saleEmployeeId;
    }

    public Double getAmountTotal() {
        return amountTotal;
    }

    public void setAmountTotal(Double amountTotal) {
        this.amountTotal = amountTotal;
    }

    public Double getAmountTax() {
        return amountTax;
    }

    public void setAmountTax(Double amountTax) {
        this.amountTax = amountTax;
    }

    public Double getAmountGratuity() {
        return amountGratuity;
    }

    public void setAmountGratuity(Double amountGratuity) {
        this.amountGratuity = amountGratuity;
    }

    public Double getAmountTendered() {
        return amountTendered;
    }

    public void setAmountTendered(Double amountTendered) {
        this.amountTendered = amountTendered;
    }

    public Double getAmountDiscount() {
        return amountDiscount;
    }

    public void setAmountDiscount(Double amountDiscount) {
        this.amountDiscount = amountDiscount;
    }

    public Double getAmountReward() {
        return amountReward;
    }

    public void setAmountReward(Double amountReward) {
        this.amountReward = amountReward;
    }

    public String getPosIdent() {
        return posIdent;
    }

    public void setPosIdent(String posIdent) {
        this.posIdent = posIdent;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Object getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Object endDateTime) {
        this.endDateTime = endDateTime;
    }

    public Integer getPOSDeviceId() {
        return pOSDeviceId;
    }

    public void setPOSDeviceId(Integer pOSDeviceId) {
        this.pOSDeviceId = pOSDeviceId;
    }

    public Integer getPaymentDeviceId() {
        return paymentDeviceId;
    }

    public void setPaymentDeviceId(Integer paymentDeviceId) {
        this.paymentDeviceId = paymentDeviceId;
    }

    public Object getEnrollmentType() {
        return enrollmentType;
    }

    public void setEnrollmentType(Object enrollmentType) {
        this.enrollmentType = enrollmentType;
    }

    public String getAdditionalAttributes() {
        return additionalAttributes;
    }

    public void setAdditionalAttributes(String additionalAttributes) {
        this.additionalAttributes = additionalAttributes;
    }

    public Integer getStatusType() {
        return statusType;
    }

    public void setStatusType(Integer statusType) {
        this.statusType = statusType;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Object getCommands() {
        return commands;
    }

    public void setCommands(Object commands) {
        this.commands = commands;
    }

    public Double getAmountLoyaltyUsed() {
        return amountLoyaltyUsed;
    }

    public void setAmountLoyaltyUsed(Double amountLoyaltyUsed) {
        this.amountLoyaltyUsed = amountLoyaltyUsed;
    }

    public Integer getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(Integer currencyType) {
        this.currencyType = currencyType;
    }

    public Double getAmountGratuity2() {
        return amountGratuity2;
    }

    public void setAmountGratuity2(Double amountGratuity2) {
        this.amountGratuity2 = amountGratuity2;
    }

    public Double getAmountGratuity3() {
        return amountGratuity3;
    }

    public void setAmountGratuity3(Double amountGratuity3) {
        this.amountGratuity3 = amountGratuity3;
    }

    public Double getAmountSurcharge() {
        return amountSurcharge;
    }

    public void setAmountSurcharge(Double amountSurcharge) {
        this.amountSurcharge = amountSurcharge;
    }

    public Object getAmountReturned() {
        return amountReturned;
    }

    public void setAmountReturned(Object amountReturned) {
        this.amountReturned = amountReturned;
    }

    public Object getOriginalOrderId() {
        return originalOrderId;
    }

    public void setOriginalOrderId(Object originalOrderId) {
        this.originalOrderId = originalOrderId;
    }

    public Object getRelayPOSDeviceId() {
        return relayPOSDeviceId;
    }

    public void setRelayPOSDeviceId(Object relayPOSDeviceId) {
        this.relayPOSDeviceId = relayPOSDeviceId;
    }

    public Object getRelayPaymentDeviceId() {
        return relayPaymentDeviceId;
    }

    public void setRelayPaymentDeviceId(Object relayPaymentDeviceId) {
        this.relayPaymentDeviceId = relayPaymentDeviceId;
    }

    public Object getTabletopId() {
        return tabletopId;
    }

    public void setTabletopId(Object tabletopId) {
        this.tabletopId = tabletopId;
    }

    public Object getPaymentDevice() {
        return paymentDevice;
    }

    public void setPaymentDevice(Object paymentDevice) {
        this.paymentDevice = paymentDevice;
    }

    public Object getPOSDevice() {
        return pOSDevice;
    }

    public void setPOSDevice(Object pOSDevice) {
        this.pOSDevice = pOSDevice;
    }

    public Object getSaleEmployee() {
        return saleEmployee;
    }

    public void setSaleEmployee(Object saleEmployee) {
        this.saleEmployee = saleEmployee;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Object> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<Object> orderItems) {
        this.orderItems = orderItems;
    }

    public List<Object> getOrderPayments() {
        return orderPayments;
    }

    public void setOrderPayments(List<Object> orderPayments) {
        this.orderPayments = orderPayments;
    }

    public List<Object> getConsumerLoyaltyProgramTransactions() {
        return consumerLoyaltyProgramTransactions;
    }

    public void setConsumerLoyaltyProgramTransactions(List<Object> consumerLoyaltyProgramTransactions) {
        this.consumerLoyaltyProgramTransactions = consumerLoyaltyProgramTransactions;
    }

    public List<Object> getOrderReviews() {
        return orderReviews;
    }

    public void setOrderReviews(List<Object> orderReviews) {
        this.orderReviews = orderReviews;
    }

    public List<Object> getMessages() {
        return messages;
    }

    public void setMessages(List<Object> messages) {
        this.messages = messages;
    }

    public List<Object> getConsumerRelationships() {
        return consumerRelationships;
    }

    public void setConsumerRelationships(List<Object> consumerRelationships) {
        this.consumerRelationships = consumerRelationships;
    }

    public List<Object> getMessageThreads() {
        return messageThreads;
    }

    public void setMessageThreads(List<Object> messageThreads) {
        this.messageThreads = messageThreads;
    }

    public List<Object> getOrders() {
        return orders;
    }

    public void setOrders(List<Object> orders) {
        this.orders = orders;
    }

    public Object getOriginalOrder() {
        return originalOrder;
    }

    public void setOriginalOrder(Object originalOrder) {
        this.originalOrder = originalOrder;
    }

    public Object getDevice2() {
        return device2;
    }

    public void setDevice2(Object device2) {
        this.device2 = device2;
    }

    public Object getDevice3() {
        return device3;
    }

    public void setDevice3(Object device3) {
        this.device3 = device3;
    }

    public Object getTabletop() {
        return tabletop;
    }

    public void setTabletop(Object tabletop) {
        this.tabletop = tabletop;
    }

    public Integer getEntityState() {
        return entityState;
    }

    public void setEntityState(Integer entityState) {
        this.entityState = entityState;
    }

    public String get$ref() {
        return $ref;
    }

    public void set$ref(String $ref) {
        this.$ref = $ref;
    }

}
