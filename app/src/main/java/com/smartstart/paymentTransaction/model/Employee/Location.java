package com.smartstart.paymentTransaction.model.Employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Location {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("LocationId")
    @Expose
    private Integer locationId;
    @SerializedName("MerchantId")
    @Expose
    private Integer merchantId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("PhoneNumber")
    @Expose
    private Long phoneNumber;
    @SerializedName("PosType")
    @Expose
    private Object posType;
    @SerializedName("ExternalIdent")
    @Expose
    private Object externalIdent;
    @SerializedName("UtcOffsetHour")
    @Expose
    private Double utcOffsetHour;
    @SerializedName("TS")
    @Expose
    private String ts;
    @SerializedName("TimeZoneType")
    @Expose
    private Integer timeZoneType;
    @SerializedName("StatusType")
    @Expose
    private Integer statusType;
    @SerializedName("PosIdent")
    @Expose
    private Object posIdent;
    @SerializedName("RewardOfferSupportType")
    @Expose
    private Object rewardOfferSupportType;
    @SerializedName("LanguageType")
    @Expose
    private Integer languageType;
    @SerializedName("CurrencyType")
    @Expose
    private Integer currencyType;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("PhoneNumberId")
    @Expose
    private Integer phoneNumberId;
    @SerializedName("BotSettings")
    @Expose
    private String botSettings;
    @SerializedName("Longitude")
    @Expose
    private Double longitude;
    @SerializedName("Latitude")
    @Expose
    private Double latitude;
    @SerializedName("HasThirdPartyPos")
    @Expose
    private Object hasThirdPartyPos;
    @SerializedName("ThirdPartyDeviceId")
    @Expose
    private Object thirdPartyDeviceId;
    @SerializedName("AuthorizationRequests")
    @Expose
    private List<Object> authorizationRequests = null;
    @SerializedName("Cards")
    @Expose
    private List<Object> cards = null;
    @SerializedName("ConsumerLoyaltyPrograms")
    @Expose
    private List<Object> consumerLoyaltyPrograms = null;
    @SerializedName("Devices")
    @Expose
    private List<Object> devices = null;
    @SerializedName("LocationConfiguration")
    @Expose
    private Object locationConfiguration;
    @SerializedName("LoyaltyProgramParticipants")
    @Expose
    private List<Object> loyaltyProgramParticipants = null;
    @SerializedName("Offers")
    @Expose
    private List<Object> offers = null;
    @SerializedName("EmployeeLocations")
    @Expose
    private List<EmployeeLocation__1> employeeLocations = null;
    @SerializedName("Merchant")
    @Expose
    private Object merchant;
    @SerializedName("Orders")
    @Expose
    private List<Object> orders = null;
    @SerializedName("Products")
    @Expose
    private List<Object> products = null;
    @SerializedName("ConsumerRelationships")
    @Expose
    private List<Object> consumerRelationships = null;
    @SerializedName("DailySaleOverviews")
    @Expose
    private List<Object> dailySaleOverviews = null;
    @SerializedName("SaleOverviews")
    @Expose
    private List<Object> saleOverviews = null;
    @SerializedName("DailyOfferOverviews")
    @Expose
    private List<Object> dailyOfferOverviews = null;
    @SerializedName("OfferOverviews")
    @Expose
    private List<Object> offerOverviews = null;
    @SerializedName("PhoneNumber1")
    @Expose
    private Object phoneNumber1;
    @SerializedName("MessageThreads")
    @Expose
    private List<Object> messageThreads = null;
    @SerializedName("Taxes")
    @Expose
    private List<Object> taxes = null;
    @SerializedName("Tabletops")
    @Expose
    private List<Object> tabletops = null;
    @SerializedName("ThirdPartyDevice")
    @Expose
    private Object thirdPartyDevice;
    @SerializedName("EntityState")
    @Expose
    private Integer entityState;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Object getPosType() {
        return posType;
    }

    public void setPosType(Object posType) {
        this.posType = posType;
    }

    public Object getExternalIdent() {
        return externalIdent;
    }

    public void setExternalIdent(Object externalIdent) {
        this.externalIdent = externalIdent;
    }

    public Double getUtcOffsetHour() {
        return utcOffsetHour;
    }

    public void setUtcOffsetHour(Double utcOffsetHour) {
        this.utcOffsetHour = utcOffsetHour;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public Integer getTimeZoneType() {
        return timeZoneType;
    }

    public void setTimeZoneType(Integer timeZoneType) {
        this.timeZoneType = timeZoneType;
    }

    public Integer getStatusType() {
        return statusType;
    }

    public void setStatusType(Integer statusType) {
        this.statusType = statusType;
    }

    public Object getPosIdent() {
        return posIdent;
    }

    public void setPosIdent(Object posIdent) {
        this.posIdent = posIdent;
    }

    public Object getRewardOfferSupportType() {
        return rewardOfferSupportType;
    }

    public void setRewardOfferSupportType(Object rewardOfferSupportType) {
        this.rewardOfferSupportType = rewardOfferSupportType;
    }

    public Integer getLanguageType() {
        return languageType;
    }

    public void setLanguageType(Integer languageType) {
        this.languageType = languageType;
    }

    public Integer getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(Integer currencyType) {
        this.currencyType = currencyType;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Integer getPhoneNumberId() {
        return phoneNumberId;
    }

    public void setPhoneNumberId(Integer phoneNumberId) {
        this.phoneNumberId = phoneNumberId;
    }

    public String getBotSettings() {
        return botSettings;
    }

    public void setBotSettings(String botSettings) {
        this.botSettings = botSettings;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Object getHasThirdPartyPos() {
        return hasThirdPartyPos;
    }

    public void setHasThirdPartyPos(Object hasThirdPartyPos) {
        this.hasThirdPartyPos = hasThirdPartyPos;
    }

    public Object getThirdPartyDeviceId() {
        return thirdPartyDeviceId;
    }

    public void setThirdPartyDeviceId(Object thirdPartyDeviceId) {
        this.thirdPartyDeviceId = thirdPartyDeviceId;
    }

    public List<Object> getAuthorizationRequests() {
        return authorizationRequests;
    }

    public void setAuthorizationRequests(List<Object> authorizationRequests) {
        this.authorizationRequests = authorizationRequests;
    }

    public List<Object> getCards() {
        return cards;
    }

    public void setCards(List<Object> cards) {
        this.cards = cards;
    }

    public List<Object> getConsumerLoyaltyPrograms() {
        return consumerLoyaltyPrograms;
    }

    public void setConsumerLoyaltyPrograms(List<Object> consumerLoyaltyPrograms) {
        this.consumerLoyaltyPrograms = consumerLoyaltyPrograms;
    }

    public List<Object> getDevices() {
        return devices;
    }

    public void setDevices(List<Object> devices) {
        this.devices = devices;
    }

    public Object getLocationConfiguration() {
        return locationConfiguration;
    }

    public void setLocationConfiguration(Object locationConfiguration) {
        this.locationConfiguration = locationConfiguration;
    }

    public List<Object> getLoyaltyProgramParticipants() {
        return loyaltyProgramParticipants;
    }

    public void setLoyaltyProgramParticipants(List<Object> loyaltyProgramParticipants) {
        this.loyaltyProgramParticipants = loyaltyProgramParticipants;
    }

    public List<Object> getOffers() {
        return offers;
    }

    public void setOffers(List<Object> offers) {
        this.offers = offers;
    }

    public List<EmployeeLocation__1> getEmployeeLocations() {
        return employeeLocations;
    }

    public void setEmployeeLocations(List<EmployeeLocation__1> employeeLocations) {
        this.employeeLocations = employeeLocations;
    }

    public Object getMerchant() {
        return merchant;
    }

    public void setMerchant(Object merchant) {
        this.merchant = merchant;
    }

    public List<Object> getOrders() {
        return orders;
    }

    public void setOrders(List<Object> orders) {
        this.orders = orders;
    }

    public List<Object> getProducts() {
        return products;
    }

    public void setProducts(List<Object> products) {
        this.products = products;
    }

    public List<Object> getConsumerRelationships() {
        return consumerRelationships;
    }

    public void setConsumerRelationships(List<Object> consumerRelationships) {
        this.consumerRelationships = consumerRelationships;
    }

    public List<Object> getDailySaleOverviews() {
        return dailySaleOverviews;
    }

    public void setDailySaleOverviews(List<Object> dailySaleOverviews) {
        this.dailySaleOverviews = dailySaleOverviews;
    }

    public List<Object> getSaleOverviews() {
        return saleOverviews;
    }

    public void setSaleOverviews(List<Object> saleOverviews) {
        this.saleOverviews = saleOverviews;
    }

    public List<Object> getDailyOfferOverviews() {
        return dailyOfferOverviews;
    }

    public void setDailyOfferOverviews(List<Object> dailyOfferOverviews) {
        this.dailyOfferOverviews = dailyOfferOverviews;
    }

    public List<Object> getOfferOverviews() {
        return offerOverviews;
    }

    public void setOfferOverviews(List<Object> offerOverviews) {
        this.offerOverviews = offerOverviews;
    }

    public Object getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(Object phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    public List<Object> getMessageThreads() {
        return messageThreads;
    }

    public void setMessageThreads(List<Object> messageThreads) {
        this.messageThreads = messageThreads;
    }

    public List<Object> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<Object> taxes) {
        this.taxes = taxes;
    }

    public List<Object> getTabletops() {
        return tabletops;
    }

    public void setTabletops(List<Object> tabletops) {
        this.tabletops = tabletops;
    }

    public Object getThirdPartyDevice() {
        return thirdPartyDevice;
    }

    public void setThirdPartyDevice(Object thirdPartyDevice) {
        this.thirdPartyDevice = thirdPartyDevice;
    }

    public Integer getEntityState() {
        return entityState;
    }

    public void setEntityState(Integer entityState) {
        this.entityState = entityState;
    }

}
