package com.smartstart.paymentTransaction.model.Employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Employee__1 {

    @SerializedName("$ref")
    @Expose
    private String $ref;

    public String get$ref() {
        return $ref;
    }

    public void set$ref(String $ref) {
        this.$ref = $ref;
    }

}
