package com.smartstart.paymentTransaction.model.Product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Product {

    @SerializedName("PageNumber")
    @Expose
    private Integer pageNumber;
    @SerializedName("DisplayCount")
    @Expose
    private Integer displayCount;
    @SerializedName("TotalRecordCount")
    @Expose
    private Integer totalRecordCount;
    @SerializedName("Collection")
    @Expose
    private List<Collection> collection = new ArrayList<Collection>();
    @SerializedName("ResponseType")
    @Expose
    private Integer responseType;
    @SerializedName("ResponseMessages")
    @Expose
    private List<Object> responseMessages = new ArrayList<Object>();
    @SerializedName("Errors")
    @Expose
    private Errors errors;

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getDisplayCount() {
        return displayCount;
    }

    public void setDisplayCount(Integer displayCount) {
        this.displayCount = displayCount;
    }

    public Integer getTotalRecordCount() {
        return totalRecordCount;
    }

    public void setTotalRecordCount(Integer totalRecordCount) {
        this.totalRecordCount = totalRecordCount;
    }

    public List<Collection> getCollection() {
        return collection;
    }

    public void setCollection(List<Collection> collection) {
        this.collection = collection;
    }

    public Integer getResponseType() {
        return responseType;
    }

    public void setResponseType(Integer responseType) {
        this.responseType = responseType;
    }

    public List<Object> getResponseMessages() {
        return responseMessages;
    }

    public void setResponseMessages(List<Object> responseMessages) {
        this.responseMessages = responseMessages;
    }

    public Errors getErrors() {
        return errors;
    }

    public void setErrors(Errors errors) {
        this.errors = errors;
    }

}
