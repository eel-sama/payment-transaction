package com.smartstart.paymentTransaction.constants;

//import com.enrollandpay.paxregister.Models.EditableTransactionItem;
//import com.enrollandpay.paymentapp.ConfigurationResponse;
import com.enrollandpay.serviceinterface.BusinessEntity.Category;
import com.enrollandpay.serviceinterface.BusinessEntity.Employee;
import com.enrollandpay.serviceinterface.BusinessEntity.Location;
import com.enrollandpay.serviceinterface.BusinessEntity.Product;
import com.enrollandpay.serviceinterface.BusinessEntity.Tabletop;
import com.enrollandpay.serviceinterface.Models.Security.Credentials;

import java.util.ArrayList;
import java.util.HashMap;

public class AppConfig {

    public static boolean ENABLE_MOCKUP = true;//- default
    public static boolean ENABLE_PA_INTENT_MOCK_UP = true; // default: false
    public static boolean PAYMENT_DECISION_CALL_V2 = false;
    public static boolean MOCK_DISCOUNT = false; // default: false
    public static boolean CLOVER_DEBUG_PAY = false; // true - skip clover processing
    public static boolean CLOVER_DEBUG_REFUND = false; // default false
    private static Boolean EnableGratuity = true;
    private static Double TipOption1 = .150;
    private static Double TipOption2 = .180;
    private static Double TipOption3 = .200;
    private static String LocationAddress = null;
    private static String LocationName = null;
    private static String LocationPhoneNumber = null;
    private static Double SurchargeCredit = 0.0;//1;
    private static Double SurchargeDebit = 0.0;//05;
    private static Boolean AllowCashDiscount = false;
    private static String EmployeeIdent = null;
    private static String EmployeeName = null;
    private static Boolean UseAuthProcess = true;
    private static Boolean IsDeviceAuthenticated = false;
    private static Credentials DeviceCredentials = null;
    private static Location Location = null;
    private static ArrayList<Employee> Employees = new ArrayList<>();
    private static ArrayList<Product> Products = new ArrayList<>();
    private static ArrayList<Tabletop> Tabletops = new ArrayList<>();
    private static Employee CurrentEmployee = null;

    private static HashMap<Integer, Category> categoryProducts = new HashMap<>();

    private static boolean enableJustReturn;

    public static Category getCurrentCategory() {
        return currentCategory;
    }

    public static void setCurrentCategory(Category currentCategory) {
        AppConfig.currentCategory = currentCategory;
    }

    private static Category currentCategory;

//    public static EditableTransactionItem getCurrentItemToAddToOrder() {
//        return currentItemToAddToOrder;
//    }
//
//    public static void setCurrentItemToAddToOrder(EditableTransactionItem currentItemToAddToOrder) {
//        AppConfig.currentItemToAddToOrder = currentItemToAddToOrder;
//    }
//
//    private static EditableTransactionItem currentItemToAddToOrder;

    // TODO - we need to bind this to a service call to pull this setting (.LocationDetail)
    private static boolean supportsOpenTab = true;

    public static ArrayList<Product> getProducts() {        return Products;    }
    public static void setProducts(ArrayList<Product> products) {        Products = products;    }
    public static HashMap<Integer, Category> getCategoryProducts() {
        return categoryProducts;
    }
    public static void setCategoryProducts(HashMap<Integer, Category> categoryProducts) {
        AppConfig.categoryProducts = categoryProducts;
    }
    public static com.enrollandpay.serviceinterface.BusinessEntity.Location getLocation() {        return Location;    }
    public static void setLocation(com.enrollandpay.serviceinterface.BusinessEntity.Location location) {        Location = location;    }
    public static ArrayList<Employee> getEmployees() {        return Employees;    }
    public static void setEmployees(ArrayList<Employee> employees) {        Employees = employees;    }
    public static Employee getCurrentEmployee() {        return CurrentEmployee;    }
    public static void setCurrentEmployee(Employee currentEmployee) {        CurrentEmployee = currentEmployee;    }
    public static ArrayList<Tabletop> getTabletops() {        return Tabletops;    }
    public static void setTabletops(ArrayList<Tabletop> tabletops) {        Tabletops = tabletops;    }

    public static boolean isSupportsOpenTab() {
        return supportsOpenTab;
    }
    public static void setSupportsOpenTab(boolean supportsOpenTab) {
        AppConfig.supportsOpenTab = supportsOpenTab;
    }

    public static Credentials getDeviceCredentials() {        return DeviceCredentials;    }
    public static void setDeviceCredentials(Credentials deviceCredentials) {        DeviceCredentials = deviceCredentials;    }
    public static Boolean getIsDeviceAuthenticated() {        return IsDeviceAuthenticated;    }
    public static void setIsDeviceAuthenticated(Boolean isDeviceAuthenticated) {        IsDeviceAuthenticated = isDeviceAuthenticated;    }
    public static Boolean getUseAuthProcess() {        return UseAuthProcess;    }
    public static void setUseAuthProcess(Boolean useAuthProcess) {        UseAuthProcess = useAuthProcess;    }
    public static Boolean getEnableGratuity() {  return EnableGratuity;    }
    public static void setEnableGratuity(Boolean enableGratuity) {        EnableGratuity = enableGratuity;    }
    public static Double getTipOption1() {        return TipOption1;    }
    public static void setTipOption1(Double tipOption1) {        TipOption1 = tipOption1;    }
    public static Double getTipOption2() {        return TipOption2;    }
    public static void setTipOption2(Double tipOption2) {        TipOption2 = tipOption2;    }
    public static Double getTipOption3() {        return TipOption3;    }
    public static void setTipOption3(Double tipOption3) {        TipOption3 = tipOption3;    }
    public static String getLocationAddress() {        return LocationAddress;    }
    public static void setLocationAddress(String locationAddress) {        LocationAddress = locationAddress;    }
    public static String getLocationName() {        return LocationName;    }
    public static void setLocationName(String locationName) {        LocationName = locationName;    }
    public static String getLocationPhoneNumber() {        return LocationPhoneNumber;    }
    public static void setLocationPhoneNumber(String locationPhoneNumber) {        LocationPhoneNumber = locationPhoneNumber;    }
    public static Double getSurchargeCredit() {        return SurchargeCredit;    }
    public static void setSurchargeCredit(Double surchargeCredit) {        SurchargeCredit = surchargeCredit;    }
    public static Double getSurchargeDebit() {        return SurchargeDebit;    }
    public static void setSurchargeDebit(Double surchargeDebit) {        SurchargeDebit = surchargeDebit;    }
    public static Boolean getAllowCashDiscount() {        return AllowCashDiscount;    }
    public static void setAllowCashDiscount(Boolean allowCashDiscount) {        AllowCashDiscount = allowCashDiscount;    }
    public static String getEmployeeIdent() {        return EmployeeIdent;    }
    public static void setEmployeeIdent(String employeeIdent) {        EmployeeIdent = employeeIdent;    }
    public static String getEmployeeName() {        return EmployeeName;    }
    public static void setEmployeeName(String employeeName) {        EmployeeName = employeeName;    }
//    public static void populate(final ConfigurationResponse configurationResponse){
//        if (configurationResponse == null){return;}
//        synchronized (AppConfig.class){
//            setEnableGratuity(configurationResponse.getIsTipEnabled());
//            Integer iter = 0;
//            if (configurationResponse.getTipOptionPercentages() != null && configurationResponse.getTipOptionPercentages().size() > 0){
//                iter = 0;
//                for (Double tipPercentage : configurationResponse.getTipOptionPercentages()){
//                    switch (iter){
//                        case 0:
//                            setTipOption1(tipPercentage);
//                            break;
//                        case 1:
//                            setTipOption2(tipPercentage);
//                            break;
//                        case 2:
//                            setTipOption3(tipPercentage);
//                            break;
//                    }
//                    iter++;
//                }
//            }
//        }
//    }
    // u003eEJM2T908TFAChnFE1PDCc5529
    static String CARD_CODE = "911431221212ALEX1122";
    public static String CARD_TOKEN = "u003eEJM2T" + CARD_CODE + "TFAChnFE1PDCc5529";

    public static String STORE_REF = "pax.register.store_ref";

    /**
     * temporary fix
     * @return
     */
    private static boolean promptSecondReceipt;
    public static boolean getEnablePromptSecondReceipt() {
        return promptSecondReceipt;
    }

    public static void setEnablePromptSecondReceipt(boolean value) {
        promptSecondReceipt = value;
    }

    public static void setEnableJustReturn(boolean value) {
        enableJustReturn = value;
    }

    public static boolean getEnableJustReturn() {
        return enableJustReturn;
    }
}