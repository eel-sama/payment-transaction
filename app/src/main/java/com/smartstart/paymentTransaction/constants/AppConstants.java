package com.smartstart.paymentTransaction.constants;

import com.enrollandpay.serviceinterface.Transaction.Request;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class AppConstants {

    public static String EXTRA_TRANSACTION_ID = "paxregister.extra.transaction.id";

    public static final String CREDIT  = "Credit";
    public static final String DEBIT  = "Debit";
    public static final String CASH  = "Cash";
    public static final String EBT  = "EBT";
    public static final String CANCEL  = "Cancel";

    public static final String KEY_OPEN_TAB = "OPENTAB";
    public static final String KEY_EXTDATA_HREF = "ExtData_HRef";
    public static final String KEY_AUTH_CODE = "AuthCode";
    public static final String KEY_REF_NUM = "RefNum";
    public static final String KEY_AMOUNT = "Amount";
    public static final String KEY_TIP_AMOUNT = "TipAmount";
    public static final String KEY_SURCHARGE_AMOUNT = "SurchargeAmount";
    public static final String KEY_TRANSACTION_ID = "TransactionId";
    public static final String KEY_TRANSACTION_TYPE = "TransactionType";
    public static final String KEY_REQUEST_AUTH_DENIED = "Authorization has been denied for this request.";
    public static final String KEY_ORDER_ID = "OrderId";
    public static final String KEY_JSON_COLLECTION = "Collection";
    public static final String KEY_ADDITIONAL_ID = "Additional_ID";

    public static final String TOS_URL = "https://www.enrollandpay.com/Member/TermsAndConditions";

    // Clover Specific
    public static final String KEY_CLOVER_INTENT_ORDER_ID = "com.clover.intent.extra.ORDER_ID";
    public static final String KEY_CLOVER_AUTO_LOGOUT = "com.clover.intent.extra.OBEY_AUTO_LOGOUT";
    public static final String KEY_CLOVER_INTENT_ORDER_MANAGE = "com.clover.intent.action.START_ORDER_MANAGE";

    // app wide default date format
    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

    // flavor definitions
    public static final String FLAVOR_PAY_ANYWHERE = "payanywhere";

    public static Set<Integer> RETURN_REQUEST_IDS = new HashSet<>(Arrays.asList(
            Request.RequestTypes.CashReturn.getValue(),
            Request.RequestTypes.CreditReturn.getValue()
    ));

    public static class EXTRA {

        public static String EXTRA_ORDER = "_application.id_order_data";
        public static String EXTRA_ORDER_PAYMENT_TYPE = "_application.id_order_data.payment_type";

        public static String EXTRA_CATEGORY_ID = "_application.id_order_data.product_category_id";
        public static String EXTRA_ADD_PRODUCT_TO_ORDER = "_application.id_add_item_to_order";
    }

}
