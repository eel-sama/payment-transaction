package com.smartstart.paymentTransaction;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.enrollandpay.serviceinterface.BusinessEntity.Category;
import com.enrollandpay.serviceinterface.BusinessEntity.Product;
import com.enrollandpay.serviceinterface.GlobalVariables;
import com.enrollandpay.serviceinterface.Service.EnPService;
import com.enrollandpay.serviceinterface.Service.ResponseCallback;
import com.enrollandpay.serviceinterface.Service.Utility;
import com.smartstart.paymentTransaction.adapter.ProductAdapter;
import com.smartstart.paymentTransaction.constants.AppConfig;
import com.smartstart.paymentTransaction.databinding.ActivityProductBinding;
import com.smartstart.paymentTransaction.listeners.OnRefreshSummaryListener;
import com.smartstart.paymentTransaction.model.Item;
import com.smartstart.paymentTransaction.utils.App;
import com.smartstart.paymentTransaction.utils.Session;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ProductActivity extends AppCompatActivity implements OnRefreshSummaryListener {

    private Category category;
    private ActivityProductBinding activityProductBinding;
    private EnPService enPService;
    private Boolean mServiceIsRunning = false;
    private List<Object> productAndCategoryList;
    private ProductAdapter productAdapter;
    Session session;
    List<Item> items;
    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            enPService = null;
            App.SetEnPService(null);
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            EnPService.EnPServiceBinder myBinder = (EnPService.EnPServiceBinder) service;
            enPService = myBinder.getService();
            App.SetEnPService(enPService);

            if (enPService.getCurrentCredentials() != null) {
                AppConfig.setDeviceCredentials(enPService.getCurrentCredentials());
                fetchProducts();
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        session = new Session(this);
        refreshSummary();
    }

    private void refreshSummary() {
        if (session.getProducts(getString(R.string.cart_key)) != null && session.getProducts(getString(R.string.cart_key)).size() > 0) {
            items = session.getProducts(getString(R.string.cart_key));
            DecimalFormat df2 = new DecimalFormat("#.##");
            double subtotal = 0, vat = 0,total = 0;
            for (Item item: items) {
                subtotal = subtotal + item.getTotal();
                vat = vat + (item.getTax().getPercentage() * item.getSaleAmount());
            }
            total = total + subtotal + vat;
            activityProductBinding.lastProductAdded.setText(items.stream().reduce((first, second) -> second).get().getName());
            activityProductBinding.itemsOnCart.setText(String.valueOf(items.size()));
            activityProductBinding.total.setText("$"+df2.format(total));
            activityProductBinding.getRoot().invalidate();
        } else {
            items = new ArrayList<>();
            activityProductBinding.lastProductAdded.setText("...");
            activityProductBinding.itemsOnCart.setText("0");
            activityProductBinding.total.setText("$0");
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityProductBinding = ActivityProductBinding.inflate(getLayoutInflater());
        setContentView(activityProductBinding.getRoot());
        getSupportActionBar().setTitle("Products");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        productAndCategoryList = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        activityProductBinding.productsRecyclerView.setLayoutManager(layoutManager);
        productAdapter = new ProductAdapter(productAndCategoryList, this);
        productAdapter.setOnRefreshSummaryListener(this);
        activityProductBinding.productsRecyclerView.setAdapter(productAdapter);

        Intent intent = this.getIntent();
        Bundle args = intent.getBundleExtra("container");
        category = (Category) args.getSerializable("category");


        Utility.saveJWT(this, "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA5LzA5L2lkZW50aXR5L2NsYWltcy9hY3RvciI6IkRldmljZSIsIkRldmljZUlkIjoiMTE1IiwiRGV2aWNlTmFtZSI6IlJ5YW4gRGV2aWNlIiwiQ2xpZW50SWQiOiIxIiwiQ2xpZW50QWNjZXNzSWQiOiJlOTE1YTI4MmVjNTc0MTliODVhM2FmYzBhNjBkYjhhNmM2NWIwNTA4ZmNhNDQzYzc5OTVlZmI0Y2Y3ZWM3ZTU2IiwiQ2xpZW50SWRlbnQiOiIxNDdlMzEzOTc2NTQ0MDAzODdmODBjOWRjN2NmZTdmMiIsIkNvYWxpdGlvbklkIjpbIiIsIiJdLCJMb2NhdGlvbklkIjoiMTMiLCJMb2NhdGlvbnMiOiJbXSIsIkxvY2F0aW9uVGltZVpvbmVUeXBlIjoiR01UTUlOVVMwODAwUEFDSUZJQ1RJTUUiLCJNZXJjaGFudElkIjoiMTkiLCJNZXJjaGFudHMiOiJbXSIsIkRldmljZVR5cGUiOiJQT1MsIFBheW1lbnRUZXJtaW5hbCIsIlBheW1lbnREZXZpY2VJZCI6IiIsIlJlbGF5RGV2aWNlSWQiOiIiLCJBUElWZXJzaW9uIjoiIiwibmJmIjoxNjEzMzczMDk0LCJleHAiOjE2MTM0NTk0OTQsImlzcyI6Imh0dHBzOi8vc2VydmljZS5lbnJvbGxhbmRwYXkuY29tIiwiYXVkIjoiMTQ3ZTMxMzk3NjU0NDAwMzg3ZjgwYzlkYzdjZmU3ZjIifQ.tXgzFAwfhl1_ncEMAEUoNsAiqe67mo0aUsRt5ZnCkLk");
        StartService();
        if (savedInstanceState != null) {
            SetMessage(savedInstanceState.getString(GlobalVariables.MESSAGE_TYPE), savedInstanceState.getString(GlobalVariables.MESSAGE_DATA));
        }

        App.ApplicationStarted = true;
    }

    private void filterProducts() {
        List<Object> prodContainsSearch = new ArrayList<>();
        if (productAndCategoryList.size() > 0) {
            prodContainsSearch = productAndCategoryList
                    .stream()
                    .filter(Product.class::isInstance)
                    .map(obj -> (Product) obj)
                    .filter(o -> o.CategoryProducts.stream().anyMatch(sub -> sub.getCategory().getCategoryId().equals(category.getCategoryId())))
                    .collect(Collectors.toList());
            productAndCategoryList = prodContainsSearch;
        }
    }

    private void SetMessage(String messageType, String messageData) {}

    private void fetchProducts() {
        enPService.LocationProducts(AppConfig.getDeviceCredentials().getLocationId(), new ResponseCallback() {
            @Override
            public void onSuccess(Object o) {
                productAndCategoryList = Stream.of(productAndCategoryList, (List<Object>) o).flatMap(x -> x.stream()).collect(Collectors.toList());
                filterProducts();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        productAdapter.setData(productAndCategoryList);
                    }
                });
            }

            @Override
            public void onFailed(String msg) {}
        });
    }

    private void StartService() {
        if (!mServiceIsRunning) {
            mServiceIsRunning = true;
            try {
                Intent intent = new Intent(this, EnPService.class);
                bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
                startService(intent);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void refreshSummary(View view) {
        this.refreshSummary();
    }
}
