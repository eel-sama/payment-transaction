package com.smartstart.paymentTransaction;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.enrollandpay.serviceinterface.GlobalVariables;

public class SendMessageReciever extends BroadcastReceiver {

    public SendMessageReciever() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent mainActivityIntent=new Intent(context, MainActivity.class);
        mainActivityIntent.putExtra(GlobalVariables.MESSAGE_DATA, intent.getStringExtra(GlobalVariables.MESSAGE_DATA));
        mainActivityIntent.putExtra(GlobalVariables.MESSAGE_TYPE, intent.getStringExtra(GlobalVariables.MESSAGE_TYPE));
        //mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(mainActivityIntent);
    }
}