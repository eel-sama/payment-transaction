package com.smartstart.paymentTransaction;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.enrollandpay.serviceinterface.BusinessEntity.Category;
import com.enrollandpay.serviceinterface.BusinessEntity.Product;
import com.enrollandpay.serviceinterface.GlobalVariables;
import com.enrollandpay.serviceinterface.Service.EnPService;
import com.enrollandpay.serviceinterface.Service.ResponseCallback;
import com.enrollandpay.serviceinterface.Service.Utility;
import com.smartstart.paymentTransaction.adapter.CategoryAdapter;
import com.smartstart.paymentTransaction.constants.AppConfig;
import com.smartstart.paymentTransaction.databinding.ActivityCategoryBinding;
import com.smartstart.paymentTransaction.listeners.OnRefreshSummaryListener;
import com.smartstart.paymentTransaction.model.Item;
import com.smartstart.paymentTransaction.utils.App;
import com.smartstart.paymentTransaction.utils.Session;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CategoryActivity extends AppCompatActivity implements OnRefreshSummaryListener {

    private static final String TAG = "Category Activity";
    private String LastMessageData;
    private String LastMessageType;
    private ActivityCategoryBinding activityCategoryBinding;
    private EnPService enPService;
    private Boolean mServiceIsRunning = false;
    private List<Object> categoryList;
    private List<Object> productAndCategoryList;
    private CategoryAdapter categoryAdapter;
    Session session;
    List<Item> items;
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            enPService = null;
            App.SetEnPService(null);
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            EnPService.EnPServiceBinder myBinder = (EnPService.EnPServiceBinder) service;
            enPService = myBinder.getService();
            App.SetEnPService(enPService);

            if (enPService.getCurrentCredentials() != null) {
                AppConfig.setDeviceCredentials(enPService.getCurrentCredentials());
                fetchCategories();
                fetchProducts();
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        session = new Session(this);
        this.refreshSummary();
    }

    private void refreshSummary() {
        if (session.getProducts(getString(R.string.cart_key)) != null && session.getProducts(getString(R.string.cart_key)).size() > 0) {
            items = session.getProducts(getString(R.string.cart_key));
            Log.e("dongskitest", String.valueOf(items.size()));
            Log.e("wew", String.valueOf(session.getProducts(getString(R.string.cart_key)) != null));
            DecimalFormat df2 = new DecimalFormat("#.##");
            double subtotal = 0, vat = 0,total = 0;
            for (Item item: items) {
                subtotal = subtotal + item.getTotal();
                vat = vat + (item.getTax().getPercentage() * item.getSaleAmount());
            }
            total = total + subtotal + vat;
            activityCategoryBinding.lastProductAdded.setText(items.stream().reduce((first, second) -> second).get().getName());
            activityCategoryBinding.itemsOnCart.setText(String.valueOf(items.size()));
            activityCategoryBinding.total.setText("$"+df2.format(total));
            activityCategoryBinding.getRoot().invalidate();
        } else {
            items = new ArrayList<>();
            activityCategoryBinding.lastProductAdded.setText("...");
            activityCategoryBinding.itemsOnCart.setText("0");
            activityCategoryBinding.total.setText("$0");
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityCategoryBinding = ActivityCategoryBinding.inflate(getLayoutInflater());
        setContentView(activityCategoryBinding.getRoot());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Categories");
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        activityCategoryBinding.categoryRecyclerView.setLayoutManager(layoutManager);
        categoryList = new ArrayList<>();
        productAndCategoryList = new ArrayList<>();
        categoryAdapter = new CategoryAdapter(categoryList, this);
        categoryAdapter.setOnRefreshSummaryListener(this);
        activityCategoryBinding.categoryRecyclerView.setAdapter(categoryAdapter);
        Utility.saveJWT(this, "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA5LzA5L2lkZW50aXR5L2NsYWltcy9hY3RvciI6IkRldmljZSIsIkRldmljZUlkIjoiMTE1IiwiRGV2aWNlTmFtZSI6IlJ5YW4gRGV2aWNlIiwiQ2xpZW50SWQiOiIxIiwiQ2xpZW50QWNjZXNzSWQiOiJlOTE1YTI4MmVjNTc0MTliODVhM2FmYzBhNjBkYjhhNmM2NWIwNTA4ZmNhNDQzYzc5OTVlZmI0Y2Y3ZWM3ZTU2IiwiQ2xpZW50SWRlbnQiOiIxNDdlMzEzOTc2NTQ0MDAzODdmODBjOWRjN2NmZTdmMiIsIkNvYWxpdGlvbklkIjpbIiIsIiJdLCJMb2NhdGlvbklkIjoiMTMiLCJMb2NhdGlvbnMiOiJbXSIsIkxvY2F0aW9uVGltZVpvbmVUeXBlIjoiR01UTUlOVVMwODAwUEFDSUZJQ1RJTUUiLCJNZXJjaGFudElkIjoiMTkiLCJNZXJjaGFudHMiOiJbXSIsIkRldmljZVR5cGUiOiJQT1MsIFBheW1lbnRUZXJtaW5hbCIsIlBheW1lbnREZXZpY2VJZCI6IiIsIlJlbGF5RGV2aWNlSWQiOiIiLCJBUElWZXJzaW9uIjoiIiwibmJmIjoxNjEzMzczMDk0LCJleHAiOjE2MTM0NTk0OTQsImlzcyI6Imh0dHBzOi8vc2VydmljZS5lbnJvbGxhbmRwYXkuY29tIiwiYXVkIjoiMTQ3ZTMxMzk3NjU0NDAwMzg3ZjgwYzlkYzdjZmU3ZjIifQ.tXgzFAwfhl1_ncEMAEUoNsAiqe67mo0aUsRt5ZnCkLk");
        StartService();
        if (savedInstanceState != null) {
            SetMessage(savedInstanceState.getString(GlobalVariables.MESSAGE_TYPE), savedInstanceState.getString(GlobalVariables.MESSAGE_DATA));
        }

        App.ApplicationStarted = true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_category, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.d(TAG, s);
                searchView.clearFocus();
                filterProducts(s);
                categoryAdapter.setData(productAndCategoryList);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
//                Log.d(TAG, s);
                return false;
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }

    private void filterProducts(String s) {
        List<Object> prodContainsSearch = new ArrayList<>();
        List<Object> catContainsSearch = new ArrayList<>();
        if (productAndCategoryList.size() > 0) {
            prodContainsSearch = productAndCategoryList
                                .stream()
                                .filter(Product.class::isInstance)
                                .map(obj -> (Product) obj)
                                .filter(o -> o.getName().contains(s))
                                .collect(Collectors.toList());
            catContainsSearch = productAndCategoryList
                                .stream()
                                .filter(Category.class::isInstance)
                                .map(obj -> (Category) obj)
                                .filter(o -> o.getName().contains(s))
                                .collect(Collectors.toList());
            productAndCategoryList = Stream
                                    .of(prodContainsSearch, catContainsSearch)
                                    .flatMap(x -> x.stream()).collect(Collectors.toList());
        }
    }

    private void SetMessage(String messageType, String messageData) {
        LastMessageData = messageData;
        LastMessageType = messageType;
        Log.e("list of Products", LastMessageData);
        Log.e("LastMessageType", LastMessageType);
    }

    private void fetchCategories() {
        enPService.LocationCategories(AppConfig.getDeviceCredentials().getLocationId(), new ResponseCallback() {
            @Override
            public void onSuccess(Object o) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        productAndCategoryList = ((List<Object>) o)
                                .stream()
                                .map(obj -> (Category) obj)
                                .filter(sub -> sub.getParent() == null)
                                .collect(Collectors.toList());
                        categoryAdapter.setData(productAndCategoryList);
                    }
                });
            }

            @Override
            public void onFailed(String msg) {
            }
        });
    }

    private void fetchProducts() {
        enPService.LocationProducts(AppConfig.getDeviceCredentials().getLocationId(), new ResponseCallback() {
            @Override
            public void onSuccess(Object o) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        productAndCategoryList = Stream.of(productAndCategoryList, (List<Object>) o)
                                .flatMap(x -> x.stream())
                                .collect(Collectors.toList());
                    }
                });
            }

            @Override
            public void onFailed(String msg) {
            }
        });
    }

    private void StartService() {
        if (!mServiceIsRunning) {
            mServiceIsRunning = true;
            try {
                Intent intent = new Intent(this, EnPService.class);
                bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
                startService(intent);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void refreshSummary(View view) {
        this.refreshSummary();
    }
}
