package com.smartstart.paymentTransaction.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smartstart.paymentTransaction.adapter.OrderAdapter;
import com.smartstart.paymentTransaction.databinding.FragmentHomeBinding;
import com.smartstart.paymentTransaction.model.Order.Collection;
import com.smartstart.paymentTransaction.model.Order.Order;
import com.smartstart.paymentTransaction.utils.Common;

import java.util.List;

public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;
    private OrderAdapter orderAdapter;
    private List<Collection> orderList;

    @Override
    public void onResume() {
        super.onResume();
//        orderList = new ArrayList<>();
//        orderAdapter = new OrderAdapter(orderList);
//        orderAdapter.notifyDataSetChanged();
//        binding.orderRecyclerView.setAdapter(orderAdapter);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.orderRecyclerView.setLayoutManager(layoutManager);

        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}