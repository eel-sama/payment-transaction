package com.smartstart.paymentTransaction;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.enrollandpay.serviceinterface.BusinessEntity.Entities;
import com.enrollandpay.serviceinterface.BusinessEntity.Order;
import com.enrollandpay.serviceinterface.BusinessEntity.OrderItem;
import com.enrollandpay.serviceinterface.Service.EnPService;
import com.enrollandpay.serviceinterface.Service.ResponseCallback;
import com.enrollandpay.serviceinterface.Service.Utility;
import com.smartstart.paymentTransaction.adapter.OrderAdapter;
import com.smartstart.paymentTransaction.constants.AppConfig;
import com.smartstart.paymentTransaction.databinding.ActivityResultSearchedOrderBinding;
import com.smartstart.paymentTransaction.utils.App;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ResultSearchedOrderActivity extends AppCompatActivity {
    private List<Order> orders;
    private OrderAdapter orderAdapter;
    ActivityResultSearchedOrderBinding activityResultSearchedOrderBinding;
    private EnPService enPService;
    private Boolean mServiceIsRunning = false;
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            enPService = null;
            App.SetEnPService(null);
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            EnPService.EnPServiceBinder myBinder = (EnPService.EnPServiceBinder) service;
            enPService = myBinder.getService();
            App.SetEnPService(enPService);

            if (enPService.getCurrentCredentials() != null) {
                AppConfig.setDeviceCredentials(enPService.getCurrentCredentials());
                fetchOrders();
            }
        }
    };
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityResultSearchedOrderBinding = ActivityResultSearchedOrderBinding.inflate(getLayoutInflater());
        setContentView(activityResultSearchedOrderBinding.getRoot());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Search Results");
        orders = new ArrayList<>();
        orderAdapter = new OrderAdapter(orders);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        activityResultSearchedOrderBinding.orderRecyclerView.setLayoutManager(layoutManager);
        activityResultSearchedOrderBinding.orderRecyclerView.setAdapter(orderAdapter);
        Utility.saveJWT(this, "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA5LzA5L2lkZW50aXR5L2NsYWltcy9hY3RvciI6IkRldmljZSIsIkRldmljZUlkIjoiMTE1IiwiRGV2aWNlTmFtZSI6IlJ5YW4gRGV2aWNlIiwiQ2xpZW50SWQiOiIxIiwiQ2xpZW50QWNjZXNzSWQiOiJlOTE1YTI4MmVjNTc0MTliODVhM2FmYzBhNjBkYjhhNmM2NWIwNTA4ZmNhNDQzYzc5OTVlZmI0Y2Y3ZWM3ZTU2IiwiQ2xpZW50SWRlbnQiOiIxNDdlMzEzOTc2NTQ0MDAzODdmODBjOWRjN2NmZTdmMiIsIkNvYWxpdGlvbklkIjpbIiIsIiJdLCJMb2NhdGlvbklkIjoiMTMiLCJMb2NhdGlvbnMiOiJbXSIsIkxvY2F0aW9uVGltZVpvbmVUeXBlIjoiR01UTUlOVVMwODAwUEFDSUZJQ1RJTUUiLCJNZXJjaGFudElkIjoiMTkiLCJNZXJjaGFudHMiOiJbXSIsIkRldmljZVR5cGUiOiJQT1MsIFBheW1lbnRUZXJtaW5hbCIsIlBheW1lbnREZXZpY2VJZCI6IiIsIlJlbGF5RGV2aWNlSWQiOiIiLCJBUElWZXJzaW9uIjoiIiwibmJmIjoxNjEzMzczMDk0LCJleHAiOjE2MTM0NTk0OTQsImlzcyI6Imh0dHBzOi8vc2VydmljZS5lbnJvbGxhbmRwYXkuY29tIiwiYXVkIjoiMTQ3ZTMxMzk3NjU0NDAwMzg3ZjgwYzlkYzdjZmU3ZjIifQ.tXgzFAwfhl1_ncEMAEUoNsAiqe67mo0aUsRt5ZnCkLk");
        StartService();
        if (savedInstanceState != null) {
//            SetMessage(savedInstanceState.getString(GlobalVariables.MESSAGE_TYPE), savedInstanceState.getString(GlobalVariables.MESSAGE_DATA));
        }

        App.ApplicationStarted = true;
    }

    private void StartService() {
        if (!mServiceIsRunning) {
            mServiceIsRunning = true;
            try {
                Intent intent = new Intent(this, EnPService.class);
                bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
                startService(intent);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void fetchOrders() {
        enPService.SearchOrders(null, null, null, null, null, null, null, null, 1, 25, null, null, null, new ResponseCallback() {
            @Override
            public void onSuccess(Object o) {
                try {
                    JSONObject jsonObject = (JSONObject) o;
                    orders = Entities.GetOrders(jsonObject.getJSONArray("Collection"));
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            orderAdapter.setData(orders);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailed(String msg) {}
        });
    }
}
