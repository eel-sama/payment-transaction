package com.smartstart.paymentTransaction.listeners;

import android.view.View;

public interface OnRefreshSummaryListener {
    public void refreshSummary(View view);
}
