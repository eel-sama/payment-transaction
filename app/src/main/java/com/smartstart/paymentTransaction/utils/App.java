package com.smartstart.paymentTransaction.utils;

import android.content.Context;
import android.util.Log;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.enrollandpay.serviceinterface.Models.LogEventModel;
import com.enrollandpay.serviceinterface.Models.Transaction.CardInformation;
import com.enrollandpay.serviceinterface.Service.EnPService;
import com.enrollandpay.serviceinterface.Transaction.Request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class App extends MultiDexApplication {
    public static final String TRANSACTION_DETAIL_SCREEN = "TransactionDetail";
    public static Boolean ApplicationStarted = false;
    public static String MessageType = null;
    public static String MessageData = null;
    static EnPService enPService;
    private static Context AppContext;
    private static final ConcurrentHashMap<String, Request> Requests = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, CardInformation> Cards = new ConcurrentHashMap<>();
    private static Boolean SupportsTokenization = null;
    private static final ConcurrentLinkedQueue<String> Logs = new ConcurrentLinkedQueue<>();
    private static Boolean EnabledFullLogReporting = false;
    private static Boolean EnableAdditionalId = false;
    private static Boolean Enable3rdPartyPOS = false;
    private static Boolean EnableRestaurantMode = false;
    private static Boolean EnableCashPayment = false;
    private static final List<Double> splitPaymentOptions = new ArrayList<>(Arrays.asList(0.15, 0.18, 0.20));

    public static void SetEnPService(EnPService service) {
        enPService = service;
    }

    public static EnPService GetEnpService() {
        return enPService;
    }

    public static Boolean getEnableCashPayment() {
        return true; // enabled to test with cash payment as option
//        return EnableCashPayment;
    }

    public static void setEnableCashPayment(Boolean enableCashPayment) {
        EnableCashPayment = enableCashPayment;
    }

    public static Context getAppContext() {
        return AppContext;
    }

    public static List<Double> GetSplitPaymentOptions() {
        return splitPaymentOptions;
    }

    public static ConcurrentHashMap<String, Request> getRequests() {
        return Requests;
    }

    public static ConcurrentHashMap<String, CardInformation> getCards() {
        return Cards;
    }

    public static Boolean getSupportsTokenization() {
        return SupportsTokenization;
    }

    public static void setSupportsTokenization(Boolean supportsTokenization) {
        SupportsTokenization = supportsTokenization;
    }

    public static Boolean getEnableAdditionalId() {
        return EnableAdditionalId;
    }

    public static void setEnableAdditionalId(Boolean enableAdditionalId) {
        EnableAdditionalId = enableAdditionalId;
    }

    public static Boolean getEnable3rdPartyPOS() {
        return Enable3rdPartyPOS;
    }

    public static void setEnable3rdPartyPOS(Boolean enable3rdPartyPOS) {
        Enable3rdPartyPOS = enable3rdPartyPOS;
    }

    public static Boolean getEnableRestaurantMode() {
        return EnableRestaurantMode;
    }

    public static void setEnableRestaurantMode(Boolean enableRestaurantMode) {
        EnableRestaurantMode = enableRestaurantMode;
    }

    public static Boolean getEnabledFullLogReporting() {
        return EnabledFullLogReporting;
    }

    public static void setEnabledFullLogReporting(Boolean enabledFullLogReporting) {
        EnabledFullLogReporting = enabledFullLogReporting;
    }

    public static void Log(String message) {
        if (Logs.size() >= 100) {
            Logs.remove(0);
        }
        Logs.add("(" + Calendar.getInstance().getTime().toString() + ") " + message);

        // log data persistence
//        AppLogs log = new AppLogs();
//        log.setLog("(" + Calendar.getInstance().getTime().toString() + ") " + message);
//        ObjectBox.GetLogs().put(log);
    }

    public static void Log(LogEventModel logEntry) {
        if (Logs.size() >= 100) {
            Logs.remove(0);
        }

        // log data persistence
//        AppLogs log = new AppLogs();
//        String requestUrl = !TextUtils.isEmpty(logEntry.getRequestUrl()) ? logEntry.getRequestUrl() : " -- ";
//        log.setLog("Request Log Entry >> " + requestUrl);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(logEntry.getRequestType() + " : ");
        stringBuilder.append(logEntry.getRequestUrl());
        stringBuilder.append("\n\n");
        stringBuilder.append("Request:\n");
        stringBuilder.append(logEntry.getRequestBody());
        stringBuilder.append("\n\n");
        stringBuilder.append("Response:\n");
        stringBuilder.append(logEntry.getResponseBody());
//        log.setLog(stringBuilder + "");
//
//        log.setRequestUrl(logEntry.getRequestUrl());
//
//        ObjectBox.GetLogs().put(log);

        Log.w("AppLog", "Request Log successfully added.");
    }

    /*    private static final android.os.Handler mHandler = new Handler();

        public static final void runOnUiThread(Runnable runnable) {
            if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
                runnable.run();
            } else {
                mHandler.post(runnable);
            }
        }
        public static void DisplayError(String message){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(AppContext);
                    alertDialog.setTitle(AppContext.getString(R.string.Error_Title));
                    alertDialog.setMessage(message);
                    alertDialog.setCancelable(true);
                    alertDialog.setPositiveButton(AppContext.getString(R.string.Ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
            });
        }*/
    public static String RetrieveLog() {
        StringBuilder stringBuilder = new StringBuilder();
        List<Object> lst = Arrays.asList(Logs.toArray());
        Collections.reverse(lst);
        for (Object s : lst) {
            stringBuilder.append(s + "\r\n");
        }
        return stringBuilder.toString();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // NOTE: enable this if we encounter TransactionTooLargeException or Bundle that
        // exceeds size limit (fragment persistence) when putting the app into background
//        TooLargeTool.startLogging(this);

        AppContext = getApplicationContext();
//        Helper.StartUp(AppContext);
//        Printer.Init(AppContext);
//        ObjectBox.init(this);

        LogCleanUp();

        // clear all persisted logs
        // ObjectBox.GetLogs().removeAll();
        // generated dummy logs
        //AddDummyLog();

        /////Load Preferences
        //SharedPreferences sharedPreferences = AppContext.getSharedPreferences(  GlobalVariables.APP_PREFS_NAME, Context.MODE_PRIVATE);
        //ConfigurationResponse configurationResponse = null;
        //if (sharedPreferences.contains(GlobalVariables.APP_PREFS_PAYMENTAPPLICATIONCONFIGURATION)){
        //    String storedConfig = sharedPreferences.getString(GlobalVariables.APP_PREFS_PAYMENTAPPLICATIONCONFIGURATION,null);
        //    if (storedConfig != null && storedConfig.length()>0){
        //        try {
        //           configurationResponse = new Gson().fromJson(storedConfig, ConfigurationResponse.class);
        //           AppConfig.populate(configurationResponse);
        //        }
        //        catch (Exception ex){

        //        }
        //    }
        //}
        //if (configurationResponse == null){
//        Configurations configurations = new Configurations(new Configurations.AsyncResponse() {
//            @Override
//            public void processFinish(ConfigurationResponse configurationResponse1) {
//                    SharedPreferences.Editor sharedPrefEditor = sharedPreferences.edit();
//                    sharedPrefEditor.putString(GlobalVariables.APP_PREFS_PAYMENTAPPLICATIONCONFIGURATION,new Gson().toJson(configurationResponse1));
//                    sharedPrefEditor.apply();
//                AppConfig.populate(configurationResponse1);
//            }
//        }, AppContext);
//        // configurations.execute();
//        configurations.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        //}
    }

    private void LogCleanUp() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -5);
        Date pastDate = calendar.getTime();

//        List<AppLogs> oldLogs = ObjectBox.GetLogs().query()
//                .less(AppLogs_.dateCreated, pastDate)
//                .build()
//                .find();
//
//        if(oldLogs.size() > 0) {
//            ObjectBox.GetLogs().remove(oldLogs);
//        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        super.attachBaseContext(IconicsContextWrapper.wrap(base));
        MultiDex.install(this);
    }
}