package com.smartstart.paymentTransaction.utils;

import com.google.gson.Gson;

public class Common {
    public static String ObjectToString(Object object) {
        return new Gson().toJson(object);
    }
}
