package com.smartstart.paymentTransaction.utils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static final String BASE_URL = "https://services.enrollandpay.com/";
    private static Retrofit retrofit;

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request newRequest = chain.request().newBuilder()
                            .addHeader("Authorization", "Bearer " + "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA5LzA5L2lkZW50aXR5L2NsYWltcy9hY3RvciI6IkRldmljZSIsIkRldmljZUlkIjoiMjAwIiwiRGV2aWNlTmFtZSI6IkV2YW4gVGVzdCIsIkNsaWVudElkIjoiMSIsIkNsaWVudEFjY2Vzc0lkIjoiZWY5NjUyY2FmYzkyNGUwMGE1M2NkOTVlOWE0OGZiOTdlY2I2OGRjZmYwY2Q0ZjE5YTNmN2Q3ZmI4N2YzNmVhMiIsIkNsaWVudElkZW50IjoiMTQ3ZTMxMzk3NjU0NDAwMzg3ZjgwYzlkYzdjZmU3ZjIiLCJDb2FsaXRpb25JZCI6WyIiLCIiXSwiTG9jYXRpb25JZCI6IjEzIiwiTG9jYXRpb25zIjoiW10iLCJMb2NhdGlvblRpbWVab25lVHlwZSI6IkdNVE1JTlVTMDgwMFBBQ0lGSUNUSU1FIiwiTWVyY2hhbnRJZCI6IjE5IiwiTWVyY2hhbnRzIjoiW10iLCJEZXZpY2VUeXBlIjoiUE9TLCBQYXltZW50VGVybWluYWwiLCJQYXltZW50RGV2aWNlSWQiOiIiLCJSZWxheURldmljZUlkIjoiIiwiQVBJVmVyc2lvbiI6IiIsIm5iZiI6MTYyODY4NTMyMiwiZXhwIjoxNjI4NzcxNzIyLCJpc3MiOiJodHRwczovL3NlcnZpY2UuZW5yb2xsYW5kcGF5LmNvbSIsImF1ZCI6IjE0N2UzMTM5NzY1NDQwMDM4N2Y4MGM5ZGM3Y2ZlN2YyIn0.4GxGXgY3w4jvjWPmR2pfFYicW1znBqzLFNau8Ize7lo")
                            .build();
                    return chain.proceed(newRequest);
                }
            }).build();
            retrofit = new retrofit2.Retrofit.Builder()
                    .client(client)
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
