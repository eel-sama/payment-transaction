package com.smartstart.paymentTransaction.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.smartstart.paymentTransaction.R;
import com.smartstart.paymentTransaction.model.Item;

import java.lang.reflect.Type;
import java.util.List;

public class Session {

    SharedPreferences sharedPreferences;

    public Session(Context context) {
        this.sharedPreferences = context.getSharedPreferences(context.getString(R.string.demo_key), Context.MODE_PRIVATE);
    }

    public void setCart(String key, List<Item> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        set(key, json);
    }

    public void set(String key, String value) {
        if (sharedPreferences != null) {
            SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
            prefsEditor.putString(key, value);
            prefsEditor.apply();
        }
    }

    public List<Item> getProducts(String key) {
        if (sharedPreferences != null) {

            Gson gson = new Gson();
            List<Item> data;

            String string = sharedPreferences.getString(key, null);
            Type type = new TypeToken<List<Item>>() {
            }.getType();
            data = gson.fromJson(string, type);
            return data;
        }
        return null;
    }

    public void putToken(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public void putId(String key, Integer value) {
        sharedPreferences.edit().putInt(key, value).apply();
    }

    public String getToken(String key) {
        return sharedPreferences.getString(key, null);
    }

    public Integer getId(String key) {
        return sharedPreferences.getInt(key, -1);
    }

    public void clearSession(String sessionId) {
        sharedPreferences.edit().remove(sessionId).apply();
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public void setSharedPreferences(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }
}
