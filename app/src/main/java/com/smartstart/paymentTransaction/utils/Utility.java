package com.smartstart.paymentTransaction.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.enrollandpay.serviceinterface.BusinessEntity.Order;
import com.enrollandpay.serviceinterface.BusinessEntity.OrderItem;
import com.enrollandpay.serviceinterface.BusinessEntity.OrderPayment;
import com.enrollandpay.serviceinterface.GlobalVariables;
import com.enrollandpay.serviceinterface.Transaction.Item;
import com.enrollandpay.serviceinterface.Transaction.Payment;
import com.enrollandpay.serviceinterface.Transaction.Request;
import com.enrollandpay.serviceinterface.Types;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class Utility {
    public static Date ParseDate(String val) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        if (val == null) {
            return null;
        }
        try {
            Date date = format.parse(val);
            return date;
        } catch (ParseException e) {
            Log.d(GlobalVariables.TAG, e.getStackTrace().toString());
        }
        return null;
    }

    public static Date ParseDate2(String val) {
        if (val == null) {
            return null;
        }
        List<String> formatStrings = Arrays.asList("M/d/y", "M-d-y");

        if (val.matches("^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$"
                + "|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$"
                + "|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$"
                + "|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$")) {
            formatStrings = Arrays.asList("yyyy-MM-dd", "yy-MM-dd");
        }

        for (String formatString : formatStrings) {
            try {
                return new SimpleDateFormat(formatString).parse(val);
            } catch (ParseException e) {
            }
        }
        return null;
    }

    public static String FormatPhoneNumber(Long val) {
        if (val == null) {
            return "";
        }
        return FormatPhoneNumber(val.toString());
    }

    public static String FormatPhoneNumber(String val) {
        if (val.startsWith("1") && val.length() == 11) {
            return "1 (" + val.substring(1, 4) + ") " + val.substring(4, 7) + "-" + val.substring(7);
        }
        return val;
    }

    public static Bitmap Base64StringToBitmap(String base64Str) throws IllegalArgumentException {
        byte[] decodedBytes = Base64.decode(
                base64Str.substring(base64Str.indexOf(",") + 1),
                Base64.DEFAULT
        );

        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public static String BitmapToString(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

    public static String longToCurrency(Long value) {
        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
        return n.format(value / 100.0);
    }

    public static double RoundToCent(Double value) {
        try {
            DecimalFormat df = new DecimalFormat("#.##");
            String taxFormatted = df.format(value);
            Double out = Double.parseDouble(taxFormatted);
            return out;
        } catch (Exception e) {
            return value; // return original value no changes
        }
    }

    public static String doubleToCurrency(Double value) {
        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.US);
        return n.format(value);
    }

    public static double longToPayableAmount(Long value) {
        return value / 100.0;
    }

    public static Request CreateRequestFromOrder(Order order) {
        Request newRequest = new Request();

        if (order != null && !TextUtils.isEmpty(order.getPosIdent())) {
            newRequest.setPOSIdent(order.getPosIdent());
        }

        newRequest.setOrderId(order.getOrderId());
        newRequest.setRequestAmount(order.getAmountTotal());
        newRequest.setTransactionAmount(order.getAmountTotal());
        newRequest.setTransactionDueAmount(order.getAmountTotal());
        newRequest.setTransactionTaxAmount(order.getAmountTax() == null ? 0.0 : order.getAmountTax());
        newRequest.setRequestTipAmount(0.0);
        newRequest.setPayments(new ArrayList<>());
        if (order.getOrderPayments() != null) {
            for (OrderPayment orderPayment : order.getOrderPayments()) {
                newRequest.getPayments().add(new Payment(orderPayment));
            }
        }

        int deviceID = com.enrollandpay.serviceinterface.Service.Utility.DeviceId(App.getAppContext());

        if (order.getPOSDeviceId() != null) {
            if (!order.getPOSDeviceId().equals(deviceID)) {
                newRequest.setMatchDevicePaymentId(false);
            }
        }

        return newRequest;
    }

    public static Request AddOrderDetails(Request request, Order orderDetail) {
        if (orderDetail.getOrderPayments() != null) {
            request.setPayments(new ArrayList<>());
            for (OrderPayment orderPayment : orderDetail.getOrderPayments()) {
                request.getPayments().add(new Payment(orderPayment));
            }
        }

        // lets get the existing discount.
        Double amountDiscount = orderDetail.getAmountReward() != null ? orderDetail.getAmountReward() :
                orderDetail.getAmountDiscount() != null ? orderDetail.getAmountDiscount() : 0.0;
        request.setTransactionDiscountAmount(amountDiscount);

//        Double tenderedAmount = orderDetail.getAmountTendered() != null ? orderDetail.getAmountTendered() : 0.0;
//        request.setTotalAmountTendered(tenderedAmount);

        if (orderDetail.getOrderItems() != null) {
            List<OrderItem> orderItems = orderDetail.getOrderItems();

            List<Item> items = new ArrayList<>();
//            List<EditableTransactionItem> editableTransactionItems = new ArrayList<>();
            List<String> addOnIds = new ArrayList<>();

            for (OrderItem orderItem : orderItems) {
                for (OrderItem addOnItem : orderItem.AddOns) {
                    addOnIds.add(addOnItem.getPosIdent());
                }
            }

            for (OrderItem orderItem : orderItems) {
                Item item = new Item();
                item.setQuantity(orderItem.getQuantity());
                item.setName(orderItem.getName());
                item.setProductId(orderItem.getProductId());
                item.setOrderItemId(orderItem.getOrderItemId());
                item.setPOSProductIdent(orderItem.getPosIdent());
                item.setTaxAmount(0.0);
                item.setTotalAmount(orderItem.getAmountTotal());
//                EditableTransactionItem editableTransactionItem = new EditableTransactionItem();
//                editableTransactionItem.setProductId(orderItem.getProductId());
//                editableTransactionItem.setOrderItemId(orderItem.getOrderItemId());
//                editableTransactionItem.setProductName(orderItem.getName());
//                editableTransactionItem.setAmountPrice(orderItem.getAmountTotal() / orderItem.getQuantity());
//                editableTransactionItem.setAmountTotal(orderItem.getAmountTotal());
//                editableTransactionItem.setQuantity(orderItem.getQuantity());
//                editableTransactionItem.setItem(item);
//                editableTransactionItem.setIsReturnMode(true);

//                ArrayList<EditableTransactionItem.EditableTransactionItemAddOn> orderAddOns = new ArrayList<>();
//                for(OrderItem addOn : orderItem.AddOns) {
//                    EditableTransactionItem.EditableTransactionItemAddOn addOnEditItem = editableTransactionItem.new EditableTransactionItemAddOn();
//                    Item addOnItem = new Item();
//                    addOnItem.setQuantity(addOn.getQuantity());
//                    addOnItem.setName(addOn.getName());
//                    addOnItem.setProductId(addOn.getProductId());
//                    addOnItem.setOrderItemId(addOn.getOrderItemId());
//                    addOnItem.setPOSProductIdent(addOn.getPosIdent());
//                    addOnItem.setTaxAmount(0.0);
//                    addOnItem.setTotalAmount(addOn.getAmountTotal());
//                    addOnEditItem.setIsSelected(true);
//                    addOnEditItem.setProductId(addOn.getProductId());
//                    addOnEditItem.setProductName(addOn.getName());
//                    addOnEditItem.setQuantity(addOn.getQuantity());
//                    addOnEditItem.setAmountPrice(addOn.getAmountTotal() / addOn.getQuantity());
//                    // addOnEditItem.setMaxReturnQuantity(addOn.getQuantity() - (addOn.getQuantityReturned() == null ? 0.0 : addOn.getQuantityReturned()));
//                    addOnEditItem.setAmountTotal(addOn.getAmountTotal());
//                    addOnEditItem.setItem(addOnItem);
//                    addOnEditItem.setIsSelected(true);
//                    addOnEditItem.setIsReturnMode(true);
//                    orderAddOns.add(addOnEditItem);
//                }
//                // set addon
//                editableTransactionItem.setAddOnOptions(orderAddOns);

                boolean isNotAnAddOnItem = false;
                for (String filteredId : addOnIds) {
                    if (orderItem.getPosIdent().equals(filteredId)) {
                        isNotAnAddOnItem = true;
                        break;
                    }
                }

//                if(!isNotAnAddOnItem) {
//                    // add item to transaction list
//                    editableTransactionItems.add(editableTransactionItem);
//                }

                item.setName(orderItem.getName());
                item.setProductId(orderItem.getProductId());
                item.setOrderItemId(orderItem.getOrderItemId());
                item.setTotalAmount(orderItem.getAmountTotal());
                item.setTaxAmount(orderItem.getAmountTax());

                item.setPOSItemIdent(orderItem.getPosIdent());

                items.add(item);
            }
            request.setItems(items);

            // TODO :: needs clarification
            request.setRequestType(Request.RequestTypes.CashSale);

            App.getRequests().put(request.getLookupId(), request);
        }

        return request;
    }

    public static boolean OrderIsOpenTab(List<Payment> payments) {
        for (Payment payment : payments) {
            if ((payment.getStatusType() & Types.PaymentStatusTypes.OpenTab.getValue()) == Types.PaymentStatusTypes.OpenTab.getValue())
                return true;
        }
        return false;
    }

    public static boolean HasReturnableOrder(Order order) {
        for (OrderItem item : order.getOrderItems()) {
            if (item.getStatusType() != null && item.getStatusType() == 2)
                return true;
        }
        return false;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();

        if (view == null) {
            view = new View(activity);
        }

        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}