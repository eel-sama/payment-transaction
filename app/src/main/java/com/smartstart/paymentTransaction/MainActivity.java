package com.smartstart.paymentTransaction;

import static com.enrollandpay.serviceinterface.GlobalVariables.Service_Intent_Message;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.enrollandpay.serviceinterface.BusinessEntity.Employee;
import com.enrollandpay.serviceinterface.Commands.CommandApplyDiscount;
import com.enrollandpay.serviceinterface.Commands.CommandCardPresented;
import com.enrollandpay.serviceinterface.Commands.CommandDiscountApplied;
import com.enrollandpay.serviceinterface.Commands.CommandDisplayDiscountSelect;
import com.enrollandpay.serviceinterface.Commands.CommandDisplayEnrollment;
import com.enrollandpay.serviceinterface.Commands.CommandDisplayQuestionModel;
import com.enrollandpay.serviceinterface.Commands.CommandDisplayRewardOfferSelect;
import com.enrollandpay.serviceinterface.Commands.CommandDisplayThankYou;
import com.enrollandpay.serviceinterface.Commands.CommandDisplayTransaction;
import com.enrollandpay.serviceinterface.Commands.CommandOrderCompleted;
import com.enrollandpay.serviceinterface.Commands.CommandPaymentComplete;
import com.enrollandpay.serviceinterface.Commands.CommandPaymentRequested;
import com.enrollandpay.serviceinterface.Commands.CommandRequestAuthorization;
import com.enrollandpay.serviceinterface.Commands.CommandRequestCard;
import com.enrollandpay.serviceinterface.Commands.CommandRequestPaymentAdjustment;
import com.enrollandpay.serviceinterface.GlobalVariables;
import com.enrollandpay.serviceinterface.Models.Security.Credentials;
import com.enrollandpay.serviceinterface.Service.EnPService;
import com.enrollandpay.serviceinterface.Service.ResponseCallback;
import com.enrollandpay.serviceinterface.Service.Utility;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.smartstart.paymentTransaction.constants.AppConfig;
import com.smartstart.paymentTransaction.databinding.ActivityMainBinding;
import com.smartstart.paymentTransaction.databinding.DialogSearchOrderBinding;
import com.smartstart.paymentTransaction.databinding.DialogSelectOptionBinding;
import com.smartstart.paymentTransaction.utils.App;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ResponseCallback{

    private String LastMessageData;
    private String LastMessageType;
    private ActivityMainBinding binding;
    private DialogSelectOptionBinding dialogSelectOptionBinding;
    private DialogSearchOrderBinding dialogSearchOrderBinding;
    private EnPService enPService;
    private Boolean mServiceIsRunning = false;
    List<Employee> employeeList;
    ArrayAdapter<Employee> employeeAdapter;
    private SendMessageReciever br;
    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            enPService = null;
            App.SetEnPService(null);
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            EnPService.EnPServiceBinder myBinder = (EnPService.EnPServiceBinder) service;
            enPService = myBinder.getService();
            App.SetEnPService(enPService);
            if (enPService.getCurrentCredentials() != null) {
                AppConfig.setDeviceCredentials(enPService.getCurrentCredentials());
                Log.e("wew", "mehehehe");
                fetchEmployees();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);
        binding.extendedFabShop.setOnClickListener(this);
        binding.extendedFabOpen.setOnClickListener(this);

        Utility.saveJWT(this, "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA5LzA5L2lkZW50aXR5L2NsYWltcy9hY3RvciI6IkRldmljZSIsIkRldmljZUlkIjoiMTE1IiwiRGV2aWNlTmFtZSI6IlJ5YW4gRGV2aWNlIiwiQ2xpZW50SWQiOiIxIiwiQ2xpZW50QWNjZXNzSWQiOiJlOTE1YTI4MmVjNTc0MTliODVhM2FmYzBhNjBkYjhhNmM2NWIwNTA4ZmNhNDQzYzc5OTVlZmI0Y2Y3ZWM3ZTU2IiwiQ2xpZW50SWRlbnQiOiIxNDdlMzEzOTc2NTQ0MDAzODdmODBjOWRjN2NmZTdmMiIsIkNvYWxpdGlvbklkIjpbIiIsIiJdLCJMb2NhdGlvbklkIjoiMTMiLCJMb2NhdGlvbnMiOiJbXSIsIkxvY2F0aW9uVGltZVpvbmVUeXBlIjoiR01UTUlOVVMwODAwUEFDSUZJQ1RJTUUiLCJNZXJjaGFudElkIjoiMTkiLCJNZXJjaGFudHMiOiJbXSIsIkRldmljZVR5cGUiOiJQT1MsIFBheW1lbnRUZXJtaW5hbCIsIlBheW1lbnREZXZpY2VJZCI6IiIsIlJlbGF5RGV2aWNlSWQiOiIiLCJBUElWZXJzaW9uIjoiIiwibmJmIjoxNjEzMzczMDk0LCJleHAiOjE2MTM0NTk0OTQsImlzcyI6Imh0dHBzOi8vc2VydmljZS5lbnJvbGxhbmRwYXkuY29tIiwiYXVkIjoiMTQ3ZTMxMzk3NjU0NDAwMzg3ZjgwYzlkYzdjZmU3ZjIifQ.tXgzFAwfhl1_ncEMAEUoNsAiqe67mo0aUsRt5ZnCkLk");
        StartService();
        registerBroadcastReceiver();
        if (savedInstanceState != null) {
            SetMessage(savedInstanceState.getString(GlobalVariables.MESSAGE_TYPE), savedInstanceState.getString(GlobalVariables.MESSAGE_DATA));
        } else if (savedInstanceState == null && App.ApplicationStarted && App.MessageType != null) {
            SetMessage(App.MessageType, App.MessageData);
            App.MessageData = null;
            App.MessageType = null;
        }
        App.ApplicationStarted = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterBroadcastReceiver();
    }

    private static Boolean requiresRestart = false;
    private void SetMessage(String messageType, String messageData) {
        LastMessageData = messageData;
        LastMessageType = messageType;
        switch (messageType) {
            case GlobalVariables.AUTHORIZATION_REQUEST:
//                setFragment(GetAuthorizeDevice(messageData), "AuthorizeDevice");
                requiresRestart = true;
                break;
            case GlobalVariables.AUTHORIZATION_REQUEST_APPROVED:
//                DisplayMain();
                break;
            case GlobalVariables.JWT_CHANGED:
                break;
            case GlobalVariables.AUTHORIZATION_ISAUTHORIZED:
                if (requiresRestart) {
                    requiresRestart = false;
                    System.exit(0);
                    return;
                }
                if (!AppConfig.getIsDeviceAuthenticated()) {
                    AppConfig.setIsDeviceAuthenticated(true);
                    try {
                        AppConfig.setDeviceCredentials(new Gson().fromJson(messageData, Credentials.class));
                        fetchEmployees();
                    } catch (Exception ex) {

                    }
                }
                break;
//            case GlobalVariables.DISPLAYENROLLMENT:
//                OnCommand(new Gson().fromJson(messageData, CommandDisplayEnrollment.class));
//                return;
//            case GlobalVariables.DISPLAYTRANSACTION:
//                OnCommand(new Gson().fromJson(messageData, CommandDisplayTransaction.class));
//                return;
//            case GlobalVariables.REQUESTCARD:
//                OnCommand(new Gson().fromJson(messageData, CommandRequestCard.class));
//                return;
//            case GlobalVariables.DISPLAYREWARDOFFERSELECT:
//                OnCommand(new Gson().fromJson(messageData, CommandDisplayRewardOfferSelect.class));
//                return;
//            case GlobalVariables.DISPLAYDISCOUNTSELECT:
//                OnCommand(new Gson().fromJson(messageData, CommandDisplayDiscountSelect.class));
//                return;
//            case GlobalVariables.DISPLAYQUESTION:
//                OnCommand(new Gson().fromJson(messageData, CommandDisplayQuestionModel.class));
//                return;
//            case GlobalVariables.DISPLAYTHANKYOU:
//                OnCommand(new Gson().fromJson(messageData, CommandDisplayThankYou.class));
//                return;
//            case GlobalVariables.REQUESTAUTHORIZATION:
//                OnCommand(new Gson().fromJson(messageData, CommandRequestAuthorization.class));
//                return;
//            case GlobalVariables.PAYMENTCOMPLETE:
//                OnCommand(new Gson().fromJson(messageData, CommandPaymentComplete.class));
//                return;
//            case GlobalVariables.APPLYDISCOUNT:
//                OnCommand(new Gson().fromJson(messageData, CommandApplyDiscount.class));
//                return;
//            case GlobalVariables.CARDPRESENTED:
//                OnCommand(new Gson().fromJson(messageData, CommandCardPresented.class));
//                return;
//            case GlobalVariables.PAYMENTREQUESTED:
//                OnCommand(new Gson().fromJson(messageData, CommandPaymentRequested.class));
//                return;
//            case GlobalVariables.DISCOUNTAPPLIED:
//                OnCommand(new Gson().fromJson(messageData, CommandDiscountApplied.class));
//                return;
//            case GlobalVariables.ORDERCOMPLETED:
//                OnCommand(new Gson().fromJson(messageData, CommandOrderCompleted.class));
//                return;
//            case GlobalVariables.PAYMENTADJUST:
//                OnCommand(new Gson().fromJson(messageData, CommandRequestPaymentAdjustment.class));
//                return;
        }
    }

    private void StartService() {
        if (!mServiceIsRunning) {
            mServiceIsRunning = true;
            try {
                Intent intent = new Intent(this, EnPService.class);
                bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
                startService(intent);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void fetchEmployees() {
        enPService.LocationEmployees(AppConfig.getDeviceCredentials().getLocationId(), this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.extended_fab_shop:
                dialogSelectOptionBinding = DialogSelectOptionBinding.inflate(LayoutInflater.from(MainActivity.this));
                Dialog dialog = new Dialog(MainActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(true);
                dialog.setContentView(dialogSelectOptionBinding.getRoot());
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialogSelectOptionBinding.autoCompleteEmployee.setAdapter(employeeAdapter);
                dialogSelectOptionBinding.autoCompleteEmployee.setThreshold(1);
                dialogSelectOptionBinding.btnByAmount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
                dialogSelectOptionBinding.btnByItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(view.getContext(), ShoppingActivity.class));
                    }
                });
                dialog.show();
                break;
            case R.id.extended_fab_open:
                dialogSearchOrderBinding = DialogSearchOrderBinding.inflate(LayoutInflater.from(MainActivity.this));
                final Dialog dialog1 = new Dialog(MainActivity.this);
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setCancelable(true);
                dialog1.setContentView(dialogSearchOrderBinding.getRoot());
                dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialogSearchOrderBinding.autoCompleteEmployee.setAdapter(employeeAdapter);
                dialogSearchOrderBinding.autoCompleteEmployee.setThreshold(1);
                dialogSearchOrderBinding.btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog1.dismiss();
                    }
                });
                dialogSearchOrderBinding.btnSearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(view.getContext(), ResultSearchedOrderActivity.class));
                    }
                });
                dialog1.show();
                break;
        }
    }

    @Override
    public void onSuccess(Object o) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                employeeList = (ArrayList<Employee>) o;
                employeeAdapter = new ArrayAdapter<>(MainActivity.this, R.layout.item_employee, employeeList);
            }
        });
    }

    @Override
    public void onFailed(String msg) {

    }

    private void registerBroadcastReceiver() {
        br = new SendMessageReciever();
        IntentFilter filter = new IntentFilter(Service_Intent_Message);
        registerReceiver(br, filter);
    }

    private void unRegisterBroadcastReceiver() {
        if (br != null) {
            unregisterReceiver(br);
        }
    }
}